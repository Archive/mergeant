/* query-fields-menu.c
 *
 * Copyright (C) 2004 - 2006 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "query-fields-menu.h"
#include "marshal.h"
#include <glib/gi18n-lib.h>

static void query_fields_menu_class_init (QueryFieldsMenuClass * class);
static void query_fields_menu_init (QueryFieldsMenu * wid);
static void query_fields_menu_dispose (GObject   * object);

/* signals */
enum
{
	FIELD_SELECTED,
	EXPR_SELECTED,
	LAST_SIGNAL
};

static gint query_fields_menu_signals[LAST_SIGNAL] = { 0, 0 };

struct _QueryFieldsMenuPriv
{
	GdaQuery      *query;
	
	GtkWidget    *menu;
	GHashTable   *hash_items; /* key = Query's GdaQueryTarget value = GtkMenuItem */
	GHashTable   *hash_signals; /* key = GdaEntity, value = signal id for entity_target_changed_cb() */
};

/* get a pointer to the parents to be able to call their destructor */
static GObjectClass *parent_class = NULL;

GType
query_fields_menu_get_type (void)
{
	static GType type = 0;

	if (!type) {
		static const GTypeInfo info = {
			sizeof (QueryFieldsMenuClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) query_fields_menu_class_init,
			NULL,
			NULL,
			sizeof (QueryFieldsMenu),
			0,
			(GInstanceInitFunc) query_fields_menu_init
		};		
		
		type = g_type_register_static (GTK_TYPE_BUTTON, "QueryFieldsMenu", &info, 0);
	}

	return type;
}

static void
query_fields_menu_class_init (QueryFieldsMenuClass * class)
{
	GObjectClass   *object_class = G_OBJECT_CLASS (class);
	parent_class = g_type_class_peek_parent (class);

	query_fields_menu_signals[FIELD_SELECTED] =
		g_signal_new ("field_selected",
			      G_TYPE_FROM_CLASS (object_class),
			      G_SIGNAL_RUN_FIRST,
			      G_STRUCT_OFFSET (QueryFieldsMenuClass, field_selected),
			      NULL, NULL,
			      marshal_VOID__POINTER_POINTER, G_TYPE_NONE,
			      2, G_TYPE_POINTER, G_TYPE_POINTER);
	query_fields_menu_signals[EXPR_SELECTED] =
		g_signal_new ("expr_selected",
			      G_TYPE_FROM_CLASS (object_class),
			      G_SIGNAL_RUN_FIRST,
			      G_STRUCT_OFFSET (QueryFieldsMenuClass, expr_selected),
			      NULL, NULL,
			      marshal_VOID__STRING, G_TYPE_NONE,
			      1, G_TYPE_STRING);
	class->field_selected = NULL;
	class->expr_selected = NULL;

	object_class->dispose = query_fields_menu_dispose;
}

static void
query_fields_menu_init (QueryFieldsMenu * wid)
{
	wid->priv = g_new0 (QueryFieldsMenuPriv, 1);
	wid->priv->query = NULL;
	wid->priv->menu = NULL;
	wid->priv->hash_items = NULL;
	wid->priv->hash_signals = NULL;
}

static gint any_event_cb (QueryFieldsMenu *fmenu, GdkEvent *event, gpointer data);
static void query_destroyed_cb (GdaQuery *query, QueryFieldsMenu *fmenu);
static void query_targets_changed_cb (GdaQuery *query, GdaQueryTarget *target, QueryFieldsMenu *fmenu);

/**
 * query_fields_menu_new
 * @query: a #GdaQuery object
 *
 * Creates a new #QueryFieldsMenu widget.
 *
 * Returns: the new widget
 */
GtkWidget *
query_fields_menu_new (GdaQuery *query)
{
	GObject    *obj;
	QueryFieldsMenu *fmenu;
	GtkWidget *wid;

	g_return_val_if_fail (query && GDA_IS_QUERY (query), NULL);
		
	obj = g_object_new (QUERY_FIELDS_MENU_TYPE, NULL);
	fmenu = QUERY_FIELDS_MENU (obj);

	wid = gtk_image_new_from_stock (GTK_STOCK_ADD, GTK_ICON_SIZE_BUTTON);
	gtk_widget_show (wid);
	gtk_container_add (GTK_CONTAINER (fmenu), wid);

	fmenu->priv->query = query;

	gda_object_connect_destroy (fmenu->priv->query,
				 G_CALLBACK (query_destroyed_cb), fmenu);
	g_signal_connect (G_OBJECT (fmenu->priv->query), "target_added",
			  G_CALLBACK (query_targets_changed_cb), fmenu);
	g_signal_connect (G_OBJECT (fmenu->priv->query), "target_removed",
			  G_CALLBACK (query_targets_changed_cb), fmenu);
	
	query_targets_changed_cb (fmenu->priv->query, NULL, fmenu);
	g_signal_connect (G_OBJECT (obj), "event",
			  G_CALLBACK (any_event_cb), NULL);

	return GTK_WIDGET (obj);
}

static void
query_destroyed_cb (GdaQuery *query, QueryFieldsMenu *fmenu)
{
	gtk_widget_destroy (GTK_WIDGET (fmenu));
}

static void
hash_foreach_disconnect (GdaEntity *ent, gulong *sigid, gpointer data)
{
	g_signal_handler_disconnect (G_OBJECT (ent), *sigid);
}

static void
query_fields_menu_dispose (GObject *object)
{
	QueryFieldsMenu *fmenu;

	g_return_if_fail (object != NULL);
	g_return_if_fail (IS_QUERY_FIELDS_MENU (object));
	fmenu = QUERY_FIELDS_MENU (object);

	if (fmenu->priv) {
		/* Weak unref the GdaQuery if necessary */
		if (fmenu->priv->query) {
			g_signal_handlers_disconnect_by_func (G_OBJECT (fmenu->priv->query),
							      G_CALLBACK (query_destroyed_cb), fmenu);
			g_signal_handlers_disconnect_by_func (G_OBJECT (fmenu->priv->query),
							      G_CALLBACK (query_targets_changed_cb), fmenu);

			if (fmenu->priv->hash_items)
				g_hash_table_destroy (fmenu->priv->hash_items);
			if (fmenu->priv->hash_signals) {
				g_hash_table_foreach (fmenu->priv->hash_signals, 
						      (GHFunc) hash_foreach_disconnect, NULL);
				g_hash_table_destroy (fmenu->priv->hash_signals);
			}
		}

		/* the private area itself */
		g_free (fmenu->priv);
		fmenu->priv = NULL;
	}

	/* for the parent class */
	parent_class->dispose (object);
}

/*
 * pop up a menu if button pressed
 */
static gint
any_event_cb (QueryFieldsMenu *fmenu, GdkEvent *event, gpointer data)
{
	if (event->type == GDK_BUTTON_PRESS) {
		GdkEventButton *bevent = (GdkEventButton *) event;
		gtk_menu_popup (GTK_MENU (fmenu->priv->menu), NULL, NULL, NULL, NULL,
				bevent->button, bevent->time);
		/* Tell calling code that we have handled this event; the buck
		 * stops here. */
		return TRUE;
	}
	
	/* Tell calling code that we have not handled this event; pass it on. */
	return FALSE;
}



static void update_target_mitems (GtkWidget *target_mitem, GdaEntity *ent, QueryFieldsMenu *fmenu);
static void entity_target_changed_cb (GdaEntity *ent, GtkWidget *target_mitem);

static void menu_item_mgfield_activate_cb (GtkMenuItem *mitem, QueryFieldsMenu *fmenu);
static void menu_item_allfields_activate_cb (GtkMenuItem *mitem, QueryFieldsMenu *fmenu);
static void menu_item_expr_activate_cb (GtkMenuItem *mitem, QueryFieldsMenu *fmenu);

/*
 * Creates or updates the top sub menus for each GdaQueryTarget
 */
static void
query_targets_changed_cb (GdaQuery *query, GdaQueryTarget *target, QueryFieldsMenu *fmenu)
{
	GtkWidget *target_mitem;
	gint target_pos = 0;
	GSList *targets, *tlist;
	GSList *updated_items = NULL;
	GList *mitems;

	GHashTable *hash, *sigs;
	GtkMenu *menu;

	/* build hash table and menu if not yet done */
	if (! fmenu->priv->menu)
		fmenu->priv->menu = gtk_menu_new ();
	if (! fmenu->priv->hash_items)
		fmenu->priv->hash_items = g_hash_table_new (NULL, NULL);
	if (! fmenu->priv->hash_signals)
		fmenu->priv->hash_signals = g_hash_table_new_full (NULL, NULL, NULL, g_free);

	menu = GTK_MENU (fmenu->priv->menu);
	hash = fmenu->priv->hash_items;
	sigs = fmenu->priv->hash_signals;

	/* update the menu structure */
	targets = gda_query_get_targets (query);
	tlist = targets;
	while (tlist) {
		GdaQueryTarget *target = GDA_QUERY_TARGET (tlist->data);
		GdaEntity *ent = gda_query_target_get_represented_entity (GDA_QUERY_TARGET (tlist->data));

		/* obtain or create target_item and set target_pos for the next created menu item */
		target_mitem = g_hash_table_lookup (hash, target);
		if (!target_mitem) {
			gchar *str = gda_query_target_get_complete_name (target);
			gulong *sigid;

			target_mitem = gtk_menu_item_new_with_label (str);
			g_free (str);
			gtk_menu_shell_insert (GTK_MENU_SHELL (menu), target_mitem, target_pos++);
			gtk_widget_show (target_mitem);
			
			g_hash_table_insert (hash, target, target_mitem);

			sigid = g_new (gulong, 1);
			*sigid = g_signal_connect (G_OBJECT (ent), "changed", 
						   G_CALLBACK (entity_target_changed_cb), target_mitem);
			g_hash_table_insert (sigs, ent, sigid);

			g_object_set_data (G_OBJECT (target_mitem), "ent", ent);
			g_object_set_data (G_OBJECT (target_mitem), "target", target);
			g_object_set_data (G_OBJECT (target_mitem), "fmenu", fmenu);
		}
		else 
			target_pos = g_list_index (GTK_MENU_SHELL (menu)->children, target_mitem) + 1;
		updated_items = g_slist_prepend (updated_items, target_mitem);
		
		/* browse through all the entity's fields */
		update_target_mitems (target_mitem, ent, fmenu);

		tlist = g_slist_next (tlist);
	}
	g_slist_free (targets);

	/* remove items which need to */
	mitems = GTK_MENU_SHELL (menu)->children;
	if (mitems) {
		GSList *to_del = NULL, *list;

		while (mitems) {
			if (! g_slist_find (updated_items, mitems->data))
				to_del = g_slist_prepend (to_del, mitems->data);
			mitems = g_list_next (mitems);
		}
		if (to_del) {
			list = to_del;
			while (list) {
				GObject *ent = g_object_get_data (G_OBJECT (list->data), "ent");
				gulong *sigid;

				if (ent) {
					sigid = g_hash_table_lookup (sigs, ent);
					g_assert (sigid);
					g_signal_handler_disconnect (ent, *sigid);
					g_hash_table_remove (sigs, ent);
				}

				gtk_widget_destroy (GTK_WIDGET (list->data));
				list = g_slist_next (list);
			}
			g_slist_free (to_del);
		}
		
		if (updated_items) 
			g_slist_free (updated_items);
	}

	/* add a menu item for field creation from a textual expression */
	target_mitem = gtk_menu_item_new_with_label (_("From expression..."));
	gtk_menu_shell_append (GTK_MENU_SHELL (menu), target_mitem);
	gtk_widget_show (target_mitem);
	g_signal_connect (G_OBJECT (target_mitem), "activate",
			  G_CALLBACK (menu_item_expr_activate_cb), fmenu);
}


/*
 * Creates or updates the list of selectable fields in the @target_mitem's contained sub-menu
 */
static void
update_target_mitems (GtkWidget *target_mitem, GdaEntity *ent, QueryFieldsMenu *fmenu)
{
	GtkWidget *submenu = NULL;
	GList *mitems;
	GSList *fields, *list;
	GtkWidget *field_mitem;
	GdaQueryTarget *target;

	target = g_object_get_data (G_OBJECT (target_mitem), "target");

	submenu = gtk_menu_item_get_submenu (GTK_MENU_ITEM (target_mitem));
	if (submenu)
		mitems = GTK_MENU_SHELL (submenu)->children;
	else
		mitems = NULL;

	fields = gda_entity_get_fields (ent);
	list = fields;
	while (list) {
		if (mitems) {
			GtkWidget *alabel;
			field_mitem = GTK_WIDGET (mitems->data);
			alabel = gtk_bin_get_child (GTK_BIN (field_mitem));
			gtk_label_set_text (GTK_LABEL (alabel),
					    gda_object_get_name (GDA_OBJECT (list->data)));
		}
		else {
			if (!submenu) {
				submenu = gtk_menu_new ();
				gtk_menu_item_set_submenu (GTK_MENU_ITEM (target_mitem), submenu);
			}
			field_mitem = gtk_menu_item_new_with_label (gda_object_get_name 
								    (GDA_OBJECT (list->data)));
			gtk_menu_shell_append (GTK_MENU_SHELL (submenu), field_mitem);
			gtk_widget_show (field_mitem);
			g_signal_connect (G_OBJECT (field_mitem), "activate",
					  G_CALLBACK (menu_item_mgfield_activate_cb), fmenu);
		}

		g_object_set_data (G_OBJECT (field_mitem), "GdaEntityField", list->data);
		g_object_set_data (G_OBJECT (field_mitem), "target", target);

		list = g_slist_next (list);
		if (mitems)
			mitems = g_list_next (mitems);
	}
	g_slist_free (fields);

	/* remove the remaining menu items, if they exist */
	if (mitems) {
		GList *tmp = g_list_copy (mitems);
		GList *dlist = tmp;
		while (dlist) {
			gtk_widget_destroy (GTK_WIDGET (dlist->data));
			dlist = g_list_next (dlist);
		}
		g_list_free (tmp);
	}

	if (submenu) {
		/* add a menu item for "target.*" field creation */
		field_mitem = gtk_menu_item_new_with_label (_("*"));
		gtk_menu_shell_append (GTK_MENU_SHELL (submenu), field_mitem);
		gtk_widget_show (field_mitem);
		g_signal_connect (G_OBJECT (field_mitem), "activate",
				  G_CALLBACK (menu_item_allfields_activate_cb), fmenu);
		g_object_set_data (G_OBJECT (field_mitem), "target", target);
	}
}

/*
 * Callback when target's represented entity has changed
 */
static void
entity_target_changed_cb (GdaEntity *ent, GtkWidget *target_mitem)
{
	QueryFieldsMenu *fmenu;

	fmenu = g_object_get_data (G_OBJECT (target_mitem), "fmenu");
	update_target_mitems (target_mitem, ent, fmenu);
}

static void
menu_item_mgfield_activate_cb (GtkMenuItem *mitem, QueryFieldsMenu *fmenu)
{
	GdaEntityField *field;
	GdaQueryField *newfield;
	GdaQueryTarget *target;

	field = g_object_get_data (G_OBJECT (mitem), "GdaEntityField");
	g_assert (field && GDA_IS_ENTITY_FIELD (field));
	target = g_object_get_data (G_OBJECT (mitem), "target");
	g_assert (target && GDA_IS_QUERY_TARGET (target));

	newfield = (GdaQueryField *) g_object_new (GDA_TYPE_QUERY_FIELD_FIELD, 
						   "dict", gda_object_get_dict ((GdaObject*) fmenu->priv->query),
						   "query", fmenu->priv->query,
						   "target", target,
						   "field", field, NULL);
	gda_entity_add_field (GDA_ENTITY (fmenu->priv->query), GDA_ENTITY_FIELD (newfield));
	g_object_unref (newfield);
}

static void
menu_item_allfields_activate_cb (GtkMenuItem *mitem, QueryFieldsMenu *fmenu)
{
	GdaQueryField *newfield;
	GdaQueryTarget *target;

	target = g_object_get_data (G_OBJECT (mitem), "target");
	g_assert (target && GDA_IS_QUERY_TARGET (target));

	newfield = (GdaQueryField *) g_object_new (GDA_TYPE_QUERY_FIELD_ALL, 
						   "dict", gda_object_get_dict ((GdaObject*) fmenu->priv->query),
						   "query", fmenu->priv->query,
						   "target", target, NULL);
	gda_entity_add_field (GDA_ENTITY (fmenu->priv->query), GDA_ENTITY_FIELD (newfield));
	g_object_unref (newfield);
}

static void
menu_item_expr_activate_cb (GtkMenuItem *mitem, QueryFieldsMenu *fmenu)
{
	GtkWidget *dlg;
	GtkWidget *label, *hbox, *vbox, *wid;
	gchar *str;
	gint result;

	dlg = gtk_dialog_new_with_buttons (_("Add a query field from an expression"),
					   GTK_WINDOW (gtk_widget_get_toplevel (GTK_WIDGET (fmenu))),
					   GTK_DIALOG_MODAL,
					   GTK_STOCK_OK,
					   GTK_RESPONSE_OK,
					   GTK_STOCK_CANCEL,
					   GTK_RESPONSE_CANCEL,
					   NULL);
	vbox = GTK_DIALOG (dlg)->vbox;
	
	label = gtk_label_new (NULL);
	str = g_strdup_printf ("<b>%s</b>\n<small>%s</small>", _("Textual expression:"),
			       _("The following expression will be analysed to create\n"
				 "a new field in the query"));
	gtk_label_set_markup (GTK_LABEL (label), str);
        g_free (str);
	gtk_box_pack_start (GTK_BOX (vbox), label, FALSE, FALSE, 0);
        gtk_widget_show (label);
        gtk_misc_set_alignment (GTK_MISC (label), 0., -1);

	hbox = gtk_hbox_new (FALSE, 0); /* HIG */
        gtk_box_pack_start (GTK_BOX (vbox), hbox, FALSE, FALSE, 0);
        gtk_widget_show (hbox);
        label = gtk_label_new ("    ");
        gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);
        gtk_widget_show (label);

	wid = gtk_entry_new ();
	gtk_box_pack_start (GTK_BOX (hbox), wid, TRUE, TRUE, 0);
	gtk_widget_show (wid);

	do {
		result = gtk_dialog_run (GTK_DIALOG (dlg));
		if (result == GTK_RESPONSE_OK) {
			GError *error = NULL;
			const gchar *cstr = gtk_entry_get_text (GTK_ENTRY (wid));
			
			if (! gda_query_add_field_from_sql (fmenu->priv->query, cstr, &error)) {
				GtkWidget *errdlg;
				gchar *msg;
				
				if (error) {
					msg = g_strdup_printf ("<b>%s</b>\n%s '%s':\n\n%s",
							       _("Error parsing/analysing field expression:"),
							       _("while parsing"),
							       cstr,
							       error->message);
					
					g_error_free (error);
				}
				else
					msg = g_strdup_printf ("<b>%s</b>\n%s '%s'",
							       _("Error parsing/analysing field expression:"),
							       _("while parsing"),
							       cstr);
				
				errdlg = gtk_message_dialog_new_with_markup (NULL, GTK_DIALOG_MODAL, GTK_MESSAGE_ERROR,
									     GTK_BUTTONS_CLOSE, msg);
				g_free (msg);
				gtk_dialog_run (GTK_DIALOG (errdlg));
				gtk_widget_destroy (errdlg);
			}
			else
				result = GTK_RESPONSE_CANCEL;
		}
	}
	while (result != GTK_RESPONSE_CANCEL);

	gtk_widget_destroy (dlg);
}
