/* mg-plugin-editor.c
 *
 * Copyright (C) 2006 - 2008 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <string.h>
#include <glib/gi18n-lib.h>
#include <libgda/libgda.h>
#include "mg-plugin-editor.h"
#include <libgnomedb/gnome-db-data-widget.h>
#include <libgnomedb/gnome-db-raw-grid.h>
#include <libgnomedb/gnome-db-raw-form.h>
#include <libgnomedb/gnome-db-data-widget-info.h>
#include <libgnomedb/gnome-db-plugin.h>
#include <libgnomedb/gnome-db-tools.h>

static void mg_plugin_editor_class_init (MgPluginEditorClass * class);
static void mg_plugin_editor_init (MgPluginEditor *wid);

static void mg_plugin_editor_set_property (GObject *object,
					   guint param_id,
					   const GValue *value,
					   GParamSpec *pspec);
static void mg_plugin_editor_get_property (GObject *object,
					   guint param_id,
					   GValue *value,
					   GParamSpec *pspec);
static void mg_plugin_editor_dispose      (GObject *object);

struct _MgPluginEditorPriv
{
	GdaConnection *cnc;
	GObject       *work_object;
	GtkWidget     *table;
	GtkWidget     *plugins_combo;
	GtkWidget     *options;
	GtkWidget     *sample_entry;

	gboolean       setting_up;
};

enum
{
	COLUMN_POINTER,
	COLUMN_STRING,
	NUM_COLUMNS
};

/* get a pointer to the parents to be able to call their destructor */
static GObjectClass *parent_class = NULL;

extern GHashTable *gnome_db_plugins_hash;

/* properties */
enum
{
	PROP_0,
	PROP_WORK_OBJECT,
	PROP_CNC
};

GType
mg_plugin_editor_get_type (void)
{
	static GType type = 0;

	if (!type) {
		static const GTypeInfo info = {
			sizeof (MgPluginEditorClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) mg_plugin_editor_class_init,
			NULL,
			NULL,
			sizeof (MgPluginEditor),
			0,
			(GInstanceInitFunc) mg_plugin_editor_init
		};		

		type = g_type_register_static (GTK_TYPE_VBOX, "MgPluginEditor", &info, 0);
	}

	return type;
}

static void
mg_plugin_editor_class_init (MgPluginEditorClass *class)
{
	GObjectClass   *object_class = G_OBJECT_CLASS (class);
	
	parent_class = g_type_class_peek_parent (class);

	/* Properties */
        object_class->set_property = mg_plugin_editor_set_property;
        object_class->get_property = mg_plugin_editor_get_property;
	object_class->dispose = mg_plugin_editor_dispose;

	g_object_class_install_property (object_class, PROP_CNC,
                                         g_param_spec_object ("cnc", NULL, NULL, 
							      GDA_TYPE_CONNECTION,
							      G_PARAM_CONSTRUCT | G_PARAM_READABLE | G_PARAM_WRITABLE));
	g_object_class_install_property (object_class, PROP_WORK_OBJECT,
                                         g_param_spec_object ("work_object", NULL, NULL, 
							      G_TYPE_OBJECT,
							      G_PARAM_CONSTRUCT | G_PARAM_READABLE | G_PARAM_WRITABLE));
}

static void
mg_plugin_editor_init (MgPluginEditor *editor)
{
	GtkWidget *label;
	
	editor->priv = g_new0 (MgPluginEditorPriv, 1);

	editor->priv->cnc = NULL;
	editor->priv->work_object = NULL;
	editor->priv->plugins_combo = gtk_combo_box_new ();
	editor->priv->options = NULL;
	editor->priv->sample_entry = NULL;
	editor->priv->table = gtk_table_new (3, 2, FALSE);

	gtk_box_pack_start (GTK_BOX (editor), editor->priv->table, TRUE, TRUE, 0);
	gtk_table_set_row_spacings (GTK_TABLE (editor->priv->table), 5);
	gtk_table_set_col_spacings (GTK_TABLE (editor->priv->table), 5);
	gtk_widget_show (editor->priv->table);

	gtk_table_attach (GTK_TABLE (editor->priv->table), editor->priv->plugins_combo, 
			  1, 2, 0, 1, GTK_EXPAND | GTK_FILL, 0, 0, 0);
	gtk_widget_show (editor->priv->plugins_combo);

	label = gtk_label_new (_("Data entry type:"));
	gtk_misc_set_alignment (GTK_MISC (label), 0., -1);
	gtk_table_attach (GTK_TABLE (editor->priv->table), label, 0, 1, 0, 1, GTK_FILL, 0, 0, 0);
	gtk_widget_show (label);
	
	label = gtk_label_new (_("Options:"));
	gtk_misc_set_alignment (GTK_MISC (label), 0., 0.);
	gtk_table_attach (GTK_TABLE (editor->priv->table), label, 0, 1, 1, 2, GTK_FILL, GTK_FILL, 0, 0);
	gtk_widget_show (label);

	label = gtk_label_new (_("Preview:"));
	gtk_misc_set_alignment (GTK_MISC (label), 0., -1);
	gtk_table_attach (GTK_TABLE (editor->priv->table), label, 0, 1, 2, 3, GTK_FILL, 0, 0, 0);
	gtk_widget_show (label);
}

static void
mg_plugin_editor_dispose (GObject *object)
{
        MgPluginEditor *editor = (MgPluginEditor *) object;

        g_return_if_fail (MG_IS_PLUGIN_EDITOR (editor));

        /* free objects references */
	if (editor->priv) {
		if (editor->priv->cnc)
			g_object_unref (editor->priv->cnc);
		mg_plugin_editor_set_work_object (editor, NULL);
		g_free (editor->priv);
		editor->priv = NULL;
	}

        /* chain to parent class */
        parent_class->dispose (object);
}
/**
 * mg_plugin_editor_new
 * @cnc: a #GdaConnection
 *
 * Creates a new #MgPluginEditor widget
 *
 *  Returns: the new widget
 */
GtkWidget *
mg_plugin_editor_new (GdaConnection *cnc)
{
	g_return_val_if_fail (GDA_IS_CONNECTION (cnc), NULL);

	return (GtkWidget*) g_object_new (MG_TYPE_PLUGIN_EDITOR, "cnc", cnc, NULL);
}


static void
mg_plugin_editor_set_property (GObject *object,
			       guint param_id,
			       const GValue *value,
			       GParamSpec *pspec)
{
	MgPluginEditor *editor;
	GdaConnection *cnc;
	editor = MG_PLUGIN_EDITOR (object);
	
	switch (param_id) {
	case PROP_WORK_OBJECT:
		mg_plugin_editor_set_work_object (editor, g_value_get_object (value));
		break;
	case PROP_CNC:
		cnc = g_value_get_object (value);
		if (editor->priv->cnc == cnc)
			break;
		if (editor->priv->cnc)
			g_object_unref (editor->priv->cnc);
		editor->priv->cnc = cnc;
		if (editor->priv->cnc)
			g_object_ref (editor->priv->cnc);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, param_id, pspec);
		break;
	}
}

static void
mg_plugin_editor_get_property (GObject *object,
			       guint param_id,
			       GValue *value,
			       GParamSpec *pspec)
{
	MgPluginEditor *editor;

	editor = MG_PLUGIN_EDITOR (object);
	
	switch (param_id) {
	case PROP_WORK_OBJECT:
		g_value_set_object (value, editor->priv->work_object);
		break;
	case PROP_CNC:
		g_value_set_object (value, editor->priv->cnc);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, param_id, pspec);
		break;
	}	
}

static gboolean  work_object_type_known (GObject *obj);
static GType     get_work_obj_type (GObject *obj);
gchar           *get_work_obj_plugin (GObject *obj);
void             set_work_obj_plugin (GObject *obj, const gchar *plugin_str);

static gint  find_plugin_index (GtkComboBox *combo, const gchar *plugin);
static void  pack_options (MgPluginEditor *editor, GtkWidget *options);
static void  pack_sample_entry (MgPluginEditor *editor, GtkWidget *options);
static void  update_sample_data_entry (MgPluginEditor *editor, const gchar *plugin_str);
static void  compute_new_model (GtkTreeModel *model, GObject *obj);
static void  plugins_combo_changed_cb (GtkComboBox *combo, MgPluginEditor *editor);
static void  options_form_param_changed_cb (GnomeDbBasicForm *form, GdaHolder *param, 
					    gboolean is_user_modif, MgPluginEditor *editor);
static void  options_quarks_foreach_func (const gchar *key, const gchar *value, GdaSet *plist);
static void  work_object_set_plugin (MgPluginEditor *editor);
/**
 * mg_plugin_editor_set_work_object
 * @editor: a #MgPluginEditor widget
 * @work_obj:
 *
 * Makes @editor display and change the plugin which will be "used" as a
 * #GnomeDbDataEntry or #GtkCellRenderer when data associated to @work_obj is
 * displayed.
 */
void
mg_plugin_editor_set_work_object (MgPluginEditor *editor, GObject *work_obj)
{
	GtkTreeModel *model;
	GtkTreeIter iter;
	gboolean ui_upd_done = FALSE;

	g_return_if_fail (MG_IS_PLUGIN_EDITOR (editor));
	if (work_obj)
		g_return_if_fail (G_IS_OBJECT (work_obj));

	model = gtk_combo_box_get_model (GTK_COMBO_BOX (editor->priv->plugins_combo));

	if (model && (editor->priv->work_object == work_obj))
		return;

	editor->priv->setting_up = TRUE;

	if (editor->priv->work_object) {
		g_object_remove_weak_pointer (editor->priv->work_object, (gpointer) editor->priv->work_object);
		editor->priv->work_object = NULL;
	}

	/* clear previous settings */
	if (model)
		gtk_list_store_clear (GTK_LIST_STORE (model));
	else {
		GtkCellRenderer *renderer;

		model = (GtkTreeModel *) gtk_list_store_new (NUM_COLUMNS,
							     G_TYPE_POINTER,
							     G_TYPE_STRING);
		gtk_combo_box_set_model (GTK_COMBO_BOX (editor->priv->plugins_combo), model);
		renderer = gtk_cell_renderer_text_new ();
		gtk_cell_layout_pack_start (GTK_CELL_LAYOUT (editor->priv->plugins_combo), renderer, FALSE);
		gtk_cell_layout_set_attributes (GTK_CELL_LAYOUT (editor->priv->plugins_combo), renderer,
						"text", COLUMN_STRING, 
						NULL);
		g_signal_connect (G_OBJECT (editor->priv->plugins_combo), "changed",
				  G_CALLBACK (plugins_combo_changed_cb), editor);
	}

	gtk_list_store_append (GTK_LIST_STORE (model), &iter);
	gtk_list_store_set (GTK_LIST_STORE (model), &iter, 
			    COLUMN_POINTER, NULL, COLUMN_STRING, _("Default"), -1);
	gtk_combo_box_set_active (GTK_COMBO_BOX (editor->priv->plugins_combo), 0);

	if (work_obj) {
		if (work_object_type_known (work_obj)) {
			gchar *plugin = NULL; /* to free */
			gchar *options = NULL; /* don't free */
			gint index, active;

			editor->priv->work_object = work_obj;
			g_object_add_weak_pointer (editor->priv->work_object, (gpointer) editor->priv->work_object);
			
			/* compute new settings */
			compute_new_model (model, editor->priv->work_object);
			plugin = get_work_obj_plugin (editor->priv->work_object);

			if (plugin) {
				gchar *ptr;;
				gint size;
				size = strlen (plugin);
				for (ptr = plugin; *ptr && (*ptr != ':'); ptr++);
				*ptr = 0;
				ptr++;
				if (ptr < plugin + size)
					options = ptr;
			}

			/* select correct plugin in combo box */
			index = find_plugin_index (GTK_COMBO_BOX (editor->priv->plugins_combo), plugin);
			active = gtk_combo_box_get_active (GTK_COMBO_BOX (editor->priv->plugins_combo));
			gtk_combo_box_set_active (GTK_COMBO_BOX (editor->priv->plugins_combo), index);
			if (active == index)
				/* force update */
				plugins_combo_changed_cb (GTK_COMBO_BOX (editor->priv->plugins_combo), editor);
			ui_upd_done = TRUE;
			
			/* fill in the options */
			if (options) {
				if (editor->priv->options && GNOME_DB_IS_BASIC_FORM (editor->priv->options)) {
					GdaQuarkList *params;
					GdaSet *plist;
					
					params = gda_quark_list_new_from_string (options);
					plist = gnome_db_basic_form_get_data_set (GNOME_DB_BASIC_FORM (editor->priv->options));
					gda_quark_list_foreach (params, (GHFunc) options_quarks_foreach_func, plist);
					gda_quark_list_free (params);
				}
				else
					g_warning (_("Options '%s' are set for the '%s' plugin, but it does "
						     "not have any option"), options, plugin);
			}

			g_free (plugin);
		}
		else
			g_warning (_("Plugin editor cannot handle %s objects"), G_OBJECT_TYPE_NAME (work_obj));
	}

	if (!ui_upd_done)
		plugins_combo_changed_cb (GTK_COMBO_BOX (editor->priv->plugins_combo), editor);
	gtk_widget_set_sensitive (GTK_WIDGET (editor->priv->table), editor->priv->work_object ? TRUE : FALSE);
	editor->priv->setting_up = FALSE;
}

static void
options_quarks_foreach_func (const gchar *key, const gchar *value, GdaSet *plist)
{
	GdaHolder *param;

	param = gda_set_get_holder (plist, key);
	if (param)
		gda_holder_set_value_str (param, NULL, value);
	else
		g_warning (_("Options '%s=%s' is set but unknown to the plugin used"), key, value);
}

typedef struct {
	gboolean      plugins_only;
	GType         for_type;
	GtkTreeModel *model;
} PluginHashData;

static void
plugin_hash_foreach_func (const gchar *plugin_name, GnomeDbPlugin *plugin, PluginHashData *data)
{
	if ((data->plugins_only && plugin->plugin_file) || 
	    (!data->plugins_only && !plugin->plugin_file)) {
		gint i;
		for (i = 0; i < plugin->nb_g_types; i++) {
			if (plugin->valid_g_types[i] == data->for_type) {
				GtkTreeIter iter;
				gtk_list_store_append (GTK_LIST_STORE (data->model), &iter);
				gtk_list_store_set (GTK_LIST_STORE (data->model), &iter, 
						    COLUMN_POINTER, plugin, COLUMN_STRING, plugin->plugin_descr, -1);
				break;
			}
		}
	}
}

static void
compute_new_model (GtkTreeModel *model, GObject *obj)
{
	PluginHashData data;
	data.model = model;
	data.for_type = get_work_obj_type (obj);

	if (! gnome_db_plugins_hash)
		gnome_db_util_init_plugins ();
	data.plugins_only = FALSE;
	g_hash_table_foreach (gnome_db_plugins_hash, (GHFunc) plugin_hash_foreach_func, &data);
	data.plugins_only = TRUE;
	g_hash_table_foreach (gnome_db_plugins_hash, (GHFunc) plugin_hash_foreach_func, &data);
}


static void
pack_options (MgPluginEditor *editor, GtkWidget *options)
{
	if (editor->priv->options)
		gtk_widget_destroy (editor->priv->options);
		
	editor->priv->options = options;
	if (options) {
		gtk_table_attach_defaults (GTK_TABLE (editor->priv->table), options, 1, 2, 1, 2);
		gtk_widget_show (options);
	}
}

static void
pack_sample_entry (MgPluginEditor *editor, GtkWidget *sample_entry)
{
	if (editor->priv->sample_entry)
		gtk_widget_destroy (editor->priv->sample_entry);
		
	editor->priv->sample_entry = sample_entry;
	if (sample_entry) {
		gtk_table_attach_defaults (GTK_TABLE (editor->priv->table), sample_entry, 1, 2, 2, 3);
		gtk_widget_show (sample_entry);
	}	
}

static void
update_sample_data_entry (MgPluginEditor *editor, const gchar *plugin_str)
{
	GtkWidget *sample = NULL;
	if (get_work_obj_type (editor->priv->work_object) != G_TYPE_INVALID) {
		sample = GTK_WIDGET (gnome_db_util_new_data_entry (get_work_obj_type (editor->priv->work_object), 
								   plugin_str));
		g_object_set (G_OBJECT (sample), "actions", FALSE, NULL);
	}

	if (!sample) {
		sample = gtk_label_new ("---");
		gtk_misc_set_alignment (GTK_MISC (sample), 0., -1);
	}
	pack_sample_entry (editor, sample);
}

static void
plugins_combo_changed_cb (GtkComboBox *combo, MgPluginEditor *editor)
{
	GtkTreeIter iter;
	GtkWidget *options = NULL;
	gchar *plugin_str;

	if (gtk_combo_box_get_active_iter (combo, &iter)) {
		GnomeDbPlugin *plugin;
		GtkTreeModel *model;

		model = gtk_combo_box_get_model (combo);
		gtk_tree_model_get (model, &iter, COLUMN_POINTER, &plugin, -1);
		if (plugin && plugin->options_xml_spec) {
			GdaSet *plist;
			GError *error = NULL;
			
			/* use %NULL as cnc because we want correct data entry widgets to be displayed, even
			 * if the provider does not support a type */
			plist = gda_set_new_from_spec_string (plugin->options_xml_spec, &error);
			if (!plist) {
				g_warning ("Cannot parse XML spec for plugin options: %s",
					   error && error->message ? error->message : "No detail");
				g_error_free (error);
			}
			else {
				options = gnome_db_basic_form_new (plist);
				g_object_unref (plist);
				g_signal_connect (G_OBJECT (options), "param_changed",
						  G_CALLBACK (options_form_param_changed_cb), editor);
			}
		}
	}

	if (!options) {
		options = gtk_label_new ("---");
		gtk_misc_set_alignment (GTK_MISC (options), 0., -1);
	}
	pack_options (editor, options);

	plugin_str = get_work_obj_plugin (editor->priv->work_object);
	update_sample_data_entry (editor, plugin_str);
	g_free (plugin_str);

	work_object_set_plugin (editor);
}

static void
work_object_set_plugin (MgPluginEditor *editor)
{
	GString *string = NULL;
	GtkTreeIter iter;

	if (editor->priv->setting_up)
		return;

	if (gtk_combo_box_get_active_iter (GTK_COMBO_BOX (editor->priv->plugins_combo), &iter)) {
		GnomeDbPlugin *plugin;
		GtkTreeModel *model;

		model = gtk_combo_box_get_model (GTK_COMBO_BOX (editor->priv->plugins_combo));
		gtk_tree_model_get (model, &iter, COLUMN_POINTER, &plugin, -1);
		if (plugin) {
			string = g_string_new (plugin->plugin_name);
			if (GNOME_DB_IS_BASIC_FORM (editor->priv->options)) {
				GSList *list;
				GdaSet *plist;
				gchar *str;

				g_string_append_c (string, ':');
				plist = gnome_db_basic_form_get_data_set (GNOME_DB_BASIC_FORM (editor->priv->options));
				for (list = plist->holders; list; list = list->next) {
					if (list != plist->holders)
						g_string_append_c (string, ';');
					g_string_append (string, gda_holder_get_id (GDA_HOLDER (list->data)));
					g_string_append_c (string, '=');
					str = gda_holder_get_value_str (GDA_HOLDER (list->data), NULL);
					if (str) {
						g_string_append (string, str);
						g_free (str);
					}
					else
						g_string_append (string, "NULL");
				}
			}
		}
	}

	set_work_obj_plugin (editor->priv->work_object, string ? string->str : NULL);

	/* update the sample data entry */
	update_sample_data_entry (editor, string ? string->str : NULL);

	if (string)
		g_string_free (string, TRUE);
}

static void
options_form_param_changed_cb (GnomeDbBasicForm *form, GdaHolder *param, gboolean is_user_modif, 
			       MgPluginEditor *editor)
{
	work_object_set_plugin (editor);
}

static gint
find_plugin_index (GtkComboBox *combo, const gchar *plugin)
{
	GtkTreeModel *model;
	GtkTreeIter iter;
	gint index = 0;

	if (!plugin)
		return 0; /* DEFAULT */

	model = gtk_combo_box_get_model (combo);
	if (!gtk_tree_model_get_iter_first (model, &iter))
		return index;
	do {
		GnomeDbPlugin *pluginptr;

		gtk_tree_model_get (model, &iter, COLUMN_POINTER, &pluginptr, -1);

		if (pluginptr && !strcmp (pluginptr->plugin_name, plugin))
			return index;
		index++;
	} while (gtk_tree_model_iter_next (model, &iter));
	
	return 0;
}

/*
 * functions which abstract the plugin management for
 * potentially various object types
 */
static gboolean
work_object_type_known (GObject *obj)
{
	gboolean known = FALSE;
	if (GDA_IS_ENTITY_FIELD (obj))
		known = TRUE;
	return known;
}

static GType
get_work_obj_type (GObject *obj)
{
	if (! G_IS_OBJECT (obj))
		return G_TYPE_INVALID;

	if (GDA_IS_ENTITY_FIELD (obj))
		return gda_entity_field_get_g_type (GDA_ENTITY_FIELD (obj));
	
	return G_TYPE_INVALID;
}

gchar *
get_work_obj_plugin (GObject *obj)
{
	gchar *plugin = NULL;

	if (GDA_IS_ENTITY_FIELD (obj))
		g_object_get (obj, "entry-plugin", &plugin, NULL);

	return plugin;
}

void
set_work_obj_plugin (GObject *obj, const gchar *plugin_str)
{
	if (GDA_IS_ENTITY_FIELD (obj))
		g_object_set (obj, "entry-plugin", plugin_str, NULL);
}
