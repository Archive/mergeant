/* Mergeant
 *
 * Copyright (C) 1999 - 2006 Vivien Malerba
 * Copyright (C) 2002 - 2003 Rodrigo Moya
 *
 * Authors:
 *       Rodrigo Moya <rodrigo@gnome-db.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "object-detail.h"

struct _ObjectDetailPrivate {
	GdaConnection *cnc;
	GdaConnectionSchema schema;
};

static void object_detail_class_init (ObjectDetailClass *klass);
static void object_detail_init (ObjectDetail *od, ObjectDetailClass *klass);
static void object_detail_finalize (GObject *object);

static GObjectClass *parent_class = NULL;

static void
object_detail_class_init (ObjectDetailClass *klass)
{
	GObjectClass *object_class;

	parent_class = g_type_class_peek_parent (klass);

	object_class = G_OBJECT_CLASS (klass);
	object_class->finalize = object_detail_finalize;

	klass->display_object = NULL;
}

static void
object_detail_init (ObjectDetail *od, ObjectDetailClass *klass)
{
	/* allocate private structure */
	od->priv = g_new0 (ObjectDetailPrivate, 1);
}

static void
object_detail_finalize (GObject *object)
{
	ObjectDetail *od = (ObjectDetail *) object;

	if (od->priv) {
		g_free (od->priv);
		od->priv = NULL;
	}

	if (parent_class->finalize)
		parent_class->finalize (object);
}

GType
object_detail_get_type (void)
{
        static GType type = 0;
                                                                                    
        if (!type) {
                static const GTypeInfo info = {
                        sizeof (ObjectDetailClass),
                        (GBaseInitFunc) NULL,
                        (GBaseFinalizeFunc) NULL,
                        (GClassInitFunc) object_detail_class_init,
                        NULL,
                        NULL,
                        sizeof (ObjectDetail),
                        0,
                        (GInstanceInitFunc) object_detail_init
                };
                type = g_type_register_static (GTK_TYPE_VBOX, "ObjectDetail", &info, 0);
        }
        return type;
}

void
object_detail_display_object (ObjectDetail *od, GdaConnection *cnc, GdaConnectionSchema schema, const gchar *name)
{
}
