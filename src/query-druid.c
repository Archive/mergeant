/* query-druid.c
 *
 * Copyright (C) 2004 - 2007 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <glib/gi18n-lib.h>
#include "query-druid.h"
#include <libgda/libgda.>
#include <libgnomedb-graph/libgnomedb-graph.h>
#include <libgnomedb-extra/libgnomedb-extra.h>
#include "workspace-page.h"
#include "query-fields-editor.h"

static void query_druid_class_init (QueryDruidClass *class);
static void query_druid_init (QueryDruid *wid);
static void query_druid_dispose (GObject *object);

static gint assistant_forward_func (gint current_page, QueryDruid *qdruid);
static void apply_page_cb (QueryDruid *qdruid, gpointer data);

typedef enum {
	PAGE_START = 0,
	PAGE_QUERY_TYPE = 1,
	PAGE_QUERY_SELECT_TARGETS = 2,
	PAGE_QUERY_SELECT_JOINS = 3,
	PAGE_QUERY_SELECT_FIELDS = 4,
	PAGE_QUERY_SELECT_COND = 5,
	PAGE_QUERY_PARAMS = 6,
	PAGE_SQL = 7,
	PAGE_CONFIRM = 8,
	LAST_PAGE
} PageReference;

struct _QueryDruidPriv
{
	GdaDict      *dict;
	GdaQuery     *build_query;
	
	/* pages index */
	gint          pages_index [LAST_PAGE];

	/* extra data for each page */
	GtkWidget    *query_name;
	GtkWidget    *query_descr;
	GtkWidget    *query_type_table;
	GtkWidget    *sql_editor;

	/* SELECT query pages */
	GtkWidget    *target_tables_selector;
	GtkWidget    *target_targets_selector;
	GtkWidget    *target_add_button;
	GtkWidget    *target_up_button;
	GtkWidget    *target_dn_button;
	GtkWidget    *target_del_button;
	GtkWidget    *fields_targets_selector;
	GtkWidget    *fields_fields_selector;
	GtkWidget    *field_add_button;
	GtkWidget    *field_up_button;
	GtkWidget    *field_dn_button;
	GtkWidget    *field_del_button;
	GtkWidget    *expr_entry;
};

/* get a pointer to the parents to be able to call their destructor */
static GObjectClass *parent_class = NULL;

GType
query_druid_get_type (void)
{
	static GType type = 0;

	if (!type) {
		static const GTypeInfo info = {
			sizeof (QueryDruidClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) query_druid_class_init,
			NULL,
			NULL,
			sizeof (QueryDruid),
			0,
			(GInstanceInitFunc) query_druid_init
		};		
		
		type = g_type_register_static (GTK_TYPE_ASSISTANT, "QueryDruid", &info, 0);
	}

	return type;
}

static void
query_druid_class_init (QueryDruidClass * class)
{
	GObjectClass   *object_class = G_OBJECT_CLASS (class);
	
	parent_class = g_type_class_peek_parent (class);

	object_class->dispose = query_druid_dispose;
}

static void
query_druid_init (QueryDruid *wid)
{
	gint i;
	wid->priv = g_new0 (QueryDruidPriv, 1);
	wid->priv->dict = NULL;
	for (i = 0; i < LAST_PAGE; i++)
		wid->priv->pages_index [i] = 0;

	gtk_assistant_set_forward_page_func (GTK_ASSISTANT (wid),
					     (GtkAssistantPageFunc) assistant_forward_func,
					     wid, NULL);
	g_signal_connect (G_OBJECT (wid), "apply",
			  G_CALLBACK (apply_page_cb), NULL);
	g_signal_connect (G_OBJECT (wid), "cancel",
			  G_CALLBACK (gtk_widget_destroy), NULL);
	g_signal_connect (G_OBJECT (wid), "delete_event",
			  G_CALLBACK (gtk_widget_destroy), NULL);
}

static void query_druid_initialize (QueryDruid *mgsel);


static void object_weak_notify (QueryDruid *mgsel, GObject *obj);
/**
 * query_druid_new
 * @dict: a #GdaDict object
 *
 * Creates a new #QueryDruid widget.
 *
 * Returns: the new widget
 */
GtkWidget *
query_druid_new (GdaDict *dict)
{
	GObject    *obj;
	QueryDruid *qdruid;

	g_return_val_if_fail (dict && GDA_IS_DICT (dict), NULL);
		
	obj = g_object_new (QUERY_DRUID_TYPE, NULL);
	qdruid = QUERY_DRUID (obj);

	qdruid->priv->dict = dict;

	g_object_weak_ref (G_OBJECT (qdruid->priv->dict),
			   (GWeakNotify) object_weak_notify, qdruid);
	
	query_druid_initialize (qdruid);

	return GTK_WIDGET (obj);
}

static void
object_weak_notify (QueryDruid *qdruid, GObject *obj)
{
	if (obj == (GObject*) qdruid->priv->dict)
		/* Tell that we don't need to weak unref the GdaDict */
		qdruid->priv->dict = NULL;
}

static void build_query_fields_changed (GdaQuery *query, GdaQueryField *field, QueryDruid *qdruid);
static void build_query_fields_order_changed (GdaQuery *query, QueryDruid *qdruid);

static void
query_druid_dispose (GObject *object)
{
	QueryDruid *qdruid;

	g_return_if_fail (object != NULL);
	g_return_if_fail (IS_QUERY_DRUID (object));
	qdruid = QUERY_DRUID (object);

	if (qdruid->priv) {
		if (qdruid->priv->build_query) {
			g_signal_handlers_disconnect_by_func (G_OBJECT (qdruid->priv->build_query),
							      G_CALLBACK (build_query_fields_changed), qdruid);
			g_signal_handlers_disconnect_by_func (G_OBJECT (qdruid->priv->build_query),
							      G_CALLBACK (build_query_fields_order_changed), qdruid);
			g_object_unref (qdruid->priv->build_query);
			qdruid->priv->build_query = NULL;
		}

		/* Weak unref the GdaDict if necessary */
		if (qdruid->priv->dict)
			g_object_weak_unref (G_OBJECT (qdruid->priv->dict),
					     (GWeakNotify) object_weak_notify, qdruid);
		
		/* the private area itself */
		g_free (qdruid->priv);
		qdruid->priv = NULL;
	}

	/* for the parent class */
	parent_class->dispose (object);
}

static void make_start_page (QueryDruid *qdruid);
static void make_query_attributes_page (QueryDruid *qdruid);
static void make_sql_query_page (QueryDruid *qdruid);
static void make_select_targets_page (QueryDruid *qdruid);
static void make_select_joins_page (QueryDruid *qdruid);
static void make_select_fields_page (QueryDruid *qdruid);
static void make_select_where_page (QueryDruid *qdruid);
static void make_select_params_page (QueryDruid *qdruid);
static void make_select_apply_page (QueryDruid *qdruid);

static void
query_druid_initialize (QueryDruid *qdruid)
{	
	/* druid itself */
	gtk_container_set_border_width (GTK_CONTAINER (qdruid), 4);

	/* query which is built during the druid usage */
	qdruid->priv->build_query = gda_query_new (qdruid->priv->dict);
	gda_query_set_query_type (qdruid->priv->build_query, GDA_QUERY_TYPE_SELECT);

	g_signal_connect (G_OBJECT (qdruid->priv->build_query), "field_added",
			  G_CALLBACK (build_query_fields_changed), qdruid);
	g_signal_connect (G_OBJECT (qdruid->priv->build_query), "field_removed",
			  G_CALLBACK (build_query_fields_changed), qdruid);
	g_signal_connect (G_OBJECT (qdruid->priv->build_query), "fields_order_changed",
			  G_CALLBACK (build_query_fields_order_changed), qdruid);

	/* pages making */
	make_start_page (qdruid);
	make_query_attributes_page (qdruid);

	make_select_targets_page (qdruid);
	make_select_joins_page (qdruid);
	make_select_fields_page (qdruid);
	make_select_where_page (qdruid);
	make_select_params_page (qdruid);
	make_select_apply_page (qdruid);

	make_sql_query_page (qdruid);

	gtk_assistant_set_current_page (GTK_ASSISTANT (qdruid), 
					qdruid->priv->pages_index [PAGE_START]);
}

static void
build_query_fields_changed (GdaQuery *query, GdaQueryField *field, QueryDruid *qdruid)
{
	build_query_fields_order_changed (query, qdruid);
}

static void
build_query_fields_order_changed (GdaQuery *query, QueryDruid *qdruid)
{
	/* compute up and down status in the fields selection page */
	GdaEntityField *selfield;
	gboolean canup = FALSE, candn = FALSE;

	selfield = (GdaEntityField *) gnome_db_selector_get_selected_object (GNOME_DB_SELECTOR (qdruid->priv->fields_fields_selector));
	if (selfield && GDA_IS_ENTITY_FIELD (selfield)) {
		GSList *fields = gda_entity_get_fields (GDA_ENTITY (qdruid->priv->build_query));
		GSList *last = g_slist_last (fields);
		candn = TRUE;
		if (last && (last->data == (gpointer) selfield))
			candn = FALSE;
		g_slist_free (fields);
		canup = gda_entity_get_field_index (GDA_ENTITY (qdruid->priv->build_query), selfield) != 0;

		gtk_widget_set_sensitive (qdruid->priv->field_up_button, canup);
		gtk_widget_set_sensitive (qdruid->priv->field_dn_button, candn);
	}	
}


/*
 * Start page
 */ 
static void
make_start_page (QueryDruid *qdruid)
{
	GtkWidget *wid;

	wid = gtk_label_new (_("This druid allows you to create a new query.\n\n"
			       "The query can be a data manipulation query of any type "
			       "(SELECT, INSERT, UPDATE or DELETE) and can also be an aggregation "
			       "query (UNION, INTERSECT or EXCEPT).\n\n"
			       "The query can be created either with a point and click interface "
			       "or entering the corresponding SQL statement."));
	gtk_label_set_line_wrap (GTK_LABEL (wid), TRUE);
	qdruid->priv->pages_index [PAGE_START] = gtk_assistant_append_page (GTK_ASSISTANT (qdruid), wid);
	gtk_assistant_set_page_type (GTK_ASSISTANT (qdruid), wid, GTK_ASSISTANT_PAGE_INTRO);
	gtk_widget_show (wid);
	gtk_assistant_set_page_title (GTK_ASSISTANT (qdruid), wid, _("New query"));
	gtk_assistant_set_page_complete (GTK_ASSISTANT (qdruid), wid, TRUE);
}


/*
 * Query attributes page
 */ 
static void query_type_rb_toggled_cb (GtkToggleButton *btn, QueryDruid *qdruid);
static void
make_query_attributes_page (QueryDruid *qdruid)
{
	GtkWidget *wid, *vbox, *table, *rb;
	GSList *rb_group1 = NULL;
	GSList *rb_group2 = NULL;
	gchar *str;

	/* page definition */
	vbox = gtk_vbox_new (FALSE, 0);
	gtk_container_set_border_width (GTK_CONTAINER (vbox), 16);

	str = g_strdup_printf ("<b>%s</b>", _("Name and description:"));
	wid = gtk_label_new (str);
	g_free (str);
	gtk_box_pack_start (GTK_BOX (vbox), wid, FALSE, FALSE, 0);
	gtk_label_set_use_markup (GTK_LABEL (wid), TRUE);
	gtk_label_set_justify (GTK_LABEL (wid), GTK_JUSTIFY_LEFT);
	gtk_misc_set_alignment (GTK_MISC (wid), 0, 0.5);

	table = gtk_table_new (2, 2, FALSE);
	gtk_table_set_row_spacings (GTK_TABLE (table), 2);
	gtk_box_pack_start (GTK_BOX (vbox), table, FALSE, FALSE, 0);
	
	wid = gtk_label_new (_("Name:"));
	gtk_table_attach (GTK_TABLE (table), wid, 0, 1, 0, 1, GTK_FILL, 0, 0, 0);
	gtk_label_set_justify (GTK_LABEL (wid), GTK_JUSTIFY_LEFT);
	gtk_misc_set_alignment (GTK_MISC (wid), 0, 0.5);
	
	wid = gtk_entry_new ();
	gtk_entry_set_text (GTK_ENTRY (wid), _("Untitled"));
	gtk_table_attach (GTK_TABLE (table), wid, 1, 2, 0, 1, GTK_EXPAND | GTK_FILL, 0, 0, 0);
	qdruid->priv->query_name = wid;

	
	wid = gtk_label_new (_("Description:"));
	gtk_table_attach (GTK_TABLE (table), wid, 0, 1, 1, 2, GTK_FILL, 0, 0, 0);
	gtk_label_set_justify (GTK_LABEL (wid), GTK_JUSTIFY_LEFT);
	gtk_misc_set_alignment (GTK_MISC (wid), 0, 0.5);
	
	wid = gtk_entry_new ();
	gtk_table_attach (GTK_TABLE (table), wid, 1, 2, 1, 2, GTK_EXPAND | GTK_FILL, 0, 0, 0);
	qdruid->priv->query_descr = wid;

	str = g_strdup_printf ("<b>%s</b>", _("Type of query:"));
	wid = gtk_label_new (str);
	g_free (str);
	gtk_box_pack_start (GTK_BOX (vbox), wid, FALSE, FALSE, 0);
	gtk_label_set_use_markup (GTK_LABEL (wid), TRUE);
	gtk_label_set_justify (GTK_LABEL (wid), GTK_JUSTIFY_LEFT);
	gtk_misc_set_alignment (GTK_MISC (wid), 0, 0.5);

	rb = gtk_radio_button_new_with_mnemonic (NULL, _("Point & click wizard"));
	gtk_box_pack_start (GTK_BOX (vbox), rb, FALSE, FALSE, 0);
	rb_group1 = gtk_radio_button_get_group (GTK_RADIO_BUTTON (rb));
	g_object_set_data (G_OBJECT (rb), "action", GINT_TO_POINTER (-1));
	g_signal_connect (G_OBJECT (rb), "toggled", G_CALLBACK (query_type_rb_toggled_cb), qdruid);
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (rb), TRUE);
	
	wid = gtk_table_new (5, 2, FALSE);
	gtk_box_pack_start (GTK_BOX (vbox), wid, FALSE, FALSE, 0);
	gtk_table_set_col_spacings (GTK_TABLE (wid), 20);
	qdruid->priv->query_type_table = wid;

	rb = gtk_radio_button_new_with_mnemonic (NULL, _("SELECT query"));
	gtk_table_attach (GTK_TABLE (wid), rb, 1, 2, 0, 1, GTK_FILL, 0, 0, 0);
	rb_group2 = gtk_radio_button_get_group (GTK_RADIO_BUTTON (rb));
	g_object_set_data (G_OBJECT (rb), "action", GINT_TO_POINTER (GDA_QUERY_TYPE_SELECT));
	g_signal_connect (G_OBJECT (rb), "toggled", G_CALLBACK (query_type_rb_toggled_cb), qdruid);
	g_object_set_data (G_OBJECT (vbox), "query_type_1", 
			   GINT_TO_POINTER (GDA_QUERY_TYPE_SELECT));

	rb = gtk_radio_button_new_with_mnemonic (NULL, _("INSERT query"));
	gtk_table_attach (GTK_TABLE (wid), rb, 1, 2, 1, 2, GTK_FILL, 0, 0, 0);
	gtk_radio_button_set_group (GTK_RADIO_BUTTON (rb), rb_group2);
	rb_group2 = gtk_radio_button_get_group (GTK_RADIO_BUTTON (rb));
	g_object_set_data (G_OBJECT (rb), "action", GINT_TO_POINTER (GDA_QUERY_TYPE_INSERT));
	g_signal_connect (G_OBJECT (rb), "toggled", G_CALLBACK (query_type_rb_toggled_cb), qdruid);

	rb = gtk_radio_button_new_with_mnemonic (NULL, _("UPDATE query"));
	gtk_table_attach (GTK_TABLE (wid), rb, 1, 2, 2, 3, GTK_FILL, 0, 0, 0);
	gtk_radio_button_set_group (GTK_RADIO_BUTTON (rb), rb_group2);
	rb_group2 = gtk_radio_button_get_group (GTK_RADIO_BUTTON (rb));
	g_object_set_data (G_OBJECT (rb), "action", GINT_TO_POINTER (GDA_QUERY_TYPE_UPDATE));
	g_signal_connect (G_OBJECT (rb), "toggled", G_CALLBACK (query_type_rb_toggled_cb), qdruid);

	rb = gtk_radio_button_new_with_mnemonic (NULL, _("DELETE query"));
	gtk_table_attach (GTK_TABLE (wid), rb, 1, 2, 3, 4, GTK_FILL, 0, 0, 0);
	gtk_radio_button_set_group (GTK_RADIO_BUTTON (rb), rb_group2);
	rb_group2 = gtk_radio_button_get_group (GTK_RADIO_BUTTON (rb));
	g_object_set_data (G_OBJECT (rb), "action", GINT_TO_POINTER (GDA_QUERY_TYPE_DELETE));
	g_signal_connect (G_OBJECT (rb), "toggled", G_CALLBACK (query_type_rb_toggled_cb), qdruid);

	rb = gtk_radio_button_new_with_mnemonic (NULL, _("Aggreation query (union, intersect or except)"));
	gtk_table_attach (GTK_TABLE (wid), rb, 1, 2, 4, 5, GTK_FILL, 0, 0, 0);
	gtk_radio_button_set_group (GTK_RADIO_BUTTON (rb), rb_group2);
	rb_group2 = gtk_radio_button_get_group (GTK_RADIO_BUTTON (rb));
	g_object_set_data (G_OBJECT (rb), "action", GINT_TO_POINTER (GDA_QUERY_TYPE_UNION));
	g_signal_connect (G_OBJECT (rb), "toggled", G_CALLBACK (query_type_rb_toggled_cb), qdruid);

	rb = gtk_radio_button_new_with_mnemonic (NULL, _("SQL query (of any type)"));
	gtk_box_pack_start (GTK_BOX (vbox), rb, FALSE, FALSE, 0);
	gtk_radio_button_set_group (GTK_RADIO_BUTTON (rb), rb_group1);
	rb_group1 = gtk_radio_button_get_group (GTK_RADIO_BUTTON (rb));
	g_object_set_data (G_OBJECT (rb), "action", GINT_TO_POINTER (-2));
	g_signal_connect (G_OBJECT (rb), "toggled", G_CALLBACK (query_type_rb_toggled_cb), qdruid);

	/* default action */
	g_object_set_data (G_OBJECT (vbox), "query_type", 
			   GINT_TO_POINTER (GDA_QUERY_TYPE_SELECT));

	/* add page to GtkAssistant */
	qdruid->priv->pages_index [PAGE_QUERY_TYPE] = gtk_assistant_append_page (GTK_ASSISTANT (qdruid), vbox);
	gtk_widget_show_all (vbox);
	gtk_assistant_set_page_title (GTK_ASSISTANT (qdruid), vbox, _("Query attributes"));
	gtk_assistant_set_page_complete (GTK_ASSISTANT (qdruid), vbox, TRUE);

}
static void
query_type_rb_toggled_cb (GtkToggleButton *btn, QueryDruid *qdruid)
{
	gint action;
	GtkWidget *page_wid = gtk_assistant_get_nth_page (GTK_ASSISTANT (qdruid), 
							  qdruid->priv->pages_index [PAGE_QUERY_TYPE]);

	if (! gtk_toggle_button_get_active (btn))
		return;

	action = GPOINTER_TO_INT (g_object_get_data (G_OBJECT (btn), "action"));

	switch (action) {
	case -1:
		gtk_widget_set_sensitive (qdruid->priv->query_type_table, TRUE);
		g_object_set_data (G_OBJECT (page_wid), "query_type", 
				   g_object_get_data (G_OBJECT (page_wid), "query_type_1"));
		break;
	case -2:
		gtk_widget_set_sensitive (qdruid->priv->query_type_table, FALSE);
		g_object_set_data (G_OBJECT (page_wid), "query_type", GINT_TO_POINTER (action));
		break;
	default:
		g_object_set_data (G_OBJECT (page_wid), "query_type_1", GINT_TO_POINTER (action));
		g_object_set_data (G_OBJECT (page_wid), "query_type", GINT_TO_POINTER (action));
		gda_query_set_query_type (qdruid->priv->build_query, action);
		g_print ("Query type set to %d\n", action);
		break;
	}
}

/*
 * SELECT query: targets page
 */
static void select_targets_table_select_changed_cb (GnomeDbSelector *mgsel, GdaDictTable *table, QueryDruid *qdruid);
static void select_targets_target_select_changed_cb (GnomeDbSelector *mgsel, GdaQueryTarget *target, QueryDruid *qdruid);
static void select_targets_add_table (GtkButton *button, QueryDruid *qdruid);
static void select_targets_up_target (GtkButton *button, QueryDruid *qdruid);
static void select_targets_dn_target (GtkButton *button, QueryDruid *qdruid);
static void select_targets_del_target (GtkButton *button, QueryDruid *qdruid);
static void
make_select_targets_page (QueryDruid *qdruid)
{
	GtkWidget *wid, *vbox, *table, *label, *bbox, *button;
	gchar *str;

	/* page definition */
	vbox = gtk_vbox_new (FALSE, 0);
	gtk_container_set_border_width (GTK_CONTAINER (vbox), 16);

	label = gtk_label_new (NULL);
	str = g_strdup_printf ("<b>%s:</b>\n%s", _("Target selection:"),
			       _("select the tables and views from which "
				 "data will be extracted."));
	gtk_label_set_markup (GTK_LABEL (label), str);
	g_free (str);
	gtk_label_set_line_wrap (GTK_LABEL (label), TRUE);
	gtk_misc_set_alignment (GTK_MISC (label), 0., -1);
	gtk_box_pack_start (GTK_BOX (vbox), label, FALSE, TRUE, 0);

	table = gtk_table_new (2, 4, FALSE);
	gtk_box_pack_start (GTK_BOX (vbox), table, TRUE, TRUE, 0);

	label = gtk_label_new (NULL);
	str = g_strdup_printf ("<b>%s</b>", _("Tables / Views:"));
	gtk_label_set_markup (GTK_LABEL (label), str);
	g_free (str);
	gtk_table_attach (GTK_TABLE (table), label, 0, 1, 0, 1,
			  (GtkAttachOptions) (GTK_FILL),
			  (GtkAttachOptions) (0), 0, 0);
	gtk_misc_set_alignment (GTK_MISC (label), 0, 0.5);

	label = gtk_label_new (NULL);
	str = g_strdup_printf ("<b>%s</b>", _("Query targets:"));
	gtk_label_set_markup (GTK_LABEL (label), str);
	g_free (str);
	gtk_table_attach (GTK_TABLE (table), label, 2, 3, 0, 1,
			  (GtkAttachOptions) (GTK_FILL),
			  (GtkAttachOptions) (0), 0, 0);
	gtk_misc_set_alignment (GTK_MISC (label), 0, 0.5);

	bbox = gtk_vbutton_box_new ();
	gtk_table_attach (GTK_TABLE (table), bbox, 1, 2, 1, 2,
			  (GtkAttachOptions) (GTK_FILL),
			  (GtkAttachOptions) (GTK_EXPAND | GTK_FILL), 0, 0);
	
	button = gtk_button_new ();
	gtk_container_add (GTK_CONTAINER (bbox), button);
	
	wid = gtk_image_new_from_stock ("gtk-go-forward", GTK_ICON_SIZE_BUTTON);
	gtk_container_add (GTK_CONTAINER (button), wid);
	gtk_widget_set_sensitive (button, FALSE);
	g_signal_connect (G_OBJECT (button), "clicked",
			  G_CALLBACK (select_targets_add_table), qdruid);
	qdruid->priv->target_add_button = button;

	bbox = gtk_vbutton_box_new ();
	gtk_table_attach (GTK_TABLE (table), bbox, 3, 4, 1, 2,
			  (GtkAttachOptions) (GTK_FILL),
			  (GtkAttachOptions) (GTK_FILL), 0, 0);
	gtk_button_box_set_layout (GTK_BUTTON_BOX (bbox), GTK_BUTTONBOX_SPREAD);

	button = gtk_button_new ();
	gtk_container_add (GTK_CONTAINER (bbox), button);

	wid = gtk_image_new_from_stock ("gtk-go-up", GTK_ICON_SIZE_BUTTON);
	gtk_container_add (GTK_CONTAINER (button), wid);
	gtk_widget_set_sensitive (button, FALSE);
	g_signal_connect (G_OBJECT (button), "clicked",
			  G_CALLBACK (select_targets_up_target), qdruid);
	qdruid->priv->target_up_button = button;
	
	button = gtk_button_new ();
	gtk_container_add (GTK_CONTAINER (bbox), button);
	
	wid = gtk_image_new_from_stock ("gtk-go-down", GTK_ICON_SIZE_BUTTON);
	gtk_container_add (GTK_CONTAINER (button), wid);
	gtk_widget_set_sensitive (button, FALSE);
	g_signal_connect (G_OBJECT (button), "clicked",
			  G_CALLBACK (select_targets_dn_target), qdruid);
	qdruid->priv->target_dn_button = button;

	button = gtk_button_new ();
	gtk_container_add (GTK_CONTAINER (bbox), button);

	wid = gtk_image_new_from_stock ("gtk-cancel", GTK_ICON_SIZE_BUTTON);
	gtk_container_add (GTK_CONTAINER (button), wid);
	gtk_widget_set_sensitive (button, FALSE);
	g_signal_connect (G_OBJECT (button), "clicked",
			  G_CALLBACK (select_targets_del_target), qdruid);
	qdruid->priv->target_del_button = button;
	
	wid = gnome_db_selector_new (qdruid->priv->dict, NULL, GNOME_DB_SELECTOR_TABLES, 0);
	qdruid->priv->target_tables_selector = wid;
	gnome_db_selector_set_headers_visible (GNOME_DB_SELECTOR (wid), FALSE);
	gtk_table_attach_defaults (GTK_TABLE (table), wid, 0, 1, 1, 2);
	g_signal_connect (G_OBJECT (wid), "selection_changed",
			  G_CALLBACK (select_targets_table_select_changed_cb), qdruid);

	wid = gnome_db_selector_new (qdruid->priv->dict, G_OBJECT (qdruid->priv->build_query),
			       GNOME_DB_SELECTOR_TARGETS, 0);
	qdruid->priv->target_targets_selector = wid;
	gnome_db_selector_set_headers_visible (GNOME_DB_SELECTOR (wid), FALSE);
	gtk_table_attach_defaults (GTK_TABLE (table), wid, 2, 3, 1, 2);
	g_signal_connect (G_OBJECT (wid), "selection_changed",
			  G_CALLBACK (select_targets_target_select_changed_cb), qdruid);

	/* add page to GtkAssistant */
	qdruid->priv->pages_index [PAGE_QUERY_SELECT_TARGETS] = gtk_assistant_append_page (GTK_ASSISTANT (qdruid), vbox);
	gtk_widget_show_all (vbox);	
	gtk_assistant_set_page_title (GTK_ASSISTANT (qdruid), vbox, _("SELECT query: targets"));
	gtk_assistant_set_page_complete (GTK_ASSISTANT (qdruid), vbox, TRUE);
}

static void
select_targets_table_select_changed_cb (GnomeDbSelector *mgsel, GdaDictTable *table, QueryDruid *qdruid)
{
	gtk_widget_set_sensitive (qdruid->priv->target_add_button, table ? TRUE : FALSE);
}

static void
select_targets_target_select_changed_cb (GnomeDbSelector *mgsel, GdaQueryTarget *target, QueryDruid *qdruid)
{
	/* gtk_widget_set_sensitive (qdruid->priv->target_up_button, target ? TRUE : FALSE); */
	/* gtk_widget_set_sensitive (qdruid->priv->target_dn_button, target ? TRUE : FALSE); */
	gtk_widget_set_sensitive (qdruid->priv->target_del_button, target ? TRUE : FALSE);
}

static void
select_targets_add_table (GtkButton *button, QueryDruid *qdruid)
{
	GdaQueryTarget *target;
	GdaDictTable *table;
	GSList *constraints, *list;
	GSList *targets = gda_query_get_targets (qdruid->priv->build_query), *tlist;

	/* actual target creation */
	table = (GdaDictTable *) 
		gnome_db_selector_get_selected_object (GNOME_DB_SELECTOR (qdruid->priv->target_tables_selector));
	g_assert (table && GDA_IS_DICT_TABLE (table));

	target = (GdaQueryTarget*) g_object_new (GDA_TYPE_QUERY_TARGET,
						 "dict", gda_object_get_dict ((GdaObject*) qdruid->priv->build_query),
						 "query", qdruid->priv->build_query,
						 "entity", GDA_ENTITY (table), NULL);
	gda_query_add_target (qdruid->priv->build_query, target, NULL);
	g_object_unref (target);

	/* using database FK constraints to create joins */
	constraints = gda_dict_database_get_tables_fk_constraints (gda_dict_get_database (qdruid->priv->dict),
							     table, NULL, FALSE);
	list = constraints;
	while (list) {
		GdaDictConstraint *cons = GDA_DICT_CONSTRAINT (list->data);
		GdaDictTable *otable;
		GdaQueryTarget *otarget = NULL;
		gboolean table_is_pkey = TRUE;

		otable = gda_dict_constraint_get_table (cons);
		if (otable == table) {
			table_is_pkey = FALSE;
			otable = gda_dict_constraint_fkey_get_ref_table (cons);
		}
		
		/* find a suitable target to make a join with */
		tlist = targets;
		while (tlist && !otarget) {
			if (gda_query_target_get_represented_entity (GDA_QUERY_TARGET (tlist->data)) == 
			    (GdaEntity *) otable)
				otarget = GDA_QUERY_TARGET (tlist->data);
			tlist = g_slist_next (tlist);
		}

		if (otarget) {
			GdaQueryJoin *join;

			/* actual join */
			join = gda_query_join_new_with_targets (qdruid->priv->build_query, otarget, target);
			gda_query_join_set_join_type (join, GDA_QUERY_JOIN_TYPE_INNER);
			gda_query_join_set_condition_from_fkcons (join);
			gda_query_add_join (qdruid->priv->build_query, join);
			g_object_unref (join);
		}

		list = g_slist_next (list);
	}
	g_slist_free (constraints);
	g_slist_free (targets);

#ifdef debug
	gda_object_dump (GDA_OBJECT (qdruid->priv->build_query), 0);
#endif
}

static void
select_targets_up_target (GtkButton *button, QueryDruid *qdruid)
{
	g_print ("%s() called\n", __FUNCTION__);
}

static void
select_targets_dn_target (GtkButton *button, QueryDruid *qdruid)
{
	g_print ("%s() called\n", __FUNCTION__);
}

static void
select_targets_del_target (GtkButton *button, QueryDruid *qdruid)
{
	GdaQueryTarget *target;
	
	target = (GdaQueryTarget *)
		gnome_db_selector_get_selected_object (GNOME_DB_SELECTOR (qdruid->priv->target_targets_selector));
	gda_query_del_target (qdruid->priv->build_query, target);
#ifdef debug
	gda_object_dump (GDA_OBJECT (qdruid->priv->build_query), 0);
#endif
}

static void
make_select_joins_page (QueryDruid *qdruid)
{
	GtkWidget *wid, *vbox, *label;
	GdaGraph *graph;
	gchar *str;

	/* page definition */
	vbox = gtk_vbox_new (FALSE, 0);
	gtk_container_set_border_width (GTK_CONTAINER (vbox), 16);

	label = gtk_label_new (NULL);
	str = g_strdup_printf ("<b>%s</b>\n%s", _("Targets' joins:"),
			       _("choose the joins that should be taken into account in the query "
				 "(most of the time there are not any target not joined to any other target)"));
	gtk_label_set_markup (GTK_LABEL (label), str);
	g_free (str);
	gtk_label_set_line_wrap (GTK_LABEL (label), TRUE);
	gtk_misc_set_alignment (GTK_MISC (label), 0., -1);
	gtk_box_pack_start (GTK_BOX (vbox), label, FALSE, TRUE, 0);

	graph = GDA_GRAPH (gda_graph_query_new (qdruid->priv->build_query));
#ifdef HAVE_GNOMEDB_GOOCANVAS
	wid = gnome_db_goo_query_struct_new (qdruid->priv->build_query, (GdaGraphQuery *) graph);
	gnome_db_goo_set_zoom_factor (GNOME_DB_GOO (wid), 0.8);
	wid = gnome_db_goo_set_in_scrolled_window (GNOME_DB_GOO (wid));
#else
	wid = gnome_db_canvas_query_struct_new (qdruid->priv->build_query, graph);
	gnome_db_canvas_set_zoom_factor (GNOME_DB_CANVAS (wid), 0.8);
	wid = gnome_db_canvas_set_in_scrolled_window (GNOME_DB_CANVAS (wid));
#endif
	g_object_unref (graph);
	gtk_box_pack_start (GTK_BOX (vbox), wid, TRUE, TRUE, 0);

	/* add page to GtkAssistant */
	qdruid->priv->pages_index [PAGE_QUERY_SELECT_JOINS] = gtk_assistant_append_page (GTK_ASSISTANT (qdruid), vbox);
	gtk_widget_show_all (vbox);
	gtk_assistant_set_page_title (GTK_ASSISTANT (qdruid), vbox, _("SELECT query: joins"));
	gtk_assistant_set_page_complete (GTK_ASSISTANT (qdruid), vbox, TRUE);
}

static void select_fields_tfield_select_changed_cb (GnomeDbSelector *mgsel, GdaEntityField *field, QueryDruid *qdruid);
static void select_fields_qfield_select_changed_cb (GnomeDbSelector *mgsel, GdaQueryField *field, QueryDruid *qdruid);
static void select_fields_add_field (GtkButton *button, QueryDruid *qdruid);
static void select_fields_add_expr (GtkButton *button, QueryDruid *qdruid);
static void select_fields_up_field (GtkButton *button, QueryDruid *qdruid);
static void select_fields_dn_field (GtkButton *button, QueryDruid *qdruid);
static void select_fields_del_field (GtkButton *button, QueryDruid *qdruid);
static void
make_select_fields_page (QueryDruid *qdruid)
{
	GtkWidget *wid, *vbox, *table, *label, *bbox, *button;
	gchar *str;
	
	/* page definition */
	vbox = gtk_vbox_new (FALSE, 0);
	gtk_container_set_border_width (GTK_CONTAINER (vbox), 16);

	label = gtk_label_new (NULL);
	str = g_strdup_printf ("<b>%s</b>\n%s", _("Fields selection:"),
			       _("select the fields from the already selected targets. "
				 "Alternatively fields can be entered as textual expressions for "
				 "functions, constants, etc."));
	gtk_label_set_markup (GTK_LABEL (label), str);
	g_free (str);
	gtk_label_set_line_wrap (GTK_LABEL (label), TRUE);
	gtk_misc_set_alignment (GTK_MISC (label), 0., -1);
	gtk_box_pack_start (GTK_BOX (vbox), label, FALSE, TRUE, 0);

	table = gtk_table_new (4, 4, FALSE);
	gtk_box_pack_start (GTK_BOX (vbox), table, TRUE, TRUE, 0);

	label = gtk_label_new (NULL);
	str = g_strdup_printf ("<b>%s</b>", _("Possible fields:"));
	gtk_label_set_markup (GTK_LABEL (label), str);
	g_free (str);
	gtk_table_attach (GTK_TABLE (table), label, 0, 1, 0, 1,
			  (GtkAttachOptions) (GTK_FILL),
			  (GtkAttachOptions) (0), 0, 0);
	gtk_misc_set_alignment (GTK_MISC (label), 0, 0.5);

	label = gtk_label_new (NULL);
	str = g_strdup_printf ("<b>%s</b>", _("Query fields:"));
	gtk_label_set_markup (GTK_LABEL (label), str);
	g_free (str);
	gtk_table_attach (GTK_TABLE (table), label, 2, 3, 0, 1,
			  (GtkAttachOptions) (GTK_FILL),
			  (GtkAttachOptions) (0), 0, 0);
	gtk_misc_set_alignment (GTK_MISC (label), 0, 0.5);

	label = gtk_label_new (NULL);
	str = g_strdup_printf ("<b>%s</b>", _("Textual\nexpression:"));
	gtk_label_set_markup (GTK_LABEL (label), str);
	g_free (str);
	gtk_table_attach (GTK_TABLE (table), label, 0, 1, 2, 3,
			  (GtkAttachOptions) (GTK_FILL),
			  (GtkAttachOptions) (0), 0, 0);
	gtk_misc_set_alignment (GTK_MISC (label), 0, 0.5);

	wid = gtk_entry_new ();
	gtk_table_attach (GTK_TABLE (table), wid, 0, 1, 3, 4,
			  (GtkAttachOptions) (GTK_FILL),
			  (GtkAttachOptions) (0), 0, 0);
	qdruid->priv->expr_entry = wid;

	bbox = gtk_vbutton_box_new ();
	gtk_table_attach (GTK_TABLE (table), bbox, 1, 2, 1, 2,
			  (GtkAttachOptions) (0),
			  (GtkAttachOptions) (GTK_EXPAND | GTK_FILL), 0, 0);
	
	button = gtk_button_new ();
	gtk_container_add (GTK_CONTAINER (bbox), button);
	
	wid = gtk_image_new_from_stock (GTK_STOCK_GO_FORWARD, GTK_ICON_SIZE_BUTTON);
	gtk_container_add (GTK_CONTAINER (button), wid);
	gtk_widget_set_sensitive (button, FALSE);
	g_signal_connect (G_OBJECT (button), "clicked",
			  G_CALLBACK (select_fields_add_field), qdruid);
	qdruid->priv->field_add_button = button;

	bbox = gtk_vbutton_box_new ();
	gtk_table_attach (GTK_TABLE (table), bbox, 1, 2, 3, 4,
			  (GtkAttachOptions) (0),
			  (GtkAttachOptions) (0), 0, 0);
	
	button = gtk_button_new ();
	gtk_container_add (GTK_CONTAINER (bbox), button);
	
	wid = gtk_image_new_from_stock (GTK_STOCK_GO_FORWARD, GTK_ICON_SIZE_BUTTON);
	gtk_container_add (GTK_CONTAINER (button), wid);
	g_signal_connect (G_OBJECT (button), "clicked",
			  G_CALLBACK (select_fields_add_expr), qdruid);

	bbox = gtk_vbutton_box_new ();
	gtk_table_attach (GTK_TABLE (table), bbox, 3, 4, 1, 2,
			  (GtkAttachOptions) (0),
			  (GtkAttachOptions) (GTK_FILL), 0, 0);
	gtk_button_box_set_layout (GTK_BUTTON_BOX (bbox), GTK_BUTTONBOX_SPREAD);

	button = gtk_button_new ();
	gtk_container_add (GTK_CONTAINER (bbox), button);

	wid = gtk_image_new_from_stock (GTK_STOCK_GO_UP, GTK_ICON_SIZE_BUTTON);
	gtk_container_add (GTK_CONTAINER (button), wid);
	gtk_widget_set_sensitive (button, FALSE);
	g_signal_connect (G_OBJECT (button), "clicked",
			  G_CALLBACK (select_fields_up_field), qdruid);
	qdruid->priv->field_up_button = button;
	
	button = gtk_button_new ();
	gtk_container_add (GTK_CONTAINER (bbox), button);
	
	wid = gtk_image_new_from_stock (GTK_STOCK_GO_DOWN, GTK_ICON_SIZE_BUTTON);
	gtk_container_add (GTK_CONTAINER (button), wid);
	gtk_widget_set_sensitive (button, FALSE);
	g_signal_connect (G_OBJECT (button), "clicked",
			  G_CALLBACK (select_fields_dn_field), qdruid);
	qdruid->priv->field_dn_button = button;

	button = gtk_button_new ();
	gtk_container_add (GTK_CONTAINER (bbox), button);

	wid = gtk_image_new_from_stock ("gtk-cancel", GTK_ICON_SIZE_BUTTON);
	gtk_container_add (GTK_CONTAINER (button), wid);
	gtk_widget_set_sensitive (button, FALSE);
	g_signal_connect (G_OBJECT (button), "clicked",
			  G_CALLBACK (select_fields_del_field), qdruid);
	qdruid->priv->field_del_button = button;
	
	wid = gnome_db_selector_new (qdruid->priv->dict, G_OBJECT (qdruid->priv->build_query), 
			       GNOME_DB_SELECTOR_TARGETS_CTS, 0);
	qdruid->priv->fields_targets_selector = wid;
	gnome_db_selector_set_headers_visible (GNOME_DB_SELECTOR (wid), FALSE);
	gtk_table_attach_defaults (GTK_TABLE (table), wid, 0, 1, 1, 2);
	g_signal_connect (G_OBJECT (wid), "selection_changed",
			  G_CALLBACK (select_fields_tfield_select_changed_cb), qdruid);

	wid = gnome_db_selector_new (qdruid->priv->dict, G_OBJECT (qdruid->priv->build_query),
			       GNOME_DB_SELECTOR_QVIS_FIELDS, 0);
	qdruid->priv->fields_fields_selector = wid;
	gnome_db_selector_set_headers_visible (GNOME_DB_SELECTOR (wid), FALSE);
	gtk_table_attach_defaults (GTK_TABLE (table), wid, 2, 3, 1, 4);
	g_signal_connect (G_OBJECT (wid), "selection_changed",
			  G_CALLBACK (select_fields_qfield_select_changed_cb), qdruid);

	/* add page to GtkAssistant */
	qdruid->priv->pages_index [PAGE_QUERY_SELECT_FIELDS] = gtk_assistant_append_page (GTK_ASSISTANT (qdruid), vbox);
	gtk_widget_show_all (vbox);
	gtk_assistant_set_page_title (GTK_ASSISTANT (qdruid), vbox, _("SELECT query: fields"));
	gtk_assistant_set_page_complete (GTK_ASSISTANT (qdruid), vbox, TRUE);
}

static void
select_fields_tfield_select_changed_cb (GnomeDbSelector *mgsel, GdaEntityField *field, QueryDruid *qdruid)
{
	gtk_widget_set_sensitive (qdruid->priv->field_add_button, field && GDA_IS_ENTITY_FIELD (field) ? TRUE : FALSE);
}

static void
select_fields_qfield_select_changed_cb (GnomeDbSelector *mgsel, GdaQueryField *field, QueryDruid *qdruid)
{
	gboolean sensitive = FALSE, candn = FALSE;

	if (field && GDA_IS_ENTITY_FIELD (field))
		sensitive = TRUE;

	if (sensitive) {
		GSList *fields = gda_entity_get_fields (GDA_ENTITY (qdruid->priv->build_query));
		GSList *last = g_slist_last (fields);
		candn = TRUE;
		if (last && (last->data == (gpointer) field))
			candn = FALSE;
		g_slist_free (fields);
	}
	gtk_widget_set_sensitive (qdruid->priv->field_up_button, 
				  sensitive && 
				  (gda_entity_get_field_index (GDA_ENTITY (qdruid->priv->build_query), GDA_ENTITY_FIELD (field)) != 0));
	gtk_widget_set_sensitive (qdruid->priv->field_dn_button, candn);
	gtk_widget_set_sensitive (qdruid->priv->field_del_button, sensitive);
}

static void
select_fields_add_field (GtkButton *button, QueryDruid *qdruid)
{
	GdaEntityField *field;
	GdaQueryTarget *target;
	GdaQueryField *newfield;

	field = (GdaEntityField *) gnome_db_selector_get_selected_object (GNOME_DB_SELECTOR (qdruid->priv->fields_targets_selector));
	target = (GdaQueryTarget *) gnome_db_selector_get_selected_object_parent (GNOME_DB_SELECTOR (qdruid->priv->fields_targets_selector));

	g_assert (field && GDA_IS_ENTITY_FIELD (field));
	g_assert (target && GDA_IS_QUERY_TARGET (target));

	newfield = (GdaQueryField*) g_object_new (GDA_TYPE_QUERY_FIELD_FIELD,
						  "dict", gda_object_get_dict ((GdaObject*) qdruid->priv->build_query),
						  "query", qdruid->priv->build_query,
						  "target", target,
						  "field", field, NULL);
	gda_object_set_name (GDA_OBJECT (newfield), gda_object_get_name (GDA_OBJECT (field)));
	gda_entity_add_field (GDA_ENTITY (qdruid->priv->build_query), GDA_ENTITY_FIELD (newfield));
	g_object_unref (newfield);
#ifdef debug
	gda_object_dump (GDA_OBJECT (qdruid->priv->build_query), 0);
#endif
}

static void
select_fields_add_expr (GtkButton *button, QueryDruid *qdruid)
{
	GError *error = NULL;
	const gchar *str;
	gchar *msg;

	str = gtk_entry_get_text (GTK_ENTRY (qdruid->priv->expr_entry));
	if (! gda_query_add_field_from_sql (qdruid->priv->build_query, str, &error)) {
		GtkWidget *dlg;

		if (error) {
			msg = g_strdup_printf ("<b>%s</b>\n%s '%s':\n\n%s",
					       _("Error parsing/analysing field expression:"),
					       _("while parsing"),
					       str,
					       error->message);
					       
			g_error_free (error);
		}
		else
			msg = g_strdup_printf ("<b>%s</b>\n%s '%s'",
					       _("Error parsing/analysing field expression:"),
					       _("while parsing"),
					       str);

		dlg = gtk_message_dialog_new_with_markup (NULL, GTK_DIALOG_MODAL, GTK_MESSAGE_ERROR,
							  GTK_BUTTONS_CLOSE, msg);
		g_free (msg);
		gtk_dialog_run (GTK_DIALOG (dlg));
		gtk_widget_destroy (dlg);
	}
}

static void
select_fields_up_field (GtkButton *button, QueryDruid *qdruid)
{
	GdaEntityField *field;
	gint pos;

	field = (GdaEntityField *) gnome_db_selector_get_selected_object (GNOME_DB_SELECTOR (qdruid->priv->fields_fields_selector));
	g_assert (field && GDA_IS_ENTITY_FIELD (field));
	pos = gda_entity_get_field_index (GDA_ENTITY (qdruid->priv->build_query), field);
	if (pos > 0) {
		GdaEntityField *ofield;
		ofield = gda_entity_get_field_by_index (GDA_ENTITY (qdruid->priv->build_query), pos - 1);
		gda_entity_swap_fields (GDA_ENTITY (qdruid->priv->build_query), field, ofield);
	}

#ifdef debug
	gda_object_dump (GDA_OBJECT (qdruid->priv->build_query), 0);
#endif
}

static void
select_fields_dn_field (GtkButton *button, QueryDruid *qdruid)
{
	GdaEntityField *field;
	gint pos;
	GdaEntityField *ofield;

	field = (GdaEntityField *) gnome_db_selector_get_selected_object (GNOME_DB_SELECTOR (qdruid->priv->fields_fields_selector));
	g_assert (field && GDA_IS_ENTITY_FIELD (field));
	pos = gda_entity_get_field_index (GDA_ENTITY (qdruid->priv->build_query), field);
	ofield = gda_entity_get_field_by_index (GDA_ENTITY (qdruid->priv->build_query), pos + 1);
	if (ofield)
		gda_entity_swap_fields (GDA_ENTITY (qdruid->priv->build_query), field, ofield);

#ifdef debug
	gda_object_dump (GDA_OBJECT (qdruid->priv->build_query), 0);
#endif
}

static void
select_fields_del_field (GtkButton *button, QueryDruid *qdruid)
{
	GdaEntityField *field;

	field = (GdaEntityField *) gnome_db_selector_get_selected_object (GNOME_DB_SELECTOR (qdruid->priv->fields_fields_selector));
	g_assert (field && GDA_IS_ENTITY_FIELD (field));
	gda_entity_remove_field (GDA_ENTITY (qdruid->priv->build_query), field);
	
#ifdef debug
	gda_object_dump (GDA_OBJECT (qdruid->priv->build_query), 0);
#endif
}


static void
make_select_where_page (QueryDruid *qdruid)
{
	GtkWidget *wid, *vbox, *label;
	gchar *str;

	/* page definition */
	vbox = gtk_vbox_new (FALSE, 0);
	gtk_container_set_border_width (GTK_CONTAINER (vbox), 16);

	label = gtk_label_new (NULL);
	str = g_strdup_printf ("<b>%s</b>\n%s", _("Selection condition and ordering:"),
			       _("Enter the conditions to apply on the selected rows "
				 "to filter the final rows, and the ordering"));
	gtk_label_set_markup (GTK_LABEL (label), str);
	g_free (str);
	gtk_label_set_line_wrap (GTK_LABEL (label), TRUE);
	gtk_misc_set_alignment (GTK_MISC (label), 0., -1);
	gtk_box_pack_start (GTK_BOX (vbox), label, FALSE, TRUE, 0);

	wid = query_fields_editor_new (qdruid->priv->build_query, QEF_FILTER_OPTION | QEF_ORDER_OPTION);
	gtk_box_pack_start (GTK_BOX (vbox), wid, TRUE, TRUE, 0);

	/* add page to GtkAssistant */
	qdruid->priv->pages_index [PAGE_QUERY_SELECT_COND] = gtk_assistant_append_page (GTK_ASSISTANT (qdruid), vbox);
	gtk_widget_show_all (vbox);
	gtk_assistant_set_page_title (GTK_ASSISTANT (qdruid), vbox, _("SELECT query: condition and ordering"));
	gtk_assistant_set_page_complete (GTK_ASSISTANT (qdruid), vbox, TRUE);
}

static void
make_select_params_page (QueryDruid *qdruid)
{
	GtkWidget *wid, *vbox;

	/* page definition */
	vbox = gtk_vbox_new (FALSE, 0);
	gtk_container_set_border_width (GTK_CONTAINER (vbox), 16);
	
	wid = gtk_label_new ("Parameters");
	gtk_box_pack_start (GTK_BOX (vbox), wid, FALSE, FALSE, 0);

	/* add page to GtkAssistant */
	qdruid->priv->pages_index [PAGE_QUERY_PARAMS] = gtk_assistant_append_page (GTK_ASSISTANT (qdruid), vbox);
	gtk_widget_show_all (vbox);
	gtk_assistant_set_page_title (GTK_ASSISTANT (qdruid), vbox, _("SELECT query: parameters definition"));
	gtk_assistant_set_page_complete (GTK_ASSISTANT (qdruid), vbox, TRUE);
}

static void
make_select_apply_page (QueryDruid *qdruid)
{
	GtkWidget *label;

	label = gtk_label_new (_("The selection query is now ready."));
	qdruid->priv->pages_index [PAGE_CONFIRM] = gtk_assistant_append_page (GTK_ASSISTANT (qdruid), label);
	gtk_widget_show (label);
	gtk_assistant_set_page_title (GTK_ASSISTANT (qdruid), label, _("Query: ready"));
	gtk_assistant_set_page_type (GTK_ASSISTANT (qdruid), label, GTK_ASSISTANT_PAGE_CONFIRM);
	gtk_assistant_set_page_complete (GTK_ASSISTANT (qdruid), label, TRUE);
}


/*
 * Query from SQL page
 */
static void sql_query_text_changed_cb (GtkButton *button, QueryDruid *qdruid);
static void
make_sql_query_page (QueryDruid *qdruid)
{
	GtkWidget *wid, *vbox, *bbox;
	gchar *str;

	/* page definition */
	vbox = gtk_vbox_new (FALSE, 0);
	gtk_container_set_border_width (GTK_CONTAINER (vbox), 16);

	str = g_strdup_printf ("<b>%s</b>", _("Enter the SQL statement representing the query:"));
	wid = gtk_label_new (str);
	g_free (str);
	gtk_box_pack_start (GTK_BOX (vbox), wid, FALSE, FALSE, 0);
	gtk_label_set_use_markup (GTK_LABEL (wid), TRUE);
	gtk_label_set_justify (GTK_LABEL (wid), GTK_JUSTIFY_LEFT);
	gtk_misc_set_alignment (GTK_MISC (wid), 0, 0.5);

	wid = gnome_db_editor_new ();
	qdruid->priv->sql_editor = wid;
        gnome_db_editor_set_editable (GNOME_DB_EDITOR (wid), TRUE);
        gnome_db_editor_set_highlight (GNOME_DB_EDITOR (wid), TRUE);
        gtk_box_pack_start (GTK_BOX (vbox), wid, TRUE, TRUE, 0);

	bbox = gtk_hbutton_box_new ();
	gtk_box_pack_start (GTK_BOX (vbox), bbox, FALSE, TRUE, 0);
	gtk_button_box_set_layout (GTK_BUTTON_BOX (bbox), GTK_BUTTONBOX_SPREAD);

	wid = gtk_button_new_with_label (_("Test query validity"));
	gtk_box_pack_start (GTK_BOX (bbox), wid, FALSE, TRUE, 0);
	g_signal_connect (G_OBJECT (wid), "clicked", G_CALLBACK (sql_query_text_changed_cb), qdruid);

	/* add page to GtkAssistant */
	qdruid->priv->pages_index [PAGE_SQL] = gtk_assistant_append_page (GTK_ASSISTANT (qdruid), vbox);
	gtk_widget_show_all (vbox);
	gtk_assistant_set_page_title (GTK_ASSISTANT (qdruid), vbox, _("SQL statement Query"));
	gtk_assistant_set_page_complete (GTK_ASSISTANT (qdruid), vbox, TRUE);
}


static void
sql_query_text_changed_cb (GtkButton *button, QueryDruid *qdruid)
{
	GdaQuery *query;
	gchar *sql;
	GError *error = NULL;
	gchar *str = NULL;
	GtkWidget *dlg, *toplevel;

	toplevel = gtk_widget_get_toplevel (GTK_WIDGET (qdruid));
	if (!GTK_WIDGET_TOPLEVEL (toplevel))
		toplevel = NULL;

	sql = gnome_db_editor_get_all_text (GNOME_DB_EDITOR (qdruid->priv->sql_editor));
	query = gda_query_new_from_sql (qdruid->priv->dict, sql, &error);
	if (gda_query_get_query_type (query) ==  GDA_QUERY_TYPE_NON_PARSED_SQL) {
		if (error) {
			str = g_strdup_printf ("<b>%s</b>\n%s", _("Parsing/analyzing error:"), error->message);
			g_error_free (error);
		}
		else
			str = g_strdup_printf ("<b>%s</b>\n%s",_("Parsing/analyzing error:"),
					       _("The query has an error"));
	}
	
	g_object_unref (G_OBJECT (query));

	if (str) {
		dlg = gtk_message_dialog_new ((GtkWindow*) toplevel, GTK_DIALOG_DESTROY_WITH_PARENT,
					      GTK_MESSAGE_WARNING, GTK_BUTTONS_CLOSE, " ");
		gtk_message_dialog_set_markup (GTK_MESSAGE_DIALOG (dlg), str);
		g_free (str);
	}
	else {
		str = g_strdup_printf ("<b>%s</b>", _("No error parsing and analyzing the query"));
		dlg = gtk_message_dialog_new_with_markup ((GtkWindow*) toplevel, GTK_DIALOG_DESTROY_WITH_PARENT,
							  GTK_MESSAGE_INFO, GTK_BUTTONS_CLOSE, str);
		g_free (str);
	}
	
	gtk_dialog_run (GTK_DIALOG (dlg));
	gtk_widget_destroy (dlg);
}

/*
 * Pages flow definition
 */
static gint
assistant_forward_func (gint current_page, QueryDruid *qdruid)
{
	gint next_page = -1;

	gnome_db_selector_expand_all (GNOME_DB_SELECTOR (qdruid->priv->fields_fields_selector));
	gnome_db_selector_expand_all (GNOME_DB_SELECTOR (qdruid->priv->fields_targets_selector));

	if (current_page == qdruid->priv->pages_index [PAGE_START]) 
		next_page = qdruid->priv->pages_index [PAGE_QUERY_TYPE];

	else if (current_page == qdruid->priv->pages_index [PAGE_QUERY_TYPE]) {
		GtkWidget *page = gtk_assistant_get_nth_page (GTK_ASSISTANT (qdruid), current_page);
		gint query_type = GPOINTER_TO_INT (g_object_get_data (G_OBJECT (page), "query_type"));
 
		switch (query_type) {
		case -2:
			next_page = qdruid->priv->pages_index [PAGE_SQL];
			break;
		case GDA_QUERY_TYPE_SELECT:
			next_page = qdruid->priv->pages_index [PAGE_QUERY_SELECT_TARGETS];
			break;
		default:
			TO_IMPLEMENT;
			next_page = current_page;
		break;
		}
	}

	else if (current_page == qdruid->priv->pages_index [PAGE_QUERY_SELECT_TARGETS]) {
		/* if there are less than 2 targets, then skip the joins page */
		GSList *targets = gda_query_get_targets (qdruid->priv->build_query);
		if (g_slist_length (targets) < 2) 
			next_page = qdruid->priv->pages_index [PAGE_QUERY_SELECT_FIELDS];
		else
			next_page = qdruid->priv->pages_index [PAGE_QUERY_SELECT_JOINS];
		g_slist_free (targets);
	}
	
	else if (current_page == qdruid->priv->pages_index [PAGE_QUERY_SELECT_JOINS])
		next_page = qdruid->priv->pages_index [PAGE_QUERY_SELECT_FIELDS];

	else if (current_page == qdruid->priv->pages_index [PAGE_QUERY_SELECT_FIELDS])
		next_page = qdruid->priv->pages_index [PAGE_QUERY_SELECT_COND];
	
	else if (current_page == qdruid->priv->pages_index [PAGE_QUERY_SELECT_COND])
		next_page = qdruid->priv->pages_index [PAGE_CONFIRM];

	else if (current_page == qdruid->priv->pages_index [PAGE_SQL])
		next_page = qdruid->priv->pages_index [PAGE_CONFIRM];

	return next_page;
}

static gboolean idle_destroy_druid (QueryDruid *qdruid);
static void
apply_page_cb (QueryDruid *qdruid, gpointer data)
{
	GdaQuery *created_query = NULL;
	GtkWidget *page = gtk_assistant_get_nth_page (GTK_ASSISTANT (qdruid), 
						      qdruid->priv->pages_index [PAGE_QUERY_TYPE]);
	gint query_type = GPOINTER_TO_INT (g_object_get_data (G_OBJECT (page), "query_type"));

	switch (query_type) {
	case -2: /* Raw SQL */ {
		GdaQuery *query;
		gchar *sql;
		const gchar *str;

		sql = gnome_db_editor_get_all_text (GNOME_DB_EDITOR (qdruid->priv->sql_editor));
		query = gda_query_new_from_sql (qdruid->priv->dict, sql, NULL);
		str = gtk_entry_get_text (GTK_ENTRY (qdruid->priv->query_name));
		if (str && *str)
			gda_object_set_name (GDA_OBJECT (query), str);
		str = gtk_entry_get_text (GTK_ENTRY (qdruid->priv->query_descr));
		if (str && *str)
			gda_object_set_description (GDA_OBJECT (query), str);

		gda_dict_assume_object (qdruid->priv->dict, (GdaObject *) query);
		created_query = query;
		g_object_unref (G_OBJECT (query));

		break;
	}
	case GDA_QUERY_TYPE_SELECT: {
		GdaGraph *graph;
		gda_dict_assume_object (qdruid->priv->dict, (GdaObject *) qdruid->priv->build_query);
		created_query = qdruid->priv->build_query;

		graph = gda_graphs_get_graph_for_object (qdruid->priv->dict, G_OBJECT (qdruid->priv->build_query));
		gda_dict_assume_object (qdruid->priv->dict, (GdaObject *) graph);

		break;
	}
	default:
		TO_IMPLEMENT;
		break;
	}

	if (created_query) {
		gda_object_set_name (GDA_OBJECT (created_query), 
				     gtk_entry_get_text (GTK_ENTRY (qdruid->priv->query_name)));
		gda_object_set_description (GDA_OBJECT (created_query), 
					    gtk_entry_get_text (GTK_ENTRY (qdruid->priv->query_descr)));
	}

	g_idle_add ((GSourceFunc) idle_destroy_druid, qdruid);
}

static gboolean
idle_destroy_druid (QueryDruid *qdruid)
{
	gtk_widget_destroy (GTK_WIDGET (qdruid));
	return FALSE;
}
