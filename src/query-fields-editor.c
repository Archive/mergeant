/* query-fields-editor.c
 *
 * Copyright (C) 2004 - 2007 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "query-fields-editor.h"
#include "query-fields-menu.h"
#include <glib/gi18n-lib.h>

static void query_fields_editor_class_init (QueryFieldsEditorClass * class);
static void query_fields_editor_init (QueryFieldsEditor * wid);
static void query_fields_editor_dispose (GObject   * object);


struct _QueryFieldsEditorPriv
{
	GdaQuery     *query;
	
	GtkTreeModel *model;
	GtkTreeView  *view;
	guint         mode;
	GtkWidget    *del_button;
	GtkWidget    *up_button;
	GtkWidget    *down_button;
};

/* get a pointer to the parents to be able to call their destructor */
static GObjectClass *parent_class = NULL;

GType
query_fields_editor_get_type (void)
{
	static GType type = 0;

	if (!type) {
		static const GTypeInfo info = {
			sizeof (QueryFieldsEditorClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) query_fields_editor_class_init,
			NULL,
			NULL,
			sizeof (QueryFieldsEditor),
			0,
			(GInstanceInitFunc) query_fields_editor_init
		};		
		
		type = g_type_register_static (GTK_TYPE_VBOX, "QueryFieldsEditor", &info, 0);
	}

	return type;
}

static void
query_fields_editor_class_init (QueryFieldsEditorClass * class)
{
	GObjectClass   *object_class = G_OBJECT_CLASS (class);
	
	parent_class = g_type_class_peek_parent (class);

	object_class->dispose = query_fields_editor_dispose;
}

enum {
	COLUMN_FIELD_PTR,   /* pointer to GnomeDbQField obj */
	COLUMN_FIELD_TXT,
	COLUMN_FIELD_EDIT,  /* TRUE if field text can be edited: for GdaQueryFieldValue, GdaQueryFieldFunc, etc */
	COLUMN_SHOWN,       /* TRUE if field visible && not internal */
	COLUMN_ALIAS,
	COLUMN_FILTER,
	COLUMN_FILTER_ENABLE, /* TRUE if the WHERE condition is composed of AND'ed individual conditions */
	COLUMN_FILTER_COND,   /* GdaQueryCondition pointer if COLUMN_FILTER does have some text */
	COLUMN_ORDER_NUMBER, /* -1 if not ordered */
	COLUMN_ORDER_ENABLE, /* TRUE if field is in the ORDER BY */
	COLUMN_ORDER_ASC,
	COLUMN_ERROR,
	N_COLUMNS
};

static void
query_fields_editor_init (QueryFieldsEditor * wid)
{
	wid->priv = g_new0 (QueryFieldsEditorPriv, 1);
	wid->priv->query = NULL;
	wid->priv->model = GTK_TREE_MODEL (gtk_list_store_new (N_COLUMNS,
							       G_TYPE_POINTER,
							       G_TYPE_STRING,
							       G_TYPE_BOOLEAN,
							       G_TYPE_BOOLEAN,
							       G_TYPE_STRING,
							       G_TYPE_STRING,
							       G_TYPE_BOOLEAN,
							       G_TYPE_POINTER,
							       G_TYPE_STRING,
							       G_TYPE_BOOLEAN,
							       G_TYPE_STRING,
							       G_TYPE_BOOLEAN));
	wid->priv->del_button = NULL;
}

static void query_fields_editor_initialize (QueryFieldsEditor *mgsel);


static void query_destroyed_cb (GdaQuery *query, QueryFieldsEditor *mgsel);
static void query_changed_cb (GdaQuery *query, QueryFieldsEditor *fedit);

/**
 * query_fields_editor_new
 * @query: a #GdaQuery object
 *
 * Creates a new #QueryFieldsEditor widget.
 *
 * Returns: the new widget
 */
GtkWidget *
query_fields_editor_new (GdaQuery *query, guint mode)
{
	GObject    *obj;
	QueryFieldsEditor *fedit;

	g_return_val_if_fail (query && GDA_IS_QUERY (query), NULL);
		
	obj = g_object_new (QUERY_FIELDS_EDITOR_TYPE, NULL);
	fedit = QUERY_FIELDS_EDITOR (obj);

	fedit->priv->query = query;
	fedit->priv->mode = mode;

	gda_object_connect_destroy (fedit->priv->query,
				 G_CALLBACK (query_destroyed_cb), fedit);
	g_signal_connect (G_OBJECT (fedit->priv->query), "changed",
			  G_CALLBACK (query_changed_cb), fedit);
	
	query_fields_editor_initialize (fedit);

	return GTK_WIDGET (obj);
}

static void
query_destroyed_cb (GdaQuery *query, QueryFieldsEditor *mgsel)
{
	gtk_widget_destroy (GTK_WIDGET (mgsel));
}

static void
query_fields_editor_dispose (GObject *object)
{
	QueryFieldsEditor *fedit;

	g_return_if_fail (object != NULL);
	g_return_if_fail (IS_QUERY_FIELDS_EDITOR (object));
	fedit = QUERY_FIELDS_EDITOR (object);

	if (fedit->priv) {
		/* Weak unref the GdaQuery if necessary */
		if (fedit->priv->query) {
			g_signal_handlers_disconnect_by_func (G_OBJECT (fedit->priv->query),
							      G_CALLBACK (query_destroyed_cb), fedit);
			g_signal_handlers_disconnect_by_func (G_OBJECT (fedit->priv->query),
							      G_CALLBACK (query_changed_cb), fedit);
		}

		if (fedit->priv->model)
			g_object_unref (fedit->priv->model);

		/* the private area itself */
		g_free (fedit->priv);
		fedit->priv = NULL;
	}

	/* for the parent class */
	parent_class->dispose (object);
}

static void renderer_field_txt_edited_cb      (GtkCellRendererText *renderer, 
					       gchar *tree_path, gchar *new_text, QueryFieldsEditor *fedit);
static void renderer_field_shown_toggled_cb   (GtkCellRendererToggle *renderer,
					       gchar *tree_path, QueryFieldsEditor *fedit);
static void renderer_field_alias_edited_cb    (GtkCellRendererText *renderer, 
					       gchar *tree_path, gchar *new_text, QueryFieldsEditor *fedit);
static void renderer_field_filter_edited_cb    (GtkCellRendererText *renderer, 
					       gchar *tree_path, gchar *new_text, QueryFieldsEditor *fedit);
static void renderer_field_orderno_edited_cb  (GtkCellRendererText *renderer, 
					       gchar *tree_path, gchar *new_text, QueryFieldsEditor *fedit);
static void renderer_field_orderty_edited_cb  (GtkCellRendererText *renderer, 
					       gchar *tree_path, gchar *new_text, QueryFieldsEditor *fedit);
static void field_selection_changed_cb        (GtkTreeSelection *selection, QueryFieldsEditor *fedit);
static void action_drop_field_cb              (GtkButton *button, QueryFieldsEditor *fedit);
static void action_up_field_cb                (GtkButton *button, QueryFieldsEditor *fedit);
static void action_down_field_cb              (GtkButton *button, QueryFieldsEditor *fedit);
static void
query_fields_editor_initialize (QueryFieldsEditor *fedit)
{
	GtkWidget *sw, *tv, *hbox, *bbox, *button, *wid;
	GtkCellRenderer *renderer;
	GtkTreeViewColumn *column;
	GtkTreeSelection *select;

	hbox = gtk_hbox_new (FALSE, 0);
	gtk_box_pack_start (GTK_BOX (fedit), hbox, TRUE, TRUE, 0);
	gtk_widget_show (hbox);

	/*
	 * TreeView initialization
	 */
	sw = gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (sw),
					GTK_POLICY_AUTOMATIC,
					GTK_POLICY_AUTOMATIC);
	gtk_box_pack_start (GTK_BOX (hbox), sw, TRUE, TRUE, 0);
	gtk_widget_show (sw);

	tv = gtk_tree_view_new_with_model (fedit->priv->model);
	gtk_container_add (GTK_CONTAINER (sw), tv);
	gtk_widget_show (tv);
	fedit->priv->view = GTK_TREE_VIEW (tv);

	select = gtk_tree_view_get_selection (GTK_TREE_VIEW (tv));
	gtk_tree_selection_set_mode (select, GTK_SELECTION_SINGLE);
	g_signal_connect (G_OBJECT (select), "changed",
			  G_CALLBACK (field_selection_changed_cb), fedit);

	
	/* field TXT */
	renderer = gtk_cell_renderer_text_new ();
	g_signal_connect (G_OBJECT (renderer), "edited",
			  G_CALLBACK (renderer_field_txt_edited_cb), fedit);
	g_object_set (G_OBJECT (renderer), "strikethrough", TRUE, NULL);
	column = gtk_tree_view_column_new_with_attributes (_("Field"),
							   renderer,
							   "text", COLUMN_FIELD_TXT,
							   "editable", COLUMN_FIELD_EDIT,
							   "strikethrough-set", COLUMN_ERROR,
							   NULL);
	gtk_tree_view_append_column (GTK_TREE_VIEW (tv), column);

	/* shown / alias */
	if (fedit->priv->mode & QEF_SHOW_OPTION) {
		renderer = gtk_cell_renderer_toggle_new ();
		g_signal_connect (G_OBJECT (renderer), "toggled",
				  G_CALLBACK (renderer_field_shown_toggled_cb), fedit);
		column = gtk_tree_view_column_new_with_attributes (_("Shown & alias"),
								   renderer,
								   "active", COLUMN_SHOWN,
								   NULL);
		renderer = gtk_cell_renderer_text_new ();
		g_signal_connect (G_OBJECT (renderer), "edited",
				  G_CALLBACK (renderer_field_alias_edited_cb), fedit);
		g_object_set (G_OBJECT (renderer), "strikethrough", TRUE, NULL);
		gtk_tree_view_column_pack_start (column, renderer, TRUE);
		gtk_tree_view_column_set_attributes (column, renderer, 
						     "text", COLUMN_ALIAS,
						     "editable", COLUMN_SHOWN,
						     "strikethrough-set", COLUMN_ERROR,
						     NULL);
		gtk_tree_view_append_column (GTK_TREE_VIEW (tv), column);
	}

	/* filter column */
	if (fedit->priv->mode & QEF_FILTER_OPTION) {
		renderer = gtk_cell_renderer_text_new ();
		g_signal_connect (G_OBJECT (renderer), "edited",
				  G_CALLBACK (renderer_field_filter_edited_cb), fedit);
		g_object_set (G_OBJECT (renderer), "strikethrough", TRUE, NULL);
		column = gtk_tree_view_column_new_with_attributes (_("Filter"),
								   renderer,
								   "text", COLUMN_FILTER,
								   "editable", COLUMN_FILTER_ENABLE,
								   "strikethrough-set", COLUMN_ERROR,
								   NULL);
		gtk_tree_view_append_column (GTK_TREE_VIEW (tv), column);
		gtk_tree_view_column_set_expand (column, TRUE);
	}

	/* GROUP By */
	
	/* ORDER By */
	if (fedit->priv->mode & QEF_ORDER_OPTION) {
		renderer = gtk_cell_renderer_text_new ();
		g_signal_connect (G_OBJECT (renderer), "edited",
				  G_CALLBACK (renderer_field_orderno_edited_cb), fedit);
		g_object_set (G_OBJECT (renderer), "strikethrough", TRUE, NULL);
		column = gtk_tree_view_column_new_with_attributes (_("Ordering"),
								   renderer,
								   "text", COLUMN_ORDER_NUMBER,
								   "strikethrough-set", COLUMN_ERROR,
								   NULL);
		g_object_set (G_OBJECT (renderer), "editable", TRUE, NULL);
		renderer = gtk_cell_renderer_text_new ();
		g_signal_connect (G_OBJECT (renderer), "edited",
				  G_CALLBACK (renderer_field_orderty_edited_cb), fedit);
		g_object_set (G_OBJECT (renderer), "strikethrough", TRUE, NULL);
		gtk_tree_view_column_pack_start (column, renderer, TRUE);
		gtk_tree_view_column_set_attributes (column, renderer, 
						     "text", COLUMN_ORDER_ASC,
						     "editable", COLUMN_ORDER_ENABLE,
						     "strikethrough-set", COLUMN_ERROR,
						     NULL);
		gtk_tree_view_append_column (GTK_TREE_VIEW (tv), column);
	}

	/*
	 * Buttons
	 */
	if (fedit->priv->mode & QEF_ACTION_BUTTONS) {
		bbox = gtk_vbutton_box_new ();
		gtk_button_box_set_layout (GTK_BUTTON_BOX (bbox), GTK_BUTTONBOX_START);
		gtk_box_pack_start (GTK_BOX (hbox), bbox, FALSE, FALSE, 6);
		gtk_widget_show (bbox);
		
		button = query_fields_menu_new (fedit->priv->query);
		gtk_container_add (GTK_CONTAINER (bbox), button);
		gtk_widget_show (button);
		
		button = gtk_button_new ();
		wid = gtk_image_new_from_stock (GTK_STOCK_DELETE, GTK_ICON_SIZE_BUTTON);
		gtk_widget_show (wid);
		gtk_container_add (GTK_CONTAINER (button), wid);
		gtk_container_add (GTK_CONTAINER (bbox), button);
		gtk_widget_show (button);
		gtk_widget_set_sensitive (button, FALSE);
		fedit->priv->del_button = button;
		g_signal_connect (G_OBJECT (button), "clicked",
				  G_CALLBACK (action_drop_field_cb), fedit);

		button = gtk_button_new ();
		wid = gtk_image_new_from_stock (GTK_STOCK_GO_UP, GTK_ICON_SIZE_BUTTON);
		gtk_widget_show (wid);
		gtk_container_add (GTK_CONTAINER (button), wid);
		gtk_container_add (GTK_CONTAINER (bbox), button);
		gtk_widget_show (button);
		gtk_widget_set_sensitive (button, FALSE);
		fedit->priv->up_button = button;
		g_signal_connect (G_OBJECT (button), "clicked",
				  G_CALLBACK (action_up_field_cb), fedit);

		button = gtk_button_new ();
		wid = gtk_image_new_from_stock (GTK_STOCK_GO_DOWN, GTK_ICON_SIZE_BUTTON);
		gtk_widget_show (wid);
		gtk_container_add (GTK_CONTAINER (button), wid);
		gtk_container_add (GTK_CONTAINER (bbox), button);
		gtk_widget_show (button);
		gtk_widget_set_sensitive (button, FALSE);
		fedit->priv->down_button = button;
		g_signal_connect (G_OBJECT (button), "clicked",
				  G_CALLBACK (action_down_field_cb), fedit);
	}

	/*
	 * Model initialization
	 */
	query_changed_cb (fedit->priv->query, fedit);
}

/*
 * Fetch the GdaQueryField held at row described by @tree_path. if @iter is not NULL, then sets
 * it to represent the corresponding row
 */
static GdaQueryField *
model_get_field_from_path (GtkTreeModel *model, GtkTreeIter *iter, const gchar *tree_path)
{
	GtkTreeIter tmpiter;
	GtkTreePath *path;
	GdaQueryField *field = NULL;	

	path = gtk_tree_path_new_from_string (tree_path);
	if (gtk_tree_model_get_iter (model, &tmpiter, path)) {
		gtk_tree_model_get (model, &tmpiter, COLUMN_FIELD_PTR, &field, -1);
		if (iter)
			*iter = tmpiter;
	}
	gtk_tree_path_free (path);

	return field;
}

/*
 * Sets @iter to point to the row representing @field
 */
static gboolean get_iter_for_field (QueryFieldsEditor *fedit, GtkTreeIter *iter, GdaEntityField *field)
{
	GdaEntityField *tmpfield;

	g_return_val_if_fail (iter, FALSE);
	g_return_val_if_fail (field, FALSE);

	if (! gtk_tree_model_get_iter_first (fedit->priv->model, iter))
		return FALSE;

	gtk_tree_model_get (fedit->priv->model, iter, COLUMN_FIELD_PTR, &tmpfield, -1);
	if (field == tmpfield)
		return TRUE;

	while (gtk_tree_model_iter_next (fedit->priv->model, iter)) {
		gtk_tree_model_get (fedit->priv->model, iter, COLUMN_FIELD_PTR, &tmpfield, -1);
		if (field == tmpfield)
			return TRUE;
	}

	return FALSE;
}


static void
renderer_field_txt_edited_cb (GtkCellRendererText *renderer, 
			      gchar *tree_path, gchar *new_text, QueryFieldsEditor *fedit)
{
	GdaQueryField *field;

	field = model_get_field_from_path (fedit->priv->model, NULL, tree_path);
	if (GDA_IS_QUERY_FIELD_VALUE (field)) {
		GValue *value;
		GType gtype;
		GdaDataHandler *dh;

		g_object_get (G_OBJECT (field), "gda-type", &gtype, NULL);
		dh = gda_dict_get_default_handler (gda_object_get_dict ((GdaObject *) field), gtype);
		value = gda_data_handler_get_value_from_sql (dh, new_text, 
							     gda_entity_field_get_g_type (GDA_ENTITY_FIELD (field)));
		if (value) {
			gda_query_field_value_set_value (GDA_QUERY_FIELD_VALUE (field), value);
			gda_value_free (value);
		}
	}
}

static void
renderer_field_shown_toggled_cb (GtkCellRendererToggle *renderer,
				 gchar *tree_path, QueryFieldsEditor *fedit)
{
	GdaQueryField *field;

	field = model_get_field_from_path (fedit->priv->model, NULL, tree_path);
	gda_query_field_set_visible (field, !gtk_cell_renderer_toggle_get_active (renderer));
}

static void
renderer_field_alias_edited_cb (GtkCellRendererText *renderer, 
				gchar *tree_path, gchar *new_text, QueryFieldsEditor *fedit)
{
	GdaQueryField *field;

	field = model_get_field_from_path (fedit->priv->model, NULL, tree_path);
	gda_query_field_set_alias (field, new_text);
}

static void
renderer_field_filter_edited_cb (GtkCellRendererText *renderer, 
				 gchar *tree_path, gchar *new_text, QueryFieldsEditor *fedit)
{
	GdaQueryField *field;
	GdaQueryCondition *newcond, *cond;
	GtkTreeIter iter;

	field = model_get_field_from_path (fedit->priv->model, &iter, tree_path);
	gtk_tree_model_get (fedit->priv->model, &iter, COLUMN_FILTER_COND, &cond, -1);

	if (new_text && *new_text) {
		GError *error = NULL;

		newcond = gda_query_condition_new_from_sql (fedit->priv->query, new_text, NULL, &error);
		if (!newcond) {
			gchar *sql;
			sql = gda_renderer_render_as_sql (GDA_RENDERER (field), NULL, NULL, 0, NULL);
			if (sql) {
				gchar *text = g_strdup_printf ("%s %s", sql, new_text);
				g_free (sql);
				newcond = gda_query_condition_new_from_sql (fedit->priv->query, text, NULL, NULL);
				g_free (text);
			}
		}

		if (!newcond) {
			GtkWidget *dlg;
			gchar *msg;

			if (error) {
				msg = g_strdup_printf ("<b>%s</b>\n%s '%s':\n\n%s",
						       _("Error parsing/analysing condition expression:"),
						       _("while parsing"),
						       new_text,
						       error->message);
				
				g_error_free (error);
			}
			else
				msg = g_strdup_printf ("<b>%s</b>\n%s '%s'",
						       _("Error parsing/analysing condition expression:"),
						       _("while parsing"),
						       new_text);
			
			dlg = gtk_message_dialog_new_with_markup (NULL, GTK_DIALOG_MODAL, GTK_MESSAGE_ERROR,
								  GTK_BUTTONS_CLOSE, msg);
			g_free (msg);
			gtk_dialog_run (GTK_DIALOG (dlg));
			gtk_widget_destroy (dlg);
			return;
		}
	}
	else
		newcond = NULL;

	if (newcond) {
		if (cond) {
			/* there was a previous condition */
			GdaQueryCondition *parent = gda_query_condition_get_parent (cond);
			if (parent) {
				GHashTable *repl;

				repl = g_hash_table_new (NULL, NULL);
				g_hash_table_insert (repl, cond, newcond);
				gda_referer_replace_refs (GDA_REFERER (parent), repl);
				g_hash_table_destroy (repl);
			}
			else 
				gda_query_set_condition (fedit->priv->query, newcond);
		}
		else 
			/* 'append' with an AND the new condition to the already existing one */
			gda_query_append_condition (fedit->priv->query, newcond, TRUE);

		g_object_unref (newcond);
	}
	else {
		if (cond) 
			gda_object_destroy (GDA_OBJECT (cond));
	}
}

static void
renderer_field_orderno_edited_cb  (GtkCellRendererText *renderer, 
				   gchar *tree_path, gchar *new_text, QueryFieldsEditor *fedit)
{
	GdaQueryField *field;
	gint order;

	field = model_get_field_from_path (fedit->priv->model, NULL, tree_path);
	order = atoi (new_text);
	if (order > 0) {
		gboolean asc;
		if (gda_query_get_order_by_field (fedit->priv->query, field, &asc) < 0)
			asc = TRUE;
		gda_query_set_order_by_field (fedit->priv->query, field, order - 1, asc);
	}
	else
		gda_query_set_order_by_field (fedit->priv->query, field, -1, TRUE);
		
}

static void
renderer_field_orderty_edited_cb  (GtkCellRendererText *renderer, 
				   gchar *tree_path, gchar *new_text, QueryFieldsEditor *fedit)
{
	GdaQueryField *field;
	gint order;

	field = model_get_field_from_path (fedit->priv->model, NULL, tree_path);

	order = gda_query_get_order_by_field (fedit->priv->query, field, NULL);
	gda_query_set_order_by_field (fedit->priv->query, field, order, 
				     new_text && ((*new_text == 'A') || (*new_text == 'a')));
}

static void 
field_selection_changed_cb (GtkTreeSelection *selection, QueryFieldsEditor *fedit)
{
	gint pos = 0;
	gboolean has_sel;
	GtkTreeIter iter;
	GdaEntityField *ofield = NULL;

	if (!fedit->priv->del_button)
		return;

	has_sel = gtk_tree_selection_get_selected (selection, NULL, &iter);
	if (has_sel) {
		GdaEntityField *field;

		gtk_tree_model_get (fedit->priv->model, &iter, COLUMN_FIELD_PTR, &field, -1);
		pos = gda_entity_get_field_index (GDA_ENTITY (fedit->priv->query), field);
		ofield = gda_entity_get_field_by_index (GDA_ENTITY (fedit->priv->query), pos + 1);
	}

	gtk_widget_set_sensitive (fedit->priv->del_button, has_sel);
	gtk_widget_set_sensitive (fedit->priv->up_button, has_sel && (pos > 0));
	gtk_widget_set_sensitive (fedit->priv->down_button, has_sel && ofield);
}

static void
action_drop_field_cb (GtkButton *button, QueryFieldsEditor *fedit)
{
	GtkTreeSelection *select;
	GtkTreeIter iter;

	select = gtk_tree_view_get_selection (GTK_TREE_VIEW (fedit->priv->view));
	if (gtk_tree_selection_get_selected (select, NULL, &iter)) {
		GdaQueryField *field;

		gtk_tree_model_get (fedit->priv->model, &iter, COLUMN_FIELD_PTR, &field, -1);
		gda_entity_remove_field (GDA_ENTITY (fedit->priv->query), GDA_ENTITY_FIELD (field));
	}
}

static void
action_up_field_cb (GtkButton *button, QueryFieldsEditor *fedit)
{
	GtkTreeSelection *select;
	GtkTreeIter iter;

	select = gtk_tree_view_get_selection (GTK_TREE_VIEW (fedit->priv->view));
	if (gtk_tree_selection_get_selected (select, NULL, &iter)) {
		GdaEntityField *field;
		gint pos;

		gtk_tree_model_get (fedit->priv->model, &iter, COLUMN_FIELD_PTR, &field, -1);
		pos = gda_entity_get_field_index (GDA_ENTITY (fedit->priv->query), field);
		if (pos > 0) {
			GdaEntityField *ofield;
			ofield = gda_entity_get_field_by_index (GDA_ENTITY (fedit->priv->query), pos - 1);
			gda_entity_swap_fields (GDA_ENTITY (fedit->priv->query), field, ofield);

			if (get_iter_for_field (fedit, &iter, field))
				gtk_tree_selection_select_iter (select, &iter);
		}
	}
}

static void
action_down_field_cb (GtkButton *button, QueryFieldsEditor *fedit)
{
	GtkTreeSelection *select;
	GtkTreeIter iter;

	select = gtk_tree_view_get_selection (GTK_TREE_VIEW (fedit->priv->view));
	if (gtk_tree_selection_get_selected (select, NULL, &iter)) {
		GdaEntityField *field, *ofield;
		gint pos;

		gtk_tree_model_get (fedit->priv->model, &iter, COLUMN_FIELD_PTR, &field, -1);
		pos = gda_entity_get_field_index (GDA_ENTITY (fedit->priv->query), field);
		ofield = gda_entity_get_field_by_index (GDA_ENTITY (fedit->priv->query), pos + 1);
		if (ofield) {
			gda_entity_swap_fields (GDA_ENTITY (fedit->priv->query), field, ofield);

			if (get_iter_for_field (fedit, &iter, field))
				gtk_tree_selection_select_iter (select, &iter);
		}
	}
}

static GSList *compute_fields_to_display (QueryFieldsEditor *fedit);

/*
 * (re)-write all the rows of the model with the fields which do qualify to be displayed
 */
static void
query_changed_cb (GdaQuery *query, QueryFieldsEditor *fedit)
{
	GSList *list, *fields;
	GtkTreeIter iter;
	GtkTreeModel *model = fedit->priv->model;
	gboolean iter_valid = FALSE;
       
	fields = compute_fields_to_display (fedit);
	list = fields;
	while (list) {
		GdaQueryField *field = GDA_QUERY_FIELD (list->data);
		gchar *sql, *order_str = NULL, *asc_str = NULL;
		gint order;
		gboolean asc = TRUE;
		GdaQueryCondition *cond;
		gchar *cond_sql = NULL;
		
		/* fetch a new iter */
		if (!iter_valid) {
			/* fetch first iter, or create a new one */
			if (! gtk_tree_model_get_iter_first (model, &iter))
				gtk_list_store_append (GTK_LIST_STORE (model), &iter);
			iter_valid = TRUE;
		}
		else {
			/* fetch next iter, or create a new one */
			if (! gtk_tree_model_iter_next (model, &iter))
				gtk_list_store_append (GTK_LIST_STORE (model), &iter);
		}
		
		/* modify the model at iter */
		sql = gda_renderer_render_as_sql (GDA_RENDERER (field), NULL, NULL, 0, NULL);
		order = gda_query_get_order_by_field (fedit->priv->query, field, &asc);
		if (order >= 0) {
			order_str = g_strdup_printf ("%d", order + 1);
			asc_str = asc ? "ASC" : "DESC";
		}
		cond = g_object_get_data (G_OBJECT (field), "qf_filter_cond");
		g_object_set_data (G_OBJECT (field), "qf_filter_cond", NULL);
		if (cond) 
			cond_sql = gda_renderer_render_as_sql (GDA_RENDERER (cond), NULL, NULL,
							       GDA_RENDERER_PARAMS_AS_DETAILED,
							       NULL);
		gtk_list_store_set (GTK_LIST_STORE (model), &iter,
				    COLUMN_FIELD_PTR, field,
				    COLUMN_FIELD_TXT, sql ? sql : gda_object_get_name (GDA_OBJECT (field)),
				    COLUMN_FIELD_EDIT, GDA_IS_QUERY_FIELD_VALUE (field),
				    COLUMN_SHOWN, gda_query_field_is_visible (field),
				    COLUMN_ALIAS, gda_query_field_get_alias (field),
				    COLUMN_ORDER_NUMBER, order_str,
				    COLUMN_ORDER_ENABLE, (order >= 0) ? TRUE : FALSE,
				    COLUMN_ORDER_ASC, asc_str,
				    COLUMN_FILTER, cond_sql,
				    COLUMN_FILTER_COND, cond,
				    COLUMN_FILTER_ENABLE, TRUE /*cond ? TRUE : FALSE*/,
				    COLUMN_ERROR, sql ? FALSE : TRUE,
				    -1);
		g_free (sql);
		g_free (order_str);
		g_free (cond_sql);
		list = g_slist_next (list);
	}	
	g_slist_free (fields);

	/* clean the remaining rows */
	if (!iter_valid) {
		if (gtk_tree_model_get_iter_first (model, &iter))
			while (gtk_list_store_remove (GTK_LIST_STORE (model), &iter));
	}
	else
		if (gtk_tree_model_iter_next (model, &iter))
			while (gtk_list_store_remove (GTK_LIST_STORE (model), &iter));

	/* update action buttons' status */
	field_selection_changed_cb (gtk_tree_view_get_selection (fedit->priv->view), fedit);
}

static GSList *
compute_fields_to_display (QueryFieldsEditor *fedit)
{
	GSList *list, *fields;
	GSList *retval = NULL;
	GSList *main_conds = NULL;
	GdaQueryCondition *cond;
	
	cond = gda_query_get_condition (fedit->priv->query);
	if (cond)
		main_conds = gda_query_condition_get_main_conditions (cond);
	
	/* FIXME: add the fields which are GROUPed BY, and the ones in HAVING condition */	
	/* analyse all query's fields */
	fields = gda_query_get_all_fields (fedit->priv->query);
	list = fields;
	while (list) {
		GdaQueryField *field = GDA_QUERY_FIELD (list->data);
		gboolean to_include = FALSE;
		GSList *clist;

		/* fields as LEFT op of a main condition are always shown */
		clist = main_conds;
		while (clist && !to_include) {
			cond = GDA_QUERY_CONDITION (clist->data);
			if (gda_query_condition_is_leaf (cond) && 
			    (gda_query_condition_leaf_get_operator (cond, GDA_QUERY_CONDITION_OP_LEFT) == field)) {
				to_include = TRUE;
				g_object_set_data (G_OBJECT (field), "qf_filter_cond", cond);
			}
			clist = g_slist_next (clist);
		}
		if (to_include) {
			retval = g_slist_prepend (retval, field);
			list = g_slist_next (list);
			continue;
		}

		/* fields in ORDER BY are always shows */
		if (gda_query_get_order_by_field (fedit->priv->query, field, NULL) >= 0) {
			retval = g_slist_prepend (retval, field);
			list = g_slist_next (list);
			continue;
		}

		/* other tests */
		to_include = TRUE;
		if (gda_query_field_is_internal (field))
			to_include = FALSE;
		if (to_include && !gda_query_field_is_visible (field))
			to_include = FALSE;
		if (to_include)
			retval = g_slist_prepend (retval, field);

		list = g_slist_next (list);
	}
	
	g_slist_free (main_conds);
	g_slist_free (fields);

	return g_slist_reverse (retval);
}
