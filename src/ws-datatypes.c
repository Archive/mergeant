/* ws-datatypes.c
 *
 * Copyright (C) 2004 - 2007 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <libgnomedb-extra/libgnomedb-extra.h>
#include "ws-datatypes.h"
#include "workspace-page.h"
#include <string.h>
#include <glib-object.h>
#include <gtk/gtk.h>
#include <glib/gi18n-lib.h>

/* 
 * Main static functions 
 */
static void ws_datatypes_class_init (WsDatatypesClass * class);
static void ws_datatypes_init (WsDatatypes *ws);
static void ws_datatypes_dispose (GObject *object);
static void ws_datatypes_finalize (GObject *object);

/* WorkspacePage interface */
static void         ws_datatypes_page_init       (WorkspacePageIface *iface);
static gchar       *ws_datatypes_get_name        (WorkspacePage *iface);
static gchar       *ws_datatypes_get_description (WorkspacePage *iface);
static GtkWidget   *ws_datatypes_get_sel_button    (WorkspacePage *iface);
static GtkWidget   *ws_datatypes_get_selector    (WorkspacePage *iface);
static GtkWidget   *ws_datatypes_get_work_area   (WorkspacePage *iface);

/* get a pointer to the parents to be able to call their destructor */
static GObjectClass  *parent_class = NULL;

/* private structure */
struct _WsDatatypesPrivate
{
	GdaDict        *dict;
	GtkWidget      *selector;
	GtkWidget      *work_area;

	GtkWidget      *notebook;
	GtkWidget      *description;
	GtkWidget      *synonyms;
	gint            filter_mode;
	GtkTextBuffer  *textbuffer;

	GObject        *sel_obj;
};

GType
ws_datatypes_get_type (void)
{
	static GType type = 0;

	if (!type) {
		static const GTypeInfo info = {
			sizeof (WsDatatypesClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) ws_datatypes_class_init,
			NULL,
			NULL,
			sizeof (WsDatatypes),
			0,
			(GInstanceInitFunc) ws_datatypes_init
		};

		static const GInterfaceInfo workspace_page_info = {
			(GInterfaceInitFunc) ws_datatypes_page_init,
			NULL,
			NULL
		};

		type = g_type_register_static (G_TYPE_OBJECT, "WsDatatypes", &info, 0);
		g_type_add_interface_static (type, WORKSPACE_PAGE_TYPE, &workspace_page_info);
	}
	return type;
}

static void 
ws_datatypes_page_init (WorkspacePageIface *iface)
{
	iface->get_name = ws_datatypes_get_name;
	iface->get_description = ws_datatypes_get_description;
	iface->get_sel_button = ws_datatypes_get_sel_button;
	iface->get_selector = ws_datatypes_get_selector;
	iface->get_work_area = ws_datatypes_get_work_area;
}

static void
ws_datatypes_class_init (WsDatatypesClass * class)
{
	GObjectClass   *object_class = G_OBJECT_CLASS (class);

	parent_class = g_type_class_peek_parent (class);

	object_class->dispose = ws_datatypes_dispose;
	object_class->finalize = ws_datatypes_finalize;
}

static void
ws_datatypes_init (WsDatatypes *ws)
{
	ws->priv = g_new0 (WsDatatypesPrivate, 1);
}

static void ws_datatypes_initialize (WsDatatypes *ws);

/**
 * ws_datatypes_new
 * @dict: a #GdaDict object
 *
 * Creates a new WsDatatypes object
 *
 * Returns: the new object
 */
GObject*
ws_datatypes_new (GdaDict *dict)
{
	GObject  *obj;
	WsDatatypes *ws;

	g_return_val_if_fail (dict && GDA_IS_DICT (dict), NULL);

	obj = g_object_new (WS_DATATYPES_TYPE, NULL);
	ws = WS_DATATYPES (obj);
	ws->priv->dict = dict;
	g_object_ref (G_OBJECT (dict));

	ws_datatypes_initialize (ws);

	return obj;
}


static void
ws_datatypes_dispose (GObject *object)
{
	WsDatatypes *ws;

	g_return_if_fail (object != NULL);
	g_return_if_fail (IS_WS_DATATYPES (object));

	ws = WS_DATATYPES (object);
	if (ws->priv) {
		if (ws->priv->selector) {
			gtk_widget_destroy (ws->priv->selector);
			ws->priv->selector = NULL;
		}

		if (ws->priv->work_area) {
			gtk_widget_destroy (ws->priv->work_area);
			ws->priv->work_area = NULL;
		}

		if (ws->priv->dict) {
			g_object_unref (G_OBJECT (ws->priv->dict));
			ws->priv->dict = NULL;
		}
	}

	/* parent class */
	parent_class->dispose (object);
}

static void
ws_datatypes_finalize (GObject   * object)
{
	WsDatatypes *ws;

	g_return_if_fail (object != NULL);
	g_return_if_fail (IS_WS_DATATYPES (object));

	ws = WS_DATATYPES (object);
	if (ws->priv) {
		g_free (ws->priv);
		ws->priv = NULL;
	}

	/* parent class */
	parent_class->finalize (object);
}



static void selector_selection_changed_cb (GnomeDbSelector *mgsel, GObject *sel_object, WsDatatypes *ws);
static void dtypes_filter_mode_changed_cb (GtkWidget *wid, WsDatatypes *ws);
static void dtype_updated_cb (GdaConnection *srv, GdaDictType *dtype, WsDatatypes *ws);
static void
ws_datatypes_initialize (WsDatatypes *ws)
{
	GtkWidget *label, *vbox, *wid, *nb, *vbox2, *bbox, *hbox, *sw;
	GtkWidget *rd1, *rd2, *rd3;

	/* Selector part */
	wid = gnome_db_selector_new (ws->priv->dict, NULL,
			       GNOME_DB_SELECTOR_DATA_TYPES, 0);
	ws->priv->selector = wid;
	g_signal_connect (G_OBJECT (ws->priv->selector), "selection_changed", 
			  G_CALLBACK (selector_selection_changed_cb), ws);

	/* WorkArea part */
	vbox = gtk_vbox_new (FALSE, 5);
	ws->priv->work_area = vbox;
	
	nb = gtk_notebook_new ();
	gtk_notebook_set_show_border (GTK_NOTEBOOK (nb), FALSE);
	gtk_notebook_set_show_tabs (GTK_NOTEBOOK (nb), FALSE);
	gtk_box_pack_start (GTK_BOX (vbox), nb, TRUE, TRUE, 0);
	gtk_widget_show (nb);
	ws->priv->notebook = nb;
	
	label = gtk_label_new (_("Please select a data type from the list on the left"));
	gtk_notebook_append_page (GTK_NOTEBOOK (nb), label, NULL);
	gtk_widget_show (label);
	
	vbox2 = gtk_vbox_new (FALSE, 5);
	gtk_notebook_append_page (GTK_NOTEBOOK (nb), vbox2, NULL);
	gtk_widget_show (vbox2);

	label = gtk_label_new (NULL);
	gtk_label_set_markup (GTK_LABEL (label), _("<b>Description:</b>"));
	gtk_box_pack_start (GTK_BOX (vbox2), label, FALSE, FALSE, 0);
	gtk_widget_show (label);
	gtk_misc_set_alignment (GTK_MISC (label), 0., -1);

	hbox = gtk_hbox_new (FALSE, 0); /* HIG */
	gtk_box_pack_start (GTK_BOX (vbox2), hbox, FALSE, FALSE, 0);
	gtk_widget_show (hbox);
	label = gtk_label_new ("    ");
	gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);
	gtk_widget_show (label);

	label = gtk_label_new (NULL);
	gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);
	gtk_misc_set_alignment (GTK_MISC (label), 0., -1);
	gtk_widget_show (label);
	ws->priv->description = label;

	label = gtk_label_new (NULL);
	gtk_label_set_markup (GTK_LABEL (label), _("<b>Synonyms:</b>"));
	gtk_box_pack_start (GTK_BOX (vbox2), label, FALSE, FALSE, 0);
	gtk_widget_show (label);
	gtk_misc_set_alignment (GTK_MISC (label), 0., -1);

	hbox = gtk_hbox_new (FALSE, 0); /* HIG */
	gtk_box_pack_start (GTK_BOX (vbox2), hbox, FALSE, FALSE, 0);
	gtk_widget_show (hbox);
	label = gtk_label_new ("    ");
	gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);
	gtk_widget_show (label);

	label = gtk_label_new (NULL);
	gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);
	gtk_misc_set_alignment (GTK_MISC (label), 0., -1);
	gtk_widget_show (label);
	ws->priv->synonyms = label;
	
	label = gtk_label_new (NULL);
	gtk_label_set_markup (GTK_LABEL (label), _("<b>Select a filter option:</b>"));
	gtk_box_pack_start (GTK_BOX (vbox2), label, FALSE, FALSE, 0);
	gtk_widget_show (label);
	gtk_misc_set_alignment (GTK_MISC (label), 0., -1);

	hbox = gtk_hbox_new (FALSE, 0); /* HIG */
	gtk_box_pack_start (GTK_BOX (vbox2), hbox, FALSE, FALSE, 0);
	gtk_widget_show (hbox);
	label = gtk_label_new ("    ");
	gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);
	gtk_widget_show (label);

	bbox = gtk_vbutton_box_new ();
	gtk_box_pack_start (GTK_BOX (hbox), bbox, FALSE, FALSE, 0);
	gtk_widget_show (bbox);

	rd1 = gtk_radio_button_new_with_label (NULL, _("Functions returning this data type"));
	gtk_box_pack_start (GTK_BOX (bbox), rd1, FALSE, FALSE, 0);
	gtk_widget_show (rd1);
	g_object_set_data (G_OBJECT (rd1), "mode", GINT_TO_POINTER (0));
	g_signal_connect (G_OBJECT (rd1), "toggled",
			  G_CALLBACK (dtypes_filter_mode_changed_cb), ws);

	rd2 = gtk_radio_button_new_with_label_from_widget (GTK_RADIO_BUTTON (rd1),
							   _("Functions using this data type"));
	gtk_box_pack_start (GTK_BOX (bbox), rd2, FALSE, FALSE, 0);
	gtk_widget_show (rd2);
	g_object_set_data (G_OBJECT (rd2), "mode", GINT_TO_POINTER (1));
	g_signal_connect (G_OBJECT (rd2), "toggled",
			  G_CALLBACK (dtypes_filter_mode_changed_cb), ws);

	rd3 = gtk_radio_button_new_with_label_from_widget (GTK_RADIO_BUTTON (rd1),
							   _("Aggregates using this data type"));
	gtk_box_pack_start (GTK_BOX (bbox), rd3, FALSE, FALSE, 0);
	gtk_widget_show (rd3);
	g_object_set_data (G_OBJECT (rd3), "mode", GINT_TO_POINTER (2));
	g_signal_connect (G_OBJECT (rd3), "toggled",
			  G_CALLBACK (dtypes_filter_mode_changed_cb), ws);

	label = gtk_label_new (NULL);
	gtk_label_set_markup (GTK_LABEL (label), _("<b>Result of filter:</b>"));
	gtk_box_pack_start (GTK_BOX (vbox2), label, FALSE, FALSE, 0);
	gtk_widget_show (label);
	gtk_misc_set_alignment (GTK_MISC (label), 0., -1);

	hbox = gtk_hbox_new (FALSE, 0); /* HIG */
	gtk_box_pack_start (GTK_BOX (vbox2), hbox, TRUE, TRUE, 0);
	gtk_widget_show (hbox);
	label = gtk_label_new ("    ");
	gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);
	gtk_widget_show (label);

	sw = gtk_scrolled_window_new (NULL, NULL);
	gtk_box_pack_start (GTK_BOX (hbox), sw, TRUE, TRUE, 0);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (sw), GTK_POLICY_AUTOMATIC,
					GTK_POLICY_AUTOMATIC);
	gtk_widget_show (sw);

	wid = gtk_text_view_new ();
	gtk_container_add (GTK_CONTAINER (sw), wid);
	gtk_text_view_set_left_margin (GTK_TEXT_VIEW (wid), 5);
	gtk_text_view_set_right_margin (GTK_TEXT_VIEW (wid), 5);
	gtk_text_view_set_editable (GTK_TEXT_VIEW (wid), FALSE);
	ws->priv->textbuffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (wid));
	gtk_text_buffer_set_text (ws->priv->textbuffer, "", -1);
	gtk_widget_show (wid);
	gtk_text_buffer_create_tag (ws->priv->textbuffer, "funcname",
				    "weight", PANGO_WEIGHT_BOLD,
				    "foreground", "blue", NULL);
	
	gtk_text_buffer_create_tag (ws->priv->textbuffer, "descr",
				    "style", PANGO_STYLE_ITALIC, NULL);
	
	g_signal_connect (G_OBJECT (ws->priv->dict), "object_updated",
			  G_CALLBACK (dtype_updated_cb), ws);
}

static void dtype_info_display_update (GdaDictType *dtype, WsDatatypes *ws);
static void
selector_selection_changed_cb (GnomeDbSelector *mgsel, GObject *sel_object, WsDatatypes *ws)
{
	if (sel_object && !GDA_IS_DICT_TYPE (sel_object))
		return;
	
	ws->priv->sel_obj = sel_object;
	gtk_notebook_set_current_page (GTK_NOTEBOOK (ws->priv->notebook), sel_object ? 1 : 0);
	dtype_info_display_update ((GdaDictType*) sel_object, ws);
}

static void 
dtypes_filter_mode_changed_cb (GtkWidget *wid, WsDatatypes *ws)
{
	if (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (wid))) {
		GObject *sel_obj;
		
		ws->priv->filter_mode = GPOINTER_TO_INT (g_object_get_data (G_OBJECT (wid), "mode"));
		sel_obj = gnome_db_selector_get_selected_object (GNOME_DB_SELECTOR (ws->priv->selector));
		selector_selection_changed_cb (GNOME_DB_SELECTOR (ws->priv->selector), sel_obj, ws);
	}

}

static void
dtype_updated_cb (GdaConnection *srv, GdaDictType *dtype, WsDatatypes *ws)
{
	if (GDA_IS_DICT_TYPE (dtype) && ((GObject *)dtype == ws->priv->sel_obj))
		dtype_info_display_update (dtype, ws);
}

static gchar *function_get_args (GdaDictFunction *func);
static void
dtype_info_display_update (GdaDictType *dtype, WsDatatypes *ws)
{
	GtkTextIter start, end;
	gchar *str = NULL;
	gchar *title = NULL;

	/* description */
	if (dtype)
		str = (gchar *) gda_object_get_description (GDA_OBJECT (dtype));
	
	if (str && *str) 
		gtk_label_set_text (GTK_LABEL (ws->priv->description), str);
	else
		gtk_label_set_text (GTK_LABEL (ws->priv->description), "---");

	/* synonyms */
	str = NULL;
	if (dtype) {
		const GSList *synlist = gda_dict_type_get_synonyms (dtype);
		if (synlist) {
			GString *string;

			string = g_string_new ((gchar *) synlist->data);
			synlist = g_slist_next (synlist);
			while (synlist) {
				g_string_append (string, ", ");
				g_string_append (string, (gchar *) synlist->data);
				synlist = g_slist_next (synlist);
			}
			str = string->str;
			g_string_free (string, FALSE);
		}
	}
	if (str) {
		gtk_label_set_text (GTK_LABEL (ws->priv->synonyms), str);
		g_free (str);
	}
	else
		gtk_label_set_text (GTK_LABEL (ws->priv->synonyms), "---");

	/* global title update */
	title = ws_datatypes_get_description (WORKSPACE_PAGE (ws));
	g_signal_emit_by_name (G_OBJECT (ws), "description_changed", title);
	g_free (title);

	/* apply filter */
	gtk_text_buffer_get_start_iter (ws->priv->textbuffer, &start);
	gtk_text_buffer_get_end_iter (ws->priv->textbuffer, &end);
	gtk_text_buffer_delete (ws->priv->textbuffer, &start, &end);
	if (dtype) {
		GSList *list, *funcs, *args, *aggs;
		gboolean getfunc;
		GdaDictType *ldt;
		
		funcs = gda_dict_get_functions (ws->priv->dict);
		aggs = gda_dict_get_aggregates (ws->priv->dict);
		list = g_slist_concat (funcs, aggs);
		funcs = list;
		while (list) {
			getfunc = FALSE;
			switch (ws->priv->filter_mode) {
			case 0:
				if (GDA_IS_DICT_FUNCTION (list->data)) {
					ldt = gda_dict_function_get_ret_dict_type (GDA_DICT_FUNCTION (list->data));
					getfunc = (ldt == dtype) ? TRUE : FALSE;
				}
				break;
			case 1:
				if (GDA_IS_DICT_FUNCTION (list->data)) {
					args = (GSList *) gda_dict_function_get_arg_dict_types (GDA_DICT_FUNCTION (list->data));
					while (args && !getfunc) {
						if ((args->data == (gpointer) dtype) || !args->data)
							getfunc = TRUE;
						args = g_slist_next (args);
					}
				}
				break;
			case 2:
				if (GDA_IS_DICT_AGGREGATE (list->data)) {
					ldt = gda_dict_aggregate_get_arg_dict_type (GDA_DICT_AGGREGATE (list->data));
					getfunc = !ldt || (ldt == dtype) ? TRUE : FALSE;
				}
				break;
			}

			if (getfunc) {
				gchar *str;

				str = (gchar *) gda_object_get_name (GDA_OBJECT (list->data));
				gtk_text_buffer_get_end_iter (ws->priv->textbuffer, &end);
				gtk_text_buffer_insert_with_tags_by_name (ws->priv->textbuffer, 
									  &end, str, -1, 
									  "funcname", NULL);

				if (GDA_IS_DICT_FUNCTION (list->data))
					str = function_get_args (GDA_DICT_FUNCTION (list->data));
				else {
					if ((ldt = gda_dict_aggregate_get_arg_dict_type (GDA_DICT_AGGREGATE (list->data)))) {
						str = g_strdup_printf (" (%s)", gda_object_get_name (GDA_OBJECT (ldt)));
					}
					else
						str = g_strdup (" (*)");
				}
				gtk_text_buffer_get_end_iter (ws->priv->textbuffer, &end);
				gtk_text_buffer_insert (ws->priv->textbuffer, &end, str, -1);
				g_free (str);

				str = (gchar *) gda_object_get_description (GDA_OBJECT (list->data));
				if (str && *str) {
					gchar *str2 = g_strdup_printf (" -- %s\n", str);
					gtk_text_buffer_get_end_iter (ws->priv->textbuffer, &end);
					gtk_text_buffer_insert_with_tags_by_name (ws->priv->textbuffer, &end, str2, -1, 
										  "descr", NULL);
					g_free (str2);
				}
				else {
					gtk_text_buffer_get_end_iter (ws->priv->textbuffer, &end);
					gtk_text_buffer_insert (ws->priv->textbuffer, &end, "\n", -1);
				}
			}
			list = g_slist_next (list);
		}
		g_slist_free (funcs);
	}
}

static gchar *
function_get_args (GdaDictFunction *func)
{
	GString *string;
	const GSList *args;
	gchar *retval;
	gboolean firstarg = TRUE;

	string = g_string_new ("");
	args = gda_dict_function_get_arg_dict_types (func);
	g_string_append (string, " (");
	while (args) {
		if (firstarg)
			firstarg = FALSE;
		else
			g_string_append (string, ", ");
		if (args->data) 
			g_string_append (string,
					 gda_dict_type_get_sqlname (GDA_DICT_TYPE (args->data)));
		else
			g_string_append (string, "*");
		args = g_slist_next (args);
	}
	g_string_append (string, ")");
	retval = string->str;
	g_string_free (string, FALSE);

	return retval;
}


/* 
 * WorkspacePage interface implementation
 */
static gchar *
ws_datatypes_get_name (WorkspacePage *iface)
{
	g_return_val_if_fail (iface && IS_WS_DATATYPES (iface), NULL);
	g_return_val_if_fail (WS_DATATYPES (iface)->priv, NULL);

	return g_strdup_printf (_("Data types"));
}

static gchar *
ws_datatypes_get_description (WorkspacePage *iface)
{
	gchar *str = NULL;
	WsDatatypes *ws;

	g_return_val_if_fail (iface && IS_WS_DATATYPES (iface), NULL);
	g_return_val_if_fail (WS_DATATYPES (iface)->priv, NULL);

	ws = WS_DATATYPES (iface);
	if (ws->priv->sel_obj) {
		const gchar *cstr;
		 cstr =  gda_object_get_name (GDA_OBJECT (ws->priv->sel_obj));
		 str = g_strdup_printf ("Data type: <b>%s</b>", (cstr && *cstr) ? cstr : "---");
	}
	else
		str = g_strdup_printf ("<b>%s</b>", _("No data type selected"));

	return str;
}

static GtkWidget *
ws_datatypes_get_sel_button (WorkspacePage *iface)
{
	GdkPixbuf *pixbuf;
	GtkWidget *button, *wid, *hbox;

	g_return_val_if_fail (iface && IS_WS_DATATYPES (iface), NULL);
	g_return_val_if_fail (WS_DATATYPES (iface)->priv, NULL);

	hbox = gtk_hbox_new (FALSE, 0);

	pixbuf = gnome_db_stock_get_icon_pixbuf (GNOME_DB_STOCK_TYPES);
	wid = gtk_image_new_from_pixbuf (pixbuf);
	g_object_unref (pixbuf);
	gtk_box_pack_start (GTK_BOX (hbox), wid, FALSE, FALSE, 0);
	gtk_widget_show (wid);

	wid = gtk_label_new (_("Types"));
	gtk_box_pack_start (GTK_BOX (hbox), wid, FALSE, FALSE, 5);
	gtk_widget_show (wid);

	button = gtk_toggle_button_new ();
	gtk_container_add (GTK_CONTAINER (button), hbox);
	gtk_widget_show (hbox);

	return button;
}

static GtkWidget *
ws_datatypes_get_selector (WorkspacePage *iface)
{
	g_return_val_if_fail (iface && IS_WS_DATATYPES (iface), NULL);
	g_return_val_if_fail (WS_DATATYPES (iface)->priv, NULL);

	return WS_DATATYPES (iface)->priv->selector;
}

static GtkWidget *
ws_datatypes_get_work_area (WorkspacePage *iface)
{
	g_return_val_if_fail (iface && IS_WS_DATATYPES (iface), NULL);
	g_return_val_if_fail (WS_DATATYPES (iface)->priv, NULL);

	return WS_DATATYPES (iface)->priv->work_area;
}
