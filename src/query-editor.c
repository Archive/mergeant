/* query-editor.c
 *
 * Copyright (C) 2004 - 2007 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "query-editor.h"
#include "query-fields-editor.h"
#include "query-params-editor.h"
#include <libgnomedb-extra/gnome-db-editor.h>
#include <libgnomedb-graph/libgnomedb-graph.h>
#include <libgda/graph/gda-dict-reg-graphs.h>

#ifdef HAVE_GNOMEDB_GOOCANVAS
#include <libgnomedb-goo/gnome-db-goo.h>
#include <libgnomedb-goo/gnome-db-goo-query-struct.h>
#else
#include <libgnomedb-graph/gnome-db-canvas.h>
#include <libgnomedb-graph/gnome-db-canvas-query-struct.h>
#endif

static void query_editor_class_init (QueryEditorClass * class);
static void query_editor_init (QueryEditor * wid);
static void query_editor_dispose (GObject   * object);


struct _QueryEditorPriv
{
	GdaQuery     *query;
	
	GtkWidget   *q_name;
	GtkWidget   *q_descr;
	GtkWidget   *notebook;
	GtkWidget   *toggle;

	guint        notebook_trans[GDA_QUERY_TYPE_NON_PARSED_SQL];

	/* SQL edition */
	GtkWidget   *sql_editor;   /* GnomeDbEditor */

	/* SELECT query page */
	GtkWidget   *select_where; /* GnomeDbEditor */
};

/* get a pointer to the parents to be able to call their destructor */
static GObjectClass *parent_class = NULL;

GType
query_editor_get_type (void)
{
	static GType type = 0;

	if (!type) {
		static const GTypeInfo info = {
			sizeof (QueryEditorClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) query_editor_class_init,
			NULL,
			NULL,
			sizeof (QueryEditor),
			0,
			(GInstanceInitFunc) query_editor_init
		};		
		
		type = g_type_register_static (GTK_TYPE_VBOX, "QueryEditor", &info, 0);
	}

	return type;
}

static void
query_editor_class_init (QueryEditorClass * class)
{
	GObjectClass   *object_class = G_OBJECT_CLASS (class);
	
	parent_class = g_type_class_peek_parent (class);

	object_class->dispose = query_editor_dispose;
}

static void
query_editor_init (QueryEditor * wid)
{
	gint i;
	wid->priv = g_new0 (QueryEditorPriv, 1);
	wid->priv->query = NULL;

	for (i=0; i<GDA_QUERY_TYPE_NON_PARSED_SQL; i++)
		wid->priv->notebook_trans[i] = 0;
}

static void query_editor_initialize (QueryEditor *mgsel);


static void query_destroyed_cb (GdaQuery *query, QueryEditor *mgsel);
static void edition_mode_toggled (GtkToggleButton *button, QueryEditor *qedit);

/**
 * query_editor_new
 * @dict: a #GdaDict object
 *
 * Creates a new #QueryEditor widget.
 *
 * Returns: the new widget
 */
GtkWidget *
query_editor_new (GdaQuery *query)
{
	GObject    *obj;
	QueryEditor *qedit;

	g_return_val_if_fail (query && GDA_IS_QUERY (query), NULL);
		
	obj = g_object_new (QUERY_EDITOR_TYPE, NULL);
	qedit = QUERY_EDITOR (obj);

	qedit->priv->query = query;

	gda_object_connect_destroy (qedit->priv->query,
				 G_CALLBACK (query_destroyed_cb), qedit);
	
	query_editor_initialize (qedit);
	edition_mode_toggled (GTK_TOGGLE_BUTTON (qedit->priv->toggle), qedit);

	return GTK_WIDGET (obj);
}

static void query_changed_cb (GdaQuery *query, QueryEditor *qedit);
static void query_type_changed_cb (GdaQuery *query, QueryEditor *qedit);
static void
query_destroyed_cb (GdaQuery *query, QueryEditor *mgsel)
{
	gtk_widget_destroy (GTK_WIDGET (mgsel));
}

static void
query_editor_dispose (GObject *object)
{
	QueryEditor *qedit;

	g_return_if_fail (object != NULL);
	g_return_if_fail (IS_QUERY_EDITOR (object));
	qedit = QUERY_EDITOR (object);

	if (qedit->priv) {
		/* Weak unref the GdaQuery if necessary */
		if (qedit->priv->query) {
			g_signal_handlers_disconnect_by_func (G_OBJECT (qedit->priv->query),
							      G_CALLBACK (query_destroyed_cb), qedit);
			g_signal_handlers_disconnect_by_func (G_OBJECT (qedit->priv->query),
							      G_CALLBACK (query_changed_cb), qedit);
			g_signal_handlers_disconnect_by_func (G_OBJECT (qedit->priv->query),
							      G_CALLBACK (query_type_changed_cb), qedit);
		}

		/* the private area itself */
		g_free (qedit->priv);
		qedit->priv = NULL;
	}

	/* for the parent class */
	parent_class->dispose (object);
}

/*
 * This function is called because otherwise the GnomeCanvas object does not display
 * its items correctly, and the user needs to move the GtkPaned gutter to have it displayed
 * correctly, which is done automatically here
 */
static void
fix_canvas_display_pb_on_show_cb (GtkPaned *vp, gpointer data)
{
	gtk_paned_set_position (vp, 151);	
}

static void entry_name_changed_cb (GtkEntry *entry, QueryEditor *qedit);
static void entry_description_changed_cb (GtkEntry *entry, QueryEditor *qedit);
static void sql_query_text_apply_cb (GtkButton *button, QueryEditor *qedit);
static void select_where_apply_cb (GtkButton *button, QueryEditor *qedit);

static void
query_editor_initialize (QueryEditor *qedit)
{
	GtkWidget *wid, *label, *table, *hbox, *nb, *vbox, *vp, *vbox2, *exp;
	GtkWidget *bbox, *button;
	GdaGraph *graph;
	gchar *str;

	gtk_box_set_spacing (GTK_BOX (qedit), 6);
	
	/* query attributes */
	label = gtk_label_new (NULL);
	str = g_strdup_printf ("<b>%s</b>", _("Query attributes:"));
	gtk_label_set_markup (GTK_LABEL (label), str);
	g_free (str);
	gtk_box_pack_start (GTK_BOX (qedit), label, FALSE, FALSE, 0);
	gtk_widget_show (label);
	gtk_misc_set_alignment (GTK_MISC (label), 0., -1);

	hbox = gtk_hbox_new (FALSE, 0); /* HIG */
	gtk_box_pack_start (GTK_BOX (qedit), hbox, FALSE, FALSE, 0);
	gtk_widget_show (hbox);
	label = gtk_label_new ("    ");
	gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);
	gtk_widget_show (label);

	table = gtk_table_new (2, 3, FALSE);
	gtk_widget_show (table);
	gtk_box_pack_start (GTK_BOX (hbox), table, TRUE, TRUE, 0);
	gtk_table_set_row_spacings (GTK_TABLE (table), 3);
	gtk_table_set_col_spacings (GTK_TABLE (table), 5);
	
	label = gtk_label_new (_("Name:"));
	gtk_widget_show (label);
	gtk_table_attach (GTK_TABLE (table), label, 0, 1, 0, 1, GTK_FILL, 0, 0, 0);
	gtk_label_set_justify (GTK_LABEL (label), GTK_JUSTIFY_LEFT);
	gtk_misc_set_alignment (GTK_MISC (label), 1, 0.5);

	wid = gtk_entry_new ();
	gtk_widget_show (wid);
	gtk_table_attach_defaults (GTK_TABLE (table), wid, 1, 2, 0, 1);
	qedit->priv->q_name = wid;
	g_signal_connect (G_OBJECT (wid), "changed", G_CALLBACK (entry_name_changed_cb), qedit);

	label = gtk_label_new (_("Description:"));
	gtk_widget_show (label);
	gtk_table_attach (GTK_TABLE (table), label, 0, 1, 1, 2, GTK_FILL, 0, 0, 0);
	gtk_label_set_justify (GTK_LABEL (label), GTK_JUSTIFY_LEFT);
	gtk_misc_set_alignment (GTK_MISC (label), 1, 0.5);

	wid = gtk_entry_new ();
	gtk_widget_show (wid);
	gtk_table_attach_defaults (GTK_TABLE (table), wid, 1, 2, 1, 2);
	qedit->priv->q_descr = wid;
	g_signal_connect (G_OBJECT (wid), "changed", G_CALLBACK (entry_description_changed_cb), qedit);

	wid = gtk_toggle_button_new_with_mnemonic (_("SQL"));
	gtk_widget_show (wid);
	gtk_table_attach (GTK_TABLE (table), wid, 2, 3, 0, 2, GTK_FILL, GTK_EXPAND | GTK_FILL, 0, 0);
	qedit->priv->toggle = wid;
	g_signal_connect (G_OBJECT (wid), "toggled", G_CALLBACK (edition_mode_toggled), qedit);
	
	/* notebook */
	nb = gtk_notebook_new ();
	gtk_widget_show (nb);
	gtk_notebook_set_show_tabs (GTK_NOTEBOOK (nb), FALSE);
	gtk_notebook_set_show_border (GTK_NOTEBOOK (nb), FALSE);
	gtk_box_pack_start (GTK_BOX (qedit), nb, TRUE, TRUE, 0);
	qedit->priv->notebook = nb;

	/* 
	 * SQL edition page 
	 */
	vbox = gtk_vbox_new (FALSE, 6);
	gtk_notebook_append_page (GTK_NOTEBOOK (nb), vbox, NULL);
	gtk_widget_show (vbox);

	str = g_strdup_printf ("<b>%s</b>\n<small>%s</small>", 
			       _("SQL statement representing the query:"),
			       _("Click on the \"Apply\" button to validate any modification in the text"));
	label = gtk_label_new (str);
	g_free (str);
	gtk_widget_show (label);
	gtk_box_pack_start (GTK_BOX (vbox), label, FALSE, FALSE, 0);
	gtk_label_set_use_markup (GTK_LABEL (label), TRUE);
	gtk_label_set_justify (GTK_LABEL (label), GTK_JUSTIFY_LEFT);
	gtk_misc_set_alignment (GTK_MISC (label), 0, 0.5);

	hbox = gtk_hbox_new (FALSE, 0); /* HIG */
	gtk_box_pack_start (GTK_BOX (vbox), hbox, TRUE, TRUE, 0);
	gtk_widget_show (hbox);
	label = gtk_label_new ("    ");
	gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);
	gtk_widget_show (label);

	wid = gnome_db_editor_new ();
	qedit->priv->sql_editor = wid;
	gtk_widget_show (wid);
        gnome_db_editor_set_editable (GNOME_DB_EDITOR (wid), TRUE);
        gnome_db_editor_set_highlight (GNOME_DB_EDITOR (wid), TRUE);
        gtk_box_pack_start (GTK_BOX (hbox), wid, TRUE, TRUE, 0);

	bbox = gtk_vbutton_box_new ();
	gtk_button_box_set_layout (GTK_BUTTON_BOX (bbox), GTK_BUTTONBOX_START);
	gtk_box_pack_start (GTK_BOX (hbox), bbox, FALSE, FALSE, 0);
	gtk_widget_show (bbox);

	button = gtk_button_new_from_stock (GTK_STOCK_APPLY);
	gtk_container_add (GTK_CONTAINER (bbox), button);
	gtk_widget_show (button);
	g_signal_connect (G_OBJECT (button), "clicked", G_CALLBACK (sql_query_text_apply_cb), qedit);
	
	g_signal_connect (G_OBJECT (qedit->priv->query), "changed",
			  G_CALLBACK (query_changed_cb), qedit);
	g_signal_connect (G_OBJECT (qedit->priv->query), "type_changed",
			  G_CALLBACK (query_type_changed_cb), qedit);

	/* 
	 * Point and click edition page: SELECT queries 
	 */
	vbox = gtk_vbox_new (FALSE, 6);
	qedit->priv->notebook_trans[GDA_QUERY_TYPE_SELECT] = gtk_notebook_append_page (GTK_NOTEBOOK (nb), 
										      vbox, NULL);
	gtk_widget_show (vbox);

	vp = gtk_vpaned_new ();
	gtk_box_pack_start (GTK_BOX (vbox), vp, TRUE, TRUE, 0);
	gtk_widget_show (vp);
	gtk_paned_set_position (GTK_PANED (vp), 150);
	g_signal_connect_after (G_OBJECT (vp), "map",
				G_CALLBACK (fix_canvas_display_pb_on_show_cb), NULL);

	vbox2 = gtk_vbox_new (FALSE, 6);
	gtk_paned_pack1 (GTK_PANED (vp), vbox2, FALSE, TRUE);
	gtk_widget_show (vbox2);

	str = g_strdup_printf ("<b>%s</b>", _("Query targets and their joining rules:"));
	label = gtk_label_new (str);
	g_free (str);
	gtk_widget_show (label);
	gtk_box_pack_start (GTK_BOX (vbox2), label, FALSE, FALSE, 0);
	gtk_label_set_use_markup (GTK_LABEL (label), TRUE);
	gtk_label_set_justify (GTK_LABEL (label), GTK_JUSTIFY_LEFT);
	gtk_misc_set_alignment (GTK_MISC (label), 0, 0.5);

	hbox = gtk_hbox_new (FALSE, 0); /* HIG */
	gtk_box_pack_start (GTK_BOX (vbox2), hbox, TRUE, TRUE, 0);
	gtk_widget_show (hbox);
	label = gtk_label_new ("    ");
	gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);
	gtk_widget_show (label);

	graph = gda_graphs_get_graph_for_object (gda_object_get_dict (GDA_OBJECT (qedit->priv->query)), 
						 G_OBJECT (qedit->priv->query));
	if (!graph) {
		/* the query did not have any graph: create one */
		graph = GDA_GRAPH (gda_graph_query_new (qedit->priv->query));
		gda_graph_query_sync_targets (GDA_GRAPH_QUERY (graph));
		gda_dict_assume_object_as (gda_object_get_dict (GDA_OBJECT (qedit->priv->query)), 
					   (GdaObject *) graph, GDA_TYPE_GRAPH);
		g_object_unref (graph);
	}
#ifdef HAVE_GNOMEDB_GOOCANVAS
	wid = gnome_db_goo_query_struct_new (qedit->priv->query, (GdaGraphQuery *) graph);
	gtk_widget_show (wid);
	gnome_db_goo_set_zoom_factor (GNOME_DB_GOO (wid), 0.8);
	wid = gnome_db_goo_set_in_scrolled_window (GNOME_DB_GOO (wid));
#else
	wid = gnome_db_canvas_query_struct_new (qedit->priv->query, graph);
	gtk_widget_show (wid);
	gnome_db_canvas_set_zoom_factor (GNOME_DB_CANVAS (wid), 0.8);
	wid = gnome_db_canvas_set_in_scrolled_window (GNOME_DB_CANVAS (wid));
#endif
	gtk_widget_show (wid);
	gtk_box_pack_start (GTK_BOX (hbox), wid, TRUE, TRUE, 0);
	
	vbox2 = gtk_vbox_new (FALSE, 6);
	gtk_paned_pack2 (GTK_PANED (vp), vbox2, TRUE, TRUE);
	gtk_widget_show (vbox2);

	str = g_strdup_printf ("<b>%s</b>", _("Query fields and their properties:"));
	label = gtk_label_new (str);
	g_free (str);
	gtk_widget_show (label);
	gtk_box_pack_start (GTK_BOX (vbox2), label, FALSE, FALSE, 0);
	gtk_label_set_use_markup (GTK_LABEL (label), TRUE);
	gtk_label_set_justify (GTK_LABEL (label), GTK_JUSTIFY_LEFT);
	gtk_misc_set_alignment (GTK_MISC (label), 0, 0.5);

	hbox = gtk_hbox_new (FALSE, 0); /* HIG */
	gtk_box_pack_start (GTK_BOX (vbox2), hbox, TRUE, TRUE, 0);
	gtk_widget_show (hbox);
	label = gtk_label_new ("    ");
	gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);
	gtk_widget_show (label);

	wid = query_fields_editor_new (qedit->priv->query, 
				       QEF_SHOW_OPTION | QEF_FILTER_OPTION | QEF_ORDER_OPTION |
				       QEF_ACTION_BUTTONS);
	gtk_widget_show (wid);
	gtk_box_pack_start (GTK_BOX (hbox), wid, TRUE, TRUE, 0);


	str = g_strdup_printf ("<b>%s</b>", _("Other properties:"));
	exp = gtk_expander_new (str);
	gtk_expander_set_use_markup (GTK_EXPANDER (exp), TRUE);
	g_free (str);
	gtk_widget_show (exp);
	gtk_box_pack_start (GTK_BOX (vbox), exp, FALSE, FALSE, 0);

	hbox = gtk_hbox_new (FALSE, 0); /* HIG */
	gtk_container_add (GTK_CONTAINER (exp), hbox);
	gtk_widget_show (hbox);
	label = gtk_label_new ("    ");
	gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);
	gtk_widget_show (label);

	table = gtk_table_new (4, 2, FALSE);
	gtk_widget_show (table);
	gtk_box_pack_start (GTK_BOX (hbox), table, TRUE, TRUE, 0);
	gtk_table_set_row_spacings (GTK_TABLE (table), 3);
	gtk_table_set_col_spacings (GTK_TABLE (table), 5);

	label = gtk_label_new (_("Global WHERE filter rule:"));
	gtk_misc_set_alignment (GTK_MISC (label), 0, 0.5);
	gtk_table_attach (GTK_TABLE (table), label, 0, 1, 0, 1, GTK_EXPAND | GTK_FILL, 0, 0, 0);
	gtk_widget_show (label);

	wid = gnome_db_editor_new ();
	gnome_db_editor_set_editable (GNOME_DB_EDITOR (wid), TRUE);
        gnome_db_editor_set_highlight (GNOME_DB_EDITOR (wid), TRUE);
	qedit->priv->select_where = wid;
	gtk_table_attach_defaults (GTK_TABLE (table), wid, 0, 1, 1, 2);
	gtk_widget_show (wid);

	bbox = gtk_vbutton_box_new ();
	gtk_button_box_set_layout (GTK_BUTTON_BOX (bbox), GTK_BUTTONBOX_START);
	gtk_table_attach (GTK_TABLE (table), bbox, 1, 2, 1, 2, 0, GTK_EXPAND | GTK_FILL, 0, 0);
	gtk_widget_show (bbox);

	button = gtk_button_new ();
	wid = gtk_image_new_from_stock (GTK_STOCK_APPLY, GTK_ICON_SIZE_BUTTON);
	gtk_widget_show (wid);
	gtk_container_add (GTK_CONTAINER (button), wid);
	gtk_container_add (GTK_CONTAINER (bbox), button);
	gtk_widget_show (button);
	g_signal_connect (G_OBJECT (button), "clicked",
 			  G_CALLBACK (select_where_apply_cb), qedit);

	label = gtk_label_new (_("Query parameters:"));
	gtk_misc_set_alignment (GTK_MISC (label), 0, 0.5);
	gtk_table_attach (GTK_TABLE (table), label, 0, 1, 2, 3, GTK_EXPAND | GTK_FILL, 0, 0, 0);
	gtk_widget_show (label);

	wid = query_params_editor_new (qedit->priv->query, QEP_ALLOW_EDITION | QEP_SHOW_ALL_VALUE_FIELDS);
	gtk_table_attach_defaults (GTK_TABLE (table), wid, 0, 1, 3, 4);
	gtk_widget_show (wid);

	/* initial filling of the widgets */
	query_changed_cb (qedit->priv->query, qedit);
}

static void
entry_name_changed_cb (GtkEntry *entry, QueryEditor *qedit)
{
	/* blocking recursive signal */
	g_signal_handlers_block_by_func (G_OBJECT (qedit->priv->query),
					 G_CALLBACK (query_changed_cb), qedit);
	gda_object_set_name (GDA_OBJECT (qedit->priv->query), gtk_entry_get_text (entry));
	/* unblocking recursive signal */
	g_signal_handlers_unblock_by_func (G_OBJECT (qedit->priv->query),
					   G_CALLBACK (query_changed_cb), qedit);
}

static void
entry_description_changed_cb (GtkEntry *entry, QueryEditor *qedit)
{
	/* blocking recursive signal */
	g_signal_handlers_block_by_func (G_OBJECT (qedit->priv->query),
					 G_CALLBACK (query_changed_cb), qedit);
	gda_object_set_description (GDA_OBJECT (qedit->priv->query), gtk_entry_get_text (entry));
	/* unblocking recursive signal */
	g_signal_handlers_unblock_by_func (G_OBJECT (qedit->priv->query),
					   G_CALLBACK (query_changed_cb), qedit);
}

static void
query_changed_cb (GdaQuery *query, QueryEditor *qedit)
{
	const gchar *str;
	gchar *sql;

	/* blocking recursive signal */
	g_signal_handlers_block_by_func (G_OBJECT (qedit->priv->q_name),
					 G_CALLBACK (entry_name_changed_cb), qedit);
	g_signal_handlers_block_by_func (G_OBJECT (qedit->priv->q_descr),
					 G_CALLBACK (entry_description_changed_cb), qedit);
	/* query name */
	str = gda_object_get_name (GDA_OBJECT (qedit->priv->query));
	if (str)
		gtk_entry_set_text (GTK_ENTRY (qedit->priv->q_name), str);
	else
		gtk_entry_set_text (GTK_ENTRY (qedit->priv->q_name), "");
	
	/* query description */
	str = gda_object_get_description (GDA_OBJECT (qedit->priv->query));
	if (str)
		gtk_entry_set_text (GTK_ENTRY (qedit->priv->q_descr), str);
	else
		gtk_entry_set_text (GTK_ENTRY (qedit->priv->q_descr), "");

	/* SQL version */
	sql = gda_renderer_render_as_sql (GDA_RENDERER (qedit->priv->query), NULL, NULL,
					  GDA_RENDERER_EXTRA_PRETTY_SQL | GDA_RENDERER_PARAMS_AS_DETAILED, NULL);
	if (sql) {
		gnome_db_editor_set_text (GNOME_DB_EDITOR (qedit->priv->sql_editor), sql, -1);
		g_free (sql);
	}
	else
		gnome_db_editor_set_text (GNOME_DB_EDITOR (qedit->priv->sql_editor), "", -1);

	/* Global WHERE condition */
	if (qedit->priv->select_where) {
		GdaQueryCondition *cond = gda_query_get_condition (qedit->priv->query);
		sql = NULL;

		if (cond) 
			sql = gda_renderer_render_as_sql (GDA_RENDERER (cond), NULL, NULL,
							  GDA_RENDERER_EXTRA_PRETTY_SQL | GDA_RENDERER_PARAMS_AS_DETAILED, 
							  NULL);
		if (sql) {
			gnome_db_editor_set_text (GNOME_DB_EDITOR (qedit->priv->select_where), sql, -1);
			g_free (sql);
		}
		else
			gnome_db_editor_set_text (GNOME_DB_EDITOR (qedit->priv->select_where), "", -1);
	}


	/* unblocking recursive signal */
	g_signal_handlers_unblock_by_func (G_OBJECT (qedit->priv->q_name),
					 G_CALLBACK (entry_name_changed_cb), qedit);
	g_signal_handlers_unblock_by_func (G_OBJECT (qedit->priv->q_descr),
					 G_CALLBACK (entry_description_changed_cb), qedit);
}

static void
sql_query_text_apply_cb (GtkButton *button, QueryEditor *qedit)
{
	gchar *sql;
	GError *error = NULL;
	gchar *str = NULL;
	GdaQuery *query = qedit->priv->query;
	GdaQuery *tmpquery;
	gboolean do_apply = FALSE;

	/* test the new SQL on a temporary query, to be able to let the user change his mind and
	 * refuse to apply the new SQL text */
	sql = gnome_db_editor_get_all_text (GNOME_DB_EDITOR (qedit->priv->sql_editor));
	tmpquery = gda_query_new_copy (query, NULL);
	gda_query_set_sql_text (tmpquery, sql, &error);

	if (gda_query_get_query_type (tmpquery) ==  GDA_QUERY_TYPE_NON_PARSED_SQL) {
		if (error) {
			str = g_strdup (error->message);
			g_error_free (error);
		}
		else
			str = g_strdup (_("Error (no details available)."));
	}

	if (str) {
		GtkWidget *dlg;
		GtkWidget *window;
		gchar *msg;
		gint result;

		msg = g_strdup_printf ("<b>%s</b>\n%s\n\n<small>%s</small>\n\n%s",
				       _("SQL query analyse warning:"),
				       _("This prevents extra treatments, such as graphical edition of this query, "
					 "which will be considered as a SQL statement specific to the "
					 "connected database."), str,
				       _("Do you still want to use that SQL for the query?"));
		window = gtk_widget_get_toplevel ((GtkWidget *) button);
		if (GTK_WIDGET_TOPLEVEL (window))
			window = NULL;
		dlg = gtk_message_dialog_new ((GtkWindow*) window, GTK_DIALOG_MODAL, 
					      GTK_MESSAGE_QUESTION, GTK_BUTTONS_YES_NO, msg);
		gtk_label_set_use_markup (GTK_LABEL (GTK_MESSAGE_DIALOG (dlg)->label), TRUE);
		g_free (msg);
		g_free (str);
		result = gtk_dialog_run (GTK_DIALOG (dlg));
		do_apply = result == GTK_RESPONSE_YES ? TRUE : FALSE;
		gtk_widget_destroy (dlg);
	}
	else 
		do_apply = TRUE;

	g_object_unref (tmpquery);
	if (do_apply)
		/* really apply the new text */
		gda_query_set_sql_text (query, sql, NULL);
	g_free (sql);
}

static void
query_type_changed_cb (GdaQuery *query, QueryEditor *qedit)
{
	edition_mode_toggled (GTK_TOGGLE_BUTTON (qedit->priv->toggle), qedit);
}

static void
edition_mode_toggled (GtkToggleButton *button, QueryEditor *qedit)
{
	GdaQueryType qtype = gda_query_get_query_type (qedit->priv->query);

	if (qtype == GDA_QUERY_TYPE_NON_PARSED_SQL) 
		gtk_notebook_set_current_page (GTK_NOTEBOOK (qedit->priv->notebook), 0);
	else 
		gtk_notebook_set_current_page (GTK_NOTEBOOK (qedit->priv->notebook),
					       gtk_toggle_button_get_active (button) ? 0 : 
					       qedit->priv->notebook_trans [qtype]);
}

static void
select_where_apply_cb (GtkButton *button, QueryEditor *qedit)
{
	GdaQueryCondition *cond;
	gchar *sql;
	GError *error = NULL;

	sql = gnome_db_editor_get_all_text (GNOME_DB_EDITOR (qedit->priv->select_where));
	if (!sql || !(*sql)) {
		gda_query_set_condition (qedit->priv->query, NULL);
		g_free (sql);
		return;
	}

	cond = gda_query_condition_new_from_sql (qedit->priv->query, sql, NULL, &error);
	if (cond) {
		gda_query_set_condition (qedit->priv->query, cond);
		g_object_unref (cond);
#ifdef debug_NO
		gda_object_dump (GDA_OBJECT (qedit->priv->query), 10);
#endif

	}
	else {
		GtkWidget *errdlg;
		gchar *msg;
		
		if (error) {
			msg = g_strdup_printf ("<b>%s</b>\n%s '%s':\n\n%s",
					       _("Error parsing/analysing WHERE expression:"),
					       _("while parsing"),
					       sql,
					       error->message);
			
			g_error_free (error);
		}
		else
			msg = g_strdup_printf ("<b>%s</b>\n%s '%s'",
					       _("Error parsing/analysing WHERE expression:"),
					       _("while parsing"),
					       sql);
		
		errdlg = gtk_message_dialog_new_with_markup (NULL, GTK_DIALOG_MODAL, GTK_MESSAGE_ERROR,
							     GTK_BUTTONS_CLOSE, msg);
		g_free (msg);
		gtk_dialog_run (GTK_DIALOG (errdlg));
		gtk_widget_destroy (errdlg);
	}

	g_free (sql);
}
