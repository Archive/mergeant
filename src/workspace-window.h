/* Mergeant
 *
 * Copyright (C) 1999 - 2008 Vivien Malerba
 * Copyright (C) 2002 - 2003 Rodrigo Moya
 *
 * Authors:
 *       Vivien Malerba <malerba@gnome-db.org>
 *       Rodrigo Moya <rodrigo@gnome-db.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef __WORKSPACE_WINDOW_H__
#define __WORKSPACE_WINDOW_H__

#include <gtk/gtkwidget.h>
#include <libgda/libgda.h>

typedef struct {
	gchar *dsn;
	gchar *user;
	gchar *pass;
	gchar *start_page;
} WorkspaceOptions;

void create_new_workspace_window      (GdaConnection *cnc);
void create_new_workspace_window_spec (WorkspaceOptions *options);

#endif
