/* Mergeant
 *
 * Copyright (C) 2006 - 2008 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "utils.h"
#include <glib/gi18n-lib.h>
#include <string.h>

static void server_op_provider_changed_cb (GnomeDbProviderSelector *prov_sel, GtkWidget *dlg);
static void server_op_dlg_response_cb (GtkDialog *dlg, gint resp_id, gpointer data);

GtkWidget *mergeant_server_op_create_dialog (GtkWindow *parent, GdaServerOperationType type, 
					     GdaConnection *cnc, GdaSet *options,
					     const gchar *title, const gchar *foreword,
					     MergeantServerOpCallback done_cb, gpointer user_data)
{
	GtkWidget *dlg, *hbox, *label, *wid;
	GtkWidget *vbox;
	gchar *str;

	g_return_val_if_fail (title, NULL);

	dlg = gtk_dialog_new_with_buttons (title, parent, 0,
					   GTK_STOCK_CANCEL, GTK_RESPONSE_REJECT,
					   GTK_STOCK_OK, GTK_RESPONSE_ACCEPT,
					   NULL);
	g_object_set_data (G_OBJECT (dlg), "optype", GINT_TO_POINTER (type));
	if (options) {
		g_object_set_data_full (G_OBJECT (dlg), "options", options, g_object_unref);
		g_object_ref (options);
	}
	if (cnc) {
		g_object_set_data_full (G_OBJECT (dlg), "cnc", cnc, g_object_unref);
		g_object_ref (cnc);
	}

	hbox = gtk_hbox_new (FALSE, 0);
	gtk_box_pack_start (GTK_BOX (GTK_DIALOG (dlg)->vbox), hbox, TRUE, TRUE, 0);
	gtk_widget_show (hbox);
	gtk_container_set_border_width (GTK_CONTAINER (hbox), 6);
	
	vbox = gtk_vbox_new (FALSE, 0);
	gtk_box_pack_start (GTK_BOX (hbox), vbox, TRUE, TRUE, 0);
	gtk_widget_show (vbox);
	gtk_container_set_border_width (GTK_CONTAINER (vbox), 6);
	g_object_set_data (G_OBJECT (dlg), "box", vbox);
	
	/* foreword */
	str = g_strdup_printf ("<span weight=\"bold\" size=\"larger\">%s</span>\n\n%s", title, foreword ? 
			       foreword : _("Please fill in the following information."));
	label = gtk_label_new ("");
	gtk_label_set_markup (GTK_LABEL (label), str);
	g_free (str);
	gtk_misc_set_alignment (GTK_MISC (label), 0., -1);
	gtk_box_pack_start (GTK_BOX (vbox), label, FALSE, FALSE, 0);
	gtk_widget_show (label);

	/* with provider selection */
	if ((type == GDA_SERVER_OPERATION_CREATE_DB) || (type == GDA_SERVER_OPERATION_DROP_DB)) {	
		label = gtk_label_new ("");
		str = g_strdup_printf ("<b>%s:</b>", _("Provider selection"));
		gtk_label_set_markup (GTK_LABEL (label), str);
		g_free (str);
		gtk_misc_set_alignment (GTK_MISC (label), 0., -1);
		gtk_box_pack_start (GTK_BOX (vbox), label, FALSE, FALSE, 5);
		gtk_widget_show (label);
		
		hbox = gtk_hbox_new (FALSE, 0); /* HIG */
		gtk_box_pack_start (GTK_BOX (vbox), hbox, FALSE, FALSE, 0);
		gtk_widget_show (hbox);
		label = gtk_label_new ("    ");
		gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);
		gtk_widget_show (label);
		
		label = gtk_label_new (_("Provider:"));
		gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 5);
		gtk_widget_show (label);
		
		wid = gnome_db_provider_selector_new ();
		gnome_db_provider_selector_set_provider (GNOME_DB_PROVIDER_SELECTOR (wid), 
							 cnc && gda_connection_get_provider_name (cnc) ? 
							 gda_connection_get_provider_name (cnc) : "SQLite");
		g_signal_connect (G_OBJECT (wid), "changed",
				  G_CALLBACK (server_op_provider_changed_cb), dlg);
		gtk_box_pack_start (GTK_BOX (hbox), wid, TRUE, TRUE, 0);
		gtk_widget_show (wid);
		server_op_provider_changed_cb (GNOME_DB_PROVIDER_SELECTOR (wid), dlg);
	}
	else {
		GtkWidget *box;
		GdaServerProvider *prov;
		GdaServerOperationType optype;

		if (!cnc)
			g_error ("No GdaConnection object available");
		prov = gda_connection_get_provider_obj (cnc);
		if (!prov)
			g_error ("GdaConnection has no associated GdaServerProvider object available");

		box = g_object_get_data (G_OBJECT (dlg), "box");

		g_object_ref (prov);
		g_object_set_data_full (G_OBJECT (dlg), "prov", prov, g_object_unref);

		optype = GPOINTER_TO_INT (g_object_get_data (G_OBJECT (dlg), "optype"));
		if (!gda_server_provider_supports_operation (prov, cnc, optype, options)) 
			wid = gtk_label_new (_("The provider associated to this connection\ndoes not support this operation"));
		else {
			GdaServerOperation *op;
			GError *error = NULL;

			op = gda_server_provider_create_operation (prov, cnc, optype, options, &error);
			if (!op) {
				gchar *str;
				
				str = g_strdup_printf (_("Could not get the information required to perform that operation:\n%s"),
						       error && error->message ? error->message : _("No detail"));
				wid = gtk_label_new (str);
				g_free (str);
				if (error)
					g_error_free (error);
			}
			else {
				wid = gnome_db_server_operation_new (op);
				g_object_set_data_full (G_OBJECT (dlg), "op", op, g_object_unref);
			}
		}
	
		gtk_box_pack_start (GTK_BOX (box), wid, TRUE, TRUE, 0);
		g_object_set_data (G_OBJECT (dlg), "opform", wid);
		gtk_widget_show (wid);
	}
	if (type == GDA_SERVER_OPERATION_CREATE_TABLE) 
		gtk_window_set_default_size (GTK_WINDOW (dlg), 600, 600);
	else
		gtk_window_set_default_size (GTK_WINDOW (dlg), 500, 400);
	g_object_set_data (G_OBJECT (dlg), "done_cb", done_cb);
	g_object_set_data (G_OBJECT (dlg), "done_cb_data", user_data);
	g_signal_connect (G_OBJECT (dlg), "response",
                          G_CALLBACK (server_op_dlg_response_cb), NULL);

	return dlg;
}

static void
server_op_dlg_response_cb (GtkDialog *dlg, gint resp_id, gpointer data)
{
	if (resp_id == GTK_RESPONSE_ACCEPT) {
		GdaServerOperation *op;
		GdaServerProvider *prov;
		GError *error = NULL;
		GtkWidget *mess = NULL;
		GdaConnection *cnc;

		MergeantServerOpCallback done_cb;
		gpointer user_data;

		op = g_object_get_data (G_OBJECT (dlg), "op");
		prov = g_object_get_data (G_OBJECT (dlg), "prov");
		cnc = g_object_get_data (G_OBJECT (dlg), "cnc");

		done_cb = g_object_get_data (G_OBJECT (dlg), "done_cb");
		user_data = g_object_get_data (G_OBJECT (dlg), "done_cb_data");

		if (!gda_server_provider_perform_operation (prov, cnc, op, &error)) {
			if (done_cb)
				done_cb ((GtkWidget *) dlg, FALSE, user_data);
			mess = gtk_message_dialog_new_with_markup (NULL, GTK_DIALOG_MODAL,
								  GTK_MESSAGE_ERROR,
								  GTK_BUTTONS_CLOSE, 
								  "<b>%s:</b>\n\n%s",
								  _("Could not perform operation"),
								  error && error->message ? error->message : _("No detail"));
			if (error)
				g_error_free (error);
                }
		else {
			if (done_cb)
				done_cb ((GtkWidget *) dlg, TRUE, user_data);

			/* mess = gtk_message_dialog_new_with_markup (NULL, GTK_DIALOG_MODAL, */
/* 								  GTK_MESSAGE_INFO, */
/* 								  GTK_BUTTONS_CLOSE,  */
/* 								  "<b>%s</b>", */
/* 								  _("Operation successfully performed")); */
		}

		gtk_widget_destroy (GTK_WIDGET (dlg));
		if (mess) {
			gtk_dialog_run (GTK_DIALOG (mess));
			gtk_widget_destroy (mess);
		}
	}
	else
		gtk_widget_destroy (GTK_WIDGET (dlg));
}

static void
server_op_provider_changed_cb (GnomeDbProviderSelector *prov_sel, GtkWidget *dlg)
{
	GdaServerOperationType optype;
	GtkWidget *box, *wid;
	GdaServerProvider *prov;
	GError *error = NULL;
	GdaServerOperation *op;

	/* unreference any previous setting */
	wid = g_object_get_data (G_OBJECT (dlg), "opform");
	if (wid) {
		gtk_widget_destroy (wid);
		g_object_set_data (G_OBJECT (dlg), "opform", NULL);
	}
	op = g_object_get_data (G_OBJECT (dlg), "op");
	if (op) 
		g_object_set_data (G_OBJECT (dlg), "op", NULL);

	prov = g_object_get_data (G_OBJECT (dlg), "prov");
	if (prov) 
		g_object_set_data (G_OBJECT (dlg), "prov", NULL);

	/* new settings */
	box = g_object_get_data (G_OBJECT (dlg), "box");
	prov = gnome_db_provider_selector_get_provider_obj (prov_sel);
	if (!prov) 
		wid = gtk_label_new (_("Internal error: can't instantiate a provider object"));
	else {
		GdaSet *options;
		GdaConnection *cnc;

		g_object_set_data_full (G_OBJECT (dlg), "prov", prov, g_object_unref);
		options = g_object_get_data (G_OBJECT (dlg), "options");
		cnc = g_object_get_data (G_OBJECT (dlg), "cnc");

		/* @cnc and @prov adjustements */
		if (cnc) {
			GdaServerProvider *cncprov;

			cncprov = gda_connection_get_provider_obj (cnc);
			if (cncprov != prov) {
				GdaProviderInfo* info, *cncinfo;

				info = gda_config_get_provider_info (gda_server_provider_get_name (prov));
				cncinfo = gda_config_get_provider_info (gda_server_provider_get_name (cncprov));

				if (!strcmp (info->id, cncinfo->id))
					prov = cncprov;
				else
					cnc = NULL;
			}
		}

		optype = GPOINTER_TO_INT (g_object_get_data (G_OBJECT (dlg), "optype"));
		if (!gda_server_provider_supports_operation (prov, cnc, optype, options)) 
			wid = gtk_label_new (_("Selected provider does not support this operation"));
		else {
			op = gda_server_provider_create_operation (prov, cnc, optype, options, &error);
			if (!op) {
				gchar *str;

				str = g_strdup_printf (_("Could not get the information required to perform that operation:\n%s"),
						       error && error->message ? error->message : _("No detail"));
				wid = gtk_label_new (str);
				g_free (str);
				if (error)
					g_error_free (error);
			}
			else {
				wid = gnome_db_server_operation_new (op);
				g_object_set_data_full (G_OBJECT (dlg), "op", op, g_object_unref);
			}
		}
	}
	
	gtk_box_pack_start (GTK_BOX (box), wid, TRUE, TRUE, 0);
	g_object_set_data (G_OBJECT (dlg), "opform", wid);
	gtk_widget_show (wid);
}
