/* workspace-page.c
 *
 * Copyright (C) 2004 - 2006 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <glib/gi18n.h>
#include "workspace-page.h"
#include "marshal.h"

/* signals */
enum
{
	DESCRIPTION_CHANGED,
	LAST_SIGNAL
};

static gint workspace_page_signals[LAST_SIGNAL] = { 0 };

static void workspace_page_iface_init (gpointer g_class);

GType
workspace_page_get_type (void)
{
	static GType type = 0;

	if (!type) {
		static const GTypeInfo info = {
			sizeof (WorkspacePageIface),
			(GBaseInitFunc) workspace_page_iface_init,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) NULL,
			NULL,
			NULL,
			0,
			0,
			(GInstanceInitFunc) NULL
		};
		
		type = g_type_register_static (G_TYPE_INTERFACE, "WorkspacePage", &info, 0);
		g_type_interface_add_prerequisite (type, G_TYPE_OBJECT);
	}
	return type;
}


static void
workspace_page_iface_init (gpointer g_class)
{
	static gboolean initialized = FALSE;

	if (! initialized) {
		workspace_page_signals [DESCRIPTION_CHANGED] = 
			g_signal_new ("description_changed",
                                      WORKSPACE_PAGE_TYPE,
                                      G_SIGNAL_RUN_FIRST,
                                      G_STRUCT_OFFSET (WorkspacePageIface, descr_changed),
                                      NULL, NULL,
                                      marshal_VOID__POINTER, G_TYPE_NONE,
                                      1, G_TYPE_POINTER);

		initialized = TRUE;
	}
}

/**
 * workspace_get_sel_button
 * @iface: an object which implements the #WorkspacePage interface
 * 
 * Returns:
 */
GtkWidget *
workspace_page_get_sel_button (WorkspacePage *iface)
{
	g_return_val_if_fail (IS_WORKSPACE_PAGE (iface), NULL);

	if (WORKSPACE_PAGE_GET_IFACE (iface)->get_sel_button)
		return (WORKSPACE_PAGE_GET_IFACE (iface)->get_sel_button) (iface);
	else
		return NULL;
}


/**
 * workspace_get_selector
 * @iface: an object which implements the #WorkspacePage interface
 * 
 * Returns:
 */
GtkWidget *
workspace_page_get_selector (WorkspacePage *iface)
{
	g_return_val_if_fail (IS_WORKSPACE_PAGE (iface), NULL);

	if (WORKSPACE_PAGE_GET_IFACE (iface)->get_selector)
		return (WORKSPACE_PAGE_GET_IFACE (iface)->get_selector) (iface);
	else
		return NULL;
}

/**
 * workspace_get_work_area
 * @iface: an object which implements the #WorkspacePage interface
 * 
 * Returns:
 */
GtkWidget *
workspace_page_get_work_area (WorkspacePage *iface)
{
	g_return_val_if_fail (IS_WORKSPACE_PAGE (iface), NULL);

	if (WORKSPACE_PAGE_GET_IFACE (iface)->get_work_area)
		return (WORKSPACE_PAGE_GET_IFACE (iface)->get_work_area) (iface);
	else
		return NULL;
}

/**
 * workspace_get_actions
 * @iface: an object which implements the #WorkspacePage interface
 * 
 * Returns:
 */
GtkActionGroup *
workspace_page_get_actions (WorkspacePage *iface)
{
	g_return_val_if_fail (IS_WORKSPACE_PAGE (iface), NULL);

	if (WORKSPACE_PAGE_GET_IFACE (iface)->get_actions)
		return (WORKSPACE_PAGE_GET_IFACE (iface)->get_actions) (iface);
	else
		return NULL;
}

/**
 * workspace_get_actions_ui
 * @iface: an object which implements the #WorkspacePage interface
 * 
 * Returns:
 */
const gchar *
workspace_page_get_actions_ui (WorkspacePage *iface)
{
	g_return_val_if_fail (IS_WORKSPACE_PAGE (iface), NULL);

	if (WORKSPACE_PAGE_GET_IFACE (iface)->get_actions_ui)
		return (WORKSPACE_PAGE_GET_IFACE (iface)->get_actions_ui) (iface);
	else
		return NULL;
}

/**
 * workspace_get_name
 * @iface: an object which implements the #WorkspacePage interface
 * 
 * Returns:
 */
gchar *
workspace_page_get_name (WorkspacePage *iface)
{
	g_return_val_if_fail (IS_WORKSPACE_PAGE (iface), NULL);

	if (WORKSPACE_PAGE_GET_IFACE (iface)->get_name)
		return (WORKSPACE_PAGE_GET_IFACE (iface)->get_name) (iface);
	else
		return NULL;
}

/**
 * workspace_get_description
 * @iface: an object which implements the #WorkspacePage interface
 * 
 * Returns:
 */
gchar *
workspace_page_get_description (WorkspacePage *iface)
{
	g_return_val_if_fail (IS_WORKSPACE_PAGE (iface), NULL);

	if (WORKSPACE_PAGE_GET_IFACE (iface)->get_description)
		return (WORKSPACE_PAGE_GET_IFACE (iface)->get_description) (iface);
	else
		return NULL;
}

void
not_yet_implemented (WorkspacePage *ws, const gchar *feature, const gchar *file, const gchar *function, gint lineno)
{
	GtkWidget *dlg;

	if (feature && *feature)
		dlg = gtk_message_dialog_new (NULL, GTK_DIALOG_MODAL, GTK_MESSAGE_INFO,
					      GTK_BUTTONS_CLOSE, 
					      _("This feature (%s)\nis not yet implemented\n\nIn %s, %s(), line %d"), 
					      feature, file, function, lineno);
	else
		dlg = gtk_message_dialog_new (NULL, GTK_DIALOG_MODAL, GTK_MESSAGE_INFO,
					      GTK_BUTTONS_CLOSE, 
					      _("This feature is not yet implemented\n\nIn %s, %s(), line %d"),
					      file, function, lineno);

	gtk_dialog_run (GTK_DIALOG (dlg));
	gtk_widget_destroy (dlg);
}
