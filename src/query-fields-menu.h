/* query-field-menu.h
 *
 * Copyright (C) 2004 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef __QUERY_FIELDS_MENU__
#define __QUERY_FIELDS_MENU__

#include <gtk/gtk.h>
#include <libgnomedb/libgnomedb.h>

G_BEGIN_DECLS

#define QUERY_FIELDS_MENU_TYPE          (query_fields_menu_get_type())
#define QUERY_FIELDS_MENU(obj)          G_TYPE_CHECK_INSTANCE_CAST (obj, query_fields_menu_get_type(), QueryFieldsMenu)
#define QUERY_FIELDS_MENU_CLASS(klass)  G_TYPE_CHECK_CLASS_CAST (klass, query_fields_menu_get_type (), QueryFieldsMenuClass)
#define IS_QUERY_FIELDS_MENU(obj)       G_TYPE_CHECK_INSTANCE_TYPE (obj, query_fields_menu_get_type ())


typedef struct _QueryFieldsMenu      QueryFieldsMenu;
typedef struct _QueryFieldsMenuClass QueryFieldsMenuClass;
typedef struct _QueryFieldsMenuPriv  QueryFieldsMenuPriv;

/* struct for the object's data */
struct _QueryFieldsMenu
{
	GtkButton            object;

	QueryFieldsMenuPriv *priv;
};

/* struct for the object's class */
struct _QueryFieldsMenuClass
{
	GtkButtonClass       parent_class;

	/* signals */
	void               (*field_selected) (QueryFieldsMenu *fmenu, GdaQueryTarget *target, GdaEntityField *field);
	void               (*expr_selected) (QueryFieldsMenu *fmenu, gchar *expr);
};

/* 
 * Generic widget's methods 
*/
GType      query_fields_menu_get_type            (void);
GtkWidget *query_fields_menu_new                 (GdaQuery *query);

G_END_DECLS

#endif



