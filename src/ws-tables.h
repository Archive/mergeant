/* ws-tables.h
 *
 * Copyright (C) 2004 - 2008 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#ifndef __WS_TABLES_H_
#define __WS_TABLES_H_

#include <libgda/libgda.h>
#include <libgnomedb/libgnomedb.h>


G_BEGIN_DECLS

#define WS_TABLES_TYPE          (ws_tables_get_type())
#define WS_TABLES(obj)          G_TYPE_CHECK_INSTANCE_CAST (obj, ws_tables_get_type(), WsTables)
#define WS_TABLES_CLASS(klass)  G_TYPE_CHECK_CLASS_CAST (klass, ws_tables_get_type (), WsTablesClass)
#define IS_WS_TABLES(obj)       G_TYPE_CHECK_INSTANCE_TYPE (obj, ws_tables_get_type ())

typedef struct _WsTables        WsTables;
typedef struct _WsTablesClass   WsTablesClass;
typedef struct _WsTablesPrivate WsTablesPrivate;

/* struct for the object's data */
struct _WsTables
{
	GObject                object;
	WsTablesPrivate       *priv;
};

/* struct for the object's class */
struct _WsTablesClass
{
	GObjectClass           parent_class;
};

GType           ws_tables_get_type          (void);
GObject        *ws_tables_new               (GdaConnection *cnc);

G_END_DECLS

#endif
