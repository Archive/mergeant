/* Mergeant
 *
 * Copyright (C) 1999 - 2008 Vivien Malerba
 * Copyright (C) 2002 - 2004 Rodrigo Moya
 *
 * Authors:
 *       Vivien Malerba <malerba@gnome-db.org>
 *       Rodrigo Moya <rodrigo@gnome-db.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <stdio.h>
#include <libgnomedb/libgnomedb.h>
#ifdef HAVE_LOCALE_H
#include <locale.h>
#endif
#include <gtk/gtk.h>
#include "workspace-window.h"
#include "session.h"

/* options */
gchar *pass = NULL;
gchar *user = NULL;
gchar *page = NULL;
gboolean list_all = FALSE;

static GOptionEntry entries[] =
{
	{ "list-all", 'l', 0, G_OPTION_ARG_NONE, &list_all, "List all configured data sources", NULL },
	{ "user", 'U', 0, G_OPTION_ARG_STRING, &user, "Username", "username" },
	{ "password", 'P', 0, G_OPTION_ARG_STRING, &pass, "Password", "password" },
	{ "show-page", 's', 0, G_OPTION_ARG_STRING, &page, "Start with page P (Tables/Queries/Datatypes/Relations)", "P" },
	{ NULL }
};

int
main (int argc, char *argv[])
{
	GError *error = NULL;
        GOptionContext *context;

	/* Initialize i18n support */
        gtk_set_locale ();

	/* command line parsing */
        context = g_option_context_new ("<Data source>");
        g_option_context_add_main_entries (context, entries, GETTEXT_PACKAGE);
        g_option_context_add_group (context, gtk_get_option_group (TRUE));
        if (!g_option_context_parse (context, &argc, &argv, &error)) {
                g_warning ("Can't parse arguments: %s", error->message);
                exit (1);
        }
        g_option_context_free (context);


	/* initialize libraries */
	gnome_db_init ();
	gtk_init (&argc, &argv);

	if (list_all) {
		GdaDataModel *model;
		gchar *appname;

		model = gda_config_list_dsn ();
		appname = gnome_db_get_application_exec_path ("gnome-database-properties");
		g_print ("==================== List of configured data sources ====================\n"
			 "Use the %s program to add or remove data sources.\n", appname);
		g_free (appname);
		gda_data_model_dump (model, stdout);
		g_object_unref (model);

		return 0;
	}

	if (page) {
		if ((*page != 't') && (*page != 'T') && 
		    (*page != 'q') && (*page != 'Q') && 
		    (*page != 'd') && (*page != 'D') && 
		    (*page != 'r') && (*page != 'R')) {
			g_warning ("Invalid start page specification: %s\n", page);
			exit (1);
		}
	}

	/* initialize application modules */
	session_setup ();

	/* run application */
	if (argc == 2) {
		GdaDataSourceInfo *dsn;
		WorkspaceOptions *options;

		/* test DSN validity */
                dsn = gda_config_get_dsn (argv[1]);
                if (!dsn) {
                        g_print ("Can't find data source: %s\n", argv[1]);
                        exit (1);
                }

		/* starting */
		options = g_new0 (WorkspaceOptions, 1);
		options->dsn = argv[1];
		options->user = user;
		options->pass = pass;
		options->start_page = page;

		create_new_workspace_window_spec (options);
	}
	else {
		if (user || pass) 
			g_warning ("No data source specified, ignoring user and password specifications");
		create_new_workspace_window_spec (NULL);
	}
	gtk_main ();

	return 0;
}
