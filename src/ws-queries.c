/* ws-queries.c
 *
 * Copyright (C) 2004 - 2007 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <glib/gi18n-lib.h>
#include <libgnomedb-extra/libgnomedb-extra.h>
#include "ws-queries.h"
#include "workspace-page.h"
#include <string.h>
#include <glib-object.h>
#include <gtk/gtk.h>
#include "query-druid.h"
#include "query-editor.h"
#include "query-params-editor.h"

/* 
 * Main static functions 
 */
static void ws_queries_class_init (WsQueriesClass * class);
static void ws_queries_init (WsQueries *ws);
static void ws_queries_dispose (GObject *object);
static void ws_queries_finalize (GObject *object);

/* WorkspacePage interface */
static void            ws_queries_page_init       (WorkspacePageIface *iface);
static gchar          *ws_queries_get_name        (WorkspacePage *iface);
static gchar          *ws_queries_get_description (WorkspacePage *iface);
static GtkWidget      *ws_queries_get_sel_button  (WorkspacePage *iface);
static GtkWidget      *ws_queries_get_selector    (WorkspacePage *iface);
static GtkWidget      *ws_queries_get_work_area   (WorkspacePage *iface);
static GtkActionGroup *ws_queries_get_actions     (WorkspacePage *iface);
static const gchar    *ws_queries_get_actions_ui  (WorkspacePage *iface);

static void            opened_dialog_closed_cb    (GtkWidget *dlg, WsQueries *ws);

/* get a pointer to the parents to be able to call their destructor */
static GObjectClass  *parent_class = NULL;

/* private structure */
struct _WsQueriesPrivate
{
	GdaDict        *dict;
	GtkWidget      *selector;
	GtkWidget      *work_area;
	GtkActionGroup *actions;

	GtkWidget      *notebook;
	GtkWidget      *description;
	GtkWidget      *sql_editor;
	GtkWidget      *params_editor;

	GObject        *sel_obj;
	GSList         *opened_dialogs;
};

GType
ws_queries_get_type (void)
{
	static GType type = 0;

	if (!type) {
		static const GTypeInfo info = {
			sizeof (WsQueriesClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) ws_queries_class_init,
			NULL,
			NULL,
			sizeof (WsQueries),
			0,
			(GInstanceInitFunc) ws_queries_init
		};

		static const GInterfaceInfo workspace_page_info = {
			(GInterfaceInitFunc) ws_queries_page_init,
			NULL,
			NULL
		};

		type = g_type_register_static (G_TYPE_OBJECT, "WsQueries", &info, 0);
		g_type_add_interface_static (type, WORKSPACE_PAGE_TYPE, &workspace_page_info);
	}
	return type;
}

static void 
ws_queries_page_init (WorkspacePageIface *iface)
{
	iface->get_name = ws_queries_get_name;
	iface->get_description = ws_queries_get_description;
	iface->get_sel_button = ws_queries_get_sel_button;
	iface->get_selector = ws_queries_get_selector;
	iface->get_work_area = ws_queries_get_work_area;
	iface->get_actions = ws_queries_get_actions;
	iface->get_actions_ui = ws_queries_get_actions_ui;
}

static void
ws_queries_class_init (WsQueriesClass * class)
{
	GObjectClass   *object_class = G_OBJECT_CLASS (class);

	parent_class = g_type_class_peek_parent (class);

	object_class->dispose = ws_queries_dispose;
	object_class->finalize = ws_queries_finalize;
}

static void
ws_queries_init (WsQueries *ws)
{
	ws->priv = g_new0 (WsQueriesPrivate, 1);
}

static void ws_queries_initialize (WsQueries *ws);

/**
 * ws_queries_new
 * @dict: a #GdaDict object
 *
 * Creates a new WsQueries object
 *
 * Returns: the new object
 */
GObject*
ws_queries_new (GdaDict *dict)
{
	GObject  *obj;
	WsQueries *ws;

	g_return_val_if_fail (GDA_IS_DICT (dict), NULL);

	obj = g_object_new (WS_QUERIES_TYPE, NULL);
	ws = WS_QUERIES (obj);
	ws->priv->dict = dict;
	g_object_ref (G_OBJECT (dict));

	ws_queries_initialize (ws);

	return obj;
}


static void query_updated_cb (GdaDict *dict, GdaQuery *query, WsQueries *ws);
static void
ws_queries_dispose (GObject *object)
{
	WsQueries *ws;

	g_return_if_fail (object != NULL);
	g_return_if_fail (IS_WS_QUERIES (object));

	ws = WS_QUERIES (object);
	if (ws->priv) {
		while (ws->priv->opened_dialogs) 
			gtk_widget_destroy (GTK_WIDGET (ws->priv->opened_dialogs->data));
		
		if (ws->priv->selector) {
			gtk_widget_destroy (ws->priv->selector);
			ws->priv->selector = NULL;
		}

		if (ws->priv->work_area) {
			gtk_widget_destroy (ws->priv->work_area);
			ws->priv->work_area = NULL;
		}

		if (ws->priv->actions) {
			g_object_unref (ws->priv->actions);
			ws->priv->actions = NULL;
		}

		if (ws->priv->dict) {
			g_signal_handlers_disconnect_by_func (G_OBJECT (ws->priv->dict),
							      G_CALLBACK (query_updated_cb), ws);

			g_object_unref (G_OBJECT (ws->priv->dict));
			ws->priv->dict = NULL;
		}
	}

	/* parent class */
	parent_class->dispose (object);
}

static void
ws_queries_finalize (GObject   * object)
{
	WsQueries *ws;

	g_return_if_fail (object != NULL);
	g_return_if_fail (IS_WS_QUERIES (object));

	ws = WS_QUERIES (object);
	if (ws->priv) {
		g_free (ws->priv);
		ws->priv = NULL;
	}

	/* parent class */
	parent_class->finalize (object);
}



static void selector_selection_changed_cb (GnomeDbSelector *mgsel, GObject *sel_object, WsQueries *ws);
static void action_query_add_cb (GtkAction *action, WsQueries *ws);
static void action_query_edit_cb (GtkAction *action, WsQueries *ws);
static void action_query_del_cb (GtkAction *action, WsQueries *ws);
static void action_query_exec_cb (GtkAction *action, WsQueries *ws);

static GtkActionEntry ui_actions[] = {
	{ "Query", NULL, "_Query", NULL, "Query", NULL },
        { "QueryAdd", GTK_STOCK_ADD, "_New query", NULL, "Create a new query", 
	  G_CALLBACK (action_query_add_cb)},
        { "QueryDel", GTK_STOCK_DELETE, "_Delete", NULL, "Delete the selected query", 
	  G_CALLBACK (action_query_del_cb)},
        { "QueryEdit", GTK_STOCK_EDIT, "_Modify", NULL, "Modify the selected query", 
	  G_CALLBACK (action_query_edit_cb)},
        { "QueryExec", GTK_STOCK_EXECUTE, "_Execute", NULL, "Execute the selected query", 
	  G_CALLBACK (action_query_exec_cb)},
};

static const gchar *ui_actions_info =
	"<ui>"
	"  <menubar name='MenuBar'>"
	"    <placeholder name='PageExtension'>"
        "      <menu name='Query' action='Query'>"
        "        <menuitem name='QueryAdd' action= 'QueryAdd'/>"
        "        <menuitem name='QueryDel' action= 'QueryDel'/>"
	"        <separator/>"
	"        <menuitem name='QueryEdit' action= 'QueryEdit'/>"
	"        <menuitem name='QueryExec' action= 'QueryExec'/>"
        "      </menu>"
	"    </placeholder>"
	"  </menubar>"
        "  <toolbar  name='ToolBar'>"
        "    <toolitem action='QueryAdd'/>"
        "    <toolitem action='QueryDel'/>"
        "    <separator/>"
        "    <toolitem action='QueryEdit'/>"
        "    <toolitem action='QueryExec'/>"
        "  </toolbar>"
        "</ui>";


static void
ws_queries_initialize (WsQueries *ws)
{
	GtkWidget *label, *vbox, *nb, *vbox2, *wid, *hbox, *vbox3, *vp;
	GtkWidget *vbox4;
	gchar *str;

	/* Selector part */
	wid = gnome_db_selector_new (ws->priv->dict, NULL,
			       GNOME_DB_SELECTOR_QUERIES, GNOME_DB_SELECTOR_COLUMN_TYPE);
	ws->priv->selector = wid;
	g_signal_connect (G_OBJECT (ws->priv->selector), "selection_changed", 
			  G_CALLBACK (selector_selection_changed_cb), ws);

	/* WorkArea part */
	vbox = gtk_vbox_new (FALSE, 5);
	ws->priv->work_area = vbox;

	nb = gtk_notebook_new ();
	gtk_notebook_set_show_border (GTK_NOTEBOOK (nb), FALSE);
	gtk_notebook_set_show_tabs (GTK_NOTEBOOK (nb), FALSE);
	gtk_box_pack_start (GTK_BOX (vbox), nb, TRUE, TRUE, 0);
	gtk_widget_show (nb);
	ws->priv->notebook = nb;
	
	label = gtk_label_new (_("Please select a query from the list on the left,\n"
				 "or create a new one using the 'Add' button below."));
	gtk_notebook_append_page (GTK_NOTEBOOK (nb), label, NULL);
	gtk_widget_show (label);

	vp = gtk_vpaned_new ();
	gtk_notebook_append_page (GTK_NOTEBOOK (nb), vp, NULL);
	gtk_widget_show (vp);
	gtk_paned_set_position (GTK_PANED (vp), 370);
	
	vbox2 = gtk_vbox_new (FALSE, 5);
	gtk_paned_add1 (GTK_PANED (vp), vbox2);
	gtk_widget_show (vbox2);

	label = gtk_label_new (NULL);
	str = g_strdup_printf ("<b>%s:</b>", _("Description"));
	gtk_label_set_markup (GTK_LABEL (label), str);
	g_free (str);
	gtk_box_pack_start (GTK_BOX (vbox2), label, FALSE, FALSE, 0);
	gtk_widget_show (label);
	gtk_misc_set_alignment (GTK_MISC (label), 0., -1);

	hbox = gtk_hbox_new (FALSE, 0); /* HIG */
	gtk_box_pack_start (GTK_BOX (vbox2), hbox, FALSE, FALSE, 0);
	gtk_widget_show (hbox);
	label = gtk_label_new ("    ");
	gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);
	gtk_widget_show (label);

	label = gtk_label_new (NULL);
	gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);
	gtk_misc_set_alignment (GTK_MISC (label), 0., -1);
	gtk_widget_show (label);
	ws->priv->description = label;


	vbox4 = gtk_vbox_new (FALSE, 0);
	gtk_box_pack_start (GTK_BOX (vbox2), vbox4, TRUE, TRUE, 0);
	gtk_widget_show (vbox4);

	label = gtk_label_new (NULL);
	str = g_strdup_printf ("<b>%s:</b>", _("SQL statement"));
	gtk_label_set_markup (GTK_LABEL (label), str);
	g_free (str);
	gtk_box_pack_start (GTK_BOX (vbox4), label, FALSE, FALSE, 0);
	gtk_widget_show (label);
	gtk_misc_set_alignment (GTK_MISC (label), 0., -1);

	hbox = gtk_hbox_new (FALSE, 0); /* HIG */
	gtk_box_pack_start (GTK_BOX (vbox4), hbox, TRUE, TRUE, 0);
	gtk_widget_show (hbox);
	label = gtk_label_new ("    ");
	gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);
	gtk_widget_show (label);

	vbox3 = gtk_vbox_new (FALSE, 0);
	gtk_box_pack_start (GTK_BOX (hbox), vbox3, TRUE, TRUE, 0);
	gtk_widget_show (vbox3);

	label = gtk_label_new (NULL);
	str = g_strdup_printf ("<small>%s</small>", _("For information only, to edit, click on the \"Edit\" button."));
	gtk_label_set_markup (GTK_LABEL (label), str);
	g_free (str);
	gtk_box_pack_start (GTK_BOX (vbox3), label, FALSE, FALSE, 0);
	gtk_widget_show (label);
	gtk_misc_set_alignment (GTK_MISC (label), 0., -1);

	wid = gnome_db_editor_new ();
        gnome_db_editor_set_editable (GNOME_DB_EDITOR (wid), FALSE);
        gnome_db_editor_set_highlight (GNOME_DB_EDITOR (wid), TRUE);
        gtk_box_pack_start (GTK_BOX (vbox3), wid, TRUE, TRUE, 0);
	gtk_widget_show (wid);
	ws->priv->sql_editor = wid;


	vbox2 = gtk_vbox_new (FALSE, 5);
	gtk_paned_add2 (GTK_PANED (vp), vbox2);
	gtk_widget_show (vbox2);
	
	label = gtk_label_new (NULL);
	str = g_strdup_printf ("<b>%s:</b>", _("Query parameters"));
	gtk_label_set_markup (GTK_LABEL (label), str);
	g_free (str);
	gtk_box_pack_start (GTK_BOX (vbox2), label, FALSE, FALSE, 0);
	gtk_widget_show (label);
	gtk_misc_set_alignment (GTK_MISC (label), 0., -1);

	hbox = gtk_hbox_new (FALSE, 0); /* HIG */
	gtk_box_pack_start (GTK_BOX (vbox2), hbox, TRUE, TRUE, 0);
	gtk_widget_show (hbox);
	label = gtk_label_new ("    ");
	gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);
	gtk_widget_show (label);

	vbox3 = gtk_vbox_new (FALSE, 0);
	gtk_box_pack_start (GTK_BOX (hbox), vbox3, TRUE, TRUE, 0);
	gtk_widget_show (vbox3);

	label = gtk_label_new (NULL);
	str = g_strdup_printf ("<small>%s</small>", _("For information only, to edit, click on the \"Edit\" button."));
	gtk_label_set_markup (GTK_LABEL (label), str);
	g_free (str);
	gtk_box_pack_start (GTK_BOX (vbox3), label, FALSE, FALSE, 0);
	gtk_widget_show (label);
	gtk_misc_set_alignment (GTK_MISC (label), 0., -1);

	wid = query_params_editor_new (NULL, 0);
        gtk_box_pack_start (GTK_BOX (vbox3), wid, TRUE, TRUE, 0);
	gtk_widget_show (wid);
	ws->priv->params_editor = wid;

	/* actions part */
	ws->priv->actions = gtk_action_group_new ("Actions");
	gtk_action_group_add_actions (ws->priv->actions, ui_actions, G_N_ELEMENTS (ui_actions), ws);

	if (ws->priv->actions) {
		GtkAction *action;

		action = gtk_action_group_get_action (ws->priv->actions, "QueryDel");
		gtk_action_set_sensitive (action, FALSE);
		action = gtk_action_group_get_action (ws->priv->actions, "QueryEdit");
		gtk_action_set_sensitive (action, FALSE);
		action = gtk_action_group_get_action (ws->priv->actions, "QueryExec");
		gtk_action_set_sensitive (action, FALSE);
	}
	
	g_signal_connect (G_OBJECT (ws->priv->dict), "object_updated",
			  G_CALLBACK (query_updated_cb), ws);
}

static void query_info_display_update (GdaQuery *query, WsQueries *ws);
static void
selector_selection_changed_cb (GnomeDbSelector *mgsel, GObject *sel_object, WsQueries *ws)
{
	GtkAction *action;

	if (sel_object && !GDA_IS_QUERY (sel_object))
		return;
	
	ws->priv->sel_obj = sel_object;
	gtk_notebook_set_current_page (GTK_NOTEBOOK (ws->priv->notebook), sel_object ? 1 : 0);

	action = gtk_action_group_get_action (ws->priv->actions, "QueryDel");
	gtk_action_set_sensitive (action, sel_object ? TRUE : FALSE);
	action = gtk_action_group_get_action (ws->priv->actions, "QueryEdit");
	gtk_action_set_sensitive (action, sel_object ? TRUE : FALSE);
	action = gtk_action_group_get_action (ws->priv->actions, "QueryExec");
	gtk_action_set_sensitive (action, sel_object ? TRUE : FALSE);

	query_info_display_update ((GdaQuery*) sel_object, ws);
}

static void
query_updated_cb (GdaDict *dict, GdaQuery *query, WsQueries *ws)
{
	if (GDA_IS_QUERY (query) && ((GObject *)query == ws->priv->sel_obj))
		query_info_display_update (query, ws);
}

static void
dialog_exec_response_cb (GtkDialog *dlg, gint response, GObject *obj)
{
	gtk_widget_destroy (GTK_WIDGET (dlg));
}

static void
action_query_add_cb (GtkAction *action, WsQueries *ws)
{
	GtkWidget *druid;

	druid = query_druid_new (ws->priv->dict);
	gtk_widget_show (druid);

	ws->priv->opened_dialogs = g_slist_append (ws->priv->opened_dialogs, druid);
	g_signal_connect (G_OBJECT (druid), "destroy",
			  G_CALLBACK (opened_dialog_closed_cb), ws);
}

static void
action_query_edit_cb (GtkAction *action, WsQueries *ws)
{
	GtkWidget *dlg, *editor;
	GObject *sel_query = ws->priv->sel_obj;
	gchar *title;

	if (!sel_query)
		return;

	editor = query_editor_new (GDA_QUERY (sel_query));
	gtk_widget_show (editor);
	title = g_strdup_printf (_("Edition of query '%s'"), gda_object_get_name (GDA_OBJECT (sel_query)));
	dlg = gtk_dialog_new_with_buttons (title, NULL, 0, GTK_STOCK_CLOSE, GTK_RESPONSE_CLOSE, NULL);
	g_free (title);
	gtk_box_pack_start (GTK_BOX (GTK_DIALOG (dlg)->vbox), editor, TRUE, TRUE, 6);
	gtk_widget_set_size_request (dlg, 770, 570);
	gtk_widget_show (dlg);
	g_signal_connect (G_OBJECT (dlg), "response",
			  G_CALLBACK (dialog_exec_response_cb), NULL);

	ws->priv->opened_dialogs = g_slist_append (ws->priv->opened_dialogs, dlg);
	g_signal_connect (G_OBJECT (dlg), "destroy",
			  G_CALLBACK (opened_dialog_closed_cb), ws);
}

static void
action_query_del_cb (GtkAction *action, WsQueries *ws)
{
	GObject *sel_query = ws->priv->sel_obj;
	GtkWidget *dlg;

	if (!sel_query)
		return;

	dlg = gtk_message_dialog_new (NULL, GTK_DIALOG_MODAL, GTK_MESSAGE_QUESTION, GTK_BUTTONS_YES_NO,
				      _("Do you want to delete the '%s' query?"), 
				      gda_object_get_name (GDA_OBJECT (sel_query)));
	if (gtk_dialog_run (GTK_DIALOG (dlg)) == GTK_RESPONSE_YES) 
		gda_object_destroy (GDA_OBJECT (sel_query));

	gtk_widget_destroy (dlg);
}

/* extra action for the grid widgets */
static void extra_action_close_cb (GtkAction *action, GtkWidget *window);
static GtkActionEntry extra_actions[] = {
	{ "Close", GTK_STOCK_CLOSE, "_Close", NULL, "Close this window", G_CALLBACK (extra_action_close_cb)}
};
static const gchar *grid_actions =
	"<ui>"
	"  <toolbar  name='ToolBar'>"
	"    <toolitem action='Close'/>"
	"  </toolbar>"
	"</ui>";


static gint
fill_context_in_dialog (WsQueries *ws, GdaParameterList *context)
{
	GtkWidget *dlg;
	GnomeDbBasicForm *sform;
	GtkWidget *parent_window;
	gint result;

	if (!context || gda_parameter_list_is_valid (context))
		return GTK_RESPONSE_ACCEPT;
	
	parent_window = gtk_widget_get_toplevel (ws->priv->work_area);
	if (! GTK_WIDGET_TOPLEVEL (parent_window))
		parent_window = NULL;

	dlg = gnome_db_basic_form_new_in_dialog (context, GTK_WINDOW (parent_window),
						 _("Values to be filled"), 
						 _("<big><b>Required values:</b></big>\n"
						   "<small>The following values are required to "
						   "execute the query.</small>"));
	sform = g_object_get_data (G_OBJECT (dlg), "form");
	gnome_db_basic_form_set_entries_auto_default (sform, TRUE);
	
	gtk_widget_show (dlg);
	result = gtk_dialog_run (GTK_DIALOG (dlg));
	gtk_widget_destroy (dlg);

	return result;
}

typedef struct {
	GdaQuery       *query; /* copied, to free */
	GtkWidget      *grid_box;
	GtkWidget      *grid;
	GtkWidget      *combo; /* to free to unref the GdaQueryTarget objects referenced in the GdaDataModel */
	GdaQueryTarget *modif_target;
} QueryExecInstance;

static void
query_exec_instance_free (QueryExecInstance *qei)
{
	/* REM: we need to destroy qei->combo first because it uses a GdaDataModel which
	 * has GValue of type G_TYPE_OBJECT which increment the refence count of
	 * the GdaQueryTarget it references, and which will be actually destroyed by the 
	 * destruction of the query */
	if (qei->combo) 
		gtk_widget_destroy (qei->combo);
	if (qei->query)
		g_object_unref (G_OBJECT (qei->query));

	g_free (qei);
}

/**
 * utility_query_execute_modif
 * @query: the #GdaQuery to be executed
 * @context: a #GdaParameterList object
 * @ask_confirm_insert:
 * @ask_confirm_update:
 * @ask_confirm_delete:
 * @parent_window: a #GtkWindow object, or a #GtkWidget and the parent window will be found automatically
 * @user_cancelled: a place to store if the user cancelled the query if the choice was given, or %NULL
 * @query_error: a place to store if there was an error, or %NULL
 *
 * Executes @query and displays question and information dialogs if necessary.
 * If a user confirmation was required and the user cancelled the execution, then 
 * the returned value is FALSE.
 *
 * Returns: TRUE if the query was executed.
 */
static gboolean
utility_query_execute_modif (GdaQuery *query, GdaParameterList *context,
			     gboolean ask_confirm_insert,
			     gboolean ask_confirm_update,
			     gboolean ask_confirm_delete,
			     GtkWidget *parent_window,
			     gboolean *user_cancelled,
			     gboolean *query_error)
{
	gchar *sql = NULL;
	GdaQueryType qtype;
	gchar *confirm = NULL;
	gboolean do_execute = TRUE;
	gboolean allok = TRUE;
	GError *error = NULL;

	g_return_val_if_fail (query && GDA_IS_QUERY (query), FALSE);
	
	/* find the GtkWindow object for @parent_window */
	while (parent_window && !GTK_IS_WINDOW (parent_window)) 
		parent_window = gtk_widget_get_parent (parent_window);

	sql = gda_renderer_render_as_sql (GDA_RENDERER (query), context, NULL, 0, &error);
	qtype = gda_query_get_query_type (query);

	switch (qtype) {
	case GDA_QUERY_TYPE_INSERT:
		if (ask_confirm_insert)
			confirm = _("Execute the following insertion query?");
		break;
	case GDA_QUERY_TYPE_UPDATE:
		if (ask_confirm_update)
			confirm = _("Execute the following update query?");
		break;
	case GDA_QUERY_TYPE_DELETE:
		if (ask_confirm_delete)
			confirm = _("Execute the following deletion query?");
		break;
	default:
		g_assert_not_reached ();
	}

	if (user_cancelled)
		*user_cancelled = FALSE;
	if (query_error)
		*query_error = FALSE;

	if (sql) {
		if (confirm) {
			GtkWidget *dlg;
			gint result;
			gchar *msg;
			
			msg = g_strdup_printf (_("<b><big>%s</big></b>\n"
						 "<small>The preferences require a confirmation for the "
						 "following query</small>\n\n%s"), confirm, sql);
			dlg = gtk_message_dialog_new (GTK_WINDOW (parent_window), 0,
						      GTK_MESSAGE_QUESTION,
						      GTK_BUTTONS_YES_NO, msg);
			g_free (msg);
			gtk_label_set_use_markup (GTK_LABEL (GTK_MESSAGE_DIALOG (dlg)->label), TRUE);
			result = gtk_dialog_run (GTK_DIALOG (dlg));
			gtk_widget_destroy (dlg);
			do_execute = (result == GTK_RESPONSE_YES);
			if (user_cancelled)
				*user_cancelled = !do_execute;
		}
			
		if (do_execute) {
			GdaDict *dict = gda_object_get_dict (GDA_OBJECT (query));
			GdaCommand *cmd;
			gint res;
#ifdef debug
			g_print ("MODIF SQL: %s\n", sql);
#endif
			cmd = gda_command_new (sql, GDA_COMMAND_TYPE_SQL, 0);
			res = gda_connection_execute_non_select_command (gda_dict_get_connection (dict), cmd, 
									 NULL, &error);
			gda_command_free (cmd);
			if (res == -1) {
				GtkWidget *dlg;
				
				dlg = gtk_message_dialog_new (GTK_WINDOW (parent_window), 0,
							      GTK_MESSAGE_ERROR,
							      GTK_BUTTONS_CLOSE,
							      error->message);
				gtk_dialog_run (GTK_DIALOG (dlg));
				gtk_widget_destroy (dlg);
				allok = FALSE;

				if (query_error)
					*query_error = TRUE;
			}
			if (error)
				g_error_free (error);
		}
		else
			allok = FALSE;
		
		g_free (sql);
	}
	else {
		GtkWidget *dlg;
		gchar *message;
		
		if (error) {
			message = g_strdup_printf (_("The following error occurred while preparing the query:\n%s"),
						   error->message);
			g_error_free (error);
		}
		else
			message = g_strdup_printf (_("An unknown error occurred while preparing the query."));
		
		dlg = gtk_message_dialog_new (GTK_WINDOW (parent_window), 0,
					      GTK_MESSAGE_ERROR,
					      GTK_BUTTONS_CLOSE,
					      message);
		g_free (message);
		gtk_dialog_run (GTK_DIALOG (dlg));
		gtk_widget_destroy (dlg);
		allok = FALSE;

		if (query_error)
			*query_error = TRUE;
	}

	return allok;
}

static void query_exec_window_destroy_cb (GtkWindow *window, QueryExecInstance *qei);
static void
action_query_exec_cb (GtkAction *action, WsQueries *ws)
{
	GdaQuery *query;
	GdaParameterList *context;
	GtkWidget *parent_window;
	QueryExecInstance *qei;
	
	parent_window = gtk_widget_get_toplevel (ws->priv->work_area);
	if (! GTK_WIDGET_TOPLEVEL (parent_window))
		parent_window = NULL;
		
	query = (GdaQuery *) ws->priv->sel_obj;

	/* make a copy of @query so we are not impacted by any future modification of @query */
	qei = g_new0 (QueryExecInstance, 1);
	qei->query = gda_query_new_copy (query, NULL);
	query = qei->query;

	if (gda_query_is_modify_query (query)) {
		/* modification query */
		context = gda_query_get_parameter_list (query);
		if (fill_context_in_dialog (ws, context) == GTK_RESPONSE_ACCEPT) 
			utility_query_execute_modif (query, context, TRUE, TRUE, TRUE, parent_window, NULL, NULL);
		g_object_unref (G_OBJECT (context));
		query_exec_instance_free (qei);
	}
	else {
		/* SELECT query */
		if (gda_query_is_select_query (query)) {
			/* parsed SELECT query */
			GtkWidget *grid;
			GdaDataModel *model;
			GError *error = NULL;

			model = gda_data_model_query_new (query);
			if (!gda_data_model_query_refresh (GDA_DATA_MODEL_QUERY (model), &error)) {
				GtkWidget *dlg;
				gchar *msg, *errmsg;

				if (error && error->message)
					errmsg = g_markup_escape_text (error->message, -1);
				else
					errmsg = _("No detail");
				msg = g_strdup_printf ("<b>%s</b>\n%s", 
						       _("Can't execute query:"), errmsg);
				if (error && error->message)
					g_free (errmsg);
				
				dlg = gtk_message_dialog_new_with_markup (GTK_WINDOW (parent_window), 0,
									  GTK_MESSAGE_ERROR,
									  GTK_BUTTONS_CLOSE,
									  msg);
				g_free (msg);
				if (error)
					g_error_free (error);
				gtk_dialog_run (GTK_DIALOG (dlg));
				gtk_widget_destroy (dlg);
				return;
			}
			
			grid = gnome_db_grid_new (model);

			qei->grid = grid;
			
			context = gda_data_model_query_get_parameter_list (GDA_DATA_MODEL_QUERY (model));
			g_object_unref (model);

			if (fill_context_in_dialog (ws, context) == GTK_RESPONSE_ACCEPT) {
				GtkWidget *window, *vbox, *toolbar;
				gchar *str;
				GtkActionGroup *extra_group;
				GtkUIManager *ui;

				str = g_strdup_printf (_("Execution of query '%s'"), 
						       gda_object_get_name (GDA_OBJECT (query)));
				window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
				g_signal_connect (G_OBJECT (window), "destroy",
						  G_CALLBACK (query_exec_window_destroy_cb), qei);
				gtk_window_set_title (GTK_WINDOW (window), str);
				g_free (str);				

				vbox = gtk_vbox_new (FALSE, 0);
				gtk_container_add (GTK_CONTAINER (window), vbox);
				gtk_widget_show (vbox);
				qei->grid_box = vbox;
								
				ui = gtk_ui_manager_new ();
				extra_group = gtk_action_group_new ("ExtraActions");
				gtk_action_group_add_actions (extra_group, extra_actions, 
							      G_N_ELEMENTS (extra_actions),
							      window);
				gtk_ui_manager_insert_action_group (ui, extra_group, 0);
				gtk_ui_manager_add_ui_from_string (ui, grid_actions, -1, NULL);
				toolbar = gtk_ui_manager_get_widget (ui, "/ToolBar");
				gtk_box_pack_start (GTK_BOX (vbox), toolbar, FALSE, FALSE, 0);
				gtk_widget_show (toolbar);
				
				gtk_box_pack_start (GTK_BOX (vbox), grid, TRUE, TRUE, 0);
				gtk_widget_show (grid);
				gtk_widget_set_size_request (grid, 800, 300);
				
				gtk_widget_show (window);

				ws->priv->opened_dialogs = g_slist_append (ws->priv->opened_dialogs, window);
				g_signal_connect (G_OBJECT (window), "destroy",
						  G_CALLBACK (opened_dialog_closed_cb), ws);
			}
			else {
				gtk_widget_destroy (grid);
				query_exec_instance_free (qei);
			}
		}
		else {
			/* unparsed query */
			context = gda_query_get_parameter_list (query);
			if (fill_context_in_dialog (ws, context) == GTK_RESPONSE_ACCEPT) {
				GtkWidget *dlg;
				gint result;
				gchar *msg;
				const gchar *sql;
				
				sql = gda_renderer_render_as_sql (GDA_RENDERER (query), context, NULL,
								  GDA_RENDERER_EXTRA_PRETTY_SQL, NULL);
				query_exec_instance_free (qei);
				msg = g_strdup_printf (_("<b><big>Execute the following query ?</big></b>\n"
							 "<small>This query can't be parsed: it may contain some "
							 "syntax or grammar which is dependant on the type of database "
							 "used, or may contain some error.</small>\n\n%s"), sql);
				dlg = gtk_message_dialog_new (GTK_WINDOW (parent_window), 0,
							      GTK_MESSAGE_QUESTION,
							      GTK_BUTTONS_YES_NO, msg);
				g_free (msg);
				gtk_label_set_use_markup (GTK_LABEL (GTK_MESSAGE_DIALOG (dlg)->label), TRUE);
				result = gtk_dialog_run (GTK_DIALOG (dlg));
				gtk_widget_destroy (dlg);
				if (result == GTK_RESPONSE_YES) {
					GError *error = NULL;
					GdaDataModel *data;
					GdaCommand *cmd;
					
					cmd = gda_command_new (sql, GDA_COMMAND_TYPE_SQL, 0);
					data = gda_connection_execute_select_command (gda_dict_get_connection (ws->priv->dict),
										      cmd, NULL, &error);
					
					if (error) {
						GtkWidget *dlg;
						gchar *message;
						
						message = g_strdup (error->message);
						g_error_free (error);
						dlg = gtk_message_dialog_new (GTK_WINDOW (parent_window), 0,
									      GTK_MESSAGE_ERROR,
									      GTK_BUTTONS_CLOSE,
									      message);
						g_free (message);
						gtk_dialog_run (GTK_DIALOG (dlg));
						gtk_widget_destroy (dlg);
					}
					else {
						if (data) {
							GtkWidget *grid, *window, *vbox, *toolbar;
							GtkActionGroup *extra_group;
							GtkUIManager *ui;

							grid = gnome_db_grid_new (data);
							gtk_widget_set_size_request (grid, 800, 300);
							g_object_unref (G_OBJECT (data));
							
							window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
							gtk_window_set_title (GTK_WINDOW (window), _("Execution results"));
							
							vbox = gtk_vbox_new (FALSE, 0);
							gtk_container_add (GTK_CONTAINER (window), vbox);
							gtk_widget_show (vbox);
							
							ui = gtk_ui_manager_new ();
							extra_group = gtk_action_group_new ("ExtraActions");
							gtk_action_group_add_actions (extra_group, extra_actions, 
										      G_N_ELEMENTS (extra_actions),
										      window);
							gtk_ui_manager_insert_action_group (ui, extra_group, 0);
							gtk_ui_manager_add_ui_from_string (ui, grid_actions, -1, NULL);
							toolbar = gtk_ui_manager_get_widget (ui, "/ToolBar");
							gtk_box_pack_start (GTK_BOX (vbox), toolbar, FALSE, FALSE, 0);
							gtk_widget_show (toolbar);
							
							gtk_box_pack_start (GTK_BOX (vbox), grid, TRUE, TRUE, 0);
							gtk_widget_show (grid);
							gtk_widget_show (window);

							ws->priv->opened_dialogs = g_slist_append (ws->priv->opened_dialogs, window);
							g_signal_connect (G_OBJECT (window), "destroy",
									  G_CALLBACK (opened_dialog_closed_cb), ws);
						}
					}
				}
			}
		}
	}
}

static void
extra_action_close_cb (GtkAction *action, GtkWidget *window)
{
	gtk_widget_destroy (window);
}

static void
query_exec_window_destroy_cb (GtkWindow *window, QueryExecInstance *qei)
{
	query_exec_instance_free (qei);
}

static void
query_info_display_update (GdaQuery *query, WsQueries *ws)
{
	const gchar *str = NULL;
	gchar *title = NULL;
	
	if (query)
		str = gda_object_get_description (GDA_OBJECT (query));
	if (str && *str) 
		gtk_label_set_text (GTK_LABEL (ws->priv->description), str);
	else
		gtk_label_set_text (GTK_LABEL (ws->priv->description), "---");

	/* global title update */
	title = ws_queries_get_description (WORKSPACE_PAGE (ws));
	g_signal_emit_by_name (G_OBJECT (ws), "description_changed", title);
	g_free (title);

	/* SQL version of the query */
	if (query) {
		gchar *sql;
		GError *error = NULL;
		sql = gda_renderer_render_as_sql (GDA_RENDERER (query), NULL, NULL,
						  GDA_RENDERER_EXTRA_PRETTY_SQL, &error);
		if (sql) {
			gnome_db_editor_set_text (GNOME_DB_EDITOR (ws->priv->sql_editor), sql, -1);
			g_free (sql);
		}
		else {
			if (error) {
				str = error->message;
				gnome_db_editor_set_text (GNOME_DB_EDITOR (ws->priv->sql_editor), str, -1);
				g_error_free (error);
			}
			else
				gnome_db_editor_set_text (GNOME_DB_EDITOR (ws->priv->sql_editor),
							  _("Non reported error"), -1);
		}
	}
	else
		gnome_db_editor_set_text (GNOME_DB_EDITOR (ws->priv->sql_editor), "", -1);

	/* query parameters */
	query_params_editor_set_query (QUERY_PARAMS_EDITOR (ws->priv->params_editor), query);
}



/* 
 * WorkspacePage interface implementation
 */
static gchar *
ws_queries_get_name (WorkspacePage *iface)
{
	g_return_val_if_fail (IS_WS_QUERIES (iface), NULL);
	g_return_val_if_fail (WS_QUERIES (iface)->priv, NULL);

	return g_strdup_printf (_("Queries"));
}

static gchar *
ws_queries_get_description (WorkspacePage *iface)
{
	gchar *str = NULL;
	WsQueries *ws;

	g_return_val_if_fail (IS_WS_QUERIES (iface), NULL);
	g_return_val_if_fail (WS_QUERIES (iface)->priv, NULL);

	ws = WS_QUERIES (iface);
	if (ws->priv->sel_obj) {
		const gchar *cstr;
		 cstr =  gda_object_get_name (GDA_OBJECT (ws->priv->sel_obj));
		 str = g_strdup_printf ("Query: <b>%s</b>", (cstr && *cstr) ? cstr : "---");
	}
	else
		str = g_strdup_printf ("<b>%s</b>", _("No query selected"));

	return str;
}

static GtkWidget *
ws_queries_get_sel_button (WorkspacePage *iface)
{
	GdkPixbuf *pixbuf;
	GtkWidget *button, *wid, *hbox;

	g_return_val_if_fail (IS_WS_QUERIES (iface), NULL);
	g_return_val_if_fail (WS_QUERIES (iface)->priv, NULL);

	hbox = gtk_hbox_new (FALSE, 0);

	pixbuf = gnome_db_stock_get_icon_pixbuf (GNOME_DB_STOCK_QUERY);
	wid = gtk_image_new_from_pixbuf (pixbuf);
	g_object_unref (pixbuf);
	gtk_box_pack_start (GTK_BOX (hbox), wid, FALSE, FALSE, 0);
	gtk_widget_show (wid);

	wid = gtk_label_new (_("Queries"));
	gtk_box_pack_start (GTK_BOX (hbox), wid, FALSE, FALSE, 5);
	gtk_widget_show (wid);

	button = gtk_toggle_button_new ();
	gtk_container_add (GTK_CONTAINER (button), hbox);
	gtk_widget_show (hbox);

	return button;
}

static GtkWidget *
ws_queries_get_selector (WorkspacePage *iface)
{
	g_return_val_if_fail (IS_WS_QUERIES (iface), NULL);
	g_return_val_if_fail (WS_QUERIES (iface)->priv, NULL);

	return WS_QUERIES (iface)->priv->selector;
}

static GtkWidget *
ws_queries_get_work_area (WorkspacePage *iface)
{
	g_return_val_if_fail (IS_WS_QUERIES (iface), NULL);
	g_return_val_if_fail (WS_QUERIES (iface)->priv, NULL);

	return WS_QUERIES (iface)->priv->work_area;
}

static GtkActionGroup *
ws_queries_get_actions (WorkspacePage *iface)
{
	g_return_val_if_fail (IS_WS_QUERIES (iface), NULL);
	g_return_val_if_fail (WS_QUERIES (iface)->priv, NULL);

	return WS_QUERIES (iface)->priv->actions;
}

static const gchar *
ws_queries_get_actions_ui (WorkspacePage *iface)
{
	g_return_val_if_fail (IS_WS_QUERIES (iface), NULL);
	g_return_val_if_fail (WS_QUERIES (iface)->priv, NULL);

	return ui_actions_info;
}

static void
opened_dialog_closed_cb (GtkWidget *dlg, WsQueries *ws)
{
	ws->priv->opened_dialogs = g_slist_remove (ws->priv->opened_dialogs, dlg);
}
