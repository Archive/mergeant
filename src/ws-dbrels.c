/* ws-dbrels.c
 *
 * Copyright (C) 2004 - 2007 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <libgnomedb-extra/libgnomedb-extra.h>
#include "ws-dbrels.h"
#include "workspace-page.h"
#include <string.h>
#include <glib-object.h>
#include <gtk/gtk.h>
#include <glib/gi18n-lib.h>

#ifdef HAVE_GNOMEDB_GOOCANVAS
#include <libgnomedb-goo/gnome-db-goo.h>
#include <libgnomedb-goo/gnome-db-goo-db-relations.h>
#else
#include <libgnomedb-graph/gnome-db-canvas.h>
#include <libgnomedb-graph/gnome-db-canvas-db-relations.h>
#endif

/* 
 * Main static functions 
 */
static void ws_dbrels_class_init (WsDbrelsClass * class);
static void ws_dbrels_init (WsDbrels *ws);
static void ws_dbrels_dispose (GObject *object);
static void ws_dbrels_finalize (GObject *object);

/* WorkspacePage interface */
static void            ws_dbrels_page_init       (WorkspacePageIface *iface);
static gchar          *ws_dbrels_get_name        (WorkspacePage *iface);
static gchar          *ws_dbrels_get_description (WorkspacePage *iface);
static GtkWidget      *ws_dbrels_get_sel_button  (WorkspacePage *iface);
static GtkWidget      *ws_dbrels_get_selector    (WorkspacePage *iface);
static GtkWidget      *ws_dbrels_get_work_area   (WorkspacePage *iface);
static GtkActionGroup *ws_dbrels_get_actions     (WorkspacePage *iface);
static const gchar    *ws_dbrels_get_actions_ui  (WorkspacePage *iface);

static void            opened_dialog_closed_cb   (GtkWidget *dlg, WsDbrels *ws);

/* get a pointer to the parents to be able to call their destructor */
static GObjectClass  *parent_class = NULL;

/* private structure */
struct _WsDbrelsPrivate
{
	GdaDict        *dict;
	GtkWidget      *selector;
	GtkWidget      *work_area;
	GtkActionGroup *actions;

	GtkWidget      *notebook;
	GtkWidget      *description;
	GtkWidget      *canvas;
	GtkWidget      *zoom_in;
	GtkWidget      *zoom_out;
	GtkWidget      *zoom_fit;

	GObject        *sel_obj;
	GSList         *opened_dialogs;
};

GType
ws_dbrels_get_type (void)
{
	static GType type = 0;

	if (!type) {
		static const GTypeInfo info = {
			sizeof (WsDbrelsClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) ws_dbrels_class_init,
			NULL,
			NULL,
			sizeof (WsDbrels),
			0,
			(GInstanceInitFunc) ws_dbrels_init
		};

		static const GInterfaceInfo workspace_page_info = {
			(GInterfaceInitFunc) ws_dbrels_page_init,
			NULL,
			NULL
		};

		type = g_type_register_static (G_TYPE_OBJECT, "WsDbrels", &info, 0);
		g_type_add_interface_static (type, WORKSPACE_PAGE_TYPE, &workspace_page_info);
	}
	return type;
}

static void 
ws_dbrels_page_init (WorkspacePageIface *iface)
{
	iface->get_name = ws_dbrels_get_name;
	iface->get_description = ws_dbrels_get_description;
	iface->get_sel_button = ws_dbrels_get_sel_button;
	iface->get_selector = ws_dbrels_get_selector;
	iface->get_work_area = ws_dbrels_get_work_area;
	iface->get_actions = ws_dbrels_get_actions;
	iface->get_actions_ui = ws_dbrels_get_actions_ui;
}

static void
ws_dbrels_class_init (WsDbrelsClass * class)
{
	GObjectClass   *object_class = G_OBJECT_CLASS (class);

	parent_class = g_type_class_peek_parent (class);

	object_class->dispose = ws_dbrels_dispose;
	object_class->finalize = ws_dbrels_finalize;
}

static void
ws_dbrels_init (WsDbrels *ws)
{
	ws->priv = g_new0 (WsDbrelsPrivate, 1);
}

static void ws_dbrels_initialize (WsDbrels *ws);

/**
 * ws_dbrels_new
 * @dict: a #GdaDict object
 *
 * Creates a new WsDbrels object
 *
 * Returns: the new object
 */
GObject*
ws_dbrels_new (GdaDict *dict)
{
	GObject  *obj;
	WsDbrels *ws;

	g_return_val_if_fail (dict && GDA_IS_DICT (dict), NULL);

	obj = g_object_new (WS_DBRELS_TYPE, NULL);
	ws = WS_DBRELS (obj);
	ws->priv->dict = dict;
	g_object_ref (G_OBJECT (dict));

	ws_dbrels_initialize (ws);

	return obj;
}


static void
ws_dbrels_dispose (GObject *object)
{
	WsDbrels *ws;

	g_return_if_fail (object != NULL);
	g_return_if_fail (IS_WS_DBRELS (object));

	ws = WS_DBRELS (object);
	if (ws->priv) {
		while (ws->priv->opened_dialogs) 
			gtk_widget_destroy (GTK_WIDGET (ws->priv->opened_dialogs->data));

		if (ws->priv->selector) {
			gtk_widget_destroy (ws->priv->selector);
			ws->priv->selector = NULL;
		}

		if (ws->priv->work_area) {
			gtk_widget_destroy (ws->priv->work_area);
			ws->priv->work_area = NULL;
		}

		if (ws->priv->actions) {
			g_object_unref (ws->priv->actions);
			ws->priv->actions = NULL;
		}

		if (ws->priv->dict) {
			g_object_unref (G_OBJECT (ws->priv->dict));
			ws->priv->dict = NULL;
		}
	}

	/* parent class */
	parent_class->dispose (object);
}

static void
ws_dbrels_finalize (GObject   * object)
{
	WsDbrels *ws;

	g_return_if_fail (object != NULL);
	g_return_if_fail (IS_WS_DBRELS (object));

	ws = WS_DBRELS (object);
	if (ws->priv) {
		g_free (ws->priv);
		ws->priv = NULL;
	}

	/* parent class */
	parent_class->finalize (object);
}



static void selector_selection_changed_cb (GnomeDbSelector *mgsel, GObject *sel_object, WsDbrels *ws);
static void zoom_in_clicked_cb (GtkButton *button, WsDbrels *ws);
static void zoom_out_clicked_cb (GtkButton *button, WsDbrels *ws);
static void zoom_fit_clicked_cb (GtkButton *button, WsDbrels *ws);
static void auto_layout_clicked_cb (GtkButton *button, WsDbrels *ws);
static void action_graph_add_cb (GtkButton *button, WsDbrels *ws);
static void action_graph_edit_cb (GtkButton *button, WsDbrels *ws);
static void action_graph_del_cb (GtkButton *button, WsDbrels *ws);

static GtkActionEntry ui_actions[] = {
	{ "Relation", NULL, "_Relation", NULL, "Relation", NULL },
        { "GraphAdd", GTK_STOCK_ADD, "_New graph", NULL, "Create a new graph", 
	  G_CALLBACK (action_graph_add_cb)},
        { "GraphDel", GTK_STOCK_DELETE, "_Delete", NULL, "Delete the selected graph", 
	  G_CALLBACK (action_graph_del_cb)},
        { "GraphProp", GTK_STOCK_PROPERTIES, "_Properties", NULL, "Edit properties of the selected graph", 
	  G_CALLBACK (action_graph_edit_cb)},
};

static const gchar *ui_actions_info =
	"<ui>"
	"  <menubar name='MenuBar'>"
	"    <placeholder name='PageExtension'>"
        "      <menu name='Relation' action='Relation'>"
        "        <menuitem name='GraphAdd' action= 'GraphAdd'/>"
        "        <menuitem name='GraphDel' action= 'GraphDel'/>"
	"        <separator/>"
	"        <menuitem name='GraphProp' action= 'GraphProp'/>"
        "      </menu>"
	"    </placeholder>"
	"  </menubar>"
        "  <toolbar  name='ToolBar'>"
        "    <toolitem action='GraphAdd'/>"
        "    <toolitem action='GraphDel'/>"
        "    <separator/>"
        "    <toolitem action='GraphProp'/>"
        "  </toolbar>"
        "</ui>";

static void
ws_dbrels_initialize (WsDbrels *ws)
{
	GtkWidget *label, *vbox, *nb, *vbox2, *bbox, *button, *wid, *hbox;

	/* Selector part */
	wid = gnome_db_selector_new (ws->priv->dict, NULL,
			       GNOME_DB_SELECTOR_GRAPHS, 0);
	ws->priv->selector = wid;
	g_signal_connect (G_OBJECT (ws->priv->selector), "selection_changed", 
			  G_CALLBACK (selector_selection_changed_cb), ws);

	/* WorkArea part */
	vbox = gtk_vbox_new (FALSE, 5);
	ws->priv->work_area = vbox;
	
	nb = gtk_notebook_new ();
	gtk_notebook_set_show_border (GTK_NOTEBOOK (nb), FALSE);
	gtk_notebook_set_show_tabs (GTK_NOTEBOOK (nb), FALSE);
	gtk_box_pack_start (GTK_BOX (vbox), nb, TRUE, TRUE, 0);
	gtk_widget_show (nb);
	ws->priv->notebook = nb;
	
	label = gtk_label_new (_("Please select a graph from the list on the left,\n"
				 "or create a new one using the 'Add' button below."));
	gtk_notebook_append_page (GTK_NOTEBOOK (nb), label, NULL);
	gtk_widget_show (label);
	
	vbox2 = gtk_vbox_new (FALSE, 5);
	gtk_notebook_append_page (GTK_NOTEBOOK (nb), vbox2, NULL);
	gtk_widget_show (vbox2);

	label = gtk_label_new (NULL);
	gtk_label_set_markup (GTK_LABEL (label), _("<b>Description:</b>"));
	gtk_box_pack_start (GTK_BOX (vbox2), label, FALSE, FALSE, 0);
	gtk_widget_show (label);
	gtk_misc_set_alignment (GTK_MISC (label), 0., -1);

	hbox = gtk_hbox_new (FALSE, 0); /* HIG */
	gtk_box_pack_start (GTK_BOX (vbox2), hbox, FALSE, FALSE, 0);
	gtk_widget_show (hbox);
	label = gtk_label_new ("    ");
	gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);
	gtk_widget_show (label);

	label = gtk_label_new (NULL);
	gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);
	gtk_misc_set_alignment (GTK_MISC (label), 0., -1);
	gtk_widget_show (label);
	ws->priv->description = label;

	label = gtk_label_new (NULL);
	gtk_label_set_markup (GTK_LABEL (label), _("<b>Relations:</b>"));
	gtk_box_pack_start (GTK_BOX (vbox2), label, FALSE, FALSE, 0);
	gtk_widget_show (label);
	gtk_misc_set_alignment (GTK_MISC (label), 0., -1);

	hbox = gtk_hbox_new (FALSE, 0); /* HIG */
	gtk_box_pack_start (GTK_BOX (vbox2), hbox, TRUE, TRUE, 0);
	gtk_widget_show (hbox);
	label = gtk_label_new ("    ");
	gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);
	gtk_widget_show (label);

#ifdef HAVE_GNOMEDB_GOOCANVAS
	wid = gnome_db_goo_db_relations_new (ws->priv->dict);
	ws->priv->canvas = wid;
	wid = gnome_db_goo_set_in_scrolled_window (GNOME_DB_GOO (wid));
#else
	wid = gnome_db_canvas_db_relations_new (ws->priv->dict, NULL);
	ws->priv->canvas = wid;
	wid = gnome_db_canvas_set_in_scrolled_window (GNOME_DB_CANVAS (wid));
#endif
        gtk_box_pack_start (GTK_BOX (hbox), wid, TRUE, TRUE, 0);
	gtk_widget_show (wid);

	hbox = gtk_hbox_new (FALSE, 0);
	gtk_box_pack_start (GTK_BOX (vbox), hbox, FALSE, FALSE, 0);
	gtk_widget_show (hbox);

	bbox = gtk_hbutton_box_new ();
	gtk_button_box_set_layout (GTK_BUTTON_BOX (bbox), GTK_BUTTONBOX_SPREAD);
	gtk_box_pack_start (GTK_BOX (hbox), bbox, TRUE, TRUE, 0);
	gtk_widget_show (bbox);

	button = gtk_button_new ();
	wid = gtk_image_new_from_stock (GTK_STOCK_ZOOM_IN, GTK_ICON_SIZE_BUTTON);
	gtk_container_add (GTK_CONTAINER (button), wid);
	gtk_widget_show (wid);
	gtk_container_add (GTK_CONTAINER (bbox), button);
	gtk_widget_show (button);
	gtk_widget_set_sensitive (button, FALSE);
	ws->priv->zoom_in = button;
	g_signal_connect (G_OBJECT (button), "clicked",
			  G_CALLBACK (zoom_in_clicked_cb), ws);

	button = gtk_button_new ();
	wid = gtk_image_new_from_stock (GTK_STOCK_ZOOM_OUT, GTK_ICON_SIZE_BUTTON);
	gtk_container_add (GTK_CONTAINER (button), wid);
	gtk_widget_show (wid);
	gtk_container_add (GTK_CONTAINER (bbox), button);
	gtk_widget_show (button);
	gtk_widget_set_sensitive (button, FALSE);
	ws->priv->zoom_out = button;
	g_signal_connect (G_OBJECT (button), "clicked",
			  G_CALLBACK (zoom_out_clicked_cb), ws);

	button = gtk_button_new ();
	wid = gtk_image_new_from_stock (GTK_STOCK_ZOOM_FIT, GTK_ICON_SIZE_BUTTON);
	gtk_container_add (GTK_CONTAINER (button), wid);
	gtk_widget_show (wid);
	gtk_container_add (GTK_CONTAINER (bbox), button);
	gtk_widget_show (button);
	gtk_widget_set_sensitive (button, FALSE);
	ws->priv->zoom_fit = button;
	g_signal_connect (G_OBJECT (button), "clicked",
			  G_CALLBACK (zoom_fit_clicked_cb), ws);
	
#ifdef HAVE_GNOMEDB_GOOCANVAS
	if (gnome_db_goo_auto_layout_enabled (GNOME_DB_GOO (ws->priv->canvas))) {
		button = gtk_button_new_with_label (_("Automatic layout"));
		gtk_container_add (GTK_CONTAINER (bbox), button);
		gtk_widget_show (button);
		gtk_widget_set_sensitive (button, FALSE);
		ws->priv->zoom_out = button;
		g_signal_connect (G_OBJECT (button), "clicked",
				  G_CALLBACK (auto_layout_clicked_cb), ws);
	}
#endif

	wid = gtk_vseparator_new ();
	gtk_box_pack_start (GTK_BOX (hbox), wid, FALSE, FALSE, 0);
	gtk_widget_show (wid);

	bbox = gtk_hbutton_box_new ();
	gtk_button_box_set_layout (GTK_BUTTON_BOX (bbox), GTK_BUTTONBOX_SPREAD);
	gtk_box_pack_start (GTK_BOX (hbox), bbox, FALSE, FALSE, 0);
	gtk_widget_show (bbox);

	/* actions part */
	ws->priv->actions = gtk_action_group_new ("Actions");
	gtk_action_group_add_actions (ws->priv->actions, ui_actions, G_N_ELEMENTS (ui_actions), ws);

	if (ws->priv->actions) {
		GtkAction *action;

		action = gtk_action_group_get_action (ws->priv->actions, "GraphDel");
		gtk_action_set_sensitive (action, FALSE);

		action = gtk_action_group_get_action (ws->priv->actions, "GraphProp");
		gtk_action_set_sensitive (action, FALSE);
	}
}

static void graph_info_display_update (GdaGraph *graph, WsDbrels *ws);
static void
selector_selection_changed_cb (GnomeDbSelector *mgsel, GObject *sel_object, WsDbrels *ws)
{
	GtkAction *action;

	if (sel_object && !GDA_IS_GRAPH (sel_object))
		return;
	
	ws->priv->sel_obj = sel_object;
	gtk_notebook_set_current_page (GTK_NOTEBOOK (ws->priv->notebook), sel_object ? 1 : 0);

	gtk_widget_set_sensitive (ws->priv->zoom_in, sel_object ? TRUE : FALSE);
	gtk_widget_set_sensitive (ws->priv->zoom_out, sel_object ? TRUE : FALSE);
	gtk_widget_set_sensitive (ws->priv->zoom_fit, sel_object ? TRUE : FALSE);

	action = gtk_action_group_get_action (ws->priv->actions, "GraphDel");
	gtk_action_set_sensitive (action, sel_object ? TRUE : FALSE);
	action = gtk_action_group_get_action (ws->priv->actions, "GraphProp");
	gtk_action_set_sensitive (action, sel_object ? TRUE : FALSE);

	graph_info_display_update ((GdaGraph*) sel_object, ws);
}

static void
zoom_in_clicked_cb (GtkButton *button, WsDbrels *ws)
{
#ifdef HAVE_GNOMEDB_GOOCANVAS
	gnome_db_goo_set_zoom_factor (GNOME_DB_GOO (ws->priv->canvas), 
				      gnome_db_goo_get_zoom_factor (GNOME_DB_GOO (ws->priv->canvas)) + .05);
#else
	gnome_db_canvas_set_zoom_factor (GNOME_DB_CANVAS (ws->priv->canvas), 
					 GNOME_CANVAS (ws->priv->canvas)->pixels_per_unit + .05);
#endif
}

static void
zoom_out_clicked_cb (GtkButton *button, WsDbrels *ws)
{
#ifdef HAVE_GNOMEDB_GOOCANVAS
	gnome_db_goo_set_zoom_factor (GNOME_DB_GOO (ws->priv->canvas), 
				      gnome_db_goo_get_zoom_factor (GNOME_DB_GOO (ws->priv->canvas)) - .05);
#else
	gnome_db_canvas_set_zoom_factor (GNOME_DB_CANVAS (ws->priv->canvas), 
					 GNOME_CANVAS (ws->priv->canvas)->pixels_per_unit - .05);
#endif
}

static void
zoom_fit_clicked_cb (GtkButton *button, WsDbrels *ws)
{
#ifdef HAVE_GNOMEDB_GOOCANVAS
	gnome_db_goo_fit_zoom_factor (GNOME_DB_GOO (ws->priv->canvas));
#else
	gnome_db_canvas_fit_zoom_factor (GNOME_DB_CANVAS (ws->priv->canvas));
#endif
}

static void
auto_layout_clicked_cb (GtkButton *button, WsDbrels *ws)
{
#ifdef HAVE_GNOMEDB_GOOCANVAS
	gnome_db_goo_perform_auto_layout (GNOME_DB_GOO (ws->priv->canvas), FALSE,
					  GNOME_DB_GOO_LAYOUT_DEFAULT);
#endif
}

static void
action_graph_add_cb (GtkButton *button, WsDbrels *ws)
{
	GdaGraph *graph;
	GdaDict *dict = ws->priv->dict;

	graph = GDA_GRAPH (gda_graph_new (dict, GDA_GRAPH_DB_RELATIONS));
	gda_dict_assume_object (dict, (GdaObject *) graph);
	g_object_unref (G_OBJECT (graph));
}

static void
action_graph_edit_cb (GtkButton *button, WsDbrels *ws)
{
	const gchar *str;
	GtkWidget *wid, *vbox, *hbox, *dlg, *table, *name, *descr;
	gint result;
	GObject *sel_graph = ws->priv->sel_obj;
	
	dlg = gtk_dialog_new_with_buttons (_("Relations view properties"), NULL, 0,
					   GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
					   GTK_STOCK_OK, GTK_RESPONSE_ACCEPT, NULL);

	hbox = gtk_hbox_new (FALSE, 9);
	gtk_widget_show (hbox);
	gtk_box_pack_start (GTK_BOX (GTK_DIALOG (dlg)->vbox), hbox, TRUE, TRUE, 0);
	
	vbox = gtk_vbox_new (FALSE, 0);
	gtk_widget_show (vbox);
	gtk_box_pack_start (GTK_BOX (hbox), vbox, FALSE, FALSE, 0);

	wid = gtk_image_new_from_stock (GTK_STOCK_PROPERTIES, GTK_ICON_SIZE_DIALOG);
	gtk_widget_show (wid);
	gtk_box_pack_start (GTK_BOX (vbox), wid, FALSE, FALSE, 5);

	table = gtk_table_new (3, 2, FALSE);
	gtk_container_set_border_width (GTK_CONTAINER (table), 5);
	gtk_table_set_row_spacings (GTK_TABLE (table), 5);
	gtk_table_set_col_spacings (GTK_TABLE (table), 5);
	gtk_widget_show (table);
	gtk_box_pack_start (GTK_BOX (hbox), table, TRUE, TRUE, 0);

	wid = gtk_label_new (_("Name:"));
	gtk_widget_show (wid);
	gtk_table_attach (GTK_TABLE (table), wid, 0, 1, 1, 2, GTK_FILL, 0, 0, 0);
	gtk_label_set_justify (GTK_LABEL (wid), GTK_JUSTIFY_LEFT);
	gtk_misc_set_alignment (GTK_MISC (wid), 0, 0.5);

	wid = gtk_label_new (_("Description:"));
	gtk_widget_show (wid);
	gtk_table_attach (GTK_TABLE (table), wid, 0, 1, 2, 3, GTK_FILL, 0, 0, 0);
	gtk_label_set_justify (GTK_LABEL (wid), GTK_JUSTIFY_LEFT);
	gtk_misc_set_alignment (GTK_MISC (wid), 0, 0.5);

	name = gtk_entry_new ();
	gtk_widget_show (name);
	gtk_table_attach (GTK_TABLE (table), name, 1, 2, 1, 2, GTK_EXPAND | GTK_FILL, 0, 0, 0);
	str = gda_object_get_name (GDA_OBJECT (sel_graph));
	if (str)
		gtk_entry_set_text (GTK_ENTRY (name), str);

	descr = gtk_entry_new ();
	gtk_widget_show (descr);
	gtk_table_attach (GTK_TABLE (table), descr, 1, 2, 2, 3, GTK_EXPAND | GTK_FILL, 0, 0, 0);
	str = gda_object_get_description (GDA_OBJECT (sel_graph));
	if (str)
		gtk_entry_set_text (GTK_ENTRY (descr), str);

	wid = gtk_label_new (_("Enter the name and description for this database's relations representation."));
	gtk_widget_show (wid);
	gtk_table_attach (GTK_TABLE (table), wid, 0, 2, 0, 1, GTK_FILL, 0, 0, 5);
	gtk_label_set_justify (GTK_LABEL (wid), GTK_JUSTIFY_LEFT);
	gtk_label_set_line_wrap (GTK_LABEL (wid), TRUE);
	gtk_misc_set_alignment (GTK_MISC (wid), 0, 0.5);

	gtk_widget_show (dlg);
	result = gtk_dialog_run (GTK_DIALOG (dlg));
	if (result == GTK_RESPONSE_ACCEPT) {
		str = gtk_entry_get_text (GTK_ENTRY (name));
		gda_object_set_name (GDA_OBJECT (sel_graph), str);
		str = gtk_entry_get_text (GTK_ENTRY (descr));
		gda_object_set_description (GDA_OBJECT (sel_graph), str);
		
	}
	gtk_object_destroy (GTK_OBJECT (dlg));
}

static void
action_graph_del_cb (GtkButton *button, WsDbrels *ws)
{
	GObject *sel_graph = ws->priv->sel_obj;
	GtkWidget *dlg;
	const gchar *name;

	if (!sel_graph)
		return;

	name = gda_object_get_name (GDA_OBJECT (sel_graph));
	if (name && *name)
		dlg = gtk_message_dialog_new (NULL, GTK_DIALOG_MODAL, GTK_MESSAGE_QUESTION, GTK_BUTTONS_YES_NO,
					      _("Do you want to delete the '%s' graph?"), name);
	else
		dlg = gtk_message_dialog_new (NULL, GTK_DIALOG_MODAL, GTK_MESSAGE_QUESTION, GTK_BUTTONS_YES_NO,
					      _("Do you want to delete the graph <no name>?"));
	
	if (gtk_dialog_run (GTK_DIALOG (dlg)) == GTK_RESPONSE_YES) 
		gda_object_destroy (GDA_OBJECT (sel_graph));

	gtk_widget_destroy (dlg);
}

static void
graph_info_display_update (GdaGraph *graph, WsDbrels *ws)
{
	const gchar *str = NULL;
	gchar *title = NULL;

	if (graph)
		str = gda_object_get_description (GDA_OBJECT (graph));
	if (str && *str) 
		gtk_label_set_text (GTK_LABEL (ws->priv->description), str);
	else
		gtk_label_set_text (GTK_LABEL (ws->priv->description), "---");

	/* global title update */
	title = ws_dbrels_get_description (WORKSPACE_PAGE (ws));
	g_signal_emit_by_name (G_OBJECT (ws), "description_changed", title);
	g_free (title);
	
	/* graph */
	g_object_set (G_OBJECT (ws->priv->canvas), "graph", graph, NULL);
#ifdef HAVE_GNOMEDB_GOOCANVAS
	gnome_db_goo_fit_zoom_factor (GNOME_DB_GOO (ws->priv->canvas));
#else
	gnome_db_canvas_fit_zoom_factor (GNOME_DB_CANVAS (ws->priv->canvas));
#endif
}



/* 
 * WorkspacePage interface implementation
 */
static gchar *
ws_dbrels_get_name (WorkspacePage *iface)
{
	g_return_val_if_fail (IS_WS_DBRELS (iface), NULL);
	g_return_val_if_fail (WS_DBRELS (iface)->priv, NULL);

	return g_strdup_printf (_("Database Relations"));
}

static gchar *
ws_dbrels_get_description (WorkspacePage *iface)
{
	gchar *str = NULL;
	WsDbrels *ws;

	g_return_val_if_fail (IS_WS_DBRELS (iface), NULL);
	g_return_val_if_fail (WS_DBRELS (iface)->priv, NULL);

	ws = WS_DBRELS (iface);
	if (ws->priv->sel_obj) {
		const gchar *cstr;
		 cstr =  gda_object_get_name (GDA_OBJECT (ws->priv->sel_obj));
		 str = g_strdup_printf ("Database relation: <b>%s</b>", (cstr && *cstr) ? cstr : "---");
	}
	else
		str = g_strdup_printf ("<b>%s</b>", _("No database relation selected"));

	return str;
}

static GtkWidget *
ws_dbrels_get_sel_button (WorkspacePage *iface)
{
	GdkPixbuf *pixbuf;
	GtkWidget *button, *wid, *hbox;

	g_return_val_if_fail (IS_WS_DBRELS (iface), NULL);
	g_return_val_if_fail (WS_DBRELS (iface)->priv, NULL);

	hbox = gtk_hbox_new (FALSE, 0);

	pixbuf = gnome_db_stock_get_icon_pixbuf (GNOME_DB_STOCK_RELATIONS);
	wid = gtk_image_new_from_pixbuf (pixbuf);
	g_object_unref (pixbuf);
	gtk_box_pack_start (GTK_BOX (hbox), wid, FALSE, FALSE, 0);
	gtk_widget_show (wid);

	wid = gtk_label_new (_("Relations"));
	gtk_box_pack_start (GTK_BOX (hbox), wid, FALSE, FALSE, 5);
	gtk_widget_show (wid);

	button = gtk_toggle_button_new ();
	gtk_container_add (GTK_CONTAINER (button), hbox);
	gtk_widget_show (hbox);

	return button;
}

static GtkWidget *
ws_dbrels_get_selector (WorkspacePage *iface)
{
	g_return_val_if_fail (IS_WS_DBRELS (iface), NULL);
	g_return_val_if_fail (WS_DBRELS (iface)->priv, NULL);

	return WS_DBRELS (iface)->priv->selector;
}

static GtkWidget *
ws_dbrels_get_work_area (WorkspacePage *iface)
{
	g_return_val_if_fail (IS_WS_DBRELS (iface), NULL);
	g_return_val_if_fail (WS_DBRELS (iface)->priv, NULL);

	return WS_DBRELS (iface)->priv->work_area;
}

static GtkActionGroup *
ws_dbrels_get_actions (WorkspacePage *iface)
{
	g_return_val_if_fail (IS_WS_DBRELS (iface), NULL);
	g_return_val_if_fail (WS_DBRELS (iface)->priv, NULL);

	return WS_DBRELS (iface)->priv->actions;
}

static const gchar *
ws_dbrels_get_actions_ui (WorkspacePage *iface)
{
	g_return_val_if_fail (IS_WS_DBRELS (iface), NULL);
	g_return_val_if_fail (WS_DBRELS (iface)->priv, NULL);

	return ui_actions_info;
}

static void
opened_dialog_closed_cb (GtkWidget *dlg, WsDbrels *ws)
{
	ws->priv->opened_dialogs = g_slist_remove (ws->priv->opened_dialogs, dlg);
}
