/* Mergeant
 *
 * Copyright (C) 2006 - 2008 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef __UTIL_H__
#define __UTIL_H__

#include <libgnomedb/libgnomedb.h>

typedef void (*MergeantServerOpCallback) (GtkWidget *dlg, gboolean success, gpointer user_data);
#define MERGEANT_SERVER_OP_CALLBACK(x) ((MergeantServerOpCallback)(x))

GtkWidget *mergeant_server_op_create_dialog (GtkWindow *parent, GdaServerOperationType type, 
					     GdaConnection *cnc, GdaSet *options, 
					     const gchar *title, const gchar *foreword,
					     MergeantServerOpCallback done_cb, gpointer user_data);

#endif
