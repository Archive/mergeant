/* Mergeant
 *
 * Copyright (C) 1999 - 2008 Vivien Malerba
 * Copyright (C) 2002 - 2003 Rodrigo Moya
 *
 * Authors:
 *       Vivien Malerba <malerba@gnome-db.org>
 *       Rodrigo Moya <rodrigo@gnome-db.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <string.h>
#include <glib/gspawn.h>
#include <gtk/gtkstatusbar.h>
#include <gtk/gtktable.h>
#include <glib/gi18n-lib.h>
#include <libgnomedb/libgnomedb.h>
#include <libgnomedb/gnome-db-login-dialog.h>
#include <libgnomedb-extra/libgnomedb-extra.h>
#include <libgnomedb-extra/gnome-db-sql-console.h>
#include "workspace.h"
#include "workspace-window.h"
#include <libgda/gda-threader.h>
#include "utils.h"
#include "binreloc/mg-binreloc.h"

#define PROGRESS_BAR_TIMEOUT 20

typedef struct {
	GtkWidget      *window;
	GtkWidget      *workspace;
	GtkWidget      *status_bar;

	/* the connection to the database */
	GdaConnection  *cnc;
	
	GSList         *consoles; /* list of GtkDialog for Sql consoles */
	GtkWidget      *transaction_status;
} WorkspaceWindowPrivate;

static GList         *opened_workspaces = NULL;
static GtkWidget     *dsn_dialog = NULL;

static void perform_dbms_update (WorkspaceWindowPrivate *priv);
static void stop_dbms_update_cb (GtkWidget *dialog, gint response, GdaConnection *cnc);

static void
destroy_private_data_cb (WorkspaceWindowPrivate *priv)
{
	if (priv) {
		opened_workspaces = g_list_remove (opened_workspaces, priv);

		while (priv->consoles) 
			gtk_widget_destroy (GTK_WIDGET (priv->consoles->data));

		if (priv->transaction_status)
			gtk_widget_destroy (priv->transaction_status);

		if (priv->cnc) {
			g_object_unref (priv->cnc);
			priv->cnc = NULL;
		}

		g_free (priv);

		if (!opened_workspaces)
			gtk_main_quit ();
	}
}

static void
on_file_new_workspace (GtkAction *action, WorkspaceWindowPrivate *priv)
{
	create_new_workspace_window (NULL);
}

static void
on_file_copy_workspace (GtkAction *action, WorkspaceWindowPrivate *priv)
{
	create_new_workspace_window (priv->cnc);
}

static gboolean
treat_close_request (WorkspaceWindowPrivate *priv)
{
	GtkWidget *window = priv->window;
	GdaTransactionStatus *tstatus;
	gboolean do_close = TRUE;
	GList *list;

	g_assert (priv);
	g_assert (gda_connection_is_opened (priv->cnc));

	/* if the connection is used by another workspace, then allow closing of this window */
	for (list = opened_workspaces; list; list = list->next) {
		if (((WorkspaceWindowPrivate*) list->data != priv) &&
		    (((WorkspaceWindowPrivate*) list->data)->cnc == priv->cnc))
			return TRUE;
	}
	
	/* if a transaction is started, then try to commit it */
	tstatus = gda_connection_get_transaction_status (priv->cnc);
	if (tstatus) {
		gchar *msg;
		GtkWidget *dlg;
		gint result;

		msg = g_strdup_printf (_("<b>A transaction has been started on the %s connection.</b>\n\n"
					 "if it is not committed, any change since the beginning of the "
					 "transaction will be lost. \n\n"
					 "<u>Rollback</u> the transaction to cancel any change made since the "
					 "transaction was started, <u>commit</u> the transaction to save those changes, "
					 "or <u>ignore</u> the transaction to let the DBMS handle the transaction (this "
					 "usually leads to lose any change). What do you want to do?"), 
				       gda_connection_get_dsn (priv->cnc));
		dlg = gtk_message_dialog_new_with_markup (GTK_WINDOW (window), GTK_DIALOG_MODAL,
							  GTK_MESSAGE_WARNING,
							  GTK_BUTTONS_NONE, msg);
		g_free (msg);
		gtk_dialog_add_buttons (GTK_DIALOG (dlg), _("Rollback transaction"), 1,
					_("Commit transaction"), 2, 
					_("Ignore transaction"), 3, NULL);
		result = gtk_dialog_run (GTK_DIALOG (dlg));
		gtk_widget_destroy (dlg);
		if ((result == 1) || (result == 2)) {
			GError *error = NULL;

			dlg = NULL;
			if (result == 1) {
				if (!gda_connection_rollback_transaction (priv->cnc, NULL, &error))
					dlg = gtk_message_dialog_new_with_markup (GTK_WINDOW (window), GTK_DIALOG_MODAL,
										  GTK_MESSAGE_ERROR,
										  GTK_BUTTONS_NONE, 
										  _("<b>Could not rollback current transaction:</b>"
										    "\n%s\n\nWhat do you want to do?"),
										  error && error->message ? 
										  error->message : _("No detail"));
			}
			else {
				if (gda_connection_commit_transaction (priv->cnc, NULL, &error))
					dlg = gtk_message_dialog_new_with_markup (GTK_WINDOW (window), GTK_DIALOG_MODAL,
										  GTK_MESSAGE_ERROR,
										  GTK_BUTTONS_NONE, 
										  _("<b>Could not commit current transaction:</b>"
										    "\n%s\n\nWhat do you want to do?"),
										  error && error->message ? 
										  error->message : _("No detail"));
			}

			if (error)
				g_error_free (error);
			if (dlg) {
				gtk_dialog_add_buttons (GTK_DIALOG (dlg), _("Close anyway"), 1,
							_("Don't close this workspace window"), 2, NULL);
				result = gtk_dialog_run (GTK_DIALOG (dlg));
				do_close = (result == 1) ? TRUE : FALSE;
				gtk_widget_destroy (dlg);
			}
		}
	}

	return do_close;
}

static gboolean
delete_event( GtkWidget *widget, GdkEvent *event, WorkspaceWindowPrivate *priv )
{
	return !treat_close_request (priv);
}

static void
on_file_close (GtkAction *action, WorkspaceWindowPrivate *priv)
{
	if (treat_close_request (priv))
		gtk_widget_destroy (priv->window);
}

static void
on_file_exit (GtkAction *action, WorkspaceWindowPrivate *priv)
{
	while (opened_workspaces) {
		priv = opened_workspaces->data;

		on_file_close (NULL, priv);
	}
}

static void
on_database_sync (GtkAction *action, WorkspaceWindowPrivate *priv)
{
	perform_dbms_update (priv);
}

static void
transations_dlg_response_cb (GtkWidget *dlg, gint response_id, WorkspaceWindowPrivate *priv)
{
	gtk_widget_destroy (dlg);
	priv->transaction_status = NULL;
}

static void
on_transaction (GtkAction *action, WorkspaceWindowPrivate *priv)
{
	if (! priv->transaction_status) {
		GtkWidget *dlg, *status;
		gchar *title;
		GdaConnection *cnc;

		/* dialog */
		dlg = gtk_dialog_new_with_buttons (_("Transactions management"),
						   NULL,
						   0, GTK_STOCK_CLOSE,
						   GTK_RESPONSE_ACCEPT, NULL);
		gtk_dialog_set_has_separator (GTK_DIALOG (dlg), FALSE);

		cnc = priv->cnc;
		title = g_strdup_printf ("Transactions management: %s", gda_connection_get_dsn (cnc));
		gtk_window_set_title (GTK_WINDOW (dlg), title);
		g_free (title);

		/* transactions status widget */
		status = gnome_db_transaction_status_new (priv->cnc);
		gtk_box_pack_start (GTK_BOX (GTK_DIALOG (dlg)->vbox), status, TRUE, TRUE, 0);
		gtk_widget_set_size_request (status, 400, 350);
		g_signal_connect (G_OBJECT (dlg), "response",
				  G_CALLBACK (transations_dlg_response_cb), priv);
		gtk_widget_show_all (dlg);
		priv->transaction_status = dlg;
	}
	else
		gdk_window_raise (priv->transaction_status->window);
}

static void
on_database_new (GtkAction *action, WorkspaceWindowPrivate *priv)
{
	GtkWidget *dlg;
	gchar *str;

	str = g_strdup_printf ("%s\n<small>%s</small>",
			       _("Database creation requires that first a database provider be selected."),
			       _("The 'SQLite' provider allows you to create a database stored in a single file\n"
				 "and does not require a database server to be set up."));
	dlg = mergeant_server_op_create_dialog (GTK_WINDOW (priv->window), GDA_SERVER_OPERATION_CREATE_DB, 
						priv->cnc, NULL,
						_("Create a database"), str, NULL, NULL);
	g_free (str);
	gtk_widget_show (dlg);
}

static void
on_database_del (GtkAction *action, WorkspaceWindowPrivate *priv)
{
	GtkWidget *dlg;

	dlg = mergeant_server_op_create_dialog (GTK_WINDOW (priv->window), GDA_SERVER_OPERATION_DROP_DB, 
						priv->cnc, NULL,
						_("Delete a database"),
						_("Database destruction requires that first a database provider be selected."),
						NULL, NULL);
	gtk_widget_show (dlg);
}

static void
on_tools_data_sources (GtkAction *action, WorkspaceWindowPrivate *priv)
{
	char *argv[2];
	GError *error = NULL;

	/* run gnome-database-properties tool */
	argv[0] = gnome_db_get_application_exec_path ("gnome-database-properties");
        argv[1] = NULL;
                                                                                  
	if (!g_spawn_async (NULL, argv, NULL, G_SPAWN_SEARCH_PATH,
			    NULL, NULL, NULL, &error)) {
		GtkWidget *dlg;
		dlg = gtk_message_dialog_new_with_markup (NULL, GTK_DIALOG_MODAL,
							  GTK_MESSAGE_ERROR,
							  GTK_BUTTONS_CLOSE, 
							  "<b>%s:</b>\n\n%s",
							  _("Could not find the 'gnome-database-properties' program"),
							  error && error->message ? error->message : _("No detail"));
		if (error)
			g_error_free (error);
		gtk_dialog_run (GTK_DIALOG (dlg));
		gtk_widget_destroy (dlg);
	}
	g_free (argv[0]);
}


static void console_destroyed_cb (GtkWidget *widget, WorkspaceWindowPrivate *priv);
static void
on_sql_console (GtkAction *action, WorkspaceWindowPrivate *priv)
{
	GtkWidget *dlg, *sw, *console;
	gchar *title;
	GdaConnection *cnc;

	/* dialog */
	dlg = gtk_dialog_new_with_buttons (_("SQL console"), NULL, 0,
					   GTK_STOCK_CLOSE, GTK_RESPONSE_CLOSE, NULL);
	gtk_dialog_set_has_separator (GTK_DIALOG (dlg), FALSE);
	
	cnc = priv->cnc;
	title = g_strdup_printf ("SQL console: %s", gda_connection_get_dsn (cnc));
	gtk_window_set_title (GTK_WINDOW (dlg), title);
	g_free (title);
	
	/* console widget */
	sw = gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (sw),
					GTK_POLICY_AUTOMATIC,
					GTK_POLICY_AUTOMATIC);
	gtk_scrolled_window_set_shadow_type (GTK_SCROLLED_WINDOW (sw), GTK_SHADOW_IN);	
	gtk_box_pack_start (GTK_BOX (GTK_DIALOG (dlg)->vbox), sw, TRUE, TRUE, 0);
	gtk_widget_show (sw);
	
	console = gnome_db_sql_console_new (priv->cnc, _("SQL Console (type \\? + ENTER for help)\n"));
	gtk_container_add (GTK_CONTAINER (sw), console);
	gtk_widget_show (console);

	gtk_widget_set_size_request (dlg, 450, 500);
	gtk_widget_show (dlg);

	/* management */
	priv->consoles = g_slist_prepend (priv->consoles, dlg);
	g_signal_connect_swapped (dlg, "response", 
				  G_CALLBACK (gtk_widget_destroy), dlg);
	g_signal_connect (G_OBJECT (dlg), "destroy",
			  G_CALLBACK (console_destroyed_cb), priv);
}

static void
console_destroyed_cb (GtkWidget *widget, WorkspaceWindowPrivate *priv)
{
	priv->consoles = g_slist_remove (priv->consoles, widget);
}

static void
on_help_about (GtkAction *action, WorkspaceWindowPrivate *priv)
{
	GtkWidget *dialog = NULL;                                          
	GdkPixbuf *icon;
	const gchar *authors[] = {
		"Vivien Malerba <malerba@gnome-db.org>",
		"Fernando Martins <fmartins@hetnet.nl>",
		"Rodrigo Moya <rodrigo@gnome-db.org>",
		"Carlos Perello Marin <carlos@gnome-db.org>",
		"Gonzalo Paniagua Javier <gonzalo@gnome-db.org>",
		NULL
	};
                                                                                    
	const gchar *documenters[] = {
		"Vivien Malerba <malerba@gnome-db.org>",
		NULL
	};
	const gchar *translator_credits =
		"Ge'ez Frontier Foundation <locales@geez.org> Amharic translations\n" \
		"Mətin ?mirov <metin@karegen.com> Azerbaijani translations\n" \
		"Aleix Badia i Bosch <abadia@ica.es> Catalan translations\n" \
		"Miloslav Trmac <mitr@volny.cz> & Michal Bukovjan <bukm@centrum.cz> Czech translations\n" \
		"Ole Laursen <olau@hardworking.dk> Danish translations\n" \
		"Gerhard Dieringer <DieringG@eba-haus.de> & Christian Neumair <chris@gnome-de.org> German translations\n" \
		"Kostas Papadimas <pkst@gnome.org> Greek translations\n" \
		"Alexander Winston <alexander.winston@comcast.net> English/Canada translations\n" \
		"David Lodge <dave@cirt.net> English/British translations\n" \
		"Francisco Javier F. Serrador <serrador@cvs.gnome.org> & Pablo del Campo <pablodc@bigfoot.com> Spanish translations\n" \
		"Roozbeh Pournader <roozbeh@sharif.edu> Persian translations\n" \
		"Christophe Merlet (RedFox) <redfox@redfoxcenter.org> French translation\n" \
		"Alastair McKinstry <mckinstry@computer.org> Irish (gaeilge) translations\n" \
		"Croatian language team <lokalizacija@linux.hr> Croatiann translations\n" \
		"Mauro Colorio <linuxbox@interfree.it> & Alessio Dessì <alkex@inwind.it> Italian translations\n" \
		"FSF-India <locale@gnu.org.in> Malayalam translations\n" \
		"Norazah Abd Aziz  <azahaziz@yahoo.com> Malay translations\n" \
		"Jan-Willem Harmanny <jwharmanny@hotmail.com> Dutch translations\n" \
		"Kjartan Maraas <kmaraas@online.no> Norwegian translation\n" \
		"GNOME PL Team <translators@gnome.pl> Polish translation\n" \
		"Afonso Celso Medina <medina@maua.br> Português/Brasil translation\n" \
		"Duarte Loreto <happyguy_pt@hotmail.com> Portuguese translations\n" \
		"Valek Filippov <frob@df.ru> Russian translations\n" \
		"Martin Lacko <lacko@host.sk> Slovak translations\n" \
		"Goran Rakić <gox@devbase.net> Serbian translations\n" \
		"Горан Ракић <gox@devbase.net> Serbian translations\n" \
		"Christian Rose <menthos@menthos.com> Swedish translations\n" \
		"Ali Pakkan <apakkan@hotmail.com> Turk translations\n" \
		"Yuriy Syrota <yuri@renome.rovno.ua> Ukrainian translations\n" \
		"Trinh Minh Thanh <tmthanh@linuxmail.org> Vietnamese translation\n" \
		"Fan Zhang <a17841@bigpond.com> Simplified Chinese translations\n"
		; 
        
	gchar *file;
	
	/*file = gnome_db_gbr_get_icon_path ("gnome-db.png");*/
	file = mg_gbr_get_file_path (MG_DATA_DIR, "pixmaps", "mergeant.png", NULL);
        icon = gdk_pixbuf_new_from_file (file, NULL);
        g_free (file);
		
	dialog = gtk_about_dialog_new ();
	gtk_about_dialog_set_name (GTK_ABOUT_DIALOG (dialog), PACKAGE);
	gtk_about_dialog_set_version (GTK_ABOUT_DIALOG (dialog), PACKAGE_VERSION);
	gtk_about_dialog_set_copyright (GTK_ABOUT_DIALOG (dialog), "(C) 1998-2007 GNOME Foundation");
	gtk_about_dialog_set_comments (GTK_ABOUT_DIALOG (dialog), _("Database services for the GNOME Desktop"));
	gtk_about_dialog_set_license (GTK_ABOUT_DIALOG (dialog), "GNU General Public License");
	gtk_about_dialog_set_website (GTK_ABOUT_DIALOG (dialog), "http://www.gnome-db.org");
	gtk_about_dialog_set_authors (GTK_ABOUT_DIALOG (dialog), authors);
	gtk_about_dialog_set_documenters (GTK_ABOUT_DIALOG (dialog), documenters);
	gtk_about_dialog_set_translator_credits (GTK_ABOUT_DIALOG (dialog), translator_credits);
	gtk_about_dialog_set_logo (GTK_ABOUT_DIALOG (dialog), icon);
	gtk_window_set_transient_for (GTK_WINDOW (dialog), GTK_WINDOW (priv->window));
	gtk_dialog_run (GTK_DIALOG (dialog));
	gtk_widget_destroy (dialog);
}

static GtkActionEntry ui_actions[] = {
	{ "File", NULL, "_File", NULL, "File", NULL },
	{ "FileNewWorkspace", GTK_STOCK_NEW, "_New workspace", NULL, "Open a new workspace", G_CALLBACK (on_file_new_workspace) },
	{ "FileCopyWorkspace", GTK_STOCK_COPY, "_Copy of this Workspace", NULL, "Open a new workspace with the same connection parameters", G_CALLBACK (on_file_copy_workspace) },
	{ "FileClose", GTK_STOCK_CLOSE, "_Close", NULL, "Close this window", G_CALLBACK (on_file_close) },
	{ "FileExit", GTK_STOCK_QUIT, "_Quit", NULL, "Exit the application", G_CALLBACK (on_file_exit) },
	{ "Database", NULL, "_Database", NULL, "Database", NULL },
	{ "DatabaseSync", GTK_STOCK_REFRESH, "_Sync", NULL, "Refresh metadata from database", G_CALLBACK (on_database_sync) },
	{ "Transactions", NULL, "_Transactions", NULL, "Transactions management", G_CALLBACK (on_transaction) },
	{ "DatabaseCreate", GTK_STOCK_NEW, "_Create a database", NULL, "Create a new database", G_CALLBACK (on_database_new) },
	{ "DatabaseDelete", GTK_STOCK_DELETE, "_Delete a database", NULL, "Delete an existing database", G_CALLBACK (on_database_del) },
	{ "ToolsDataSources", GTK_STOCK_PREFERENCES, "_Datasources", NULL, "Manage the list of declared datasources", G_CALLBACK (on_tools_data_sources) },
	{ "SqlConsole", GNOME_DB_STOCK_SQL_CONSOLE, "_Console SQL", NULL, "Open an SQL console", G_CALLBACK (on_sql_console) },
	{ "About", NULL, "_About", NULL, "About", NULL },
	{ "HelpAbout", GTK_STOCK_ABOUT, "_About", NULL, "Show information about Mergeant", G_CALLBACK (on_help_about) }
};

static const gchar *ui_actions_info =
	"<ui>"
        "  <menubar name='MenuBar'>"
        "    <menu name='File' action='File'>"
        "      <menuitem name='FileNewWorkspace' action= 'FileNewWorkspace'/>"
        "      <menuitem name='FileCopyWorkspace' action= 'FileCopyWorkspace'/>"
        "      <separator/>"
        "      <menuitem name='FileClose' action= 'FileClose'/>"
        "      <menuitem name='FileExit' action= 'FileExit'/>"
        "    </menu>"
        "    <menu name='Database' action='Database'>"
        "      <menuitem name='ToolsDataSources' action= 'ToolsDataSources'/>"
        "      <menuitem name='DatabaseCreate' action= 'DatabaseCreate'/>"
        "      <menuitem name='DatabaseDelete' action= 'DatabaseDelete'/>"
        "      <separator/>"
        "      <menuitem name='DatabaseSync' action= 'DatabaseSync'/>"
        "      <separator/>"
        "      <menuitem name='Transactions' action= 'Transactions'/>"
        "      <separator/>"
        "      <menuitem name='SqlConsole' action='SqlConsole'/>"
        "    </menu>"
	"    <placeholder name='PageExtension'/>"
        "    <menu name='About' action='About'>"
        "      <menuitem name='HelpAbout' action= 'HelpAbout'/>"
        "    </menu>"
        "  </menubar>"
        "  <toolbar  name='ToolBar'>"
        "    <toolitem action='ToolsDataSources'/>"
        "    <toolitem action='SqlConsole'/>"
	"    <separator/>"
        "  </toolbar>"
        "</ui>";


static void
stop_dbms_update_cb (GtkWidget *dialog, gint response, GdaConnection *cnc)
{
	/*gda_dict_stop_update_dbms_meta_data (dict);*/
	TO_IMPLEMENT;
}

static void
perform_dbms_update (WorkspaceWindowPrivate *priv)
{
	GtkWidget *dialog, *view;
	GError *error = NULL;

	dialog = gtk_dialog_new_with_buttons (_("Metadata synchronisation"),
					      NULL,
					      GTK_DIALOG_MODAL | GTK_DIALOG_NO_SEPARATOR,
					      GTK_STOCK_CANCEL, GTK_RESPONSE_REJECT, NULL);
	g_signal_connect (G_OBJECT (dialog), "response", G_CALLBACK (stop_dbms_update_cb), priv->cnc);
	
	view = gtk_label_new (_("Reading database's meta data, please wait..."));
	gtk_widget_show (view);
	gtk_box_pack_start (GTK_BOX (GTK_DIALOG (dialog)->vbox), view, TRUE, TRUE, 6);
	gtk_widget_show (dialog);
	
	if (!gda_connection_update_meta_store (priv->cnc, NULL, &error)) {
		GtkWidget *msg;
		
		msg = gtk_message_dialog_new (GTK_WINDOW (dialog),
					      GTK_DIALOG_DESTROY_WITH_PARENT,
					      GTK_MESSAGE_ERROR, GTK_BUTTONS_CLOSE,
					      _("Error updating Server metadata:\n%s\n"), error->message);
		gtk_dialog_run (GTK_DIALOG (msg));
		gtk_widget_destroy (msg);
		g_error_free (error);
	}	
	
	gtk_widget_destroy (dialog);
}

/*
 * If no other workspace exists, then we need to quit the application
 */
static void
quit_if_no_other_workspace (void)
{
	if (!opened_workspaces)
		gtk_main_quit ();
}

static void create_new_workspace_window_no_cnc (WorkspaceOptions *options);
static void create_new_workspace_window_with_cnc (GdaConnection *cnc, gboolean force_dbms_update, WorkspaceOptions *options);

/**
 * create_new_workspace_window
 * @cnc: a #GdaConnection object, or %NULL
 *
 * Creates a new workspace window. If @cnc is not %NULL
 * then it is reused for the new workspace.
 *
 * Returns:
 */
void
create_new_workspace_window (GdaConnection *cnc)
{
	if (cnc) {
		if (! GDA_IS_CONNECTION (cnc)) {
			g_warning ("cnc is not a GdaConnection object, cancelling workspace window creation");
			quit_if_no_other_workspace ();
			return;
		}
		
		create_new_workspace_window_with_cnc (cnc, FALSE, NULL);
	}
	else
		create_new_workspace_window_no_cnc (NULL);
}

/**
 * create_new_workspace_window_spec
 * @options: 
 *
 * Same as create_new_workspace_window() but with specific options
 */
void
create_new_workspace_window_spec (WorkspaceOptions *options)
{
	create_new_workspace_window_no_cnc (options);
}

static void 
create_new_workspace_window_with_cnc (GdaConnection *cnc, gboolean force_dbms_update, WorkspaceOptions *options)
{
	WorkspaceWindowPrivate *priv;
	GtkWidget *table;
	gchar *title;
	GtkActionGroup *actions;
        GtkUIManager *ui;
	GtkWidget *toolbar, *menubar;

	/* here we MUST have a valid connection */
	g_assert (cnc && GDA_IS_CONNECTION (cnc));

	/* create the private structure */
	priv = g_new0 (WorkspaceWindowPrivate, 1);
	priv->cnc = cnc;
	g_object_ref (cnc);
	opened_workspaces = g_list_prepend (opened_workspaces, priv);

	if (force_dbms_update) 
		perform_dbms_update (priv);

	/* create the window */
	priv->window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
	g_signal_connect (G_OBJECT (priv->window), "delete_event",
			  G_CALLBACK (delete_event), priv);
	gtk_window_set_title (GTK_WINDOW (priv->window), "Mergeant");
	g_object_set_data_full (G_OBJECT (priv->window), "Mergeant_WorkspaceWindowPrivate",
				priv, (GDestroyNotify) destroy_private_data_cb);
	gchar *str;
	str = mg_gbr_get_file_path (MG_DATA_DIR, "pixmaps", "mergeant.png", NULL);
	gtk_window_set_icon_from_file (GTK_WINDOW (priv->window), str, NULL);
	g_free (str);

	cnc = priv->cnc;
	title = g_strdup_printf ("%s - Mergeant", gda_connection_get_dsn (cnc));
	gtk_window_set_title (GTK_WINDOW (priv->window), title);
	g_free (title);
	gtk_widget_set_size_request (priv->window, 900, 700);
	table = gtk_table_new (4, 1, FALSE);

	/* add the menu bar/toolbar */
	actions = gtk_action_group_new ("Actions");
	gtk_action_group_add_actions (actions, ui_actions, G_N_ELEMENTS (ui_actions), priv);

	ui = gtk_ui_manager_new ();
        gtk_ui_manager_insert_action_group (ui, actions, 0);
        gtk_ui_manager_add_ui_from_string (ui, ui_actions_info, -1, NULL);

	menubar = gtk_ui_manager_get_widget (ui, "/MenuBar");
	gtk_table_attach (GTK_TABLE (table), menubar, 0, 1, 0, 1, GTK_FILL, GTK_FILL, 0, 0);
	gtk_widget_show (menubar);

	toolbar = gtk_ui_manager_get_widget (ui, "/ToolBar");
	gtk_table_attach (GTK_TABLE (table), toolbar, 0, 1, 1, 2, GTK_FILL, GTK_FILL, 0, 0);
	gtk_widget_show (toolbar);

	/* create the workspace */
	priv->workspace = workspace_new (priv->cnc, ui);
	gtk_widget_show (priv->workspace);
	gtk_table_attach (GTK_TABLE (table), priv->workspace, 0, 1, 2, 3,
			  GTK_SHRINK | GTK_FILL | GTK_EXPAND,
			  GTK_SHRINK | GTK_FILL | GTK_EXPAND, 6, 6);

	priv->status_bar = gtk_statusbar_new ();
	gtk_widget_show (priv->status_bar);
	gtk_table_attach (GTK_TABLE (table), priv->status_bar, 0, 1, 3, 4, GTK_FILL, GTK_FILL, 0, 0);
	gtk_container_add (GTK_CONTAINER (priv->window), table);
	gtk_widget_show (table);

	/* display the window */
	gtk_widget_show (priv->window);

	/* apply options */
	if (options) {
		if (options->start_page) {
			switch (* options->start_page) {
			case 'q':
			case 'Q':
				workspace_show_page (WORKSPACE (priv->workspace), WORKSPACE_QUERIES_PAGE);
				break;
			case 'd':
			case 'D':
				workspace_show_page (WORKSPACE (priv->workspace), WORKSPACE_TYPES_PAGE);
				break;
			case 'r':
			case 'R':
				workspace_show_page (WORKSPACE (priv->workspace), WORKSPACE_RELATIONS_PAGE);
				break;
			case 't':
			case 'T':
			default:
				workspace_show_page (WORKSPACE (priv->workspace), WORKSPACE_TABLES_PAGE);
				break;
			}
		}
	}
}

/* structure holding data used during the threads usage */
typedef struct {
	GdaThreader   *threader;
	GError       **error;

	gchar         *dsn;
	gchar         *auth;
	GdaConnection *cnc;

	gboolean       func_retval;
	gboolean       force_dbms_update;

	GtkWidget     *label;
	gint           timeout_id;
	GtkWidget     *bar; /* pulse bar */
} ThreadData;

static void
thread_data_free (ThreadData *thdata) 
{
	if (thdata->threader)
		g_object_unref (thdata->threader);
	if (thdata->cnc)
		g_object_unref (thdata->cnc);
	if (thdata->error) {
		if (*(thdata->error))
			g_error_free (*(thdata->error));
		g_free (thdata->error);
	}
	g_free (thdata->dsn);
	g_free (thdata->auth);
	g_free (thdata);
}

/*
 * Make the progress bar pulse
 */
static gboolean
update_progress_bar (GtkProgressBar *bar)
{
	gtk_progress_bar_pulse (bar);
	return TRUE;
}

/* 
 * function executed in another thread, to open the connection
 */
static void
thread_open_connect (ThreadData *thdata)
{
	thdata->cnc = gda_connection_open_from_dsn (thdata->dsn, thdata->auth, 
						    GDA_CONNECTION_OPTIONS_NONE, thdata->error);
}

/* 
 * called in main thread by an idle loop when thread_open_connect() has finished
 */
static void
thread_open_connect_finished (GdaThreader *thread, guint job_id, ThreadData *thdata)
{
	g_source_remove (thdata->timeout_id);
	thdata->timeout_id = 0;

	if (!thdata->cnc) {
		GError *error = *(thdata->error);
		/* gdk_threads_enter (); */

		GtkWidget *msg;

		msg = gtk_message_dialog_new (NULL,
					      GTK_DIALOG_DESTROY_WITH_PARENT,
					      GTK_MESSAGE_ERROR, GTK_BUTTONS_CLOSE,
					      _("Error Opening connection:\n%s\n"), 
					      error && error->message ? error->message : _("No detail"));
		gtk_dialog_run (GTK_DIALOG (msg));
		gtk_widget_destroy (msg);

		/* gdk_threads_leave (); */
		if (error) {
			g_error_free (error);
			*(thdata->error) = NULL;
		}
		quit_if_no_other_workspace ();
	}
	else {
		/* associate meta store */
		gchar *filename;
		WorkspaceOptions *options;
		GdaMetaStore *store;

#define USER_CONFIG_DIR G_DIR_SEPARATOR_S ".libgda"
		filename = g_strdup_printf ("%s%smergeant-%s.db",
					    g_get_home_dir (), USER_CONFIG_DIR G_DIR_SEPARATOR_S,
					    thdata->dsn);
		if (! g_file_test (filename, G_FILE_TEST_EXISTS))
			thdata->force_dbms_update = TRUE;
		store = gda_meta_store_new_with_file (filename);
		g_free (filename);
		g_object_set (G_OBJECT (thdata->cnc), "meta-store", store, NULL);

		options = g_object_get_data (G_OBJECT (dsn_dialog), "options");
		/* gdk_threads_enter (); */
		create_new_workspace_window_with_cnc (thdata->cnc, thdata->force_dbms_update, options);
		/* gdk_threads_leave (); */
	}
	
	thread_data_free (thdata);
	gtk_widget_hide (dsn_dialog);
}

static void 
create_new_workspace_window_no_cnc_choose_cb (GtkDialog *dialog, gint response, gpointer data)
{
	WorkspaceWindowPrivate *priv;
	GdaConnection *cnc = NULL;
	const gchar *dsn, *auth;
	GList *list = opened_workspaces;
	GtkWidget *nb;

	if (response != GTK_RESPONSE_OK) {
		/* User hasn't selected a data source */
		gtk_widget_hide (GTK_WIDGET (dialog));
		quit_if_no_other_workspace ();
		return;
	}

	/* User has selected a data source */
	auth = gnome_db_login_dialog_get_auth (GNOME_DB_LOGIN_DIALOG (dialog));
		
	/* try to find another workspace window with the same DSN */
	dsn = gnome_db_login_dialog_get_dsn (GNOME_DB_LOGIN_DIALOG (dialog));
	while (list && !cnc) {
		gchar *dsn2;
		priv = (WorkspaceWindowPrivate *) list->data;
		dsn2 = (gchar *) gda_connection_get_dsn (priv->cnc);
		if (!strcmp (dsn, dsn2)) 
			cnc = priv->cnc;
		g_free (dsn2);
		
		list = g_list_next (list);
	}

	/* changing the login dialog's page */
	nb = g_object_get_data (G_OBJECT (dsn_dialog), "main_part");
	gtk_notebook_set_current_page (GTK_NOTEBOOK (nb), 1);
	gtk_widget_set_sensitive (GTK_DIALOG (dsn_dialog)->action_area, FALSE);

	/* use cnc if not NULL, or create a new one */
	if (cnc) {
		create_new_workspace_window_with_cnc (cnc, FALSE, NULL);
		gtk_widget_hide (dsn_dialog);
	}
	else {
		ThreadData *thdata;
		GdaThreader *threader;
		GtkWidget *wid;		
		gchar *str;

		/* structure passed during the multi-thread */
		thdata = g_new0 (ThreadData, 1);
		thdata->dsn = g_strdup (dsn);
		if (auth)
			thdata->auth = g_strdup (auth);
		thdata->func_retval = FALSE;
		thdata->error = g_new0 (GError *, 1);

		/* set the label */
		wid = g_object_get_data (G_OBJECT (dsn_dialog), "info_label");
		thdata->label = wid;
		str = g_strdup_printf ("<b>%s:</b>\n%s", _("Opening connection"),
				       _("The connection to the data source is now being opened."));
		gtk_label_set_markup (GTK_LABEL (wid), str);
		g_free (str);
		gtk_widget_show (wid);
		
		/* show the progress pulse bar */
		wid = g_object_get_data (G_OBJECT (dsn_dialog), "info_bar");
		thdata->bar = wid;
		gtk_widget_show (wid);
		
		thdata->timeout_id = g_timeout_add (PROGRESS_BAR_TIMEOUT, (GSourceFunc) update_progress_bar, wid);

		/* try opening the connection */
		threader = GDA_THREADER (gda_threader_new ());
		thdata->threader = threader;
		gda_threader_start_thread (thdata->threader, (GThreadFunc) thread_open_connect, thdata, 
					   (GdaThreaderFunc) thread_open_connect_finished, NULL, NULL);

		
	}
}

static void 
create_new_workspace_window_no_cnc_destroy_cb (GtkDialog *dialog, gpointer data)
{
	gtk_widget_hide (GTK_WIDGET (dialog));
	quit_if_no_other_workspace ();
}

static void 
create_new_workspace_window_no_cnc (WorkspaceOptions *options)
{
	GtkWidget *nb;

	if (!dsn_dialog) {
		GtkWidget *wid, *hbox, *vbox;

		/* dialog to choose the connection */
		dsn_dialog = gnome_db_login_dialog_new (_("Connect"), NULL);
		g_signal_connect (G_OBJECT (dsn_dialog), "destroy", G_CALLBACK
				  (create_new_workspace_window_no_cnc_destroy_cb), NULL);
		g_signal_connect (G_OBJECT (dsn_dialog), "response", G_CALLBACK
				  (create_new_workspace_window_no_cnc_choose_cb), NULL);
		gtk_window_set_modal (GTK_WINDOW (dsn_dialog), TRUE);

		nb = g_object_get_data (G_OBJECT (dsn_dialog), "main_part");

		vbox = gtk_vbox_new (FALSE, 0);
		gtk_notebook_append_page (GTK_NOTEBOOK (nb), vbox, NULL);
		gtk_widget_show (vbox);
			
		/* create label */
		wid = gtk_label_new ("");
		gtk_misc_set_alignment (GTK_MISC (wid), 0, 0);
		gtk_label_set_line_wrap (GTK_LABEL (wid), TRUE);
		g_object_set_data (G_OBJECT (dsn_dialog), "info_label", wid);
		gtk_box_pack_start (GTK_BOX (vbox), wid, FALSE, TRUE, 0);
		gtk_widget_show (wid);
			
		/* create progress bar */
		hbox = gtk_hbox_new (FALSE, 10);
		gtk_box_pack_start (GTK_BOX (vbox), hbox, FALSE, FALSE, 10);
		gtk_widget_show (hbox);

		wid = gtk_progress_bar_new ();
		gtk_progress_bar_set_pulse_step (GTK_PROGRESS_BAR (wid), 0.02);
		g_object_set_data (G_OBJECT (dsn_dialog), "info_bar", wid);
		gtk_box_pack_start (GTK_BOX (hbox), wid, FALSE, TRUE, 0);
		gtk_widget_show (wid);
	}
	else
		nb = g_object_get_data (G_OBJECT (dsn_dialog), "main_part");

	gtk_widget_show (dsn_dialog);
	gtk_notebook_set_current_page (GTK_NOTEBOOK (nb), 0);
	gtk_widget_set_sensitive (GTK_DIALOG (dsn_dialog)->action_area, TRUE);

	g_object_set_data (G_OBJECT (dsn_dialog), "options", options);
	if (options) {
		GnomeDbLogin *login;
		
		login = GNOME_DB_LOGIN (gnome_db_login_dialog_get_login_widget (GNOME_DB_LOGIN_DIALOG (dsn_dialog)));
		gnome_db_login_set_dsn (login, options->dsn);
		if (options->user)
			gnome_db_login_set_username (login, options->user);
		if (options->pass)
			gnome_db_login_set_password (login, options->pass);
		gtk_dialog_response (GTK_DIALOG (dsn_dialog), GTK_RESPONSE_OK);
	}
}
