/* ws-tables.c
 *
 * Copyright (C) 2004 - 2008 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <libgnomedb-extra/libgnomedb-extra.h>
#include "ws-tables.h"
#include "workspace-page.h"
#include <string.h>
#include <glib-object.h>
#include <gtk/gtk.h>
#include <libgda/gda-enums.h>
#include <glib/gi18n.h>
#include "utils.h"
#include "mg-extra-formgrid.h"
#include "mg-plugin-editor.h"
#include <sql-parser/gda-sql-statement.h>

/* 
 * Main static functions 
 */
static void ws_tables_class_init (WsTablesClass * class);
static void ws_tables_init (WsTables *ws);
static void ws_tables_dispose (GObject *object);
static void ws_tables_finalize (GObject *object);

/* WorkspacePage interface */
static void            ws_tables_page_init       (WorkspacePageIface *iface);
static gchar          *ws_tables_get_name        (WorkspacePage *iface);
static gchar          *ws_tables_get_description (WorkspacePage *iface);
static GtkWidget      *ws_tables_get_sel_button  (WorkspacePage *iface);
static GtkWidget      *ws_tables_get_selector    (WorkspacePage *iface);
static GtkWidget      *ws_tables_get_work_area   (WorkspacePage *iface);
static GtkActionGroup *ws_tables_get_actions     (WorkspacePage *iface);
static const gchar    *ws_tables_get_actions_ui  (WorkspacePage *iface);

static void            opened_dialog_closed_cb   (GtkWidget *dlg, WsTables *ws);

/* get a pointer to the parents to be able to call their destructor */
static GObjectClass  *parent_class = NULL;

/* private structure */
struct _WsTablesPrivate
{
	GdaConnection  *cnc;
	GtkWidget      *selector;
	GtkWidget      *work_area;
	GtkActionGroup *actions;

	GtkWidget      *notebook;
	GtkWidget      *description;
	GtkWidget      *fields;
	GtkTextBuffer  *integ_descr;

	GdaMetaStruct   *mstruct;
	GdaMetaDbObject *sel_dbo;
	GValue          *sel_catalog;
	GValue          *sel_schema;
	GValue          *sel_table;

	GSList         *opened_dialogs;
};

GType
ws_tables_get_type (void)
{
	static GType type = 0;

	if (!type) {
		static const GTypeInfo info = {
			sizeof (WsTablesClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) ws_tables_class_init,
			NULL,
			NULL,
			sizeof (WsTables),
			0,
			(GInstanceInitFunc) ws_tables_init
		};

		static const GInterfaceInfo workspace_page_info = {
			(GInterfaceInitFunc) ws_tables_page_init,
			NULL,
			NULL
		};

		type = g_type_register_static (G_TYPE_OBJECT, "WsTables", &info, 0);
		g_type_add_interface_static (type, WORKSPACE_PAGE_TYPE, &workspace_page_info);
	}
	return type;
}

static void 
ws_tables_page_init (WorkspacePageIface *iface)
{
	iface->get_name = ws_tables_get_name;
	iface->get_description = ws_tables_get_description;
	iface->get_sel_button = ws_tables_get_sel_button;
	iface->get_selector = ws_tables_get_selector;
	iface->get_work_area = ws_tables_get_work_area;
	iface->get_actions = ws_tables_get_actions;
	iface->get_actions_ui = ws_tables_get_actions_ui;
}

static void
ws_tables_class_init (WsTablesClass * class)
{
	GObjectClass   *object_class = G_OBJECT_CLASS (class);

	parent_class = g_type_class_peek_parent (class);

	object_class->dispose = ws_tables_dispose;
	object_class->finalize = ws_tables_finalize;
}

static void
ws_tables_init (WsTables *ws)
{
	ws->priv = g_new0 (WsTablesPrivate, 1);
}

static void ws_tables_initialize (WsTables *ws);

/**
 * ws_tables_new
 * @cnc: a #GdaConnection object
 *
 * Creates a new WsTables object
 *
 * Returns: the new object
 */
GObject*
ws_tables_new (GdaConnection *cnc)
{
	GObject  *obj;
	WsTables *ws;

	g_return_val_if_fail (GDA_IS_CONNECTION (cnc), NULL);

	obj = g_object_new (WS_TABLES_TYPE, NULL);
	ws = WS_TABLES (obj);
	ws->priv->cnc = cnc;
	g_object_ref (cnc);

	ws_tables_initialize (ws);

	return obj;
}


static void
ws_tables_dispose (GObject *object)
{
	WsTables *ws;

	g_return_if_fail (object != NULL);
	g_return_if_fail (IS_WS_TABLES (object));

	ws = WS_TABLES (object);
	if (ws->priv) {
		if (ws->priv->mstruct) {
			g_object_unref (ws->priv->mstruct);
			ws->priv->mstruct = NULL;
			ws->priv->sel_dbo = NULL;
		}

		if (ws->priv->sel_catalog) {
			gda_value_free (ws->priv->sel_catalog);
			ws->priv->sel_catalog = NULL;
		}
		if (ws->priv->sel_schema) {
			gda_value_free (ws->priv->sel_schema);
			ws->priv->sel_schema = NULL;
		}
		if (ws->priv->sel_table) {
			gda_value_free (ws->priv->sel_table);
			ws->priv->sel_table = NULL;
		}

		while (ws->priv->opened_dialogs) 
			gtk_widget_destroy (GTK_WIDGET (ws->priv->opened_dialogs->data));

		if (ws->priv->selector) {
			gtk_widget_destroy (ws->priv->selector);
			ws->priv->selector = NULL;
		}

		if (ws->priv->work_area) {
			gtk_widget_destroy (ws->priv->work_area);
			ws->priv->work_area = NULL;
		}

		if (ws->priv->actions) {
			g_object_unref (ws->priv->actions);
			ws->priv->actions = NULL;
		}

		if (ws->priv->cnc) {
			g_object_unref (G_OBJECT (ws->priv->cnc));
			ws->priv->cnc = NULL;
		}
	}

	/* parent class */
	parent_class->dispose (object);
}

static void
ws_tables_finalize (GObject   * object)
{
	WsTables *ws;

	g_return_if_fail (object != NULL);
	g_return_if_fail (IS_WS_TABLES (object));

	ws = WS_TABLES (object);
	if (ws->priv) {
		g_free (ws->priv);
		ws->priv = NULL;
	}

	/* parent class */
	parent_class->finalize (object);
}

static void action_create_table_cb (GtkAction *action, WsTables *ws);
static void action_drop_table_cb (GtkAction *action, WsTables *ws);
static void action_rename_table_cb (GtkAction *action, WsTables *ws);
static void action_add_column_cb (GtkAction *action, WsTables *ws);
static void action_drop_column_cb (GtkAction *action, WsTables *ws);
static void action_table_contents_cb (GtkAction *action, WsTables *ws);

static GtkActionEntry ui_actions[] = {
	{ "Table", NULL, "_Table", NULL, "Table", NULL },
        { "CREATE_TABLE", GTK_STOCK_ADD, "_New table", NULL, "Create a new table", 
	  G_CALLBACK (action_create_table_cb)},
        { "DROP_TABLE", GTK_STOCK_DELETE, "_Delete", NULL, "Delete the selected table", 
	  G_CALLBACK (action_drop_table_cb)},
        { "RENAME_TABLE", NULL, "_Rename", NULL, "Rename the selected table", 
	  G_CALLBACK (action_rename_table_cb)},
        { "ADD_COLUMN", NULL, "_Add column", NULL, "Add a column to the selected table", 
	  G_CALLBACK (action_add_column_cb)},
        { "DROP_COLUMN", NULL, "_Delete column", NULL, "Delete a column from the selected table", 
	  G_CALLBACK (action_drop_column_cb)},
        { "TableContents", GTK_STOCK_EDIT, "_Contents", NULL, "Display the contents of the selected table", 
	  G_CALLBACK (action_table_contents_cb)},
};

static const gchar *ui_actions_info =
	"<ui>"
	"  <menubar name='MenuBar'>"
	"    <placeholder name='PageExtension'>"
        "      <menu name='Table' action='Table'>"
        "        <menuitem name='CREATE_TABLE' action= 'CREATE_TABLE'/>"
        "        <menuitem name='DROP_TABLE' action= 'DROP_TABLE'/>"
        "        <menuitem name='RENAME_TABLE' action= 'RENAME_TABLE'/>"
	"        <separator/>"
        "        <menuitem name='ADD_COLUMN' action= 'ADD_COLUMN'/>"
        "        <menuitem name='DROP_COLUMN' action= 'DROP_COLUMN'/>"
	"        <separator/>"
	"        <menuitem name='TableContents' action= 'TableContents'/>"
        "      </menu>"
	"    </placeholder>"
	"  </menubar>"
        "  <toolbar name='ToolBar'>"
        "    <toolitem action='CREATE_TABLE'/>"
        "    <toolitem action='DROP_TABLE'/>"
        "    <separator/>"
        "    <toolitem action='TableContents'/>"
        "  </toolbar>"
        "</ui>";


static void table_row_activated_cb (GnomeDbSelector *mgsel, GtkTreePath *path,
				    GtkTreeViewColumn *column, WsTables *ws);
static void table_selection_changed_cb (GnomeDbSelector *mgsel, GnomeDbSelectorPart *part,
					GtkTreeStore *store, GtkTreeIter *iter, WsTables *ws);
static void field_selection_changed_cb (GnomeDbSelector *mgsel, GObject *sel_object, MgPluginEditor *editor);
static void meta_changed_cb (GdaMetaStore *store, GSList *changes, WsTables *ws);
static void
ws_tables_initialize (WsTables *ws)
{
	GtkWidget *label, *vbox, *wid, *nb, *vp, *vbox2, *hbox, *sw, *exp;

	/* Selector part */
	sw = gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (sw), GTK_POLICY_AUTOMATIC,
					GTK_POLICY_AUTOMATIC);
	ws->priv->selector = sw;

	wid = gnome_db_selector_new ();
	gnome_db_selector_add_parts_for_feature (GNOME_DB_SELECTOR (wid), gda_connection_get_meta_store (ws->priv->cnc), 
						 GNOME_DB_SELECTOR_FEATURE_SCHEMAS | 
						 GNOME_DB_SELECTOR_FEATURE_SCHEMA_TABLES |
						 GNOME_DB_SELECTOR_FEATURE_SCHEMA_VIEWS);
	gtk_widget_show (wid);
	gtk_container_add (GTK_CONTAINER (sw), wid);
	g_signal_connect (G_OBJECT (wid), "selected-object-changed", 
			  G_CALLBACK (table_selection_changed_cb), ws);
	gtk_widget_set_size_request (wid, 200, -1);
	g_signal_connect (G_OBJECT (wid), "row-activated",
			  G_CALLBACK (table_row_activated_cb), ws);

	/* WorkArea part */
	vbox = gtk_vbox_new (FALSE, 5);
	ws->priv->work_area = vbox;
	
	nb = gtk_notebook_new ();
	gtk_notebook_set_show_border (GTK_NOTEBOOK (nb), FALSE);
	gtk_notebook_set_show_tabs (GTK_NOTEBOOK (nb), FALSE);
	gtk_box_pack_start (GTK_BOX (vbox), nb, TRUE, TRUE, 0);
	gtk_widget_show (nb);
	ws->priv->notebook = nb;
	
	label = gtk_label_new (_("Please select a table or view from the list on the left,\n"
				 "or create a new one using the 'Add' button below."));
	gtk_notebook_append_page (GTK_NOTEBOOK (nb), label, NULL);
	gtk_widget_show (label);
	
	vp = gtk_vpaned_new ();
	gtk_notebook_append_page (GTK_NOTEBOOK (nb), vp, NULL);
	gtk_widget_show (vp);

	vbox2 = gtk_vbox_new (FALSE, 5);
	gtk_paned_add1 (GTK_PANED (vp), vbox2);
	gtk_widget_show (vbox2);

	label = gtk_label_new (NULL);
	gtk_label_set_markup (GTK_LABEL (label), _("<b>Description:</b>"));
	gtk_box_pack_start (GTK_BOX (vbox2), label, FALSE, FALSE, 0);
	gtk_widget_show (label);
	gtk_misc_set_alignment (GTK_MISC (label), 0., -1);

	label = gtk_label_new (NULL);
	gtk_box_pack_start (GTK_BOX (vbox2), label, FALSE, FALSE, 0);
	gtk_misc_set_alignment (GTK_MISC (label), 0., -1);
	gtk_widget_show (label);
	ws->priv->description = label;
	
	label = gtk_label_new (NULL);
	gtk_label_set_markup (GTK_LABEL (label), _("<b>Fields:</b>"));
	gtk_box_pack_start (GTK_BOX (vbox2), label, FALSE, FALSE, 0);
	gtk_widget_show (label);
	gtk_misc_set_alignment (GTK_MISC (label), 0., -1);

	hbox = gtk_hbox_new (FALSE, 0); /* HIG */
	gtk_box_pack_start (GTK_BOX (vbox2), hbox, TRUE, TRUE, 0);
	gtk_widget_show (hbox);
	label = gtk_label_new ("    ");
	gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);
	gtk_widget_show (label);

	wid = gnome_db_grid_new (NULL);
	gtk_box_pack_start (GTK_BOX (hbox), wid, TRUE, TRUE, 0);
	gtk_widget_show (wid);
	GnomeDbDataWidgetInfo *winfo;
	g_object_get (G_OBJECT (wid), "raw-grid", &(ws->priv->fields), "widget-info", &winfo, NULL);
	g_object_unref (ws->priv->fields);
	g_object_set (G_OBJECT (winfo), "flags", GNOME_DB_DATA_WIDGET_INFO_NONE, NULL);
	g_object_unref (winfo);

	vbox2 = gtk_vbox_new (FALSE, 5);
	gtk_paned_add2 (GTK_PANED (vp), vbox2);
	gtk_widget_show (vbox2);

	label = gtk_label_new (NULL);
	gtk_label_set_markup (GTK_LABEL (label), _("<b>Constraints and integrity rules:</b>"));
	gtk_box_pack_start (GTK_BOX (vbox2), label, FALSE, FALSE, 0);
	gtk_widget_show (label);
	gtk_misc_set_alignment (GTK_MISC (label), 0., -1);

	hbox = gtk_hbox_new (FALSE, 0); /* HIG */
	gtk_box_pack_start (GTK_BOX (vbox2), hbox, TRUE, TRUE, 0);
	gtk_widget_show (hbox);
	label = gtk_label_new ("    ");
	gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);
	gtk_widget_show (label);

	sw = gtk_scrolled_window_new (NULL, NULL);
	gtk_box_pack_start (GTK_BOX (hbox), sw, TRUE, TRUE, 0);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (sw), GTK_POLICY_AUTOMATIC,
					GTK_POLICY_AUTOMATIC);
	gtk_widget_show (sw);

	wid = gtk_text_view_new ();
	gtk_container_add (GTK_CONTAINER (sw), wid);
	gtk_text_view_set_left_margin (GTK_TEXT_VIEW (wid), 5);
	gtk_text_view_set_right_margin (GTK_TEXT_VIEW (wid), 5);
	gtk_text_view_set_editable (GTK_TEXT_VIEW (wid), FALSE);
	ws->priv->integ_descr = gtk_text_view_get_buffer (GTK_TEXT_VIEW (wid));
	gtk_text_buffer_set_text (ws->priv->integ_descr, "", -1);
	gtk_widget_show (wid);

	gtk_text_buffer_create_tag (ws->priv->integ_descr, "header",
				    "weight", PANGO_WEIGHT_BOLD,
				    "foreground", "red", NULL);

	gtk_text_buffer_create_tag (ws->priv->integ_descr, "section",
				    "weight", PANGO_WEIGHT_BOLD,
				    "foreground", "blue", NULL);

	GdaMetaStore *store;
	store = gda_connection_get_meta_store (ws->priv->cnc);
	g_signal_connect (G_OBJECT (store), "meta-changed",
			  G_CALLBACK (meta_changed_cb), ws);


	exp = gtk_expander_new (_("<b>Field's display specifications</b>"));
	gtk_expander_set_use_markup (GTK_EXPANDER (exp), TRUE);
	gtk_box_pack_start (GTK_BOX (vbox2), exp, FALSE, TRUE, 0);
	gtk_widget_show (exp);

	hbox = gtk_hbox_new (FALSE, 0); /* HIG */
	gtk_container_add (GTK_CONTAINER (exp), hbox);
	gtk_widget_show (hbox);
	label = gtk_label_new ("    ");
	gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);
	gtk_widget_show (label);

#ifdef CODEOK
	wid = mg_plugin_editor_new (ws->priv->cnc);
	gtk_box_pack_start (GTK_BOX (hbox), wid, TRUE, TRUE, 0);
	gtk_widget_show (wid);
	
	g_signal_connect (G_OBJECT (ws->priv->fields), "selection-changed",
			  G_CALLBACK (field_selection_changed_cb), wid);
#endif

	/* actions part */
	ws->priv->actions = gtk_action_group_new ("Actions");
	gtk_action_group_add_actions (ws->priv->actions, ui_actions, G_N_ELEMENTS (ui_actions), ws);

	if (ws->priv->actions) {
		GtkAction *action;
		GdaConnection *cnc;
		GdaServerProvider *prov;

		cnc = ws->priv->cnc;
		prov = gda_connection_get_provider_obj (cnc);
		g_assert (prov);

		action = gtk_action_group_get_action (ws->priv->actions, "CREATE_TABLE");
		if (!gda_server_provider_supports_operation (prov, cnc, GDA_SERVER_OPERATION_CREATE_TABLE, NULL))
			gtk_action_set_visible (action, FALSE);

		action = gtk_action_group_get_action (ws->priv->actions, "DROP_TABLE");
		if (gda_server_provider_supports_operation (prov, cnc, GDA_SERVER_OPERATION_DROP_TABLE, NULL))
			gtk_action_set_sensitive (action, FALSE);
		else
			gtk_action_set_visible (action, FALSE);

		action = gtk_action_group_get_action (ws->priv->actions, "RENAME_TABLE");
		if (gda_server_provider_supports_operation (prov, cnc, GDA_SERVER_OPERATION_RENAME_TABLE, NULL))
			gtk_action_set_sensitive (action, FALSE);
		else
			gtk_action_set_visible (action, FALSE);

		action = gtk_action_group_get_action (ws->priv->actions, "ADD_COLUMN");
		if (gda_server_provider_supports_operation (prov, cnc, GDA_SERVER_OPERATION_ADD_COLUMN, NULL))
			gtk_action_set_sensitive (action, FALSE);
		else
			gtk_action_set_visible (action, FALSE);

		action = gtk_action_group_get_action (ws->priv->actions, "DROP_COLUMN");
		if (gda_server_provider_supports_operation (prov, cnc, GDA_SERVER_OPERATION_DROP_COLUMN, NULL))
			gtk_action_set_sensitive (action, FALSE);
		else
			gtk_action_set_visible (action, FALSE);

		action = gtk_action_group_get_action (ws->priv->actions, "TableContents");
		gtk_action_set_sensitive (action, FALSE);
	}
}

static void table_info_display_update (WsTables *ws);

static void
table_selection_changed_cb (GnomeDbSelector *mgsel, GnomeDbSelectorPart *part,
			    GtkTreeStore *store, GtkTreeIter *iter, WsTables *ws)
{
	GtkAction *action;
	const GValue *cvalue1, *cvalue2, *cvalue3;
	gboolean have_table_name = FALSE;

	cvalue1 = gnome_db_selector_part_get_value (part, "catalog", store, iter);
	cvalue2 = gnome_db_selector_part_get_value (part, "schema", store, iter);
	cvalue3 = gnome_db_selector_part_get_value (part, "table_name", store, iter);
	if (! gda_value_compare_ext (cvalue1, ws->priv->sel_catalog) &&
	    ! gda_value_compare_ext (cvalue2, ws->priv->sel_schema) &&
	    ! gda_value_compare_ext (cvalue3, ws->priv->sel_table))
		return;

	ws->priv->sel_dbo = NULL;
	
	if (ws->priv->sel_catalog) {
		gda_value_free (ws->priv->sel_catalog);
		ws->priv->sel_catalog = NULL;
	}
	if (ws->priv->sel_schema) {
		gda_value_free (ws->priv->sel_schema);
		ws->priv->sel_schema = NULL;
	}
	if (ws->priv->sel_table) {
		gda_value_free (ws->priv->sel_table);
		ws->priv->sel_table = NULL;
	}

	if (cvalue3) {
		ws->priv->sel_table = gda_value_copy (cvalue3);

		if (!ws->priv->mstruct) 
			/* FIXME: check that meta struct's current data is still up to date */
			ws->priv->mstruct = gda_meta_struct_new (gda_connection_get_meta_store (ws->priv->cnc),
								 GDA_META_STRUCT_FEATURE_FOREIGN_KEYS);

		ws->priv->sel_dbo = gda_meta_struct_complement (ws->priv->mstruct, 
								GDA_META_DB_TABLE, cvalue1,
								cvalue2, cvalue3, NULL);
		if (!ws->priv->sel_dbo)
			ws->priv->sel_dbo = gda_meta_struct_complement (ws->priv->mstruct, 
									GDA_META_DB_VIEW, cvalue1,
									cvalue2, cvalue3, NULL);
		if (ws->priv->sel_dbo) {
			have_table_name = TRUE;
			g_value_set_string ((ws->priv->sel_catalog = gda_value_new (G_TYPE_STRING)), 
					    ws->priv->sel_dbo->obj_catalog);
			g_value_set_string ((ws->priv->sel_schema = gda_value_new (G_TYPE_STRING)), 
					    ws->priv->sel_dbo->obj_schema);
		}
	}

	gtk_notebook_set_current_page (GTK_NOTEBOOK (ws->priv->notebook), have_table_name ? 1 : 0);

	action = gtk_action_group_get_action (ws->priv->actions, "DROP_TABLE");
	if (action)
		gtk_action_set_sensitive (action, have_table_name ? TRUE : FALSE);

	action = gtk_action_group_get_action (ws->priv->actions, "RENAME_TABLE");
	if (action)
		gtk_action_set_sensitive (action, have_table_name ? TRUE : FALSE);

	action = gtk_action_group_get_action (ws->priv->actions, "ADD_COLUMN");
	if (action)
		gtk_action_set_sensitive (action, have_table_name ? TRUE : FALSE);

	action = gtk_action_group_get_action (ws->priv->actions, "DROP_COLUMN");
	if (action)
		gtk_action_set_sensitive (action, have_table_name ? TRUE : FALSE);

	action = gtk_action_group_get_action (ws->priv->actions, "TableContents");
	gtk_action_set_sensitive (action, have_table_name ? TRUE : FALSE);

	table_info_display_update (ws);
}

static void show_table_contents (WsTables *ws);

static void
table_row_activated_cb (GnomeDbSelector *mgsel, GtkTreePath *path,
			GtkTreeViewColumn *column, WsTables *ws)
{
	/* show contents of @sel_object */
	if (!ws->priv->sel_dbo || !(ws->priv->sel_dbo->obj_type == GDA_META_DB_TABLE))
		return;
	show_table_contents (ws);
}

static void
meta_changed_cb (GdaMetaStore *store, GSList *changes, WsTables *ws)
{
	/* FIXME: filter on the @changes */
	table_info_display_update (ws);
}

static void
action_table_contents_cb (GtkAction *action, WsTables *ws)
{
	show_table_contents (ws);
}

static void
show_table_contents (WsTables *ws)
{
	/* statement creation... */
	GdaStatement *stmt;
	GdaSqlStatement *st;
	GdaSqlStatementSelect *sst;
	GdaSqlSelectTarget *target;
	GdaSqlSelectField *selfield;
	GdaSqlExpr *expr;
	
	g_assert (ws->priv->sel_dbo);
	g_assert ((ws->priv->sel_dbo->obj_type == GDA_META_DB_VIEW) ||
		  (ws->priv->sel_dbo->obj_type == GDA_META_DB_TABLE));

	sst = g_new0 (GdaSqlStatementSelect, 1);
        GDA_SQL_ANY_PART (sst)->type = GDA_SQL_ANY_STMT_SELECT;

	sst->from = g_new0 (GdaSqlSelectFrom, 1);
	GDA_SQL_ANY_PART (sst->from)->type = GDA_SQL_ANY_SQL_SELECT_FROM;
	GDA_SQL_ANY_PART (sst->from)->parent = GDA_SQL_ANY_PART (sst);

	/* ... target part */
	target = g_new0 (GdaSqlSelectTarget, 1);
	GDA_SQL_ANY_PART (target)->type = GDA_SQL_ANY_SQL_SELECT_TARGET;
	GDA_SQL_ANY_PART (target)->parent = GDA_SQL_ANY_PART (sst->from);
	sst->from->targets = g_slist_append (NULL, target);
	
	expr = g_new0 (GdaSqlExpr, 1);
	GDA_SQL_ANY_PART (expr)->type = GDA_SQL_ANY_EXPR;
	GDA_SQL_ANY_PART (expr)->parent = GDA_SQL_ANY_PART (target);
	target->expr = expr;
	g_value_set_string ((expr->value = gda_value_new (G_TYPE_STRING)), ws->priv->sel_dbo->obj_full_name);

	/* ... fields list part */
	GdaMetaTable *mtable = GDA_META_TABLE (ws->priv->sel_dbo);
	GSList *list;
	for (list = mtable->columns; list; list = list->next) {
		selfield = g_new0 (GdaSqlSelectField, 1);
		GDA_SQL_ANY_PART (selfield)->type = GDA_SQL_ANY_SQL_SELECT_FIELD;
		GDA_SQL_ANY_PART (selfield)->parent = GDA_SQL_ANY_PART (sst);
		sst->expr_list = g_slist_append (sst->expr_list, selfield);
		
		expr = g_new0 (GdaSqlExpr, 1);
		GDA_SQL_ANY_PART (expr)->type = GDA_SQL_ANY_EXPR;
		GDA_SQL_ANY_PART (expr)->parent = GDA_SQL_ANY_PART (selfield);
		selfield->expr = expr;
		g_value_set_string ((expr->value = gda_value_new (G_TYPE_STRING)), 
				    GDA_META_TABLE_COLUMN (list->data)->column_name);
	}
	
	st = gda_sql_statement_new (GDA_SQL_STATEMENT_SELECT);
        st->contents = sst;
	g_print ("==> %s\n", gda_sql_statement_serialize (st));

	stmt = gda_statement_new ();
	g_object_set (stmt, "structure", st, NULL);
	gda_sql_statement_free (st);

	GError *error = NULL;
	GtkWidget *window = NULL;

	GtkWidget *grid, *label;
	gchar *str, *mstr, *warn = NULL;
	GdaDataModel *model;

	/* data model */
	model = gda_data_model_query_new (ws->priv->cnc, stmt);
	g_object_unref (stmt);
	if (ws->priv->sel_dbo->obj_type == GDA_META_DB_TABLE) {
		if (!gda_data_model_query_compute_modification_queries (GDA_DATA_MODEL_QUERY (model), NULL, 0, &error)) {
			if (error && error->message)
				warn = g_strdup_printf (_("Data cannot be modified for the following reason:\n%s"), 
							error->message);
			else
				warn = g_strdup (_("Data cannot be modified"));
			g_error_free (error);
			error = NULL;
		}
	}
	else
		warn = g_strdup (_("Data cannot be modified because it is a view's data"));

	/* widgets */
	grid = mg_extra_formgrid_new (model);
	g_object_unref (model);

	if (ws->priv->sel_dbo->obj_type == GDA_META_DB_VIEW)
		str = g_strdup_printf (_("Contents of view '%s'"), ws->priv->sel_dbo->obj_short_name);
	else
		str = g_strdup_printf (_("Contents of table '%s'"), ws->priv->sel_dbo->obj_short_name);

	window = gtk_dialog_new_with_buttons (str, NULL, 0,
					      GTK_STOCK_CLOSE, GTK_RESPONSE_ACCEPT,
					      NULL);
	g_signal_connect_swapped (window,
				  "response", 
				  G_CALLBACK (gtk_widget_destroy),
				  window);

	if (warn) {
		mstr = g_strdup_printf ("<span weight=\"bold\" size=\"larger\">%s</span>\n"
					"<small>%s</small>\n", str, warn);
		g_free (warn);
	}
	else
		mstr = g_strdup_printf ("<span weight=\"bold\" size=\"larger\">%s</span>\n", str);
	g_free (str);
	label = gtk_label_new ("");
	gtk_label_set_markup (GTK_LABEL (label), mstr);
	gtk_misc_set_alignment (GTK_MISC (label), 0., -1);
	g_free (mstr);


	gtk_box_pack_start (GTK_BOX (GTK_DIALOG (window)->vbox), label, FALSE, TRUE, 0);
	gtk_widget_show (label);

	gtk_box_pack_start (GTK_BOX (GTK_DIALOG (window)->vbox), grid, TRUE, TRUE, 0);
	gtk_widget_show (grid);

	gtk_window_set_default_size (GTK_WINDOW (window), 800, 300);
	gtk_widget_show (window);

	ws->priv->opened_dialogs = g_slist_append (ws->priv->opened_dialogs, window);
	g_signal_connect (G_OBJECT (window), "destroy",
			  G_CALLBACK (opened_dialog_closed_cb), ws);
}

static void
table_info_display_update (WsTables *ws)
{
	
	const gchar *str = NULL;
	gchar *title = NULL;
	GtkTextIter start, end;
	
	if (ws->priv->sel_dbo) {
		g_assert ((ws->priv->sel_dbo->obj_type == GDA_META_DB_TABLE) ||
			  (ws->priv->sel_dbo->obj_type == GDA_META_DB_VIEW));
		str = "A missing description...";
	}
	if (str && *str) 
		gtk_label_set_text (GTK_LABEL (ws->priv->description), str);
	else
		gtk_label_set_text (GTK_LABEL (ws->priv->description), "---");
	
	if (ws->priv->sel_dbo) {
		GdaDataModel *model;
		GError *error = NULL;

		model = gda_meta_store_extract (gda_connection_get_meta_store (ws->priv->cnc),
						"SELECT column_name, data_type, is_nullable, column_default, column_comments "
						"FROM _columns "
						"WHERE table_catalog = ##catalog::string AND table_schema = ##schema::string "
						"AND table_name = ##tname::string "
						"ORDER BY ordinal_position", &error,
						"catalog", ws->priv->sel_catalog, 
						"schema", ws->priv->sel_schema, 
						"tname", ws->priv->sel_table, 
						NULL);
		if (!model) {
			TO_IMPLEMENT; /* show a warning message */
			g_error_free (error);
		}
		else {
			gda_data_model_set_column_title (model, 0, _("Column"));
			gda_data_model_set_column_title (model, 1, _("Type"));
			gda_data_model_set_column_title (model, 2, _("Can be NULL"));
			gda_data_model_set_column_title (model, 3, _("Default"));
			gda_data_model_set_column_title (model, 4, _("Comments"));
		}
		
		g_object_set (G_OBJECT (ws->priv->fields), "model", model, NULL);
		g_object_unref (model);
	}
       
	/* global title update */
	title = ws_tables_get_description (WORKSPACE_PAGE (ws));
	g_signal_emit_by_name (G_OBJECT (ws), "description_changed", title);
	g_free (title);

	/* integrity rules update */
	gtk_text_buffer_get_start_iter (ws->priv->integ_descr, &start);
	gtk_text_buffer_get_end_iter (ws->priv->integ_descr, &end);
	gtk_text_buffer_delete (ws->priv->integ_descr, &start, &end);
	
	if (ws->priv->sel_dbo) {
		GdaMetaTable *mtable = GDA_META_TABLE (ws->priv->sel_dbo);
		GtkTextIter current;
		gtk_text_buffer_get_start_iter (ws->priv->integ_descr, &current);

		/* Primary key */
		if (mtable->pk_cols_nb > 0) {
			gint i;

			gtk_text_buffer_insert_with_tags_by_name (ws->priv->integ_descr,
								  &current, 
								  _("Primary key"), -1,
								  "section", NULL);
			gtk_text_buffer_insert (ws->priv->integ_descr, &current, "\n", -1);
			for (i = 0; i < mtable->pk_cols_nb; i++) {
				GdaMetaTableColumn *col;
				if (i > 0)
					gtk_text_buffer_insert (ws->priv->integ_descr, &current, ", ", -1);
				col = g_slist_nth_data (mtable->columns, mtable->pk_cols_array [i]);
				gtk_text_buffer_insert (ws->priv->integ_descr, &current, 
							col ? col->column_name : "???", -1);
			}
			gtk_text_buffer_insert (ws->priv->integ_descr, &current, "\n\n", -1);
		}
		
		/* Foreign keys */
		GSList *list;
		for (list = mtable->fk_list; list; list = list->next) {
			GdaMetaTableForeignKey *fk = GDA_META_TABLE_FOREIGN_KEY (list->data);
			gchar *str;
			GdaMetaDbObject *fdbo = fk->depend_on;
			gint i;

			if (fdbo->obj_type == GDA_META_DB_UNKNOWN) {
				GValue *v1, *v2, *v3;
				g_value_set_string ((v1 = gda_value_new (G_TYPE_STRING)), fdbo->obj_catalog);
				g_value_set_string ((v2 = gda_value_new (G_TYPE_STRING)), fdbo->obj_schema);
				g_value_set_string ((v3 = gda_value_new (G_TYPE_STRING)), fdbo->obj_name);
				gda_meta_struct_complement (ws->priv->mstruct, 
							    GDA_META_DB_TABLE, v1, v2, v3, NULL);
				gda_value_free (v1);
				gda_value_free (v2);
				gda_value_free (v3);
			}
			if (fdbo->obj_type != GDA_META_DB_UNKNOWN)
				str = g_strdup_printf (_("Foreign key on '%s'"), fdbo->obj_short_name);
			else
				str = g_strdup_printf (_("Foreign key on '%s'"), fdbo->obj_name);
			gtk_text_buffer_insert_with_tags_by_name (ws->priv->integ_descr,
								  &current, 
								  str, -1,
								  "section", NULL);
			g_free (str);
			for (i = 0; i < fk->cols_nb; i++) {
				gtk_text_buffer_insert (ws->priv->integ_descr, &current, "\n", -1);
				gtk_text_buffer_insert (ws->priv->integ_descr, &current, fk->fk_names_array [i], -1);
				gtk_text_buffer_insert (ws->priv->integ_descr, &current, " --> ", -1);
				gtk_text_buffer_insert (ws->priv->integ_descr, &current, fk->ref_pk_names_array [i], -1);
			}
			
			gtk_text_buffer_insert (ws->priv->integ_descr, &current, "\n\n", -1);
			
		}

		/* Unique constraints */
		GdaDataModel *model;
		GError *error = NULL;
		model = gda_meta_store_extract (gda_connection_get_meta_store (ws->priv->cnc),
						"SELECT tc.constraint_name, k.column_name FROM _key_column_usage k INNER JOIN _table_constraints tc ON (k.table_catalog=tc.table_catalog AND k.table_schema=tc.table_schema AND k.table_name=tc.table_name AND k.constraint_name=tc.constraint_name) WHERE tc.constraint_type='UNIQUE' AND k.table_catalog = ##catalog::string AND k.table_schema = ##schema::string AND k.table_name = ##tname::string ORDER by k.ordinal_position", &error,
						"catalog", ws->priv->sel_catalog, 
						"schema", ws->priv->sel_schema, 
						"tname", ws->priv->sel_table, NULL);
		if (!model) {
			g_warning ("Could not compute table's UNIQUE constraints for %s.%s.%s",
				   g_value_get_string (ws->priv->sel_catalog), 
				   g_value_get_string (ws->priv->sel_schema), 
				   g_value_get_string (ws->priv->sel_table));
			g_error_free (error);
		}
		else {
			gint nrows;
			
			nrows = gda_data_model_get_n_rows (model);
			if (nrows > 0) {
				gint i;
				GValue *current_value = NULL;
				const GValue *cvalue;
				for (i = 0; i < nrows; i++) {
					cvalue = gda_data_model_get_value_at (model, 0, i);
					if (!current_value || 
					    gda_value_compare_ext (cvalue, current_value)) {
						gtk_text_buffer_insert_with_tags_by_name (ws->priv->integ_descr,
											  &current, 
											  _("Unique constraint"), -1,
											  "section", NULL);
						gtk_text_buffer_insert (ws->priv->integ_descr, &current, "\n", -1);
						current_value = gda_value_copy (cvalue);
					}
					else
						gtk_text_buffer_insert (ws->priv->integ_descr, &current, ", ", -1);
					cvalue = gda_data_model_get_value_at (model, 1, i);
					gtk_text_buffer_insert (ws->priv->integ_descr, &current, 
								g_value_get_string (cvalue), -1);
				}
			}
			g_object_unref (model);
		}
	}
}

static void
field_selection_changed_cb (GnomeDbSelector *mgsel, GObject *sel_object, MgPluginEditor *editor)
{
#ifdef CODEOK
	mg_plugin_editor_set_work_object (editor, sel_object);
#endif
}


/* 
 * WorkspacePage interface implementation
 */
static gchar *
ws_tables_get_name (WorkspacePage *iface)
{
	g_return_val_if_fail (IS_WS_TABLES (iface), NULL);
	g_return_val_if_fail (WS_TABLES (iface)->priv, NULL);

	return g_strdup_printf (_("Tables and views"));
}

static gchar *
ws_tables_get_description (WorkspacePage *iface)
{
	gchar *str = NULL;
	WsTables *ws;

	g_return_val_if_fail (IS_WS_TABLES (iface), NULL);
	g_return_val_if_fail (WS_TABLES (iface)->priv, NULL);

	ws = WS_TABLES (iface);
	if (ws->priv->sel_dbo) {
		if (ws->priv->sel_dbo->obj_type == GDA_META_DB_TABLE)
			str = g_strdup_printf ("Table: <b>%s</b>", ws->priv->sel_dbo->obj_short_name);
		else
			str = g_strdup_printf ("View: <b>%s</b>", ws->priv->sel_dbo->obj_short_name);
	}
	else
		str = g_strdup_printf ("<b>%s</b>", _("No table selected"));

	return str;
}

static GtkWidget *
ws_tables_get_sel_button (WorkspacePage *iface)
{
	GdkPixbuf *pixbuf;
	GtkWidget *button, *wid, *hbox;

	g_return_val_if_fail (IS_WS_TABLES (iface), NULL);
	g_return_val_if_fail (WS_TABLES (iface)->priv, NULL);

	hbox = gtk_hbox_new (FALSE, 0);

	pixbuf = gnome_db_stock_get_icon_pixbuf (GNOME_DB_STOCK_TABLES);
	wid = gtk_image_new_from_pixbuf (pixbuf);
	g_object_unref (pixbuf);
	gtk_box_pack_start (GTK_BOX (hbox), wid, FALSE, FALSE, 0);
	gtk_widget_show (wid);

	wid = gtk_label_new (_("Tables"));
	gtk_box_pack_start (GTK_BOX (hbox), wid, FALSE, FALSE, 5);
	gtk_widget_show (wid);

	button = gtk_toggle_button_new ();
	gtk_container_add (GTK_CONTAINER (button), hbox);
	gtk_widget_show (hbox);

	return button;
}

static GtkWidget *
ws_tables_get_selector (WorkspacePage *iface)
{
	g_return_val_if_fail (IS_WS_TABLES (iface), NULL);
	g_return_val_if_fail (WS_TABLES (iface)->priv, NULL);

	return WS_TABLES (iface)->priv->selector;
}

static GtkWidget *
ws_tables_get_work_area (WorkspacePage *iface)
{
	g_return_val_if_fail (IS_WS_TABLES (iface), NULL);
	g_return_val_if_fail (WS_TABLES (iface)->priv, NULL);

	return WS_TABLES (iface)->priv->work_area;
}

static GtkActionGroup *
ws_tables_get_actions (WorkspacePage *iface)
{
	g_return_val_if_fail (IS_WS_TABLES (iface), NULL);
	g_return_val_if_fail (WS_TABLES (iface)->priv, NULL);

	return WS_TABLES (iface)->priv->actions;
}

static const gchar *
ws_tables_get_actions_ui (WorkspacePage *iface)
{
	g_return_val_if_fail (IS_WS_TABLES (iface), NULL);
	g_return_val_if_fail (WS_TABLES (iface)->priv, NULL);

	return ui_actions_info;
}
static void action_done_cb (GtkWidget *dlg, gboolean succeed, WsTables *ws);
static void
action_create_table_cb (GtkAction *action, WsTables *ws)
{
	GtkWidget *dlg;

	dlg = mergeant_server_op_create_dialog (NULL, GDA_SERVER_OPERATION_CREATE_TABLE, 
						ws->priv->cnc, NULL,
						_("Create a table"), NULL,
						MERGEANT_SERVER_OP_CALLBACK (action_done_cb), ws);
	gtk_widget_show (dlg);

	ws->priv->opened_dialogs = g_slist_append (ws->priv->opened_dialogs, dlg);
	g_signal_connect (G_OBJECT (dlg), "destroy",
			  G_CALLBACK (opened_dialog_closed_cb), ws);
}

static void
action_drop_table_cb (GtkAction *action, WsTables *ws)
{
#ifdef CODEOK
	GdaSet *options;
	GtkWidget *dlg;

	g_return_if_fail (GDA_IS_DICT_TABLE (ws->priv->sel_obj));

	options = gda_set_new (NULL);
	gda_parameter_list_add_param_from_string (options, "/TABLE_DESC_P/TABLE_NAME", G_TYPE_STRING,
						  ws->priv->sel_dbo->obj_name);

	dlg = mergeant_server_op_create_dialog (NULL, GDA_SERVER_OPERATION_DROP_TABLE, 
						ws->priv->cnc, options,
						_("Delete a table"), NULL,
						MERGEANT_SERVER_OP_CALLBACK (action_done_cb), ws);
	g_object_unref (options);

	gtk_widget_show (dlg);

	ws->priv->opened_dialogs = g_slist_append (ws->priv->opened_dialogs, dlg);
	g_signal_connect (G_OBJECT (dlg), "destroy",
			  G_CALLBACK (opened_dialog_closed_cb), ws);
#endif
}

static void
action_done_cb (GtkWidget *dlg, gboolean succeed, WsTables *ws)
{
	if (succeed) {
		GdaServerOperation *op;
		const GValue *value;
		
		op = g_object_get_data (G_OBJECT (dlg), "op");

		value = gda_server_operation_get_value_at (op, "/TABLE_DEF_P/TABLE_NAME");
		if (!value) 
			value = gda_server_operation_get_value_at (op, "/TABLE_DESC_P/TABLE_NAME");
		if (!value)
			value = gda_server_operation_get_value_at (op, "/TABLE_DESC_P/TABLE_NEW_NAME");
		if (!value)
			value = gda_server_operation_get_value_at (op, "/COLUMN_DEF_P/TABLE_NAME");
		if (!value)
			value = gda_server_operation_get_value_at (op, "/COLUMN_DESC_P/TABLE_NAME");

		if (value) {
			GdaMetaContext context;
			context.table_name = "_tables";
			context.size = 1;
			context.column_names = g_new0 (gchar *, 1);
			context.column_names [0] =  "table_name";
			context.column_values = g_new0 (GValue *, 1);
			context.column_values[0] = (GValue*) value;
			gda_connection_update_meta_store (ws->priv->cnc, &context, NULL);
			g_free (context.column_values);
		}
	}
}

static void
action_rename_table_cb (GtkAction *action, WsTables *ws)
{
#ifdef CODEOK
	GdaParameterList *options;
	GtkWidget *dlg;

	g_return_if_fail (GDA_IS_DICT_TABLE (ws->priv->sel_obj));

	options = gda_set_new (NULL);
	gda_parameter_list_add_param_from_string (options, "/TABLE_DESC_P/TABLE_NAME", G_TYPE_STRING,
						  gda_object_get_name (GDA_OBJECT (ws->priv->sel_obj)));

	dlg = mergeant_server_op_create_dialog (NULL, GDA_SERVER_OPERATION_RENAME_TABLE, 
						ws->priv->cnc, options,
						_("Rename a table"), NULL,
						MERGEANT_SERVER_OP_CALLBACK (action_done_cb), ws);
	g_object_unref (options);

	gtk_widget_show (dlg);

	ws->priv->opened_dialogs = g_slist_append (ws->priv->opened_dialogs, dlg);
	g_signal_connect (G_OBJECT (dlg), "destroy",
			  G_CALLBACK (opened_dialog_closed_cb), ws);
#endif
}

static void
action_add_column_cb (GtkAction *action, WsTables *ws)
{
#ifdef CODEOK
	GdaParameterList *options;
	GtkWidget *dlg;

	g_return_if_fail (GDA_IS_DICT_TABLE (ws->priv->sel_obj));

	options = gda_set_new (NULL);
	gda_parameter_list_add_param_from_string (options, "/COLUMN_DEF_P/TABLE_NAME", G_TYPE_STRING,
						  gda_object_get_name (GDA_OBJECT (ws->priv->sel_obj)));

	dlg = mergeant_server_op_create_dialog (NULL, GDA_SERVER_OPERATION_ADD_COLUMN, 
						ws->priv->cnc, options,
						_("Add a column to a table"), NULL,
						MERGEANT_SERVER_OP_CALLBACK (action_done_cb), ws);
	g_object_unref (options);

	gtk_widget_show (dlg);

	ws->priv->opened_dialogs = g_slist_append (ws->priv->opened_dialogs, dlg);
	g_signal_connect (G_OBJECT (dlg), "destroy",
			  G_CALLBACK (opened_dialog_closed_cb), ws);
#endif
}

static void
action_drop_column_cb (GtkAction *action, WsTables *ws)
{
#ifdef CODEOK
	GdaParameterList *options;
	GtkWidget *dlg;
	GdaDictField *sel_field;

	g_return_if_fail (GDA_IS_DICT_TABLE (ws->priv->sel_obj));

	options = gda_set_new (NULL);
	gda_parameter_list_add_param_from_string (options, "/COLUMN_DESC_P/TABLE_NAME", G_TYPE_STRING,
						  gda_object_get_name (GDA_OBJECT (ws->priv->sel_obj)));

	sel_field = gnome_db_selector_get_selected_object (GNOME_DB_SELECTOR (ws->priv->fields));
	if (sel_field) 
		gda_parameter_list_add_param_from_string (options, "/COLUMN_DESC_P/COLUMN_NAME", G_TYPE_STRING,
						  gda_object_get_name (GDA_OBJECT (sel_field)));

	dlg = mergeant_server_op_create_dialog (NULL, GDA_SERVER_OPERATION_DROP_COLUMN, 
						ws->priv->cnc, options,
						_("Delete a column from a table"), NULL,
						MERGEANT_SERVER_OP_CALLBACK (action_done_cb), ws);
	g_object_unref (options);

	gtk_widget_show (dlg);

	ws->priv->opened_dialogs = g_slist_append (ws->priv->opened_dialogs, dlg);
	g_signal_connect (G_OBJECT (dlg), "destroy",
			  G_CALLBACK (opened_dialog_closed_cb), ws);
#endif
}

static void
opened_dialog_closed_cb (GtkWidget *dlg, WsTables *ws)
{
	ws->priv->opened_dialogs = g_slist_remove (ws->priv->opened_dialogs, dlg);
}
