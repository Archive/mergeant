/* Mergeant
 *
 * Copyright (C) 1999 - 2003 Vivien Malerba
 * Copyright (C) 2002 - 2003 Rodrigo Moya
 *
 * Authors:
 *       Rodrigo Moya <rodrigo@gnome-db.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef __OBJECT_DETAIL_VIEWER_H__
#define __OBJECT_DETAIL_VIEWER_H__

#include <libgda/gda-connection.h>
#include <gtk/gtkvbox.h>

typedef struct _ObjectDetailPrivate ObjectDetailPrivate;

typedef struct {
	GtkVBox parent;
	ObjectDetailPrivate *priv;
} ObjectDetail;

typedef struct {
	GtkVBoxClass parent_class;

	/* virtual methods */
	void (* display_object) (ObjectDetail *od, GdaConnection *cnc, GdaConnectionSchema schema, const gchar *name);
} ObjectDetailClass;

GType object_detail_get_type (void);

void  object_detail_display_object (ObjectDetail *od, GdaConnection *cnc, GdaConnectionSchema schema, const gchar *name);

#endif
