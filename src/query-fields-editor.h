/* query-field-editor.h
 *
 * Copyright (C) 2004 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef __QUERY_FIELDS_EDITOR__
#define __QUERY_FIELDS_EDITOR__

#include <gtk/gtk.h>
#include <libgnomedb/libgnomedb.h>

G_BEGIN_DECLS

#define QUERY_FIELDS_EDITOR_TYPE          (query_fields_editor_get_type())
#define QUERY_FIELDS_EDITOR(obj)          G_TYPE_CHECK_INSTANCE_CAST (obj, query_fields_editor_get_type(), QueryFieldsEditor)
#define QUERY_FIELDS_EDITOR_CLASS(klass)  G_TYPE_CHECK_CLASS_CAST (klass, query_fields_editor_get_type (), QueryFieldsEditorClass)
#define IS_QUERY_FIELDS_EDITOR(obj)       G_TYPE_CHECK_INSTANCE_TYPE (obj, query_fields_editor_get_type ())


typedef struct _QueryFieldsEditor      QueryFieldsEditor;
typedef struct _QueryFieldsEditorClass QueryFieldsEditorClass;
typedef struct _QueryFieldsEditorPriv  QueryFieldsEditorPriv;
typedef enum   _QueryFieldsMode        QueryFieldsMode;

/* struct for the object's data */
struct _QueryFieldsEditor
{
	GtkVBox          object;

	QueryFieldsEditorPriv *priv;
};

/* struct for the object's class */
struct _QueryFieldsEditorClass
{
	GtkVBoxClass      parent_class;
};

enum _QueryFieldsMode {
	QEF_SHOW_OPTION    = 1 << 0,
	QEF_FILTER_OPTION  = 1 << 1,
	QEF_ORDER_OPTION   = 1 << 2,
	QEF_ACTION_BUTTONS = 1 << 3
};

/* 
 * Generic widget's methods 
*/
GType      query_fields_editor_get_type            (void);
GtkWidget *query_fields_editor_new                 (GdaQuery *query, guint mode);

G_END_DECLS

#endif



