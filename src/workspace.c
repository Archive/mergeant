/* Mergeant
 *
 * Copyright (C) 1999 - 2008 Vivien Malerba
 * Copyright (C) 2002 - 2003 Rodrigo Moya
 *
 * Authors:
 *       Rodrigo Moya <rodrigo@gnome-db.org>
 *       Vivien Malerba <malerba@gnome-db.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <gtk/gtklabel.h>
#include <gtk/gtkmenuitem.h>
#include <gtk/gtkstock.h>
#include <gtk/gtktable.h>
#include <gtk/gtkvpaned.h>
#include <libgnomedb/libgnomedb.h>
#include <libgnomedb-extra/libgnomedb-extra.h>
#include "workspace.h"
#include <string.h>
#include "query-druid.h"
#include "query-editor.h"

#include "workspace-page.h"
#include "ws-tables.h"
#include "ws-datatypes.h"
#include "ws-queries.h"
#include "ws-dbrels.h"


struct _WorkspacePrivate {
	GdaConnection  *cnc;
	GtkUIManager   *ui;
	
	/* main page */
	GtkWidget      *main_horiz_pane;

	GtkWidget      *selectors_title;     /* gray bar */
	GtkWidget      *selectors_nb;     /* notebook for selectors */

	GtkWidget      *work_areas_table;
	GtkWidget      *work_areas_title;    /* gray bar */
	GtkWidget      *work_areas_nb; /* notebook for the work areas */

	GSList         *types_buttons;
	GSList         *pages_objects;
};

static void workspace_class_init (WorkspaceClass *klass);
static void workspace_init (Workspace *wk, WorkspaceClass *klass);
static void workspace_dispose (GObject *object);
static void workspace_finalize (GObject *object);

static void create_widgets (Workspace *wk);


static GObjectClass *parent_class = NULL;

static void
workspace_class_init (WorkspaceClass *klass)
{
	GObjectClass *object_class;

	parent_class = g_type_class_peek_parent (klass);

	object_class = G_OBJECT_CLASS (klass);
	object_class->dispose = workspace_dispose;
	object_class->finalize = workspace_finalize;
}

static void
workspace_init (Workspace *wk, WorkspaceClass *klass)
{
	/* allocate private structure */
	wk->priv = g_new0 (WorkspacePrivate, 1);
}

static void
workspace_dispose (GObject *object)
{
	Workspace *wk = (Workspace *) object;

	/* free memory */
	if (wk->priv) {
		GSList *list = wk->priv->pages_objects;
		while (list ) {
			g_object_unref (G_OBJECT (list->data));
			list = g_slist_next (list);
		}

		if (wk->priv->pages_objects) {
			g_slist_free (wk->priv->pages_objects);
			wk->priv->pages_objects = NULL;
		}

		if (wk->priv->types_buttons) {
			g_slist_free (wk->priv->types_buttons);
			wk->priv->types_buttons = NULL;
		}
		if (wk->priv->ui) {
			g_object_unref (wk->priv->ui);
			wk->priv->ui = NULL;
		}
	}

	if (parent_class->dispose)
		parent_class->dispose (object);
}

static void
workspace_finalize (GObject *object)
{
	Workspace *wk = (Workspace *) object;

	/* free memory */
	if (wk->priv) {
		g_free (wk->priv);
		wk->priv = NULL;
	}
	
	if (parent_class->finalize)
		parent_class->finalize (object);
}

GType
workspace_get_type (void)
{
        static GType type = 0;
                                                                                    
        if (!type) {
                static const GTypeInfo info = {
                        sizeof (WorkspaceClass),
                        (GBaseInitFunc) NULL,
                        (GBaseFinalizeFunc) NULL,
                        (GClassInitFunc) workspace_class_init,
                        NULL,
                        NULL,
                        sizeof (Workspace),
                        0,
                        (GInstanceInitFunc) workspace_init
                };
                type = g_type_register_static (GTK_TYPE_VBOX, "Workspace", &info, 0);
        }
        return type;
}

/**
 * workspace_new
 * @cnc: a connection object
 *
 * Returns:
 */
GtkWidget *
workspace_new (GdaConnection *cnc, GtkUIManager *ui)
{
	Workspace *wk;
	WorkspacePage *page;

	wk = g_object_new (workspace_get_type (), NULL);
	workspace_set_connection (wk, cnc);

	if (ui) {
		wk->priv->ui = ui;
		g_object_ref (wk->priv->ui);
	}

	/* create the pages' objects */
	wk->priv->pages_objects = NULL;
	page = WORKSPACE_PAGE (ws_tables_new (cnc));
	wk->priv->pages_objects = g_slist_append (wk->priv->pages_objects, page);

#ifdef CODEOK
	page = WORKSPACE_PAGE (ws_queries_new (cnc));
	wk->priv->pages_objects = g_slist_append (wk->priv->pages_objects, page);

	page = WORKSPACE_PAGE (ws_dbrels_new (cnc));
	wk->priv->pages_objects = g_slist_append (wk->priv->pages_objects, page);

	page = WORKSPACE_PAGE (ws_datatypes_new (cnc));
	wk->priv->pages_objects = g_slist_append (wk->priv->pages_objects, page);
#endif

	/* initialize all the widgets */
	create_widgets (wk);

	return GTK_WIDGET (wk);
}

GdaConnection *
workspace_get_connection (Workspace *wk)
{
	return wk->priv->cnc;
}

void
workspace_set_connection (Workspace *wk, GdaConnection *cnc)
{
	if (wk->priv->cnc)
		g_object_unref (wk->priv->cnc);
	wk->priv->cnc = cnc;
	if (cnc)
		g_object_ref (cnc);
}


static void description_changed_cb (WorkspacePage *page, gchar *description, Workspace *wk);
static void object_type_toggled_cb (GtkButton *button, Workspace *wk);

/*
 * Creates the widgets composing the different 'pages', and sets
 * up any required signal callback
 */
static void
create_widgets (Workspace *wk)
{
	GtkWidget *table, *button, *wid, *vbox;
	gchar *str;
	GSList *list;
	gint i;

	/* add main page */
	wk->priv->main_horiz_pane = gtk_hpaned_new ();
	gtk_box_pack_start (GTK_BOX (wk), wk->priv->main_horiz_pane, TRUE, TRUE, 0);
	gtk_widget_show (wk->priv->main_horiz_pane);

	/* left part of main page, gray bar for selectors */
	vbox = gtk_vbox_new (FALSE, 5);
	gtk_widget_show (vbox);

	wk->priv->selectors_title = gnome_db_gray_bar_new ("<b>---</b>");
	gtk_box_pack_start (GTK_BOX (vbox), wk->priv->selectors_title, FALSE, FALSE, 0);
	gtk_widget_show (wk->priv->selectors_title);
	
	/* left part, selectors */
	wk->priv->selectors_nb = gtk_notebook_new ();
	gtk_notebook_set_show_tabs (GTK_NOTEBOOK (wk->priv->selectors_nb), FALSE);
	gtk_notebook_set_show_border (GTK_NOTEBOOK (wk->priv->selectors_nb), FALSE);
	gtk_widget_show (wk->priv->selectors_nb);

	list = wk->priv->pages_objects;
	while (list) {
		wid = workspace_page_get_selector (WORKSPACE_PAGE (list->data));
		gtk_notebook_append_page (GTK_NOTEBOOK (wk->priv->selectors_nb), wid, NULL);
		gtk_widget_show (wid);
		list = g_slist_next (list);
	}
	gtk_widget_show (wk->priv->selectors_nb);
	gtk_box_pack_start (GTK_BOX (vbox), wk->priv->selectors_nb, TRUE, TRUE, 0);

	/* left part: object type selection */
	table = gtk_table_new ((g_slist_length (wk->priv->pages_objects) +1) / 2, 2, TRUE);
	gtk_table_set_row_spacings (GTK_TABLE (table), 6);
	gtk_table_set_col_spacings (GTK_TABLE (table), 6);
	gtk_widget_show (table);
	gtk_box_pack_start (GTK_BOX (vbox), table, FALSE, FALSE, 0);
	list = wk->priv->pages_objects;
	i = WORKSPACE_TABLES_PAGE;
	while (list) {
		button = workspace_page_get_sel_button (WORKSPACE_PAGE (list->data));
		wk->priv->types_buttons = g_slist_prepend (wk->priv->types_buttons, button);
		if (i == 0)
			gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (button), TRUE);

		gtk_table_attach_defaults (GTK_TABLE (table), button, i%2, i%2 +1, i/2, i/2+1);
		gtk_widget_show (button);
		g_signal_connect (G_OBJECT (button), "toggled", G_CALLBACK (object_type_toggled_cb), wk);
		g_object_set_data (G_OBJECT (button), "pageno", GINT_TO_POINTER (i));

		i++;
		list = g_slist_next (list);
	}

	str = workspace_page_get_name (WORKSPACE_PAGE (wk->priv->pages_objects->data));
	gnome_db_gray_bar_set_text (GNOME_DB_GRAY_BAR (wk->priv->selectors_title), str);
	g_free (str);

	gtk_paned_pack1 (GTK_PANED (wk->priv->main_horiz_pane), vbox, FALSE, FALSE);
	

	/* right part of main page, gray bar */
	wk->priv->work_areas_table = gtk_table_new (2, 1, FALSE);
	gtk_table_set_row_spacings (GTK_TABLE (wk->priv->work_areas_table), 6);
	gtk_table_set_col_spacings (GTK_TABLE (wk->priv->work_areas_table), 6);

	wk->priv->work_areas_title = gnome_db_gray_bar_new ("<b>---</b>");
	gnome_db_gray_bar_set_show_icon (GNOME_DB_GRAY_BAR (wk->priv->work_areas_title), TRUE);
	gnome_db_gray_bar_set_icon_from_stock (GNOME_DB_GRAY_BAR (wk->priv->work_areas_title),
					       GTK_STOCK_NEW, GTK_ICON_SIZE_BUTTON);
	gnome_db_gray_bar_set_show_icon (GNOME_DB_GRAY_BAR (wk->priv->work_areas_title), FALSE);
	gtk_widget_show (wk->priv->work_areas_title);
	gtk_table_attach (GTK_TABLE (wk->priv->work_areas_table),
			  wk->priv->work_areas_title, 0, 1, 0, 1,
			  GTK_FILL | GTK_EXPAND | GTK_SHRINK, GTK_FILL, 0, 0);
	list = wk->priv->pages_objects;
	while (list) {
		g_signal_connect (G_OBJECT (list->data), "description_changed", 
				  G_CALLBACK (description_changed_cb), wk);
		list = g_slist_next (list);
	}
	str = workspace_page_get_description (WORKSPACE_PAGE (wk->priv->pages_objects->data));
	gnome_db_gray_bar_set_text (GNOME_DB_GRAY_BAR (wk->priv->work_areas_title), str);
	g_free (str);

	/*  right part, work area */
	wk->priv->work_areas_nb = gtk_notebook_new ();
	gtk_notebook_set_show_border (GTK_NOTEBOOK (wk->priv->work_areas_nb), FALSE);
	gtk_notebook_set_show_tabs (GTK_NOTEBOOK (wk->priv->work_areas_nb), FALSE);
	gtk_widget_show (wk->priv->work_areas_nb);
	gtk_table_attach_defaults (GTK_TABLE (wk->priv->work_areas_table),
				   wk->priv->work_areas_nb, 0 ,1, 1, 2);
	gtk_widget_show (wk->priv->work_areas_nb);

	gtk_paned_pack2 (GTK_PANED (wk->priv->main_horiz_pane), wk->priv->work_areas_table, TRUE, FALSE);
	gtk_widget_show (wk->priv->work_areas_table);

	list = wk->priv->pages_objects;
	while (list) {
		wid = workspace_page_get_work_area (WORKSPACE_PAGE (list->data));
		gtk_notebook_append_page (GTK_NOTEBOOK (wk->priv->work_areas_nb), wid, NULL);
		gtk_widget_show (wid);
		list = g_slist_next (list);
	}

	/* action based ui manager */
	if (wk->priv->ui) {
		list = wk->priv->pages_objects;
		while (list) {
			GtkActionGroup *actions;

			actions = workspace_page_get_actions (WORKSPACE_PAGE (list->data));
			if (actions) {
				gtk_ui_manager_insert_action_group (wk->priv->ui, actions, 0);
				if (list == wk->priv->pages_objects) {
					const gchar *ui_info;
					guint uid;

					ui_info = workspace_page_get_actions_ui (WORKSPACE_PAGE (list->data));
					uid = gtk_ui_manager_add_ui_from_string (wk->priv->ui, ui_info, -1, NULL);
					g_object_set_data (G_OBJECT (wk->priv->ui), "uid", GUINT_TO_POINTER (uid));
				}
			}
			list = g_slist_next (list);
		}
	}
}

static void
description_changed_cb (WorkspacePage *page, gchar *description, Workspace *wk)
{
	gnome_db_gray_bar_set_text (GNOME_DB_GRAY_BAR (wk->priv->work_areas_title), description);
}

static void
object_type_toggled_cb (GtkButton *button, Workspace *wk)
{
	if (! gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (button)))
		return;

	workspace_show_page (wk, GPOINTER_TO_INT (g_object_get_data (G_OBJECT (button), "pageno")));
}

/**
 * workspace_show_page
 * @wk:
 * @page_id:
 *
 * Shows a specific page
 */
void
workspace_show_page (Workspace *wk, WorkspacePageId page_id)
{
	gchar *title;
	WorkspacePage *page;
	gchar *str;
	GSList *list;

	g_return_if_fail (wk && IS_WORKSPACE (wk));
	g_return_if_fail (wk->priv);

	list = wk->priv->types_buttons;
	while (list) {
		if (GPOINTER_TO_INT (g_object_get_data (G_OBJECT (list->data), "pageno")) != page_id)
			gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (list->data), FALSE);
		else {
			g_signal_handlers_block_by_func (G_OBJECT (list->data), G_CALLBACK (object_type_toggled_cb), wk);
			gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (list->data), TRUE);
			g_signal_handlers_unblock_by_func (G_OBJECT (list->data), G_CALLBACK (object_type_toggled_cb), wk);
		}
		list = g_slist_next (list);
	}

	gtk_notebook_set_current_page (GTK_NOTEBOOK (wk->priv->selectors_nb), page_id);
	gtk_notebook_set_current_page (GTK_NOTEBOOK (wk->priv->work_areas_nb), page_id);

	page = g_slist_nth_data (wk->priv->pages_objects, page_id);
	str = workspace_page_get_name (WORKSPACE_PAGE (page));
	gnome_db_gray_bar_set_text (GNOME_DB_GRAY_BAR (wk->priv->selectors_title), str);
	g_free (str);

	title = workspace_page_get_description (page);
	gnome_db_gray_bar_set_text (GNOME_DB_GRAY_BAR (wk->priv->work_areas_title), title);
	g_free (title);

	if (wk->priv->ui) {
		guint uid;
		WorkspacePage *ws;
		const gchar *ui_info;

		uid = GPOINTER_TO_UINT (g_object_get_data (G_OBJECT (wk->priv->ui), "uid"));
		if (uid > 0) {
			gtk_ui_manager_remove_ui (wk->priv->ui, uid);
			g_object_set_data (G_OBJECT (wk->priv->ui), "uid", GUINT_TO_POINTER (0));
		}
		
		ws = g_slist_nth_data (wk->priv->pages_objects, page_id);
		g_assert (IS_WORKSPACE_PAGE (ws));
		ui_info = workspace_page_get_actions_ui (ws);
		if (ui_info) {
			uid = gtk_ui_manager_add_ui_from_string (wk->priv->ui, ui_info, -1, NULL);
			g_object_set_data (G_OBJECT (wk->priv->ui), "uid", GUINT_TO_POINTER (uid));
		}
	}
}
