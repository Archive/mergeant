/* query-editor.h
 *
 * Copyright (C) 2004 - 2008 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef __QUERY_EDITOR__
#define __QUERY_EDITOR__

#include <glib/gi18n-lib.h>
#include <gtk/gtk.h>
#include <libgnomedb/libgnomedb.h>

G_BEGIN_DECLS

#define QUERY_EDITOR_TYPE          (query_editor_get_type())
#define QUERY_EDITOR(obj)          G_TYPE_CHECK_INSTANCE_CAST (obj, query_editor_get_type(), QueryEditor)
#define QUERY_EDITOR_CLASS(klass)  G_TYPE_CHECK_CLASS_CAST (klass, query_editor_get_type (), QueryEditorClass)
#define IS_QUERY_EDITOR(obj)       G_TYPE_CHECK_INSTANCE_TYPE (obj, query_editor_get_type ())


typedef struct _QueryEditor      QueryEditor;
typedef struct _QueryEditorClass QueryEditorClass;
typedef struct _QueryEditorPriv  QueryEditorPriv;


/* struct for the object's data */
struct _QueryEditor
{
	GtkVBox          object;

	QueryEditorPriv *priv;
};

/* struct for the object's class */
struct _QueryEditorClass
{
	GtkVBoxClass     parent_class;
};

/* 
 * Generic widget's methods 
*/
GType      query_editor_get_type            (void);
GtkWidget *query_editor_new                 (GdaStatement *stmt);

G_END_DECLS

#endif



