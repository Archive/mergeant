/* mg-extra-formgrid.c
 *
 * Copyright (C) 2006 - 2007 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <string.h>
#include <glib/gi18n-lib.h>
#include <libgda/libgda.h>
#include "mg-extra-formgrid.h"
#include <libgnomedb/gnome-db-data-widget.h>
#include <libgnomedb/gnome-db-raw-grid.h>
#include <libgnomedb/gnome-db-raw-form.h>
#include <libgnomedb/gnome-db-data-widget-info.h>
#include "binreloc/mg-binreloc.h"

static void mg_extra_formgrid_class_init (MgExtraFormGridClass * class);
static void mg_extra_formgrid_init (MgExtraFormGrid *wid);

static void mg_extra_formgrid_set_property (GObject *object,
					    guint param_id,
					    const GValue *value,
					    GParamSpec *pspec);
static void mg_extra_formgrid_get_property (GObject *object,
					    guint param_id,
					    GValue *value,
					    GParamSpec *pspec);

struct _MgExtraFormGridPriv
{
	GtkWidget   *nb;
	GtkWidget   *raw_form;
	GtkWidget   *raw_grid;
	GtkWidget   *info;
	GtkTooltips *tooltips;
};

/* get a pointer to the parents to be able to call their destructor */
static GObjectClass *parent_class = NULL;

/* properties */
enum
	{
		PROP_0,
		PROP_RAW_GRID,
		PROP_RAW_FORM,
		PROP_INFO,
	};

GType
mg_extra_formgrid_get_type (void)
{
	static GType type = 0;

	if (!type) {
		static const GTypeInfo info = {
			sizeof (MgExtraFormGridClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) mg_extra_formgrid_class_init,
			NULL,
			NULL,
			sizeof (MgExtraFormGrid),
			0,
			(GInstanceInitFunc) mg_extra_formgrid_init
		};		

		type = g_type_register_static (GTK_TYPE_VBOX, "MgExtraFormGrid", &info, 0);
	}

	return type;
}

static void
mg_extra_formgrid_class_init (MgExtraFormGridClass *class)
{
	GObjectClass   *object_class = G_OBJECT_CLASS (class);
	
	parent_class = g_type_class_peek_parent (class);

	/* Properties */
        object_class->set_property = mg_extra_formgrid_set_property;
        object_class->get_property = mg_extra_formgrid_get_property;
	g_object_class_install_property (object_class, PROP_RAW_GRID,
                                         g_param_spec_object ("raw_grid", NULL, NULL, 
							      GNOME_DB_TYPE_RAW_GRID,
							      G_PARAM_READABLE));
	g_object_class_install_property (object_class, PROP_RAW_FORM,
                                         g_param_spec_object ("raw_form", NULL, NULL, 
							      GNOME_DB_TYPE_RAW_GRID,
							      G_PARAM_READABLE));
	g_object_class_install_property (object_class, PROP_INFO,
                                         g_param_spec_object ("widget_info", NULL, NULL, 
							      GNOME_DB_TYPE_DATA_WIDGET_INFO,
							      G_PARAM_READABLE));
}

static void form_grid_toggled_cb (GtkToggleButton *button, MgExtraFormGrid *formgrid);

static void
mg_extra_formgrid_init (MgExtraFormGrid *formgrid)
{
	GtkWidget *sw;
	GtkWidget *hbox, *button;
	
	formgrid->priv = g_new0 (MgExtraFormGridPriv, 1);
	formgrid->priv->raw_grid = NULL;
	formgrid->priv->info = NULL;

	formgrid->priv->tooltips = gtk_tooltips_new ();

	/* notebook */
	formgrid->priv->nb = gtk_notebook_new ();
	gtk_notebook_set_show_tabs (GTK_NOTEBOOK (formgrid->priv->nb), FALSE);
	gtk_notebook_set_show_border (GTK_NOTEBOOK (formgrid->priv->nb), FALSE);
	gtk_box_pack_start (GTK_BOX (formgrid), formgrid->priv->nb, TRUE, TRUE, 0);
	gtk_widget_show (formgrid->priv->nb);

	/* grid on 1st page of notebook */
	sw = gtk_scrolled_window_new (NULL, NULL);
        gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (sw), GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
        gtk_scrolled_window_set_shadow_type (GTK_SCROLLED_WINDOW (sw), GTK_SHADOW_IN);
	gtk_notebook_append_page (GTK_NOTEBOOK (formgrid->priv->nb), sw, NULL);
	gtk_widget_show (sw);

	formgrid->priv->raw_grid = gnome_db_raw_grid_new (NULL);
	gnome_db_data_widget_column_show_actions (GNOME_DB_DATA_WIDGET (formgrid->priv->raw_grid), -1, TRUE);
	gtk_container_add (GTK_CONTAINER (sw), formgrid->priv->raw_grid);
	gtk_widget_show (formgrid->priv->raw_grid);

	/* form on the 2nd page of the notebook */
	formgrid->priv->raw_form = gnome_db_raw_form_new (NULL);
	gnome_db_data_widget_column_show_actions (GNOME_DB_DATA_WIDGET (formgrid->priv->raw_form), -1, TRUE);
	gtk_notebook_append_page (GTK_NOTEBOOK (formgrid->priv->nb), formgrid->priv->raw_form, NULL);
        gtk_widget_show (formgrid->priv->raw_form);

	/* info widget and toggle button at last */
	hbox = gtk_hbox_new (FALSE, 0);
	gtk_box_pack_start (GTK_BOX (formgrid), hbox, FALSE, TRUE, 0);
	gtk_widget_show (hbox);
		
	button = gtk_toggle_button_new ();
	gchar *str = mg_gbr_get_file_path (MG_DATA_DIR, "pixmaps", "mergeant", "mg-extra-grid.png", NULL);
	gtk_button_set_image (GTK_BUTTON (button), gtk_image_new_from_file (str));
	g_free (str);

	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (button), TRUE);
	gtk_box_pack_start (GTK_BOX (hbox), button, FALSE, TRUE, 0);
	gtk_widget_show (button);
	g_signal_connect (G_OBJECT (button), "toggled",
			  G_CALLBACK (form_grid_toggled_cb), formgrid);
	gtk_tooltips_set_tip (formgrid->priv->tooltips, button, _("Toggle between grid and form presentations"), 
			      NULL);

	formgrid->priv->info = gnome_db_data_widget_info_new (GNOME_DB_DATA_WIDGET (formgrid->priv->raw_grid), 
							      GNOME_DB_DATA_WIDGET_INFO_CURRENT_ROW |
							      GNOME_DB_DATA_WIDGET_INFO_ROW_MODIFY_BUTTONS |
							      GNOME_DB_DATA_WIDGET_INFO_CHUNCK_CHANGE_BUTTONS);
	gtk_box_pack_start (GTK_BOX (hbox), formgrid->priv->info, TRUE, TRUE, 0);
	gtk_widget_show (formgrid->priv->info);
}

static void
form_grid_toggled_cb (GtkToggleButton *button, MgExtraFormGrid *formgrid)
{
	GdaDataModelIter *iter;
	gint row;
	gchar *str;

	if (!gtk_toggle_button_get_active (button)) {
		/* switch to form  view */
		gtk_notebook_set_current_page (GTK_NOTEBOOK (formgrid->priv->nb), 1);
		g_object_set (G_OBJECT (formgrid->priv->info),
			      "data-widget", formgrid->priv->raw_form,
			      "flags", GNOME_DB_DATA_WIDGET_INFO_CURRENT_ROW |
			      GNOME_DB_DATA_WIDGET_INFO_ROW_MODIFY_BUTTONS |
			      GNOME_DB_DATA_WIDGET_INFO_ROW_MOVE_BUTTONS, NULL);
		str = mg_gbr_get_file_path (MG_DATA_DIR, "pixmaps", "mergeant", "mg-extra-form.png", NULL);
		gtk_button_set_image (GTK_BUTTON (button), gtk_image_new_from_file (str));
		g_free (str);
		iter = gnome_db_data_widget_get_current_data (GNOME_DB_DATA_WIDGET (formgrid->priv->raw_grid));
		row = gda_data_model_iter_get_row (iter);
		iter = gnome_db_data_widget_get_current_data (GNOME_DB_DATA_WIDGET (formgrid->priv->raw_form));
	}
	else {
		/* switch to grid view */
		gtk_notebook_set_current_page (GTK_NOTEBOOK (formgrid->priv->nb), 0);
		g_object_set (G_OBJECT (formgrid->priv->info),
			      "data-widget", formgrid->priv->raw_grid,
			      "flags", GNOME_DB_DATA_WIDGET_INFO_CURRENT_ROW |
			      GNOME_DB_DATA_WIDGET_INFO_ROW_MODIFY_BUTTONS |
			      GNOME_DB_DATA_WIDGET_INFO_CHUNCK_CHANGE_BUTTONS, NULL);
		str = mg_gbr_get_file_path (MG_DATA_DIR, "pixmaps", "mergeant", "mg-extra-grid.png", NULL);
		gtk_button_set_image (GTK_BUTTON (button), gtk_image_new_from_file (str));
		g_free (str);
		iter = gnome_db_data_widget_get_current_data (GNOME_DB_DATA_WIDGET (formgrid->priv->raw_form));
		row = gda_data_model_iter_get_row (iter);
		iter = gnome_db_data_widget_get_current_data (GNOME_DB_DATA_WIDGET (formgrid->priv->raw_grid));
	}

	gda_data_model_iter_set_at_row (iter, row);
}

/**
 * mg_extra_formgrid_new
 * @model: a #GdaDataModel
 *
 * Creates a new #MgExtraFormGrid widget suitable to display the data in @model
 *
 *  Returns: the new widget
 */
GtkWidget *
mg_extra_formgrid_new (GdaDataModel *model)
{
	MgExtraFormGrid *formgrid;
	GdaDataProxy *proxy;

	g_return_val_if_fail (!model || GDA_IS_DATA_MODEL (model), NULL);

	formgrid = (MgExtraFormGrid *) g_object_new (MG_EXTRA_TYPE_FORMGRID, NULL);

	/* a raw form and a raw grid for the same proxy */
	g_object_set (formgrid->priv->raw_grid, "model", model, NULL);
	proxy = gnome_db_data_widget_get_proxy (GNOME_DB_DATA_WIDGET (formgrid->priv->raw_grid));
	g_object_set (formgrid->priv->raw_form, "model", proxy, NULL);
	gnome_db_data_widget_set_write_mode (GNOME_DB_DATA_WIDGET (formgrid->priv->raw_form),
					     GNOME_DB_DATA_WIDGET_WRITE_ON_ROW_CHANGE);

	/* no more than 300 rows at a time */
	gda_data_proxy_set_sample_size (proxy, 300);

	return (GtkWidget *) formgrid;
}


static void
mg_extra_formgrid_set_property (GObject *object,
				guint param_id,
				const GValue *value,
				GParamSpec *pspec)
{
	MgExtraFormGrid *formgrid;
	
	formgrid = MG_EXTRA_FORMGRID (object);
	
	switch (param_id) {
		
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, param_id, pspec);
		break;
	}
}

static void
mg_extra_formgrid_get_property (GObject *object,
				guint param_id,
				GValue *value,
				GParamSpec *pspec)
{
	MgExtraFormGrid *formgrid;

	formgrid = MG_EXTRA_FORMGRID (object);
	
	switch (param_id) {
	case PROP_RAW_GRID:
		g_value_set_object (value, formgrid->priv->raw_grid);
		break;
	case PROP_RAW_FORM:
		g_value_set_object (value, formgrid->priv->raw_form);
		break;
	case PROP_INFO:
		g_value_set_object (value, formgrid->priv->info);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, param_id, pspec);
		break;
	}	
}

/**
 * mg_extra_formgrid_get_selection
 * @formgrid: a #MgExtraFormGrid widget
 * 
 * Returns the list of the currently selected rows in a #MgExtraFormGrid widget. 
 * The returned value is a list of integers, which represent each of the selected rows.
 *
 * If new rows have been inserted, then those new rows will have a row number equal to -1.
 * This function is a wrapper around the gnome_db_raw_grid_get_selection() function.
 *
 * Returns: a new list, should be freed (by calling g_list_free) when no longer needed.
 */
GList *
mg_extra_formgrid_get_selection (MgExtraFormGrid *formgrid)
{
	g_return_val_if_fail (MG_EXTRA_IS_FORMGRID (formgrid), NULL);
	g_return_val_if_fail (formgrid->priv, NULL);

	return gnome_db_raw_grid_get_selection (GNOME_DB_RAW_GRID (formgrid->priv->raw_grid));
}

/**
 * mg_extra_formgrid_set_sample_size
 * @formgrid: a #MgExtraFormGrid widget
 * @sample_size:
 *
 *
 */
void
mg_extra_formgrid_set_sample_size (MgExtraFormGrid *formgrid, gint sample_size)
{
	g_return_if_fail (MG_EXTRA_IS_FORMGRID (formgrid));
	g_return_if_fail (formgrid->priv);

	gnome_db_raw_grid_set_sample_size (GNOME_DB_RAW_GRID (formgrid->priv->raw_grid), sample_size);
}
