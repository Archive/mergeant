/* ws-datatypes.h
 *
 * Copyright (C) 2004 - 2008 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#ifndef __WS_DATATYPES_H_
#define __WS_DATATYPES_H_

#include <libgda/libgda.h>
#include <libgnomedb/libgnomedb.h>


G_BEGIN_DECLS

#define WS_DATATYPES_TYPE          (ws_datatypes_get_type())
#define WS_DATATYPES(obj)          G_TYPE_CHECK_INSTANCE_CAST (obj, ws_datatypes_get_type(), WsDatatypes)
#define WS_DATATYPES_CLASS(klass)  G_TYPE_CHECK_CLASS_CAST (klass, ws_datatypes_get_type (), WsDatatypesClass)
#define IS_WS_DATATYPES(obj)       G_TYPE_CHECK_INSTANCE_TYPE (obj, ws_datatypes_get_type ())

typedef struct _WsDatatypes        WsDatatypes;
typedef struct _WsDatatypesClass   WsDatatypesClass;
typedef struct _WsDatatypesPrivate WsDatatypesPrivate;

/* struct for the object's data */
struct _WsDatatypes
{
	GObject                object;
	WsDatatypesPrivate       *priv;
};

/* struct for the object's class */
struct _WsDatatypesClass
{
	GObjectClass           parent_class;
};

GType           ws_datatypes_get_type          (void);
GObject        *ws_datatypes_new               (GdaConnection *cnc);

G_END_DECLS

#endif
