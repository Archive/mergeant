/* query-druid.h
 *
 * Copyright (C) 2004 - 2008 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef __QUERY_DRUID__
#define __QUERY_DRUID__

#include <gtk/gtk.h>
#include <libgnomedb/libgnomedb.h>

G_BEGIN_DECLS

#define QUERY_DRUID_TYPE          (query_druid_get_type())
#define QUERY_DRUID(obj)          G_TYPE_CHECK_INSTANCE_CAST (obj, query_druid_get_type(), QueryDruid)
#define QUERY_DRUID_CLASS(klass)  G_TYPE_CHECK_CLASS_CAST (klass, query_druid_get_type (), QueryDruidClass)
#define IS_QUERY_DRUID(obj)       G_TYPE_CHECK_INSTANCE_TYPE (obj, query_druid_get_type ())


typedef struct _QueryDruid      QueryDruid;
typedef struct _QueryDruidClass QueryDruidClass;
typedef struct _QueryDruidPriv  QueryDruidPriv;


/* struct for the object's data */
struct _QueryDruid
{
	GtkAssistant      object;

	QueryDruidPriv   *priv;
};

/* struct for the object's class */
struct _QueryDruidClass
{
	GtkAssistantClass parent_class;
};

/* 
 * Generic widget's methods 
*/
GType      query_druid_get_type            (void);
GtkWidget *query_druid_new                 (GdaConnection *cnc);

G_END_DECLS

#endif



