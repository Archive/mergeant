/* ws-queries.h
 *
 * Copyright (C) 2004 - 2008 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#ifndef __WS_QUERIES_H_
#define __WS_QUERIES_H_

#include <libgda/libgda.h>
#include <libgnomedb/libgnomedb.h>
#include <workspace.h>


G_BEGIN_DECLS

#define WS_QUERIES_TYPE          (ws_queries_get_type())
#define WS_QUERIES(obj)          G_TYPE_CHECK_INSTANCE_CAST (obj, ws_queries_get_type(), WsQueries)
#define WS_QUERIES_CLASS(klass)  G_TYPE_CHECK_CLASS_CAST (klass, ws_queries_get_type (), WsQueriesClass)
#define IS_WS_QUERIES(obj)       G_TYPE_CHECK_INSTANCE_TYPE (obj, ws_queries_get_type ())

typedef struct _WsQueries        WsQueries;
typedef struct _WsQueriesClass   WsQueriesClass;
typedef struct _WsQueriesPrivate WsQueriesPrivate;

/* struct for the object's data */
struct _WsQueries
{
	GObject                object;
	WsQueriesPrivate       *priv;
};

/* struct for the object's class */
struct _WsQueriesClass
{
	GObjectClass           parent_class;
};

GType           ws_queries_get_type          (void);
GObject        *ws_queries_new               (GdaConnection *cnc);

G_END_DECLS

#endif
