/* workspace-page.h
 *
 * Copyright (C) 2004 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#ifndef __WORKSPACE_PAGE_H_
#define __WORKSPACE_PAGE_H_

#include <gtk/gtk.h>

G_BEGIN_DECLS

#define WORKSPACE_PAGE_TYPE          (workspace_page_get_type())
#define WORKSPACE_PAGE(obj)          G_TYPE_CHECK_INSTANCE_CAST (obj, workspace_page_get_type(), WorkspacePage)
#define IS_WORKSPACE_PAGE(obj)       G_TYPE_CHECK_INSTANCE_TYPE (obj, workspace_page_get_type ())
#define WORKSPACE_PAGE_GET_IFACE(obj)  (G_TYPE_INSTANCE_GET_INTERFACE ((obj), WORKSPACE_PAGE_TYPE, WorkspacePageIface))

typedef struct _WorkspacePage       WorkspacePage;
typedef struct _WorkspacePageIface  WorkspacePageIface;

/* struct for the interface */
struct _WorkspacePageIface
{
	GTypeInterface           g_iface;

	/* virtual table */
	gchar          *(* get_name)       (WorkspacePage *iface);
	gchar          *(* get_description)(WorkspacePage *iface);
	GtkWidget      *(* get_sel_button) (WorkspacePage *iface);
	GtkWidget      *(* get_selector)   (WorkspacePage *iface);
	GtkWidget      *(* get_work_area)  (WorkspacePage *iface);

	GtkActionGroup *(* get_actions)    (WorkspacePage *iface);
	const gchar    *(* get_actions_ui) (WorkspacePage *iface);

	/* signals */
	void         (* descr_changed)  (WorkspacePage *iface, gchar *new_title);
};

GType           workspace_page_get_type            (void) G_GNUC_CONST;

gchar          *workspace_page_get_name        (WorkspacePage *page);
gchar          *workspace_page_get_description (WorkspacePage *page);
GtkWidget      *workspace_page_get_sel_button  (WorkspacePage *page);
GtkWidget      *workspace_page_get_selector    (WorkspacePage *page);
GtkWidget      *workspace_page_get_work_area   (WorkspacePage *page);
GtkActionGroup *workspace_page_get_actions     (WorkspacePage *page);
const gchar    *workspace_page_get_actions_ui  (WorkspacePage *page);


void  not_yet_implemented (WorkspacePage *ws, const gchar *feature, 
			   const gchar *file, const gchar *function, gint lineno);
#define NOT_YET_IMPLEMENTED(ws,feature) not_yet_implemented(ws, feature, __FILE__, __FUNCTION__, __LINE__)

G_END_DECLS

#endif
