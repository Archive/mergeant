/* Mergeant
 *
 * Copyright (C) 1999 - 2008 Vivien Malerba
 * Copyright (C) 2002 - 2003 Rodrigo Moya
 *
 * Authors:
 *       Vivien Malerba <malerba@gnome-db.org>
 *       Rodrigo Moya <rodrigo@gnome-db.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef __WORKSPACE_H__
#define __WORKSPACE_H__

#include <libgda/libgda.h>
#include <gtk/gtkvbox.h>

#define WORKSPACE_TYPE          (workspace_get_type())
#define WORKSPACE(obj)          G_TYPE_CHECK_INSTANCE_CAST (obj, workspace_get_type(), Workspace)
#define WORKSPACE_CLASS(klass)  G_TYPE_CHECK_CLASS_CAST (klass, workspace_get_type (), WorkspaceClass)
#define IS_WORKSPACE(obj)       G_TYPE_CHECK_INSTANCE_TYPE (obj, workspace_get_type ())

typedef struct _WorkspacePrivate WorkspacePrivate;

typedef struct {
	GtkVBox           parent;
	WorkspacePrivate *priv;
} Workspace;

typedef struct {
	GtkVBoxClass      parent_class;
} WorkspaceClass;

typedef enum {
	WORKSPACE_TABLES_PAGE,
	WORKSPACE_QUERIES_PAGE,
	WORKSPACE_RELATIONS_PAGE,
	WORKSPACE_TYPES_PAGE,
} WorkspacePageId;

GType           workspace_get_type        (void);
GtkWidget      *workspace_new             (GdaConnection *cnc, GtkUIManager *ui);

GdaConnection  *workspace_get_connection  (Workspace *wk);
void            workspace_set_connection  (Workspace *wk, GdaConnection *cnc);
void            workspace_show_page       (Workspace *wk, WorkspacePageId page_id);

#endif
