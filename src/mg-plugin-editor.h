/* mg-plugin-editor.h
 *
 * Copyright (C) 2006 - 2008 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef __MG_PLUGIN_EDITOR__
#define __MG_PLUGIN_EDITOR__

#include <gtk/gtk.h>
#include <libgda/libgda.h>

G_BEGIN_DECLS

#define MG_TYPE_PLUGIN_EDITOR         (mg_plugin_editor_get_type())
#define MG_PLUGIN_EDITOR(obj)          G_TYPE_CHECK_INSTANCE_CAST (obj, mg_plugin_editor_get_type(), MgPluginEditor)
#define MG_PLUGIN_EDITOR_CLASS(klass)  G_TYPE_CHECK_CLASS_CAST (klass, mg_plugin_editor_get_type (), MgPluginEditorClass)
#define MG_IS_PLUGIN_EDITOR(obj)       G_TYPE_CHECK_INSTANCE_TYPE (obj, mg_plugin_editor_get_type ())


typedef struct _MgPluginEditor      MgPluginEditor;
typedef struct _MgPluginEditorClass MgPluginEditorClass;
typedef struct _MgPluginEditorPriv  MgPluginEditorPriv;

/* struct for the object's data */
struct _MgPluginEditor
{
	GtkVBox             object;

	MgPluginEditorPriv *priv;
};

/* struct for the object's class */
struct _MgPluginEditorClass
{
	GtkVBoxClass        parent_class;
};

/* 
 * Generic widget's methods 
 */
GType             mg_plugin_editor_get_type            (void);

GtkWidget        *mg_plugin_editor_new                 (GdaConnection *cnc);
void              mg_plugin_editor_set_work_object     (MgPluginEditor *editor, GObject *work_obj);

G_END_DECLS

#endif



