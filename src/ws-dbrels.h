/* ws-dbrels.h
 *
 * Copyright (C) 2004 - 2008 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#ifndef __WS_DBRELS_H_
#define __WS_DBRELS_H_

#include <libgda/libgda.h>
#include <libgnomedb/libgnomedb.h>
#include <libgnomedb-graph/libgnomedb-graph.h>


G_BEGIN_DECLS

#define WS_DBRELS_TYPE          (ws_dbrels_get_type())
#define WS_DBRELS(obj)          G_TYPE_CHECK_INSTANCE_CAST (obj, ws_dbrels_get_type(), WsDbrels)
#define WS_DBRELS_CLASS(klass)  G_TYPE_CHECK_CLASS_CAST (klass, ws_dbrels_get_type (), WsDbrelsClass)
#define IS_WS_DBRELS(obj)       G_TYPE_CHECK_INSTANCE_TYPE (obj, ws_dbrels_get_type ())

typedef struct _WsDbrels        WsDbrels;
typedef struct _WsDbrelsClass   WsDbrelsClass;
typedef struct _WsDbrelsPrivate WsDbrelsPrivate;

/* struct for the object's data */
struct _WsDbrels
{
	GObject                object;
	WsDbrelsPrivate       *priv;
};

/* struct for the object's class */
struct _WsDbrelsClass
{
	GObjectClass           parent_class;
};

GType           ws_dbrels_get_type          (void);
GObject        *ws_dbrels_new               (GdaConnection *cnc);

G_END_DECLS

#endif
