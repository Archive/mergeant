/* query-params-editor.h
 *
 * Copyright (C) 2004 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef __QUERY_PARAMS_EDITOR__
#define __QUERY_PARAMS_EDITOR__

#include <gtk/gtk.h>
#include <libgnomedb/libgnomedb.h>

G_BEGIN_DECLS

#define QUERY_PARAMS_EDITOR_TYPE          (query_params_editor_get_type())
#define QUERY_PARAMS_EDITOR(obj)          G_TYPE_CHECK_INSTANCE_CAST (obj, query_params_editor_get_type(), QueryParamsEditor)
#define QUERY_PARAMS_EDITOR_CLASS(klass)  G_TYPE_CHECK_CLASS_CAST (klass, query_params_editor_get_type (), QueryParamsEditorClass)
#define IS_QUERY_PARAMS_EDITOR(obj)       G_TYPE_CHECK_INSTANCE_TYPE (obj, query_params_editor_get_type ())


typedef struct _QueryParamsEditor      QueryParamsEditor;
typedef struct _QueryParamsEditorClass QueryParamsEditorClass;
typedef struct _QueryParamsEditorPriv  QueryParamsEditorPriv;
typedef enum   _QueryParamsMode        QueryParamsMode;

/* struct for the object's data */
struct _QueryParamsEditor
{
	GtkVBox          object;

	QueryParamsEditorPriv *priv;
};

/* struct for the object's class */
struct _QueryParamsEditorClass
{
	GtkVBoxClass      parent_class;
};

enum _QueryParamsMode {
	QEP_SHOW_PARAM_SOURCE    = 1 << 0,
	QEP_ALLOW_EDITION        = 1 << 1,
	QEP_SHOW_ALL_VALUE_FIELDS= 1 << 2
};

/* 
 * Generic widget's methods 
*/
GType      query_params_editor_get_type            (void);
GtkWidget *query_params_editor_new                 (GdaQuery *query, guint mode);
void       query_params_editor_set_query           (QueryParamsEditor *editor, GdaQuery *query);

G_END_DECLS

#endif



