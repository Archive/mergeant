/* query-params-editor.c
 *
 * Copyright (C) 2004 - 2007 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "query-params-editor.h"
#include <glib/gi18n-lib.h>
#include <libgda/gda-parameter-util.h>

static void query_params_editor_class_init (QueryParamsEditorClass * class);
static void query_params_editor_init (QueryParamsEditor * wid);
static void query_params_editor_dispose (GObject   * object);


struct _QueryParamsEditorPriv
{
	GdaQuery      *query;
	
	GtkTreeModel *model;
	GtkTreeView  *view;
	guint         mode;
	
	GSList       *params;
};

/* get a pointer to the parents to be able to call their destructor */
static GObjectClass *parent_class = NULL;

GType
query_params_editor_get_type (void)
{
	static GType type = 0;

	if (!type) {
		static const GTypeInfo info = {
			sizeof (QueryParamsEditorClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) query_params_editor_class_init,
			NULL,
			NULL,
			sizeof (QueryParamsEditor),
			0,
			(GInstanceInitFunc) query_params_editor_init
		};		
		
		type = g_type_register_static (GTK_TYPE_VBOX, "QueryParamsEditor", &info, 0);
	}

	return type;
}

static void
query_params_editor_class_init (QueryParamsEditorClass * class)
{
	GObjectClass   *object_class = G_OBJECT_CLASS (class);
	
	parent_class = g_type_class_peek_parent (class);

	object_class->dispose = query_params_editor_dispose;
}

enum {
	COLUMN_PARAM_PTR,   /* pointer to GdaParameter obj */
	COLUMN_QFVALUE_PTR, /* pointer to GdaQueryFieldValue obj if not param */
	COLUMN_ISPARAM,     /* TRUE if we reference a GdaParameter obj, and FALSE if a GdaQueryFieldValue obj */
	COLUMN_PARAM_NAME,
	COLUMN_PARAM_VALUE,
	COLUMN_PARAM_DESCR,
	COLUMN_PARAM_TYPE,
	COLUMN_PARAM_NULLOK,
	N_COLUMNS
};

static void
query_params_editor_init (QueryParamsEditor * wid)
{
	wid->priv = g_new0 (QueryParamsEditorPriv, 1);
	wid->priv->query = NULL;
	wid->priv->params = NULL;
	wid->priv->model = GTK_TREE_MODEL (gtk_list_store_new (N_COLUMNS,
							       G_TYPE_POINTER,
							       G_TYPE_POINTER,
							       G_TYPE_BOOLEAN,
							       G_TYPE_STRING,
							       G_TYPE_STRING,
							       G_TYPE_STRING,
							       G_TYPE_STRING,
							       G_TYPE_BOOLEAN));
}

static void query_params_editor_initialize (QueryParamsEditor *mgsel);


static void query_destroyed_cb (GdaQuery *query, QueryParamsEditor *mgsel);
static void query_changed_cb (GdaQuery *query, QueryParamsEditor *pedit);

/**
 * query_params_editor_new
 * @query: a #GdaQuery object
 *
 * Creates a new #QueryParamsEditor widget.
 *
 * Returns: the new widget
 */
GtkWidget *
query_params_editor_new (GdaQuery *query, guint mode)
{
	GObject    *obj;
	QueryParamsEditor *pedit;
		
	obj = g_object_new (QUERY_PARAMS_EDITOR_TYPE, NULL);
	pedit = QUERY_PARAMS_EDITOR (obj);

	pedit->priv->mode = mode;

	query_params_editor_initialize (pedit);
	query_params_editor_set_query (pedit, query);	

	return GTK_WIDGET (obj);
}

/**
 * query_params_editor_set_query
 * @editor: a #QueryParamsEditor widget
 * @query: a #GdaQuery, or %NULL
 *
 * Sets the #GdaQuery for which @editor will display the parameters
 */
void
query_params_editor_set_query (QueryParamsEditor *editor, GdaQuery *query)
{
	g_return_if_fail (editor && IS_QUERY_PARAMS_EDITOR (editor));
	g_return_if_fail (editor->priv);

	if (editor->priv->query == query)
		return;

	/* OLD one */
	if (editor->priv->query) {
		g_signal_handlers_disconnect_by_func (G_OBJECT (editor->priv->query),
						      G_CALLBACK (query_destroyed_cb), editor);
		g_signal_handlers_disconnect_by_func (G_OBJECT (editor->priv->query),
						      G_CALLBACK (query_changed_cb), editor);
		editor->priv->query = NULL;
	}

	/* new one */
	if (query) {
		g_return_if_fail (query && GDA_IS_QUERY (query));

		editor->priv->query = query;
		gda_object_connect_destroy (editor->priv->query,
					 G_CALLBACK (query_destroyed_cb), editor);
		g_signal_connect (G_OBJECT (editor->priv->query), "changed",
				  G_CALLBACK (query_changed_cb), editor);

		query_changed_cb (editor->priv->query, editor);
	}
}

static void
query_destroyed_cb (GdaQuery *query, QueryParamsEditor *mgsel)
{
	query_params_editor_set_query (mgsel, NULL);
}

/* unref all the parameters we have in reference */
static void 
params_free (QueryParamsEditor *pedit)
{
	GSList *list = pedit->priv->params;
	while (list) {
		g_object_unref (G_OBJECT (list->data));
		list = g_slist_next (list);
	}
	g_slist_free (pedit->priv->params);
	pedit->priv->params = NULL;
}

static void
query_params_editor_dispose (GObject *object)
{
	QueryParamsEditor *pedit;

	g_return_if_fail (object != NULL);
	g_return_if_fail (IS_QUERY_PARAMS_EDITOR (object));
	pedit = QUERY_PARAMS_EDITOR (object);

	if (pedit->priv) {
		if (pedit->priv->params)
			params_free (pedit);

		/* Weak unref the GdaQuery if necessary */
		if (pedit->priv->query) {
			g_signal_handlers_disconnect_by_func (G_OBJECT (pedit->priv->query),
							      G_CALLBACK (query_destroyed_cb), pedit);
			g_signal_handlers_disconnect_by_func (G_OBJECT (pedit->priv->query),
							      G_CALLBACK (query_changed_cb), pedit);
		}

		if (pedit->priv->model)
			g_object_unref (pedit->priv->model);

		/* the private area itself */
		g_free (pedit->priv);
		pedit->priv = NULL;
	}

	/* for the parent class */
	parent_class->dispose (object);
}

static void renderer_param_name_edited_cb    (GtkCellRendererText *renderer, 
					      gchar *tree_path, gchar *new_text, QueryParamsEditor *pedit);
static void renderer_param_value_edited_cb   (GtkCellRendererText *renderer,
					      gchar *tree_path, gchar *new_text, QueryParamsEditor *pedit);
static void renderer_param_descr_edited_cb   (GtkCellRendererText *renderer, 
					      gchar *tree_path, gchar *new_text, QueryParamsEditor *pedit);
static void renderer_param_type_edited_cb    (GtkCellRendererText *renderer, 
					      gchar *tree_path, gchar *new_text, QueryParamsEditor *pedit);
static void renderer_param_nullok_toggled_cb (GtkCellRendererToggle *renderer,
					      gchar *tree_path, QueryParamsEditor *pedit);
static void renderer_param_isparam_toggled_cb(GtkCellRendererToggle *renderer,
					      gchar *tree_path, QueryParamsEditor *pedit);
static void param_selection_changed_cb       (GtkTreeSelection *selection, QueryParamsEditor *pedit);

static void
query_params_editor_initialize (QueryParamsEditor *pedit)
{
	GtkWidget *sw, *tv, *hbox;
	GtkCellRenderer *renderer;
	GtkTreeViewColumn *column;
	GtkTreeSelection *select;

	hbox = gtk_hbox_new (FALSE, 0);
	gtk_box_pack_start (GTK_BOX (pedit), hbox, TRUE, TRUE, 0);
	gtk_widget_show (hbox);

	/*
	 * TreeView initialization
	 */
	sw = gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (sw),
					GTK_POLICY_AUTOMATIC,
					GTK_POLICY_AUTOMATIC);
	gtk_box_pack_start (GTK_BOX (hbox), sw, TRUE, TRUE, 0);
	gtk_widget_show (sw);

	tv = gtk_tree_view_new_with_model (pedit->priv->model);
	gtk_container_add (GTK_CONTAINER (sw), tv);
	gtk_widget_show (tv);
	pedit->priv->view = GTK_TREE_VIEW (tv);

	select = gtk_tree_view_get_selection (GTK_TREE_VIEW (tv));
	gtk_tree_selection_set_mode (select, GTK_SELECTION_SINGLE);
	g_signal_connect (G_OBJECT (select), "changed",
			  G_CALLBACK (param_selection_changed_cb), pedit);
	
	/* param name */
	renderer = gtk_cell_renderer_text_new ();
	if (pedit->priv->mode & QEP_ALLOW_EDITION) {
		g_object_set (G_OBJECT (renderer), "editable", TRUE,
			      "editable-set", TRUE, NULL);
		g_signal_connect (G_OBJECT (renderer), "edited",
				  G_CALLBACK (renderer_param_name_edited_cb), pedit);
	}

	column = gtk_tree_view_column_new_with_attributes (_("Name"),
							   renderer,
							   "text", COLUMN_PARAM_NAME,
							   NULL);
	gtk_tree_view_append_column (GTK_TREE_VIEW (tv), column);

	/* IS param ? */
	if (pedit->priv->mode & QEP_SHOW_ALL_VALUE_FIELDS) {
		renderer = gtk_cell_renderer_toggle_new ();
		if (pedit->priv->mode & QEP_ALLOW_EDITION) {
			g_object_set (G_OBJECT (renderer), "activatable", TRUE, NULL);
			g_signal_connect (G_OBJECT (renderer), "toggled",
					  G_CALLBACK (renderer_param_isparam_toggled_cb), pedit);
		}
		column = gtk_tree_view_column_new_with_attributes (_("Parameter?"),
								   renderer,
								   "active", COLUMN_ISPARAM,
								   NULL);
		gtk_tree_view_append_column (GTK_TREE_VIEW (tv), column);
	}

	/* param value */
	renderer = gtk_cell_renderer_text_new ();
	if (pedit->priv->mode & QEP_ALLOW_EDITION) {
		g_object_set (G_OBJECT (renderer), "editable", TRUE,
			      "editable-set", TRUE, NULL);
		g_signal_connect (G_OBJECT (renderer), "edited",
				  G_CALLBACK (renderer_param_value_edited_cb), pedit);
	}
	column = gtk_tree_view_column_new_with_attributes (_("Value"),
							   renderer,
							   "text", COLUMN_PARAM_VALUE,
							   NULL);
	gtk_tree_view_append_column (GTK_TREE_VIEW (tv), column);

	/* param description */
	renderer = gtk_cell_renderer_text_new ();
	if (pedit->priv->mode & QEP_ALLOW_EDITION) {
		g_object_set (G_OBJECT (renderer), "editable", TRUE,
			      "editable-set", TRUE, NULL);
		g_signal_connect (G_OBJECT (renderer), "edited",
				  G_CALLBACK (renderer_param_descr_edited_cb), pedit);
	}
	column = gtk_tree_view_column_new_with_attributes (_("Description"),
							   renderer,
							   "text", COLUMN_PARAM_DESCR,
							   NULL);
	gtk_tree_view_append_column (GTK_TREE_VIEW (tv), column);
	gtk_tree_view_column_set_expand (column, TRUE);

	/* param's type */
	/* FIXME: use a combo box when using a GTK version which supports it */
	renderer = gtk_cell_renderer_text_new ();
	if (pedit->priv->mode & QEP_ALLOW_EDITION) {
		g_object_set (G_OBJECT (renderer), "editable", TRUE,
			      "editable-set", TRUE, NULL);
		g_signal_connect (G_OBJECT (renderer), "edited",
				  G_CALLBACK (renderer_param_type_edited_cb), pedit);
	}
	column = gtk_tree_view_column_new_with_attributes (_("Data type"),
							   renderer,
							   "text", COLUMN_PARAM_TYPE,
							   NULL);
	gtk_tree_view_append_column (GTK_TREE_VIEW (tv), column);
	
	/* NULL ok? */
	renderer = gtk_cell_renderer_toggle_new ();
	if (pedit->priv->mode & QEP_ALLOW_EDITION) {
		g_object_set (G_OBJECT (renderer), "activatable", TRUE, NULL);
		g_signal_connect (G_OBJECT (renderer), "toggled",
				  G_CALLBACK (renderer_param_nullok_toggled_cb), pedit);
	}
	column = gtk_tree_view_column_new_with_attributes (_("NULL Ok?"),
							   renderer,
							   "active", COLUMN_PARAM_NULLOK,
							   NULL);
	gtk_tree_view_append_column (GTK_TREE_VIEW (tv), column);

	/*
	 * Buttons
	 */
	if (pedit->priv->mode & QEP_SHOW_PARAM_SOURCE) {
		TO_IMPLEMENT;
	}
}

/*
 * Fetch a list of GdaQueryFieldValue objects which can get their value from the 
 * GdaParameter stored at row described by @tree_path. if @iter is not NULL, then sets
 * it to represent the corresponding row
 *
 * The returned list must then be free'd
 */
static GSList *
model_get_value_field_from_path (GtkTreeModel *model, GtkTreeIter *iter, const gchar *tree_path)
{
	GSList *list = NULL;
	GtkTreeIter tmpiter;
	GtkTreePath *path;
	GdaQueryField *field = NULL;	
	GdaParameter *param = NULL;	

	path = gtk_tree_path_new_from_string (tree_path);
	if (gtk_tree_model_get_iter (model, &tmpiter, path)) {
		gtk_tree_model_get (model, &tmpiter, COLUMN_PARAM_PTR, &param, COLUMN_QFVALUE_PTR, &field, -1);
		if (iter)
			*iter = tmpiter;
	}
	gtk_tree_path_free (path);

	if (param) 
		list = g_slist_copy (gda_parameter_get_param_users (GDA_PARAMETER (param)));
	if (field)
		list = g_slist_append (list, field);

	return list;
}

static void
renderer_param_name_edited_cb (GtkCellRendererText *renderer, 
			       gchar *tree_path, gchar *new_text, QueryParamsEditor *pedit)
{
	GSList *fields, *list;
	GdaQueryField *field;

	fields = model_get_value_field_from_path (pedit->priv->model, NULL, tree_path);
	list = fields;
	while (list) {
		field = (GdaQueryField *) (list->data);

		if (GDA_IS_QUERY_FIELD_VALUE (field)) 
			gda_object_set_name (GDA_OBJECT (field), new_text);
		
		list = g_slist_next (list);
	}
	g_slist_free (fields);
}

static void
renderer_param_value_edited_cb (GtkCellRendererText *renderer,
				gchar *tree_path, gchar *new_text, QueryParamsEditor *pedit)
{
	GSList *fields, *list;
	GdaQueryField *field;

	fields = model_get_value_field_from_path (pedit->priv->model, NULL, tree_path);
	list = fields;
	while (list) {
		field = (GdaQueryField *) (list->data);

		if (GDA_IS_QUERY_FIELD_VALUE (field)) {
			GValue *value;
			GType vtype;
			GdaDataHandler *dh;
			GdaDictType *dtype;

			dtype = gda_entity_field_get_dict_type (GDA_ENTITY_FIELD (field));
			if (dtype)
				vtype = gda_dict_type_get_g_type (dtype);
			else
				g_object_get (G_OBJECT (field), "gda-type", &vtype, NULL);

			dh = gda_dict_get_default_handler (gda_object_get_dict (GDA_OBJECT (field)), vtype);
			value = gda_data_handler_get_value_from_sql (dh, new_text, 
								     gda_entity_field_get_g_type (GDA_ENTITY_FIELD (field)));
			if (value) {
				gda_query_field_value_set_value (GDA_QUERY_FIELD_VALUE (field), value);
				gda_value_free (value);
			}
		}
		
		list = g_slist_next (list);
	}
	g_slist_free (fields);
}

static void
renderer_param_descr_edited_cb (GtkCellRendererText *renderer, 
				gchar *tree_path, gchar *new_text, QueryParamsEditor *pedit)
{
	GSList *fields, *list;
	GdaQueryField *field;

	fields = model_get_value_field_from_path (pedit->priv->model, NULL, tree_path);
	list = fields;
	while (list) {
		field = (GdaQueryField *) (list->data);

		if (GDA_IS_QUERY_FIELD_VALUE (field)) 
			gda_object_set_description (GDA_OBJECT (field), new_text);
		
		list = g_slist_next (list);
	}
	g_slist_free (fields);
}

static void
renderer_param_type_edited_cb (GtkCellRendererText *renderer, 
			       gchar *tree_path, gchar *new_text, QueryParamsEditor *pedit)
{
	GdaDictType *dtype;
	
	dtype = gda_dict_get_dict_type_by_name (gda_object_get_dict (GDA_OBJECT (pedit->priv->query)),
						new_text);
	if (dtype) {
		GSList *fields, *list;
		GdaQueryField *field;

		fields = model_get_value_field_from_path (pedit->priv->model, NULL, tree_path);
		list = fields;
		while (list) {
			field = (GdaQueryField *) (list->data);
			
			if (GDA_IS_QUERY_FIELD_VALUE (field)) 
				gda_entity_field_set_dict_type (GDA_ENTITY_FIELD (field), dtype);
			
			list = g_slist_next (list);
		}
		g_slist_free (fields);
	}
	else {
		GtkWidget *dlg;
		gchar *str;

		str = g_strdup_printf (_("Error: data type '%s' does not exist"), new_text);
		dlg = gtk_message_dialog_new_with_markup (NULL, GTK_DIALOG_MODAL, GTK_MESSAGE_ERROR,
							  GTK_BUTTONS_CLOSE, "<b>%s</b>", str);
		g_free (str);
		gtk_dialog_run (GTK_DIALOG (dlg));
		gtk_widget_destroy (dlg);
	}
}

static void
renderer_param_nullok_toggled_cb (GtkCellRendererToggle *renderer,
				  gchar *tree_path, QueryParamsEditor *pedit)
{
	GSList *fields, *list;
	GdaQueryField *field;

	fields = model_get_value_field_from_path (pedit->priv->model, NULL, tree_path);
	list = fields;
	while (list) {
		field = (GdaQueryField *) (list->data);

		if (GDA_IS_QUERY_FIELD_VALUE (field)) {
			gda_query_field_value_set_not_null (GDA_QUERY_FIELD_VALUE (field), 
						  gtk_cell_renderer_toggle_get_active (renderer));

			/* we need to force the "changed" signal emission because GdaQuery normally does
			 * not do it in this case */
			gda_object_signal_emit_changed (GDA_OBJECT (pedit->priv->query));
		}
		
		list = g_slist_next (list);
	}
	g_slist_free (fields);
}

static void
renderer_param_isparam_toggled_cb (GtkCellRendererToggle *renderer,
				   gchar *tree_path, QueryParamsEditor *pedit)
{
	GSList *fields, *list;
	GdaQueryField *field;

	fields = model_get_value_field_from_path (pedit->priv->model, NULL, tree_path);
	list = fields;
	while (list) {
		field = (GdaQueryField *) (list->data);

		if (GDA_IS_QUERY_FIELD_VALUE (field)) {
			gda_query_field_value_set_is_parameter (GDA_QUERY_FIELD_VALUE (field), 
						      !gtk_cell_renderer_toggle_get_active (renderer));

			/* we need to force the "changed" signal emission because GdaQuery normally does
			 * not do it in this case */
			gda_object_signal_emit_changed (GDA_OBJECT (pedit->priv->query));
		}
		
		list = g_slist_next (list);
	}
	g_slist_free (fields);
}

static void 
param_selection_changed_cb (GtkTreeSelection *selection, QueryParamsEditor *pedit)
{
	gboolean has_sel;
	GtkTreeIter iter;

	has_sel = gtk_tree_selection_get_selected (selection, NULL, &iter);
	if (has_sel) {
		GdaEntityField *field;
		
		gtk_tree_model_get (pedit->priv->model, &iter, COLUMN_PARAM_PTR, &field, -1);
	}
}


/*
 * (re)-write all the rows of the model with the params to be displayed
 */
static void
query_changed_cb (GdaQuery *query, QueryParamsEditor *pedit)
{
	GSList *list, *params;
	GtkTreeIter iter;
	GtkTreeModel *model = pedit->priv->model;
	gboolean iter_valid = FALSE;
	GdaDict *dict;
       
	dict = gda_object_get_dict (GDA_OBJECT (pedit->priv->query));

	/*
	 * Parameters
	 */
	if (pedit->priv->params)
		params_free (pedit);
	params = gda_query_get_parameters (pedit->priv->query);
	pedit->priv->params = params;

	list = params;
	while (list) {
		GdaParameter *param = GDA_PARAMETER (list->data);
		gchar *valstr;
		const gchar *typestr;
		const GValue *value;

		/* fetch a new iter */
		if (!iter_valid) {
			/* fetch first iter, or create a new one */
			if (! gtk_tree_model_get_iter_first (model, &iter))
				gtk_list_store_append (GTK_LIST_STORE (model), &iter);
			iter_valid = TRUE;
		}
		else {
			/* fetch next iter, or create a new one */
			if (! gtk_tree_model_iter_next (model, &iter))
				gtk_list_store_append (GTK_LIST_STORE (model), &iter);
		}

		/* compute strings */
		value = gda_parameter_get_value (param);
		if (value && !gda_value_is_null ((GValue *)value)) {
			GdaDataHandler *dh = gda_dict_get_default_handler (dict, G_VALUE_TYPE ((GValue *)value));
			valstr = gda_data_handler_get_sql_from_value (dh, value);
		}
		else
			valstr = g_strdup ("");

		typestr = gda_g_type_to_string (gda_parameter_get_g_type (param));
		
		/* modify the model at iter */
		gtk_list_store_set (GTK_LIST_STORE (model), &iter,
				    COLUMN_PARAM_PTR, param,
				    COLUMN_QFVALUE_PTR, NULL,
				    COLUMN_ISPARAM, TRUE,
				    COLUMN_PARAM_NAME, gda_object_get_name (GDA_OBJECT (param)),
				    COLUMN_PARAM_VALUE, valstr,
				    COLUMN_PARAM_DESCR, gda_object_get_description (GDA_OBJECT (param)),
				    COLUMN_PARAM_TYPE, typestr,
				    COLUMN_PARAM_NULLOK, !gda_parameter_get_not_null (param),
				    -1);
		g_free (valstr);
		list = g_slist_next (list);
	}

	/*
	 * GdaQueryFieldValue fields which are not parameters
	 */
	if (pedit->priv->mode & QEP_SHOW_ALL_VALUE_FIELDS) {
		GSList *fields = gda_query_get_all_fields (pedit->priv->query);

		list = fields;
		while (list) {
			if (GDA_IS_QUERY_FIELD_VALUE (list->data) && 
			    !gda_query_field_value_get_is_parameter (GDA_QUERY_FIELD_VALUE (list->data))) {
				GdaQueryFieldValue *qfield = GDA_QUERY_FIELD_VALUE (list->data);
				gchar *valstr;
				const gchar *typestr;
				const GValue *value;

				/* fetch a new iter */
				if (!iter_valid) {
					/* fetch first iter, or create a new one */
					if (! gtk_tree_model_get_iter_first (model, &iter))
						gtk_list_store_append (GTK_LIST_STORE (model), &iter);
					iter_valid = TRUE;
				}
				else {
					/* fetch next iter, or create a new one */
					if (! gtk_tree_model_iter_next (model, &iter))
						gtk_list_store_append (GTK_LIST_STORE (model), &iter);
				}

				/* compute strings */
				value = gda_query_field_value_get_value (qfield);
				if (value && !gda_value_is_null ((GValue *)value)) {
					GdaDataHandler *dh = gda_dict_get_default_handler (dict,
										  G_VALUE_TYPE ((GValue *)value));
					valstr = gda_data_handler_get_sql_from_value (dh, value);
				}
				else
					valstr = g_strdup ("");
				
				typestr = gda_dict_type_get_sqlname (gda_entity_field_get_dict_type (GDA_ENTITY_FIELD (qfield)));
				gtk_list_store_set (GTK_LIST_STORE (model), &iter,
						    COLUMN_PARAM_PTR, NULL,
						    COLUMN_QFVALUE_PTR, qfield,
						    COLUMN_ISPARAM, FALSE,
						    COLUMN_PARAM_NAME, gda_object_get_name (GDA_OBJECT (qfield)),
						    COLUMN_PARAM_VALUE, valstr,
						    COLUMN_PARAM_DESCR, gda_object_get_description (GDA_OBJECT (qfield)),
						    COLUMN_PARAM_TYPE, typestr,
						    COLUMN_PARAM_NULLOK, !gda_query_field_value_get_not_null (qfield),
						    -1);
				g_free (valstr);
			}
			list = g_slist_next (list);
		}
	}

	/* clean the remaining rows */
	if (!iter_valid) {
		if (gtk_tree_model_get_iter_first (model, &iter))
			while (gtk_list_store_remove (GTK_LIST_STORE (model), &iter));
	}
	else
		if (gtk_tree_model_iter_next (model, &iter))
			while (gtk_list_store_remove (GTK_LIST_STORE (model), &iter));

	/* selection related update */
	param_selection_changed_cb (gtk_tree_view_get_selection (pedit->priv->view), pedit);
}
