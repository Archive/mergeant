/* mg-extra-formgrid.h
 *
 * Copyright (C) 2006 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef __MG_EXTRA_FORMGRID__
#define __MG_EXTRA_FORMGRID__

#include <gtk/gtk.h>
#include <libgda/gda-decl.h>

G_BEGIN_DECLS

#define MG_EXTRA_TYPE_FORMGRID          (mg_extra_formgrid_get_type())
#define MG_EXTRA_FORMGRID(obj)          G_TYPE_CHECK_INSTANCE_CAST (obj, mg_extra_formgrid_get_type(), MgExtraFormGrid)
#define MG_EXTRA_FORMGRID_CLASS(klass)  G_TYPE_CHECK_CLASS_CAST (klass, mg_extra_formgrid_get_type (), MgExtraFormGridClass)
#define MG_EXTRA_IS_FORMGRID(obj)       G_TYPE_CHECK_INSTANCE_TYPE (obj, mg_extra_formgrid_get_type ())


typedef struct _MgExtraFormGrid      MgExtraFormGrid;
typedef struct _MgExtraFormGridClass MgExtraFormGridClass;
typedef struct _MgExtraFormGridPriv  MgExtraFormGridPriv;

/* struct for the object's data */
struct _MgExtraFormGrid
{
	GtkVBox             object;

	MgExtraFormGridPriv     *priv;
};

/* struct for the object's class */
struct _MgExtraFormGridClass
{
	GtkVBoxClass       parent_class;
};

/* 
 * Generic widget's methods 
 */
GType             mg_extra_formgrid_get_type            (void);

GtkWidget        *mg_extra_formgrid_new                 (GdaDataModel *model);
GList            *mg_extra_formgrid_get_selection       (MgExtraFormGrid *grid);
void              mg_extra_formgrid_set_sample_size     (MgExtraFormGrid *grid, gint sample_size);

G_END_DECLS

#endif



