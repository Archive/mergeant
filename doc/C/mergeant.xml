<?xml version="1.0"?>
<!DOCTYPE book PUBLIC "-//OASIS//DTD DocBook XML V4.1.2//EN" "http://www.oasis-open.org/docbook/xml/4.1.2/docbookx.dtd">
<book>
	<bookinfo>
		<author><surname/></author><abstract><para>Mergeant users' Manual</para></abstract>
	</bookinfo>
	<title>Mergeant - User guide</title>
	<chapter>
		<title>General introduction</title>
		<para>Mergeant aims at making it easy to manage databases, through a nice and easy to use graphical interface. It already offers all the necessary features to edit the contents of the tables: either directly where the contents of each table can be browsed and modified, or by creating selection queries where the resulting data can be modified.</para>
		<para>Connections to any database supported by the Libgda library can be opened, and several connections can be opened at the same time, thus offering an easy way to compare data (automatic data comparison and migration between databases is a feature which is not yet implemented). Mergeant uses the concept of workspaces: a workspace is a window where data corresponding to a connection is managed.</para>
		<para>The first time a connection is opened, Mergeant will extract a dictionary of all the structural objects present in the database, and store it in a file for the next time the connection is used. At any time it is possible to synchronize the dictionary with the actual database structure (for example if the database structure has changed since the dictionary was built).</para>
	</chapter>
	<chapter>
		<title>Connections management</title>
		<para/>
		<sect1>
			<title>Connections declarations</title>
			<para>Before any connection to a database can be made by Mergeant, it needs to be declared. The system maintains a list of connections, some global to all the users, and some local to a given user. The connections are identified by a unique identifier and each connection uses a database driver ("database provider") to access a specific database type (MySQL, PostgreSQL, Oracle, etc). Apart from choosing a database driver, each connection needs some information about where the database is located (on which machine), which database to connect to (in case a server handles more than one database), what user name and password to use, and various other information dependant on the database driver.</para>
		</sect1>
		<sect1>
			<title>Adding and removing connections</title>
			<para>When Mergeant is run, it opens a dialog window to select which connection to use. From that dialog it is possible to declare new data sources before using them in Mergeant. Data sources are saved from one session to the other, so they need be created only once.</para>
			<note>
				<para>Data sources are only a "declaration" to tell Mergeant where the database is and how to access it; databases must exist before the data source can be created (Mergeant does not yet support the creation of databases).</para>
			</note>
			<figure>
				<title>Data source selection dialog when running Mergeant</title>
				<screenshot>
					<mediaobject>
						<imageobject>
							<imagedata fileref="images/datasources.png"/>
						</imageobject>
					</mediaobject>
				</screenshot>
			</figure>
			<para>After having selected the data source to use in Mergeant, it is possible to run the Gnome Database Properties from the "Tools" menu.</para>
			<note>
				<para>The Gnome Database Properties can also be accessed through the preferences menu of the GNOME desktop, or by running the <command>gnome-database-properties</command> program.</para>
			</note>
			<figure>
				<title>The Gnome Database Properties to manage data sources</title>
				<screenshot>
					<mediaobject>
						<imageobject>
							<imagedata fileref="images/dba_properties.png"/>
						</imageobject>
					</mediaobject>
				</screenshot>
			</figure>
		</sect1>
		<sect1>
			<title>Advanced topic: under the hood</title>
			<para>As Mergeant uses the <productname>Libgda</productname> and <productname>Libgnomedb</productname> libraries, the data sources management is done through thos libraries. Any program making use of these same libraries will benefit from the same data sources facilities.</para>
			<para>Data sources are configured in the GConf registry, and stored in the <filename>.libgda/config.xml</filename> file in each user's home directory.</para>
		</sect1>
	</chapter>
	<chapter>
		<title>Workspace</title><para>Mergeant uses the notion of workspace: a workspace is a window corresponding to a specific data source connection. Several workspaces can be opened at the same time, and some of them for the same data source (in this case, every modification made in one workspace will immediately be made available to the other workspace).</para><para>A workspace provides several work areas, displayed in the same window; a menu situated on the top left part of the window allowing to change the displayed work area, which are:</para><itemizedlist><listitem><para>The tables and views work area: shows tables and views, their column, and the referential integrity rules.</para></listitem><listitem><para>The data types work area: lists data types and functions operating on the selected data types</para></listitem><listitem><para>The queries work area: enables management of user defined queries for data manipulation (SELECT, INSERT, UPDATE and DELETE operations)</para></listitem><listitem><para>The relations work area: shows the database structure regarding referential integrity rules.</para></listitem><listitem><para>The forms work area where user defined custom forms can be managed.</para></listitem></itemizedlist><figure><title>A workspace</title><screenshot><mediaobject><imageobject><imagedata fileref="images/workspace.png"/></imageobject></mediaobject></screenshot></figure>
	</chapter>
	<chapter>
		<title>The tables and views work area</title><para>This work area intends to show the database structure regarding tables and views. The left part of the work space shows a list of alphabetically-sorted tables and views, and the right part shows information about the selected table or view: the list of fields and their properties, and the list of constraints applied to the selected table (when a table is selected).</para><para>The data cannot be modified, but future versions will include the possibility to do such work as create new tables or views, destroy or rename them, and modify each of them.</para><screenshot><mediaobject><imageobject><imagedata fileref="images/workspace.png"/></imageobject></mediaobject></screenshot>
		
	</chapter>
	<chapter>
		<title>The data types work area</title><para>This work area shows information about the data types and functions defined on the data types. The left part of the workspace shows a list of data types defined within the database; they are the data types supported by the DBMS, and the user-defined data types if the DBMS supports them. Selecting a data type will display some information about it on the right part of the workspace.</para><para>Functions and aggregates using the selected data type can be listed by selecting a filter on the right part of the workspace.</para><screenshot><mediaobject><imageobject><imagedata fileref="images/workspace-data-types.png"/></imageobject></mediaobject></screenshot>
		
	</chapter>
	<chapter>
		<title>The queries area</title><para>This work area is the place where user defined queries are managed. The queries are any kind of SELECT, INSERT, UPDATE or DELETE queries. Queries are created using a creation druid, where all the steps required to create a query are proposed.</para><para>A query can also be created from an SQL statement, which is parsed and analysed. Parameters can be introduced in the query when a value is required but not known when the query is created. A very simple extension to the SQL has been created for that purpose, and is best illustrated by a few examples:</para><para>When a parameter is required </para><screenshot><mediaobject><imageobject><imagedata fileref="images/workspace-queries.png"/></imageobject></mediaobject></screenshot>
		<para/>
	</chapter>
	<chapter>
		<title>The relations area</title><para>This area is used to create some custom views of the relationships betwenn tables of the database (relations are implemented as foreign key constraints). Any number of views (called graphs) can be created to represent different "parts" of the database structure.</para><para>The links shown as solid lines are the relations implemented in the database as foreign key constraints; and the links shown as dotted lines are links defined by the user using drag and drop, but not implemented in the database as foreign key constraints.</para><screenshot><mediaobject><imageobject><imagedata fileref="images/workspace-graphs.png"/></imageobject></mediaobject></screenshot>
		
	</chapter>
</book>
